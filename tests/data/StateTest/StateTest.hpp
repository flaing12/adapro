
/*
 * File:   StateTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 11 June 2017, 10:33:38
 */

#ifndef ADAPRO_STATE_TEST_HPP
#define ADAPRO_STATE_TEST_HPP

#include <cppunit/extensions/HelperMacros.h>

class StateTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(StateTest);

    CPPUNIT_TEST(test_symbol);
    CPPUNIT_TEST(test_show);

    CPPUNIT_TEST_SUITE_END();

public:
    StateTest();
    virtual ~StateTest();
    void setUp();
    void tearDown();

private:
    void test_symbol();
    void test_show();
};

#endif /* ADAPRO_STATE_TEST_HPP */

