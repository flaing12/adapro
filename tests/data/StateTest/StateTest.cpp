
/*
 * File:   StateTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 11 June 2017, 10:33:38
 */

#include <string>
#include <stdexcept>
#include "StateTest.hpp"
#include "../../../headers/library/GenericFunctions.hpp"
#include "../../../headers/data/State.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION(StateTest);

using namespace std;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

StateTest::StateTest() {}

StateTest::~StateTest() {}

void StateTest::setUp() {}

void StateTest::tearDown() {}

void StateTest::test_symbol()
{
    CPPUNIT_ASSERT("Rdy"    == symbol<State>(READY));
    CPPUNIT_ASSERT("Stg"    == symbol<State>(STARTING));
    CPPUNIT_ASSERT("Rng"    == symbol<State>(RUNNING));
    CPPUNIT_ASSERT("Psd"    == symbol<State>(PAUSED));
    CPPUNIT_ASSERT("Spg"    == symbol<State>(STOPPING));
    CPPUNIT_ASSERT("Spd"    == symbol<State>(STOPPED));
    CPPUNIT_ASSERT("Abg"    == symbol<State>(ABORTING));
    CPPUNIT_ASSERT("Abd"    == symbol<State>(ABORTED));
    CPPUNIT_ASSERT("Rdy"    == symbol<State>((State) 0x01));
    CPPUNIT_ASSERT("Stg"    == symbol<State>((State) 0x02));
    CPPUNIT_ASSERT("Rng"    == symbol<State>((State) 0x04));
    CPPUNIT_ASSERT("Psd"    == symbol<State>((State) 0x08));
    CPPUNIT_ASSERT("Spg"    == symbol<State>((State) 0x10));
    CPPUNIT_ASSERT("Spd"    == symbol<State>((State) 0x20));
    CPPUNIT_ASSERT("Abg"    == symbol<State>((State) 0x40));
    CPPUNIT_ASSERT("Abd"    == symbol<State>((State) 0x80));
    bool exception(false);
    try
    {
        symbol<State>((State) 0x07);
    }
    catch (const domain_error& e)
    {
        exception = true;
    }
    CPPUNIT_ASSERT(exception);
}

void StateTest::test_show()
{
    CPPUNIT_ASSERT("READY"      == show<State>(READY));
    CPPUNIT_ASSERT("STARTING"   == show<State>(STARTING));
    CPPUNIT_ASSERT("RUNNING"    == show<State>(RUNNING));
    CPPUNIT_ASSERT("PAUSED"     == show<State>(PAUSED));
    CPPUNIT_ASSERT("STOPPING"   == show<State>(STOPPING));
    CPPUNIT_ASSERT("STOPPED"    == show<State>(STOPPED));
    CPPUNIT_ASSERT("ABORTING"   == show<State>(ABORTING));
    CPPUNIT_ASSERT("ABORTED"    == show<State>(ABORTED));
    CPPUNIT_ASSERT("READY"      == show<State>((State) 0x01));
    CPPUNIT_ASSERT("STARTING"   == show<State>((State) 0x02));
    CPPUNIT_ASSERT("RUNNING"    == show<State>((State) 0x04));
    CPPUNIT_ASSERT("PAUSED"     == show<State>((State) 0x08));
    CPPUNIT_ASSERT("STOPPING"   == show<State>((State) 0x10));
    CPPUNIT_ASSERT("STOPPED"    == show<State>((State) 0x20));
    CPPUNIT_ASSERT("ABORTING"   == show<State>((State) 0x40));
    CPPUNIT_ASSERT("ABORTED"    == show<State>((State) 0x80));
    bool exception(false);
    try
    {
        show<State>((State) 0x07);
    }
    catch (const domain_error& e)
    {
        exception = true;
    }
    CPPUNIT_ASSERT(exception);
}
