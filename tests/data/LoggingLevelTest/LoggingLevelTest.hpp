
/*
 * File:   LoggingLevelTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 11 June 2017, 10:41:51
 */

#ifndef ADAPRO_LOGGING_LEVEL_TEST_HPP
#define ADAPRO_LOGGING_LEVEL_TEST_HPP

#include <cppunit/extensions/HelperMacros.h>

class LoggingLevelTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(LoggingLevelTest);

    CPPUNIT_TEST(test_read);
    CPPUNIT_TEST(test_symbol);

    CPPUNIT_TEST_SUITE_END();

public:
    LoggingLevelTest();
    virtual ~LoggingLevelTest();
    void setUp();
    void tearDown();

private:
    void test_read();
    void test_symbol();
};

#endif /* ADAPRO_LOGGING_LEVEL_TEST_HPP */

