
/*
 * File:   ParametersTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 7 June 2017, 12:38:10
 */

#include "ParametersTest.hpp"
#include <iostream>
#include <string>
#include <regex>
#include <map>
#include <utility>
#include <functional>
#include "../../../headers/data/Parameters.hpp"
#include "headers/control/Worker.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION(ParametersTest);

using namespace std;
using namespace ADAPRO::Data;
using namespace ADAPRO::Library;

ParametersTest::ParametersTest() {}

ParametersTest::~ParametersTest() {}

void ParametersTest::setUp() {}

void ParametersTest::tearDown() {}

void ParametersTest::test_regexes()
{
    CPPUNIT_ASSERT( is_integer("-1"));
    CPPUNIT_ASSERT( is_integer("0"));
    CPPUNIT_ASSERT( is_integer("1"));
    CPPUNIT_ASSERT( is_integer("2047"));
    CPPUNIT_ASSERT(!is_integer(""));
    CPPUNIT_ASSERT(!is_integer("12\n"));
    CPPUNIT_ASSERT(!is_integer("\n12"));
    CPPUNIT_ASSERT(!is_integer("false"));
    CPPUNIT_ASSERT(!is_integer("-1.0"));

    // Apparently, there's something called std::is_unsigned already:
    CPPUNIT_ASSERT(!ADAPRO::Library::is_unsigned("-1"));
    CPPUNIT_ASSERT( ADAPRO::Library::is_unsigned("0"));
    CPPUNIT_ASSERT( ADAPRO::Library::is_unsigned("1"));
    CPPUNIT_ASSERT( ADAPRO::Library::is_unsigned("2047"));
    CPPUNIT_ASSERT(!ADAPRO::Library::is_unsigned(""));
    CPPUNIT_ASSERT(!ADAPRO::Library::is_unsigned("12\n"));
    CPPUNIT_ASSERT(!ADAPRO::Library::is_unsigned("\n12"));
    CPPUNIT_ASSERT(!ADAPRO::Library::is_unsigned("false"));
    CPPUNIT_ASSERT(!ADAPRO::Library::is_unsigned("-1.0"));

    CPPUNIT_ASSERT( is_IPv4_address("0.0.0.0"));
    CPPUNIT_ASSERT( is_IPv4_address("192.168.10.1"));
    CPPUNIT_ASSERT(!is_IPv4_address(""));
    CPPUNIT_ASSERT(!is_IPv4_address("false"));
    CPPUNIT_ASSERT(!is_IPv4_address("1.2.3"));
    CPPUNIT_ASSERT(!is_IPv4_address("\n192.168.10.1"));
    CPPUNIT_ASSERT(!is_IPv4_address("192.168.10.1\n"));
    CPPUNIT_ASSERT(!is_IPv4_address("292.168.10.1"));
    CPPUNIT_ASSERT(!is_IPv4_address("192.268.10.1"));
    CPPUNIT_ASSERT(!is_IPv4_address("192.168.300.1"));
    CPPUNIT_ASSERT(!is_IPv4_address("192.168.10.256"));
    CPPUNIT_ASSERT(!is_IPv4_address("3232238081"));

    CPPUNIT_ASSERT( is_hostname("757"));
    CPPUNIT_ASSERT( is_hostname("localhost"));
    CPPUNIT_ASSERT( is_hostname("test.domain"));
    CPPUNIT_ASSERT( is_hostname("test-domain"));
    CPPUNIT_ASSERT( is_hostname("test_domain"));
    CPPUNIT_ASSERT( is_hostname("t35t.d0ma1n"));
    CPPUNIT_ASSERT( is_hostname("192.168.10.1"));
    CPPUNIT_ASSERT(!is_hostname(""));
    CPPUNIT_ASSERT(!is_hostname("test/domain"));
    CPPUNIT_ASSERT(!is_hostname("\ntest.domain"));
    CPPUNIT_ASSERT(!is_hostname("test.domain\n"));
    CPPUNIT_ASSERT(!is_hostname("test\tdomain"));
    CPPUNIT_ASSERT(!is_hostname("bad@test.value"));

    CPPUNIT_ASSERT( is_DIM_service_name("757"));
    CPPUNIT_ASSERT( is_DIM_service_name("localhost"));
    CPPUNIT_ASSERT(!is_DIM_service_name("test.domain"));
    CPPUNIT_ASSERT( is_DIM_service_name("test-domain"));
    CPPUNIT_ASSERT( is_DIM_service_name("test_domain"));
    CPPUNIT_ASSERT(!is_DIM_service_name("t35t.d0ma1n"));
    CPPUNIT_ASSERT(!is_DIM_service_name("192.168.10.1"));
    CPPUNIT_ASSERT(!is_DIM_service_name(""));
    CPPUNIT_ASSERT( is_DIM_service_name("test/domain"));
    CPPUNIT_ASSERT(!is_DIM_service_name("\ntest.domain"));
    CPPUNIT_ASSERT(!is_DIM_service_name("test.domain\n"));
    CPPUNIT_ASSERT(!is_DIM_service_name("test\tdomain"));
    CPPUNIT_ASSERT(!is_DIM_service_name("bad@test.value"));

    CPPUNIT_ASSERT(is_DIM_service_description("I"));
    CPPUNIT_ASSERT(is_DIM_service_description("I:1"));
    CPPUNIT_ASSERT(is_DIM_service_description("I:2"));
    CPPUNIT_ASSERT(is_DIM_service_description("I:1;C:2"));
    CPPUNIT_ASSERT(is_DIM_service_description("C:16"));
    CPPUNIT_ASSERT(!is_DIM_service_description("I1"));
    CPPUNIT_ASSERT(!is_DIM_service_description("I;C:2"));
    CPPUNIT_ASSERT( is_DIM_service_description("I:2;C"));
    CPPUNIT_ASSERT(!is_DIM_service_description("I;C"));
    CPPUNIT_ASSERT(!is_DIM_service_description("42"));

    CPPUNIT_ASSERT( is_word("757"));
    CPPUNIT_ASSERT( is_word("localhost"));
    CPPUNIT_ASSERT(!is_word("test.domain"));
    CPPUNIT_ASSERT(!is_word("test-domain"));
    CPPUNIT_ASSERT( is_word("test_domain"));
    CPPUNIT_ASSERT(!is_word("t35t.d0ma1n"));
    CPPUNIT_ASSERT(!is_word("192.168.10.1"));
    CPPUNIT_ASSERT(!is_word(""));
    CPPUNIT_ASSERT(!is_word("test/domain"));
    CPPUNIT_ASSERT(!is_word("\ntest.domain"));
    CPPUNIT_ASSERT(!is_word("test.domain\n"));
    CPPUNIT_ASSERT(!is_word("test\tdomain"));
    CPPUNIT_ASSERT(!is_word("bad@test.value"));

    CPPUNIT_ASSERT( is_whitespace(" "));
    CPPUNIT_ASSERT(!is_whitespace(" ")); // NBSP
    CPPUNIT_ASSERT( is_whitespace("  "));
    CPPUNIT_ASSERT( is_whitespace("\t"));
    CPPUNIT_ASSERT( is_whitespace("\n"));
    CPPUNIT_ASSERT( is_whitespace("\r"));
    CPPUNIT_ASSERT(!is_whitespace(""));
    CPPUNIT_ASSERT(!is_whitespace("."));
    CPPUNIT_ASSERT(!is_whitespace(" u"));
    CPPUNIT_ASSERT(!is_whitespace("A "));

    CPPUNIT_ASSERT( is_filename("/etc/mtab"));
    CPPUNIT_ASSERT( is_filename("a.out"));
    CPPUNIT_ASSERT( is_filename("keyboard-smash-w4trwrefnsd.xyz"));
    CPPUNIT_ASSERT( is_filename("./foo.tar.gz"));
    CPPUNIT_ASSERT( is_filename("1234"));
    CPPUNIT_ASSERT(!is_filename("."));
    CPPUNIT_ASSERT(!is_filename("filename.txt."));
    CPPUNIT_ASSERT(!is_filename(""));
    CPPUNIT_ASSERT(!is_filename(" "));
    CPPUNIT_ASSERT(!is_filename("/etc/"));

    CPPUNIT_ASSERT( is_directory_name("/test/path/"));
    CPPUNIT_ASSERT( is_directory_name("/test-path/"));
    CPPUNIT_ASSERT( is_directory_name("/test_path/"));
    CPPUNIT_ASSERT( is_directory_name("/test.path/"));
    CPPUNIT_ASSERT( is_directory_name("/t3st/path/"));
    CPPUNIT_ASSERT( is_directory_name("test/path/"));
    CPPUNIT_ASSERT( is_directory_name(".test/path/"));
    CPPUNIT_ASSERT( is_directory_name("./.test/path/"));
    CPPUNIT_ASSERT( is_directory_name("./"));
    CPPUNIT_ASSERT( is_directory_name("../"));
    CPPUNIT_ASSERT( is_directory_name("../../foo/"));
    CPPUNIT_ASSERT( is_directory_name("../foo/../"));
    CPPUNIT_ASSERT( is_directory_name("../foo/.././"));
    CPPUNIT_ASSERT(!is_directory_name(""));
    CPPUNIT_ASSERT(!is_directory_name("..test/path/"));
    CPPUNIT_ASSERT(!is_directory_name("\n/test/path/"));
    CPPUNIT_ASSERT(!is_directory_name("/test/path/\n"));
    CPPUNIT_ASSERT(!is_directory_name("/test/path./"));
    CPPUNIT_ASSERT(!is_directory_name("/test/path"));
    CPPUNIT_ASSERT(!is_directory_name("//test/path/"));
    CPPUNIT_ASSERT(!is_directory_name("/test//path/"));
    CPPUNIT_ASSERT(!is_directory_name("\\test\\path/"));
    CPPUNIT_ASSERT(!is_directory_name("/test path/"));
}

void ParametersTest::test_default_relation()
{
    CPPUNIT_ASSERT( default_relation("ADAPRO_NICE", "-20"));
    CPPUNIT_ASSERT( default_relation("ADAPRO_NICE", "0"));
    CPPUNIT_ASSERT( default_relation("ADAPRO_NICE", "19"));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_NICE", ""));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_NICE", "xyz"));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_NICE", "5\n"));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_NICE", "\n5"));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_NICE", "5.0"));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_NICE", "20"));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_NICE", "-21"));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_NICE", "1 2"));

    CPPUNIT_ASSERT( default_relation("ADAPRO_LOCK_MEMORY", "TRUE"));
    CPPUNIT_ASSERT( default_relation("ADAPRO_LOCK_MEMORY", "FALSE"));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_LOCK_MEMORY", "0"));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_LOCK_MEMORY", "1"));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_LOCK_MEMORY", "MAYBE"));

    CPPUNIT_ASSERT( default_relation("ADAPRO_LOGGING_MASK", "ADISWEF"));
    CPPUNIT_ASSERT( default_relation("ADAPRO_LOGGING_MASK", "SADWIFE"));
    CPPUNIT_ASSERT( default_relation("ADAPRO_LOGGING_MASK", "A"));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_LOGGING_MASK", ""));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_LOGGING_MASK", "AA"));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_LOGGING_MASK", "AB"));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_LOGGING_MASK", "ABC"));

    CPPUNIT_ASSERT( default_relation("ADAPRO_DIM_SERVER_NAME", "test-server.com"));    // hostname
    CPPUNIT_ASSERT( default_relation("ADAPRO_DIM_SERVER_NAME", "123.234.89.122"));     // IPv4
    CPPUNIT_ASSERT( default_relation("ADAPRO_DIM_SERVER_NAME", "123.234.89.122.223")); // "hostname"
    CPPUNIT_ASSERT(!default_relation("ADAPRO_DIM_SERVER_NAME", ""));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_DIM_SERVER_NAME", "test@server.com"));

    CPPUNIT_ASSERT(!default_relation("ADAPRO_DIM_DNS_PORT", "-20"));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_DIM_DNS_PORT", "0"));
    CPPUNIT_ASSERT( default_relation("ADAPRO_DIM_DNS_PORT", "19"));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_DIM_DNS_PORT", ""));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_DIM_DNS_PORT", "xyz"));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_DIM_DNS_PORT", "5\n"));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_DIM_DNS_PORT", "\n5"));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_DIM_DNS_PORT", "5.0"));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_DIM_DNS_PORT", "65536"));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_DIM_DNS_PORT", "-21"));
    CPPUNIT_ASSERT(!default_relation("ADAPRO_DIM_DNS_PORT", "1 2"));

    CPPUNIT_ASSERT( default_relation("FOO", "BAR"));
}

void ParametersTest::test_make_default_configuration()
{
    const map<string, string> expected
    {
          {show(COLOURS_ENABLED)      , default_value(COLOURS_ENABLED)      }
        , {show(DAEMON_ENABLED)       , default_value(DAEMON_ENABLED)       }
        , {show(DAEMON_LOGFILE)       , default_value(DAEMON_LOGFILE)       }
        , {show(DIM_CALLBACK_CORE)    , default_value(DIM_CALLBACK_CORE)    }
        , {show(DIM_CONTROLLER_CORE)  , default_value(DIM_CONTROLLER_CORE)  }
        , {show(DIM_DNS_NODE)         , default_value(DIM_DNS_NODE)         }
        , {show(DIM_DNS_PORT)         , default_value(DIM_DNS_PORT)         }
        , {show(DIM_SERVER_ENABLED)   , default_value(DIM_SERVER_ENABLED)   }
        , {show(DIM_SERVER_NAME)      , default_value(DIM_SERVER_NAME)      }
        , {show(LOCK_MEMORY)          , default_value(LOCK_MEMORY)          }
        , {show(LOGGING_MASK)         , default_value(LOGGING_MASK)         }
        , {show(MAIN_CORE)            , default_value(MAIN_CORE)            }
        , {show(NICE)                 , default_value(NICE)                 }
        , {show(SERIALIZE_COMMANDS)   , default_value(SERIALIZE_COMMANDS)   }
        , {show(SUPERVISOR_CORE)      , default_value(SUPERVISOR_CORE)      }
    };
    const map<string, string> actual{make_default_configuration()};
    CPPUNIT_ASSERT(expected == actual);
}
