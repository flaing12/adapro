
/*
 * File:   ParametersTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 7 June 2017, 12:38:10
 */

#ifndef ADAPRO_PARAMETERS_TEST_HPP
#define ADAPRO_PARAMETERS_TEST_HPP

#include <cppunit/extensions/HelperMacros.h>

class ParametersTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(ParametersTest);

    CPPUNIT_TEST(test_regexes);
    CPPUNIT_TEST(test_default_relation);
    CPPUNIT_TEST(test_make_default_configuration);

    CPPUNIT_TEST_SUITE_END();

public:
    ParametersTest();
    virtual ~ParametersTest();
    void setUp();
    void tearDown();

private:
    void test_regexes();
    void test_default_relation();
    void test_make_default_configuration();
};

#endif /* ADAPRO_PARAMETERS_TEST_HPP */

