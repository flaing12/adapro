/*
 * File:   StringUtilsTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 19.7.2015, 13:14:15
 */
#ifndef ADAPRO_STRING_UTILS_TEST_HPP
#define	ADAPRO_STRING_UTILS_TEST_HPP

#include <cppunit/extensions/HelperMacros.h>

class StringUtilsTest : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE(StringUtilsTest);

    CPPUNIT_TEST(test_char_ptr_ptr_to_string);
    CPPUNIT_TEST(test_random_string);
    CPPUNIT_TEST(test_random_string2);
    CPPUNIT_TEST(test_hash_code);
//    CPPUNIT_TEST(test_hash_code_performance);
    CPPUNIT_TEST(test_to_upper_case);
    CPPUNIT_TEST(test_to_lower_case);
    CPPUNIT_TEST(test_split);
    CPPUNIT_TEST(test_split_by_whitespace);
    CPPUNIT_TEST(test_to_hex_big_endian);
    CPPUNIT_TEST(test_to_hex_little_endian);

    CPPUNIT_TEST_SUITE_END();

public:
    StringUtilsTest();
    virtual ~StringUtilsTest();
    void setUp();
    void tearDown();

private:
    void test_char_ptr_ptr_to_string();
    void test_random_string();
    void test_random_string2();
    void test_hash_code();
    void test_hash_code_performance();
    void test_to_upper_case();
    void test_to_lower_case();
    void test_split();
    void test_split_by_whitespace();
    void test_to_hex_big_endian();
    void test_to_hex_little_endian();
};

#endif	/* ADAPRO_STRING_UTILS_TEST_HPP */

