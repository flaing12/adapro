/*
 * File:   StringUtilsTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 19.7.2015, 13:14:15
 */
#include <cstdio>
#include <string>
#include <vector>
#include <map>
#include <regex>
#include <iostream>
#include <sstream>
#include <fstream>
#include <functional>
#include <chrono>
#include <unordered_map>
#include "StringUtilsTest.hpp"
#include "../../../headers/control/Logger.hpp"
#include "../../../headers/library/StringUtils.hpp"

using namespace std;

CPPUNIT_TEST_SUITE_REGISTRATION(StringUtilsTest);

StringUtilsTest::StringUtilsTest() {}

StringUtilsTest::~StringUtilsTest() {}

void StringUtilsTest::setUp()
{
    srand(16777213 * time(nullptr) % 2000000011);
}

void StringUtilsTest::tearDown() {}

void StringUtilsTest::test_char_ptr_ptr_to_string()
{
    char* argv[62];
    argv[0]  = "Lorem";         argv[1]  = "ipsum";     argv[2]  = "dolor";
    argv[3]  = "sit";           argv[4]  = "amet,";     argv[5]  = "consectetur";
    argv[6]  = "adipiscing";    argv[7]  = "elit.";     argv[8]  = "Curabitur";
    argv[9]  = "vel";           argv[10] = "eleifend";  argv[11] = "arcu,";
    argv[12] = "sed";           argv[13] = "lacinia";   argv[14] = "turpis.";
    argv[15] = "Etiam";         argv[16] = "a";         argv[17] = "metus";
    argv[18] = "elit.";         argv[19] = "Etiam";     argv[20] = "iaculis";
    argv[21] = "elit";          argv[22] = "id";        argv[23] = "velit";
    argv[24] = "ornare";        argv[25] = "efficitur.";argv[26] = "In";
    argv[27] = "bibendum";      argv[28] = "pharetra";  argv[29] = "felis,";
    argv[30] = "sed";           argv[31] = "auctor";    argv[32] = "sapien";
    argv[33] = "pulvinar";      argv[34] = "at.";       argv[35] = "Etiam";
    argv[36] = "pretium,";      argv[37] = "neque";     argv[38] = "non";
    argv[39] = "aliquet";       argv[40] = "porttitor,";argv[41] = "enim";
    argv[42] = "velit";         argv[43] = "elementum"; argv[44] = "ex,";
    argv[45] = "non";           argv[46] = "accumsan";  argv[47] = "enim";
    argv[48] = "nibh";          argv[49] = "nec";       argv[50] = "est.";
    argv[51] = "Pellentesque";  argv[52] = "at";        argv[53] = "efficitur";
    argv[54] = "nisi.";         argv[55] = "Morbi";     argv[56] = "consectetur";
    argv[57] = "eros";          argv[58] = "in";        argv[59] = "erat";
    argv[60] = "sollicitudin";  argv[61] = "pulvinar.";
    int argc{62};
    const string expected
    {
        "ipsum, dolor, sit, amet,, consectetur, adipiscing,\n"
        "                            elit., Curabitur, vel, eleifend, arcu,, sed,\n"
        "                            lacinia, turpis., Etiam, a, metus, elit., Etiam,\n"
        "                            iaculis, elit, id, velit, ornare, efficitur., In,\n"
        "                            bibendum, pharetra, felis,, sed, auctor, sapien,\n"
        "                            pulvinar, at., Etiam, pretium,, neque, non, aliquet,\n"
        "                            porttitor,, enim, velit, elementum, ex,, non,\n"
        "                            accumsan, enim, nibh, nec, est., Pellentesque, at,\n"
        "                            efficitur, nisi., Morbi, consectetur, eros, in,\n"
        "                            erat, sollicitudin, pulvinar."
    };
    const string actual{ADAPRO::Library::char_ptr_ptr_to_string(argc, argv, 28)};
//    cout << "Expected:                   " << expected << endl;
//    cout << "Actual:                     " << actual << endl;
    CPPUNIT_ASSERT(actual == expected);
}

void StringUtilsTest::test_random_string()
{
    // The probability for two exactly same strings is so low, that if this
    // assertion fails, it's fairly safe to assume a bug in the code:
    CPPUNIT_ASSERT(ADAPRO::Library::random_string(52) !=
            ADAPRO::Library::random_string(52));

//    cout << "--------------------------------------------------------------------------------" << '\n';
//    cout << "Random sample with uniform distribution:" << '\n';
//    for (size_t i = 0; i < 10; ++i)
//    {
//        cout << ADAPRO::Library::random_string(52) << '\n';
//    }
//    cout << "--------------------------------------------------------------------------------" << '\n';
}

void StringUtilsTest::test_random_string2()
{
    // The probability for two exactly same strings is so low, that if this
    // assertion fails, it's fairly safe to assume a bug in the code:
    CPPUNIT_ASSERT(ADAPRO::Library::random_string2(52) !=
            ADAPRO::Library::random_string2(52));

//    cout << "--------------------------------------------------------------------------------" << '\n';
//    cout << "Random sample with 'ADAPOS' distribution:" << '\n';
//    for (size_t i = 0; i < 10; ++i)
//    {
//        cout << ADAPRO::Library::random_string2(52) << '\n';
//    }
//    cout << "--------------------------------------------------------------------------------" << '\n';
}

void StringUtilsTest::test_hash_code()
{
    CPPUNIT_ASSERT(ADAPRO::Library::hash_code("xyz") ==
            ADAPRO::Library::hash_code("xyz"));
    CPPUNIT_ASSERT(ADAPRO::Library::hash_code("abc") ==
            ADAPRO::Library::hash_code("ABC"));
    CPPUNIT_ASSERT(ADAPRO::Library::hash_code("abc") !=
            ADAPRO::Library::hash_code("ABD"));
}

void run_statistics(const size_t string_length, function<string(size_t)> randomizer, const string& dist_name)
{
    cout << "--------------------------------------------------------------------------------" << '\n';
    cout << "    Calculating hashes for " << string_length << "-char random "
            "strings using " << dist_name << " distribution." << '\n';
    uint64_t min(1000000000);
    uint64_t max(0);
    uint64_t avg(0);
    uint16_t highest_collision_count(0);
    unordered_map<uint64_t, uint16_t> occurences;
    for (size_t i = 0; i < 1000000; ++i)
    {
        string random_string(randomizer(string_length));
        chrono::high_resolution_clock::time_point beginning(
                chrono::high_resolution_clock::now()
                );
        const uint64_t code(
                ADAPRO::Library::hash_code(random_string)
        );
        const uint64_t current(chrono::duration_cast<std::chrono::nanoseconds>(
                chrono::high_resolution_clock::now() - beginning
        ).count());
        if (current < min)
        {
            min = current;
        }
        else if (current > max)
        {
            max = current;
        }
        avg += current;
        const uint16_t collision_count(occurences[code] + 1);
        if (collision_count > highest_collision_count)
        {
            highest_collision_count = collision_count;
        }
        occurences[code] = collision_count;
    }
    cout << "    Average duration was " << (avg / 1000000) << " ns." << '\n';
    cout << "    Lowest duration was " << min << " ns." << '\n';
    cout << "    Highest duration was " << max << " ns." << '\n';
    if (highest_collision_count > 1)
    {
        cout << "    Highest collision count was " << highest_collision_count << '\n';
    }
}

void StringUtilsTest::test_hash_code_performance()
{
    chrono::high_resolution_clock::time_point beginning(
            chrono::high_resolution_clock::now()
            );
    ADAPRO::Library::hash_code("This string should have exactly 52 characters in it.");
    const uint64_t result(chrono::duration_cast<std::chrono::nanoseconds>(
            chrono::high_resolution_clock::now() - beginning
    ).count());
    cout << "    Calculating a constant 52-char hash took on " << result << " ns." << '\n';

    run_statistics(20, ADAPRO::Library::random_string, "uniform");
    run_statistics(20, ADAPRO::Library::random_string2, "\'ADAPOS\'");
    run_statistics(52, ADAPRO::Library::random_string, "uniform");
    run_statistics(52, ADAPRO::Library::random_string2, "\'ADAPOS\'");
    cout << "--------------------------------------------------------------------------------" << '\n';
}

void StringUtilsTest::test_to_upper_case()
{
    string s("!fooBar_");
    ADAPRO::Library::to_upper_case(s);
    CPPUNIT_ASSERT(s == "!FOOBAR_");
}

void StringUtilsTest::test_to_lower_case()
{
    string s("!fooBar_");
    ADAPRO::Library::to_lower_case(s);
    CPPUNIT_ASSERT(s == "!foobar_");
}

void StringUtilsTest::test_split()
{
    const string input = "/a/b//c;d/e/";
    const vector<string> parts = ADAPRO::Library::split(input, '/');
    CPPUNIT_ASSERT(parts.size() == 6);
    CPPUNIT_ASSERT(parts[0].empty());
    CPPUNIT_ASSERT(parts[1].compare("a") == 0);
    CPPUNIT_ASSERT(parts[2].compare("b") == 0);
    CPPUNIT_ASSERT(parts[3].empty());
    CPPUNIT_ASSERT(parts[4].compare("c;d") == 0);
    CPPUNIT_ASSERT(parts[5].compare("e") == 0);
}

void StringUtilsTest::test_split_by_whitespace()
{
    const string input1 = "abc";
    const string input2 = "abc def";
    const string input3 = "abc    def";
    const string input4 = " abc def   ";
    const string input5 = " abc def   ghi";
    const vector<string> parts1 = ADAPRO::Library::split_by_whitespace(input1);
    const vector<string> parts2 = ADAPRO::Library::split_by_whitespace(input2);
    const vector<string> parts3 = ADAPRO::Library::split_by_whitespace(input3);
    const vector<string> parts4 = ADAPRO::Library::split_by_whitespace(input4);
    const vector<string> parts5 = ADAPRO::Library::split_by_whitespace(input5);
    CPPUNIT_ASSERT(parts1.size() == 1);
    CPPUNIT_ASSERT(parts2.size() == 2);
    CPPUNIT_ASSERT(parts3.size() == 2);
    CPPUNIT_ASSERT(parts4.size() == 2);
    CPPUNIT_ASSERT(parts5.size() == 3);
}

void StringUtilsTest::test_to_hex_big_endian()
{
    long* input1 = new long(0x0123456789ABCDEFL);
    char* input2 = (char*) "This is a test string.";
    CPPUNIT_ASSERT(ADAPRO::Library::to_hex_big_endian((char*) input1, 8) ==
            "01 23 45 67 89 AB CD EF");
    CPPUNIT_ASSERT(ADAPRO::Library::to_hex_big_endian((char*) input2, 21) ==
            "67 6E 69 72 74 73 20 74 73 65 74 20 61 20 73 69 20 73 69 68 54");
    delete input1;
//    delete[] input2;
}

void StringUtilsTest::test_to_hex_little_endian()
{
    long* input1 = new long(0x0123456789ABCDEFL);
    char* input2 = (char*) "This is a test string.";
    CPPUNIT_ASSERT(ADAPRO::Library::to_hex_little_endian((char*) input1, 8) ==
            "EF CD AB 89 67 45 23 01");
    CPPUNIT_ASSERT(ADAPRO::Library::to_hex_little_endian((char*) input2, 21) ==
            "54 68 69 73 20 69 73 20 61 20 74 65 73 74 20 73 74 72 69 6E 67");
    delete input1;
//    delete[] input2;
}