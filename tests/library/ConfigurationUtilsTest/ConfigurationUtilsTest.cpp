/*
 * File:   ConfigurationTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 17 January 2017, 15:38:52
 */

#include <map>
#include <string>
#include <fstream>
#include <iostream>
#include <regex>
#include "../../../headers/control/Logger.hpp"
#include "../../../headers/library/ConfigurationUtils.hpp"
#include "../../../headers/data/LoggingLevel.hpp"
#include "../../../headers/data/Parameters.hpp"
#include "../../../headers/data/Typedefs.hpp"
#include "ConfigurationUtilsTest.hpp"

using namespace std;
using namespace ADAPRO::Data;
//using namespace ADAPRO::Library;

CPPUNIT_TEST_SUITE_REGISTRATION(ConfigurationUtilsTest);

static const config_t DEFAULT_CONFIGURATION{ADAPRO::Library::make_default_configuration()};

/**
 * This macro generates a test against a specific trivial error scenario with
 * <tt>import_configuration</tt>.
 *
 * @param test_name A string (without quotes) identifying the test being run. It
 * is printed in case of error along with the error message.
 * @param filename  A string (without quotes) to be used as a filename to the
 * import function.
 * @param <tt>true</tt> if and only if the import function is expected to throw
 * an exception, and <tt>false</tt> otherwise.
 * @param typename of the exception class to be caught.
 */
#define TEST_IMPORT_MAP_WITH_TRIVIAL_ERROR(test_name, filename, exception_expected, exception_type)\
    bool exception(false);\
    try\
    {\
        config_t k_v_map;\
        ADAPRO::Library::import_configuration(k_v_map, list<string>{#filename});\
    }\
    catch (const exception_type& e)\
    {\
        exception = true;\
    }\
    CPPUNIT_ASSERT(exception_expected == exception);

ConfigurationUtilsTest::ConfigurationUtilsTest() {}

ConfigurationUtilsTest::~ConfigurationUtilsTest() {}

void ConfigurationUtilsTest::setUp() {}

void ConfigurationUtilsTest::tearDown() {}

void ConfigurationUtilsTest::test_query()
{
    const map<string, string> test_configuration {DEFAULT_CONFIGURATION};
    string value_1, value_2;
    bool exception_thrown{false};
    try
    {
        value_1 = ADAPRO::Library::query(test_configuration, (Parameter) 50);
    }
    catch (const domain_error& e)
    {
        exception_thrown = true;
    }
    value_2 = ADAPRO::Library::query(test_configuration, MAIN_CORE);
    CPPUNIT_ASSERT(exception_thrown);
    CPPUNIT_ASSERT(test_configuration.size() == 15);
    CPPUNIT_ASSERT(value_1 == "");
    CPPUNIT_ASSERT(value_2 == "-1");
    CPPUNIT_ASSERT(test_configuration == DEFAULT_CONFIGURATION);
}

void ConfigurationUtilsTest::test_import_configuration_with_nonexisting_file()
{
    TEST_IMPORT_MAP_WITH_TRIVIAL_ERROR(nonexisting file, nonexistent.file, true, std::ios_base::failure);
}

void ConfigurationUtilsTest::test_import_configuration_with_multiple_files()
{
    const config_t expected
    {
            {"TST_136", "Raw/Int"       },  {"TST_137", "Raw/Uint"      },
            {"TST_138", "Raw/Double"    },  {"TST_139", "Raw/Bool"      },
            {"TST_140", "Raw/Char"      },  {"TST_141", "Raw/String"    },
            {"TST_142", "Raw/Time"      },  {"TST_143", "Raw/Binary"    },
            {"TST_144", "DPVAL/Int"     },  {"TST_145", "DPVAL/Uint"    },
            {"TST_146", "DPVAL/Double"  },  {"TST_147", "DPVAL/Bool"    },
            {"TST_148", "DPVAL/Char"    },  {"TST_149", "DPVAL/String"  },
            {"TST_150", "DPVAL/Time"    },  {"TST_151", "DPVAL/Binary"  }
    };
    list<string> paths
    {
            "nonexistent.file",
            "tests/resources/services.lst",
            "tests/resources/services_old_fmt.lst"
    };
    config_t actual;
    const string& filename = ADAPRO::Library::import_configuration
    (
            actual,
            paths,
            true
    );
    CPPUNIT_ASSERT(expected == actual);
    CPPUNIT_ASSERT(filename == "tests/resources/services.lst");
}

void ConfigurationUtilsTest::test_import_configuration_with_too_few_tokens()
{
    TEST_IMPORT_MAP_WITH_TRIVIAL_ERROR(too few tokens, tests/resources/too_few_tokens.txt, true, std::domain_error);
}

void ConfigurationUtilsTest::test_import_configuration_with_too_many_tokens()
{
    TEST_IMPORT_MAP_WITH_TRIVIAL_ERROR(too many tokens, tests/resources/too_many_tokens.txt, true, std::domain_error);
}

void ConfigurationUtilsTest::test_import_configuration_with_key_redefinition()
{
    TEST_IMPORT_MAP_WITH_TRIVIAL_ERROR(key redefinition, tests/resources/key_redefinition.txt, true, std::domain_error);
}

void ConfigurationUtilsTest::test_import_configuration_with_malformed_key()
{
    TEST_IMPORT_MAP_WITH_TRIVIAL_ERROR(malformed key, tests/resources/malformed_key.txt, true, std::domain_error);
}

void ConfigurationUtilsTest::test_import_configuration_with_unknown_key()
{
    config_t k_v_map
    {
        {"TST_136", "None"}, {"TST_137", "None"}, {"TST_138", "None"},
        {"TST_140", "None"}, {"TST_141", "None"}, {"TST_142", "None"}
    };
    bool exception{false};
    try
    {
        ADAPRO::Library::import_configuration
        (
                k_v_map,
                list<string>{"tests/resources/services.lst"},
                false
        );
    }
    catch (const domain_error& e)
    {
        exception = true;
    }
    CPPUNIT_ASSERT(exception);
}

void ConfigurationUtilsTest::test_import_configuration_with_relation_failure()
{
    config_t k_v_map;
    bool exception{false};
    try
    {
        ADAPRO::Library::import_configuration
        (
                k_v_map,
                list<string>{"tests/resources/services.lst"},
                true,
                [](const string k, const string v) {return v != "Raw/Double";}
        );
    }
    catch (const domain_error& e)
    {
        exception = true;
    }
    CPPUNIT_ASSERT(exception);
}

void ConfigurationUtilsTest::test_import_configuration_with_default_relation()
{
    const config_t expected
    {
        {"ADAPRO_NICE",                 "10"},
        {"ADAPRO_LOCK_MEMORY",          "FALSE"},
        {"ADAPRO_DAEMON_ENABLED",       "FALSE"},
        {"ADAPRO_DAEMON_LOGFILE",       "tests/resources/test.log"},
        {"ADAPRO_LOGGING_MASK",         "DISWEF"},
        {"ADAPRO_COLOURS_ENABLED",      "FALSE"},
        {"ADAPRO_DIM_DNS_NODE",         "localhost"},
        {"ADAPRO_DIM_DNS_PORT",         "2505"},
        {"ADAPRO_DIM_SERVER_NAME",      "ADAPRO_Example_Application"},
        {"ADAPRO_DIM_SERVER_ENABLED",   "FALSE"},
        {"ADAPRO_MAIN_CORE",            "-1"},
        {"ADAPRO_SUPERVISOR_CORE",      "-1"},
        {"ADAPRO_DIM_CALLBACK_CORE",    "-1"},
        {"ADAPRO_DIM_CONTROLLER_CORE",  "-1"},
        {"ADAPRO_SERIALIZE_COMMANDS",   "TRUE"}
    };
    config_t actual;
    list<string> paths{"tests/resources/test.conf"};
    const string& filename = ADAPRO::Library::import_configuration
    (
            actual, paths, true

    );
//    cout << paths.front() << '\n';
    CPPUNIT_ASSERT(expected == actual);
    CPPUNIT_ASSERT(DEFAULT_CONFIGURATION != actual);
    CPPUNIT_ASSERT(filename == paths.front());

}

void ConfigurationUtilsTest::test_import_configuration_with_custom_relation()
{
    // Since ADAPOS support was removed from the master branch, a new test case
    // is needed here.
}

void ConfigurationUtilsTest::test_import_configuration_with_different_whitespace()
{
    config_t k_v_map;
    list<string> paths{"tests/resources/separators.txt"};
    string filename;
    try
    {
    filename = ADAPRO::Library::import_configuration
    (
            k_v_map,
            paths,
            true
    );
        // Currently, non-breaking space doesn't work as a separator, so this
        // line is unreachable:
        CPPUNIT_ASSERT(filename == "tests/resources/separators.txt");
    }
    catch(const std::domain_error& e)
    {
        CPPUNIT_ASSERT(filename == "");
        CPPUNIT_ASSERT(k_v_map["key_1"] == "value_1");
        CPPUNIT_ASSERT(k_v_map["key_2"] == "");
        CPPUNIT_ASSERT(k_v_map["key_3"] == "value_3");
        CPPUNIT_ASSERT(k_v_map["key_4"] == "");
    }
}

void ConfigurationUtilsTest::test_export_configuration_with_illegal_keys()
{
    config_t test_map{{"foo", "bar"}, {"illegal$character", "23"}, {"x", "4"}};
    bool exception{false};
    try
    {
        ADAPRO::Library::export_configuration
        (
                test_map,
                "tests/resources/test1.out",
                false,
                ADAPRO::Library::default_relation
        );
    }
    catch (const invalid_argument& e)
    {
        exception = true;
    }
    ifstream file{"tests/resources/test1.out"};
    CPPUNIT_ASSERT(!file.is_open());
    CPPUNIT_ASSERT(exception);
}

void ConfigurationUtilsTest::test_export_configuration_with_illegal_values()
{
    config_t test_map{{"foo", "bar"}, {"illegal_value", "2 3"}, {"x", "4"}};
    bool exception{false};
    try
    {
        ADAPRO::Library::export_configuration
        (
                test_map,
                "tests/resources/test2.out",
                false,
                ADAPRO::Library::default_relation
        );
    }
    catch (const invalid_argument& e)
    {
        exception = true;
    }
    ifstream file{"tests/resources/test2.out"};
    CPPUNIT_ASSERT(!file.is_open());
    CPPUNIT_ASSERT(exception);
}

void ConfigurationUtilsTest::test_export_configuration()
{
    config_t test_map{{"foo", "bar"}, {"very_long", "23"}, {"x", "4"}};
    ADAPRO::Library::export_configuration
    (
            test_map,
            "tests/resources/test3.out",
            false,
            ADAPRO::Library::default_relation
    );
    ifstream file{"tests/resources/test3.out"};
    CPPUNIT_ASSERT(file.is_open());
    stringstream buffer;
    buffer << file.rdbuf();
    file.close();
    remove("tests/resources/test3.out");
    CPPUNIT_ASSERT(buffer.str() == "foo bar\nvery_long 23\nx 4\n");
}

void ConfigurationUtilsTest::test_export_configuration_tabulated()
{
    config_t test_map{{"foo", "bar"}, {"very_long", "23"}, {"x", "4"}};
    ADAPRO::Library::export_configuration
    (
            test_map,
            "tests/resources/test4.out",
            true,
            ADAPRO::Library::default_relation
    );
    ifstream file{"tests/resources/test4.out"};
    CPPUNIT_ASSERT(file.is_open());
    stringstream buffer;
    buffer << file.rdbuf();
    file.close();
    remove("tests/resources/test4.out");
    CPPUNIT_ASSERT(buffer.str() == "foo       bar\nvery_long 23\nx         4\n");
}

void ConfigurationUtilsTest::test_export_import_configuration()
{
    config_t test_map{{"foo", "bar"}, {"very_long", "23"}, {"x", "4"}};
    ADAPRO::Library::export_configuration
    (
            test_map,
            "tests/resources/test5.out",
            false,
            ADAPRO::Library::default_relation
    );
    config_t test_map2;
    list<string> paths{"tests/resources/test5.out"};
    const string& filename = ADAPRO::Library::import_configuration
    (
            test_map2,
            paths,
            true,
            ADAPRO::Library::default_relation
    );
    remove("tests/resources/test5.out");
    CPPUNIT_ASSERT(test_map == test_map2);
    CPPUNIT_ASSERT(filename == "tests/resources/test5.out");
}

void ConfigurationUtilsTest::test_print_configuration()
{
    ADAPRO::Control::Logger* logger = new ADAPRO::Control::Logger
    {
            0xFF,
            "tests/resources/test6.out",
            false
    };
    ADAPRO::Library::print_configuration
    (
            *logger,
            "CONFIGURATION",
            DEFAULT_CONFIGURATION
    );
    delete logger;
    regex expected_first_line{
            "^\\[\\d{4}-\\d{2}-\\d{2} ((0|1)\\d|2[0-3]):[0-5]\\d:[0-5]\\d\\] \\(I\\) CONFIGURATION$"};
    const string expexted_lines[]
    {
            "---------------------------------------"                          ,
            ""                                                                 ,
            "    ADAPRO_COLOURS_ENABLED      FALSE"                            ,
            "    ADAPRO_DAEMON_ENABLED       FALSE"                            ,
            "    ADAPRO_DAEMON_LOGFILE       /var/log/adapro.log"              ,
            "    ADAPRO_DIM_CALLBACK_CORE    -1"                               ,
            "    ADAPRO_DIM_CONTROLLER_CORE  -1"                               ,
            "    ADAPRO_DIM_DNS_NODE         localhost"                        ,
            "    ADAPRO_DIM_DNS_PORT         2505"                             ,
            "    ADAPRO_DIM_SERVER_ENABLED   TRUE"                             ,
            "    ADAPRO_DIM_SERVER_NAME      ADAPRO_application"               ,
            "    ADAPRO_LOCK_MEMORY          FALSE"                            ,
            "    ADAPRO_LOGGING_MASK         DEFIW"                            ,
            "    ADAPRO_MAIN_CORE            -1"                               ,
            "    ADAPRO_NICE                 0"                                ,
            "    ADAPRO_SERIALIZE_COMMANDS   TRUE"                             ,
            "    ADAPRO_SUPERVISOR_CORE      -1"                               ,
            ""                                                                 ,
            "========================================"
            "========================================"                         ,
            ""
    };

    ifstream file{"tests/resources/test6.out"};
    string line;
    bool conditions[20];
    conditions[0] = file.is_open();
    getline(file, line);
    conditions[1] = regex_match(line, expected_first_line);
    size_t line_number{0};
    while (getline(file, line))
    {
        conditions[line_number] = (line == expexted_lines[line_number]);
        ++line_number;
    }
    file.close();
    remove("tests/resources/test6.out");
    for (size_t i = 0; i < 20; ++i)
    {
        CPPUNIT_ASSERT(conditions[i]);
    }
}
