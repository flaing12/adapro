/*
 * File:   ControllerTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 1 June 2017, 9:12:01
 */

#include <cstdint>
#include <string>
#include <map>
#include <utility>
#include <thread>
#include <chrono>
#include "ControllerTest.hpp"
#include "../../../headers/control/Logger.hpp"
#include "../../../headers/control/Supervisor.hpp"
#include "../../../headers/DIM/Controller.hpp"
#include "../../../headers/DIM/Wrapper.hpp"
#include "../../../headers/DIM/TaskArguments.hpp"
#include "../../../headers/DIM/SubscriptionType.hpp"
#include "../../../headers/DIM/Typedefs.hpp"
#include "../../../headers/data/State.hpp"
#include "../../../headers/data/LoggingLevel.hpp"
#include "../../../headers/data/Parameters.hpp"
#include "../../../headers/data/Typedefs.hpp"

#define CHECK_FOR_EXCEPTION(n, task) \
    try\
    {\
        controller.task;\
    }\
    catch (const runtime_error& e)\
    {\
        LOGGER.print("Exception: " + string(e.what()), USER_DEBUG);\
        condition_##n = true;\
    }

CPPUNIT_TEST_SUITE_REGISTRATION(ControllerTest);

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::DIM;
using namespace ADAPRO::Data;

// TODO: Also test the actual DIM server functionality by subscribing/calling
// the registered services in a test case through DIMWrapper.

Logger ControllerTest::LOGGER{FATAL};
//Logger ControllerTest::LOGGER{WARNING | ERROR | FATAL, "", true};
config_t ControllerTest::CONFIGURATION
{
#ifdef __linux__
    {"ADAPRO_DAEMON_ENABLED", "FALSE"},
#endif
    {"ADAPRO_SUPERVISOR_CORE", "-1"}
    , {"ADAPRO_DIM_CONTROLLER_CORE", "-1"}
#ifndef EXCLUDE_DIM
    , {"ADAPRO_DIM_CALLBACK_CORE", "-1"}
    , {"ADAPRO_DIM_DNS_NODE", "localhost"}
    , {"ADAPRO_DIM_DNS_PORT", "2505"}
    , {"ADAPRO_DIM_SERVER_NAME", "ADAPRO_DIM_Controller_Test"}
    , {"ADAPRO_DIM_SERVER_ENABLED", "TRUE"}
#endif
};

uint32_t ControllerTest::SUPERVISOR_STATE{0};

Supervisor ControllerTest::SUPERVISOR
{
    LOGGER
    , CONFIGURATION
    , std::move(list<worker_factory_t>{})
#ifndef EXCLUDE_DIM
    , list<command_factory_t>{}
    , Wrapper::default_dim_error_handler
    , Wrapper::default_dim_error_handler
    , &SUPERVISOR_STATE
#endif
};

ControllerTest::ControllerTest() {}

ControllerTest::~ControllerTest() {}

void ControllerTest::setUp() {}

void ControllerTest::tearDown() {}

void ControllerTest::test_publish_service()
{
    SUPERVISOR_STATE = 0;
    Controller controller
    {
            LOGGER
            , SUPERVISOR
            , CONFIGURATION
#ifndef EXCLUDE_DIM
            , list<command_factory_t>{}
            , Wrapper::default_dim_error_handler
            , Wrapper::default_dim_error_handler
            , &SUPERVISOR_STATE
#endif
    };
    controller.publish(make_shared<ServicePublication>(
            "TEST/SERVICE_1",
            "I:1",
            (void*) &SUPERVISOR_STATE,
            sizeof(int)
    ));
    controller.start_sync();
    this_thread::sleep_for(chrono::milliseconds(200));
    controller.stop_sync();
    CPPUNIT_ASSERT(controller.get_state() == STOPPED);
}

void ControllerTest::test_publish_command()
{
    SUPERVISOR_STATE = 0;
    Controller controller
    {
            LOGGER
            , SUPERVISOR
            , CONFIGURATION
#ifndef EXCLUDE_DIM
            , list<command_factory_t>{}
            , Wrapper::default_dim_error_handler
            , Wrapper::default_dim_error_handler
            , &SUPERVISOR_STATE
#endif
    };
    controller.publish(make_shared<CommandPublication>(
            "TEST/COMMAND_1",
            "I:1",
            [](void* tag, void* buffer, int* size){}
    ));
    controller.start_sync();
    this_thread::sleep_for(chrono::milliseconds(200));
    controller.stop_sync();
    CPPUNIT_ASSERT(controller.get_state() == STOPPED);
}

void ControllerTest::test_update_service()
{
    SUPERVISOR_STATE = 0;
    Controller controller
    {
            LOGGER
            , SUPERVISOR
            , CONFIGURATION
#ifndef EXCLUDE_DIM
            , list<command_factory_t>{}
            , Wrapper::default_dim_error_handler
            , Wrapper::default_dim_error_handler
            , &SUPERVISOR_STATE
#endif
    };
    controller.start_sync();
#ifndef EXCLUDE_DIM
    controller.update(make_shared<ServiceID>(0));
    this_thread::sleep_for(chrono::milliseconds(200));
    controller.stop_sync();
    CPPUNIT_ASSERT(controller.get_state() == ABORTED);
#else
    controller.wait_for_state_mask(HALTED_MASK);
    CPPUNIT_ASSERT(controller.get_state() == STOPPED);
#endif
}

void ControllerTest::test_subscribe_to_service()
{
    SUPERVISOR_STATE = 0;
    Controller controller
    {
            LOGGER
            , SUPERVISOR
            , CONFIGURATION
#ifndef EXCLUDE_DIM
            , list<command_factory_t>{}
            , Wrapper::default_dim_error_handler
            , Wrapper::default_dim_error_handler
            , &SUPERVISOR_STATE
#endif
    };
    controller.start_sync();
#ifndef EXCLUDE_DIM
    controller.subscribe(make_shared<ServiceSubscription>(
            "TEST/SERVICE_1",
            SubscriptionType::ONCE_ONLY,
            0,
            nullptr,
            0,
            [](void* tag, void* buffer, int* size){}
    ));
    this_thread::sleep_for(chrono::milliseconds(200));
    controller.stop_async();
#endif
    controller.wait_for_state_mask(HALTED_MASK);
    CPPUNIT_ASSERT(controller.get_state() == STOPPED);
}

void ControllerTest::test_call_command()
{
    SUPERVISOR_STATE = 0;
    Controller controller
    {
            LOGGER
            , SUPERVISOR
            , CONFIGURATION
#ifndef EXCLUDE_DIM
            , list<command_factory_t>{}
            , Wrapper::default_dim_error_handler
            , Wrapper::default_dim_error_handler
            , &SUPERVISOR_STATE
#endif
    };
    controller.start_sync();
#ifndef EXCLUDE_DIM
    controller.call(make_shared<CommandInvocation>(
            "DUMMY",
            nullptr,
            0
    ));
    this_thread::sleep_for(chrono::milliseconds(200));
    controller.stop_async();
#endif
    controller.wait_for_state_mask(HALTED_MASK);
    CPPUNIT_ASSERT(controller.get_state() == STOPPED);
}

void ControllerTest::test_overwrite_timestamp()
{
    SUPERVISOR_STATE = 0;
    Controller controller
    {
            LOGGER
            , SUPERVISOR
            , CONFIGURATION
#ifndef EXCLUDE_DIM
            , list<command_factory_t>{}
            , Wrapper::default_dim_error_handler
            , Wrapper::default_dim_error_handler
            , &SUPERVISOR_STATE
#endif
    };
    controller.start_sync();
#ifndef EXCLUDE_DIM
    int x, y;
    controller.overwrite(make_shared<TimestampRequest>(0, &x, &y));
    this_thread::sleep_for(chrono::milliseconds(200));
    controller.stop_sync();
    CPPUNIT_ASSERT(controller.get_state() == ABORTED);
#else
    controller.wait_for_state_mask(HALTED_MASK);
    CPPUNIT_ASSERT(controller.get_state() == STOPPED);
#endif
}

void ControllerTest::test_illegal_task_states()
{
    bool condition_1(false);
    bool condition_2(false);
    bool condition_3(false);
    bool condition_4(false);
    bool condition_5(false);
    bool condition_6(false);

    SUPERVISOR_STATE = 0;
    Controller controller
    {
            LOGGER
            , SUPERVISOR
            , CONFIGURATION
#ifndef EXCLUDE_DIM
            , list<command_factory_t>{}
            , Wrapper::default_dim_error_handler
            , Wrapper::default_dim_error_handler
            , &SUPERVISOR_STATE
#endif
    };
    CHECK_FOR_EXCEPTION
    (
            1,
            update(make_shared<ServiceID>(0))
    );
    int seconds, milliseconds;
    CHECK_FOR_EXCEPTION
    (
            2,
            overwrite(make_shared<TimestampRequest>(0, &milliseconds, &seconds))
    );

    controller.start_sync();

    CHECK_FOR_EXCEPTION
    (
            3,
            publish(make_shared<ServicePublication>(
                    "DUMMY",
                    "I:1",
                    nullptr,
                    0
            ))
    );
    CHECK_FOR_EXCEPTION
    (
            4,
            publish(make_shared<CommandPublication>(
                    "DUMMY",
                    "I:1",
                    nullptr,
                    0
            ))
    );

    controller.stop_sync();

    CHECK_FOR_EXCEPTION
    (
            5,
            subscribe(make_shared<ServiceSubscription>(
                    "DUMMY",
                    SubscriptionType::ONCE_ONLY,
                    0,
                    nullptr,
                    0
            ))
    );
    CHECK_FOR_EXCEPTION
    (
            6,
            call(make_shared<CommandInvocation>(
                    "DUMMY",
                    nullptr,
                    0
            ))
    );

    CPPUNIT_ASSERT(condition_1);
    CPPUNIT_ASSERT(condition_2);
    CPPUNIT_ASSERT(condition_3);
    CPPUNIT_ASSERT(condition_4);
    CPPUNIT_ASSERT(condition_5);
    CPPUNIT_ASSERT(condition_6);
}
