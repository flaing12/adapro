/*
 * File:   SupervisorTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 16 January 2017, 16:23:52
 */

#ifndef ADAPRO_SUPERVISOR_TEST_HPP
#define ADAPRO_SUPERVISOR_TEST_HPP

#include <cppunit/extensions/HelperMacros.h>
#include "../../../headers/control/Logger.hpp"

class SupervisorTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(SupervisorTest);

    CPPUNIT_TEST(test_serial_lifetime);
    CPPUNIT_TEST(test_parallel_lifetime);
    CPPUNIT_TEST(test_watchdog_mechanism);

    CPPUNIT_TEST_SUITE_END();

public:
    SupervisorTest();
    virtual ~SupervisorTest();
    void setUp();
    void tearDown();

private:
    static ADAPRO::Control::Logger LOGGER;

    void test_serial_lifetime();
    void test_parallel_lifetime();
    void test_watchdog_mechanism(); // Fails with Debug builds.
};

#endif /* ADAPRO_SUPERVISOR_TEST_HPP */

