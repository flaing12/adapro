#include <sys/cdefs.h>
#include "SessionTest.hpp"

__skipcfl int main(int argc, char** argv)
{
    SessionTest t;
    t.setUp();
    t.test_worker_crash();
    t.setUp();
    t.test_SIGSEGV_handler();
    t.setUp();
    t.test_SIGABRT_handler();
    t.setUp();
    t.test_SIGTERM_handler();
    t.setUp();
    t.test_SIGINT_handler();
    t.setUp();
    t.test_SIGILL_handler();
    t.setUp();
    t.test_SIGFPE_handler();
#ifdef _POSIX_C_SOURCE
    t.setUp();
    t.test_SIGHUP_handler();
#endif
    t.setUp();
    t.test_unsupported_signal();
    t.setUp();
    t.test_unhandled_exception();
    t.setUp();
    t.test_good_session();
    t.setUp();
    t.test_good_session_with_configuration_file_access();
    return 0;
}