/*
 * File:   SessionTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 24 March 2017, 14:25:13
 */

#ifndef ADAPRO_SESSION_TEST_HPP
#define ADAPRO_SESSION_TEST_HPP

#include <cstdint>
#include <thread>
#include <atomic>
#ifndef __divine__
#include <cppunit/extensions/HelperMacros.h>
#endif

#ifdef __divine__
class SessionTest final
{
#else
class SessionTest : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE(SessionTest);

    CPPUNIT_TEST(test_worker_crash);
    CPPUNIT_TEST(test_SIGSEGV_handler);
    CPPUNIT_TEST(test_SIGABRT_handler);
    CPPUNIT_TEST(test_SIGTERM_handler);
    CPPUNIT_TEST(test_SIGINT_handler);
    CPPUNIT_TEST(test_SIGILL_handler);
    CPPUNIT_TEST(test_SIGFPE_handler);
#ifdef _POSIX_C_SOURCE
    CPPUNIT_TEST(test_SIGHUP_handler);
#endif
//    CPPUNIT_TEST(test_unsupported_signal); // Let's call this undefined behaviour
//    CPPUNIT_TEST(test_unhandled_exception); // Investigate this. Might be beyond repair
    CPPUNIT_TEST(test_good_session);
    CPPUNIT_TEST(test_good_session_with_configuration_file_access);

    CPPUNIT_TEST_SUITE_END();
#endif

public:
    SessionTest();
    virtual ~SessionTest();
    void setUp();
    void tearDown();

#ifndef __divine__
private:
#endif
    static std::atomic<size_t> SIGSEGV_COUNT;
    static std::atomic<size_t> SIGABRT_COUNT;
    static std::atomic<size_t> SIGTERM_COUNT;
    static std::atomic<size_t> SIGINT_COUNT;
    static std::atomic<size_t> SIGILL_COUNT;
    static std::atomic<size_t> SIGFPE_COUNT;
#ifdef _POSIX_C_SOURCE
    static std::atomic<size_t> SIGHUP_COUNT;
#endif

    void test_worker_crash();
    void test_SIGSEGV_handler();
    void test_SIGABRT_handler();
    void test_SIGTERM_handler();
    void test_SIGINT_handler();
    void test_SIGILL_handler();
    void test_SIGFPE_handler();
#ifdef _POSIX_C_SOURCE
    void test_SIGHUP_handler();
#endif
    void test_unsupported_signal();
    void test_unhandled_exception();
    void test_good_session();
    void test_good_session_with_configuration_file_access();
};

#endif /* ADAPRO_SESSION_TEST_HPP */

