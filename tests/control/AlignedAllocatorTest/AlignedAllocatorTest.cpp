/*
 * File:   AlignedAllocatorTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 22 February 2017, 13:12:01
 */

#include <vector>
#include "../../../headers/control/AlignedAllocator.hpp"
#include "AlignedAllocatorTest.hpp"

using namespace std;
using namespace ADAPRO::Control;

CPPUNIT_TEST_SUITE_REGISTRATION(AlignedAllocatorTest);

AlignedAllocatorTest::AlignedAllocatorTest() {}

AlignedAllocatorTest::~AlignedAllocatorTest() {}

void AlignedAllocatorTest::setUp() {}

void AlignedAllocatorTest::tearDown() {
}

void AlignedAllocatorTest::test()
{
    // TODO: Since ADAPOS support was removed from the master branch, new test
    // has to be implemented here.
}
