/*
 * File:   AlignedAllocatorTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 22 February 2017, 13:12:01
 */

#ifndef ADAPRO_ALIGNED_ALLOCATOR_TEST_HPP
#define ADAPRO_ALIGNED_ALLOCATOR_TEST_HPP

#include <cppunit/extensions/HelperMacros.h>

class AlignedAllocatorTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(AlignedAllocatorTest);

    CPPUNIT_TEST(test);

    CPPUNIT_TEST_SUITE_END();

public:
    AlignedAllocatorTest();
    virtual ~AlignedAllocatorTest();
    void setUp();
    void tearDown();

private:
    void test();
};

#endif /* ADAPRO_ALIGNED_ALLOCATOR_TEST_HPP */

