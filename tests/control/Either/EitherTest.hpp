
/*
 * File:   EitherTest.hpp
 * Author: John Lång
 *
 * Created on 2 January 2019, 15:45:53
 */

#ifndef ADAPRO_EITHER_TEST_HPP
#define ADAPRO_EITHER_TEST_HPP

#include <cppunit/extensions/HelperMacros.h>

class EitherTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(EitherTest);

    CPPUNIT_TEST(test_make_left_and_right);
    CPPUNIT_TEST(test_lift);
    CPPUNIT_TEST(test_then);
#ifdef DEBUG
    CPPUNIT_TEST(test_precondition);
    CPPUNIT_TEST(test_postcondition);
#endif

    CPPUNIT_TEST_SUITE_END();

public:
    EitherTest();
    virtual ~EitherTest();
    void setUp();
    void tearDown();

private:
    void test_make_left_and_right();
    void test_lift();
    void test_then();
#ifdef DEBUG
    void test_precondition();
    void test_postcondition();
#endif
};

#endif /* ADAPRO_EITHER_TEST_HPP */

