/*
 * File:   EitherTest.cpp
 * Author: John Lång
 *
 * Created on 2 January 2019, 15:45:53
 */

#include <functional>
#include <string>
#include <iostream>
#include "../../../headers/control/Either.hpp"
#include "../../../headers/data/Typedefs.hpp"
#include "EitherTest.hpp"

using namespace std;
using namespace ADAPRO::Data;
using namespace ADAPRO::Control;

CPPUNIT_TEST_SUITE_REGISTRATION(EitherTest);

action_t<int,int> rational // This is actually just x + 2.
{
        [](const int& x)
        {
//            cout << "x = " << x << '\n';
            if (x != -1)
            {
                return (x * x + 3 * x + 2) / (x + 1);
            }
            else
            {
                throw std::domain_error("Division by zero.");
            }
        }
};

EitherTest::EitherTest() {}

EitherTest::~EitherTest() {}

void EitherTest::setUp() {}

void EitherTest::tearDown() {}

void EitherTest::test_make_left_and_right()
{
    Either<int,int> m1(false, "Nothing", 0);
    Either<int,int> m2(true, "", 1);
    Either<int,int> m3{Either<int,int>::make_left("Nothing")};
    Either<int,int> m4{Either<int,int>::make_right(1)};
    CPPUNIT_ASSERT(m1.is_right  != m2.is_right);
    CPPUNIT_ASSERT(m1.left      != m2.left);
    CPPUNIT_ASSERT(m1.right     != m2.right);
    CPPUNIT_ASSERT(m1.is_right  == m3.is_right);
    CPPUNIT_ASSERT(m1.left      == m3.left);
    CPPUNIT_ASSERT(m1.right     == m3.right);
    CPPUNIT_ASSERT(m2.is_right  == m4.is_right);
    CPPUNIT_ASSERT(m2.left      == m4.left);
    CPPUNIT_ASSERT(m2.right     == m4.right);
}

void EitherTest::test_lift()
{
    int y(rational(5));
    Either<int,int> m1{Either<int,int>::lift(rational, 5)};
//    cout << "y = " << y << ", m1.right = " << m1.right << '\n';
    CPPUNIT_ASSERT(m1.is_right);
    CPPUNIT_ASSERT(y == m1.right);
    Either<int,int> m2{Either<int,int>::lift(rational, -1)};
//    cout << m2.left << '\n';
    CPPUNIT_ASSERT(!m2.is_right);
    CPPUNIT_ASSERT(m2.left == "std::domain_error:\n    \"Division by zero.\"");
}

static string failing_function_1(const string& s)
{
    throw std::runtime_error("Test exception 1.");
}

static string failing_function_2(const string& s)
{
    throw std::runtime_error("Test exception 2.");
}

void EitherTest::test_then()
{
    Either<string,string> m1(true, string(), "xyz");

    string t{"zyx"};

    action_t<string,string> append_tail
    {
        [&](const string& s){return s + t;}
    };

    Either<string,string> m2{m1.then(failing_function_1).then(append_tail)};
    Either<string,string> m3{m1.then(failing_function_2).then(failing_function_1)};
    CPPUNIT_ASSERT(m1.is_right);
    CPPUNIT_ASSERT(m1.left == string());
    CPPUNIT_ASSERT(m1.right == "xyz");
    CPPUNIT_ASSERT((m1.then(append_tail)).right == "xyzzyx");
    CPPUNIT_ASSERT(!m2.is_right);
//    cout << "m2.left = " << m2.left << ", m2.right = " << m2.right << '\n';
    CPPUNIT_ASSERT(m2.left == "std::runtime_error:\n    \"Test exception 1.\"");
    CPPUNIT_ASSERT(m2.right == string());
    CPPUNIT_ASSERT(!m3.is_right);
    CPPUNIT_ASSERT(m3.left == "std::runtime_error:\n    \"Test exception 2.\"");
    CPPUNIT_ASSERT(m3.right == string());
}

#ifdef DEBUG
void EitherTest::test_precondition()
{
    predicate_t<int> precondition([](const int& n){return n != -1;});
    Either<int,int> success{Either<int,int>::lift(rational, -3, precondition)};
    Either<int,int> failure = success.then(rational, precondition);
    CPPUNIT_ASSERT(success.is_right);
//    cout << "success.right = " << success.right << '\n';
    CPPUNIT_ASSERT(success.right == -1);
    CPPUNIT_ASSERT(!failure.is_right);
    CPPUNIT_ASSERT(failure.left == "std::logic_error:\n    \"Precondition failure.\"");
}

void EitherTest::test_postcondition()
{
    predicate_t<int> postcondition([](const int& n){return n != 0;});
    Either<int,int> success{Either<int,int>::lift(rational, -4, TOP<int>, postcondition)};
    Either<int,int> failure{success.then(rational, TOP<int>, postcondition)};
    CPPUNIT_ASSERT(success.is_right);
    CPPUNIT_ASSERT(success.right == -2);
    CPPUNIT_ASSERT(!failure.is_right);
    CPPUNIT_ASSERT(failure.left == "std::logic_error:\n    \"Postcondition failure.\"");
}
#endif