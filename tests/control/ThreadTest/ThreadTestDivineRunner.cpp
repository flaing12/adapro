#include <sys/cdefs.h>
#include "ThreadTest.hpp"

/**
 * This ad-hoc test runner runs the Thread unit tests. It's meant to be verified
 * with the DIVINE model checker.
 *
 * @param argc  Number of command arguments. Will be ignored.
 * @param argv  The command line argument values. Will be ignored.
 * @return      The exit status code.
 */
__skipcfl int main(int argc, char** argv)
{
    ThreadTest t;
    t.test_get_state();
    t.test_get_command();
    t.test_state_in();
    t.test_command_in();
    t.test_report();
    t.test_destructor();
    t.test_start_stop();
#ifdef __linux__
    t.test_start_cancel();
#endif
    t.test_start_pause_stop();
    t.test_pause_resume();
    t.test_unhandled_exception_1();
    t.test_unhandled_exception_2();
    t.test_stop_aborted_thread();
    t.test_wait_for_state();
//    t.test_wait_for_state_with_timeout();         // This is a real-time test.
    t.test_wait_for_state_mask();
//    t.test_wait_for_state_mask_with_timeout();    // Also a real-time test.
    t.test_transition_callback();
    t.test_propagating_state_transition_callback();
    t.test_life_cycle();
    t.test_FSM_consistency();
    t.test_set_affinity();
//    t.test_spinlock();                            // Real-time
//    t.test_spinlock_precision();                  // Real-time
    return 0;
}