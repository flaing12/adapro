/*
 * File:   ThreadTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 26.7.2015, 21:54:34
 */

#ifdef __divine__
#include <cassert>
#include <sys/cdefs.h>
#endif
#ifndef VERIFY
#include <iostream>
#endif
#include <thread>
#include <chrono>
#include <mutex>
#include <atomic>
#include <stdexcept>
#include <queue>
#include <list>
#include <unordered_map>
#include <utility>
#include <limits>
#ifdef __linux__
#include <sched.h>
#endif
#include "ThreadTest.hpp"
#ifndef VERIFY
#include "../../../headers/control/Logger.hpp"
#endif
#include "../../../headers/control/Thread.hpp"
#include "../../../headers/library/Clock.hpp"
#include "../../../headers/data/State.hpp"
#include "../../../headers/data/Command.hpp"
#include "../../../headers/data/LoggingLevel.hpp"

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::Data;

#ifdef __divine__
#define THREAD_TEST_ASSERT(x) assert(x);
#else
#define THREAD_TEST_ASSERT(x) CPPUNIT_ASSERT(x)
CPPUNIT_TEST_SUITE_REGISTRATION(ThreadTest);
#endif

#ifndef VERIFY
//Logger ThreadTest::LOGGER{WARNING | ERROR | FATAL, "", true};
Logger ThreadTest::LOGGER{SPECIAL, "", true};
//Logger ThreadTest::LOGGER{0xFF, "", true};
#endif

#ifdef __divine__
    __skipcfl
#endif
static void nop_cb(const State s) noexcept {}

#ifdef __divine__
    __skipcfl
#endif
ThreadTest::ThreadTest() {}

#ifdef __divine__
    __skipcfl
#endif
ThreadTest::~ThreadTest() {}

void ThreadTest::setUp() {}

void ThreadTest::tearDown() {}

#ifdef __divine__
    __skipcfl
#endif
void ThreadTest::test_get_state()
{
    TestThread t
    {
#ifndef VERIFY
        LOGGER,
#endif
        std::move("test1")
    };
    THREAD_TEST_ASSERT(READY == t.get_state());
}

#ifdef __divine__
    __skipcfl
#endif
void ThreadTest::test_get_command()
{
    TestThread t
    {
#ifndef VERIFY
        LOGGER,
#endif
        std::move("test2")
    };
    THREAD_TEST_ASSERT(CONTINUE == t.get_command());
}

#ifdef __divine__
    __skipcfl
#endif
void ThreadTest::test_state_in()
{
    TestThread t
    {
#ifndef VERIFY
        LOGGER,
#endif
        std::move("test3")
    };
    THREAD_TEST_ASSERT(!t.state_in(RUNNING | PAUSED));
    THREAD_TEST_ASSERT(t.state_in(READY));
}

#ifdef __divine__
    __skipcfl
#endif
void ThreadTest::test_command_in()
{
    TestThread t
    {
#ifndef VERIFY
        LOGGER,
#endif
        std::move("test4")
    };
    THREAD_TEST_ASSERT(!t.command_in(PAUSE | STOP));
    THREAD_TEST_ASSERT(t.command_in(CONTINUE));
}

#ifdef __divine__
    __skipcfl
#endif
void ThreadTest::test_report()
{
    TestThread t
    {
#ifndef VERIFY
        LOGGER,
#endif
        std::move("test5")
    };
    const bool condition1{t.report() == "test5: state = READY, command = CONTINUE"};
    t.start_sync();
    const bool condition2{t.report() == "test5: state = RUNNING, command = CONTINUE"};
    t.pause_sync();
    const bool condition3{t.report() == "test5: state = PAUSED, command = PAUSE"};
    t.resume_sync();
    this_thread::sleep_for(chrono::milliseconds(20));
    const bool condition4{t.report() == "test5: state = RUNNING, command = CONTINUE"};
    t.stop_sync();
    const bool condition5{t.report() == "test5: state = STOPPED, command = STOP"};
    THREAD_TEST_ASSERT(condition1);
    THREAD_TEST_ASSERT(condition2);
    THREAD_TEST_ASSERT(condition3);
    THREAD_TEST_ASSERT(condition4);
    THREAD_TEST_ASSERT(condition5);
}

#ifdef __divine__
    __skipcfl
#endif
void ThreadTest::test_destructor()
{
    {
        TestThread t1
        {
#ifndef VERIFY
        LOGGER,
#endif
            std::move("test6")
        };
    }
    {
        TestThread t2
        {
#ifndef VERIFY
        LOGGER,
#endif
            std::move("test7")
        };
//        t2.start_async(); // Causes the destructor to get stuck because of a known issue.
        t2.start_sync();
//        t2.stop_sync();
    }
}

#ifdef __divine__
    __skipcfl
#endif
void ThreadTest::test_start_stop()
{
    TestThread t
    {
#ifndef VERIFY
        LOGGER,
#endif
        std::move("test8")
    };
    t.start_sync();
    const bool condition1{CONTINUE  == t.get_command()};
    const bool condition2{RUNNING   == t.get_state()};
    t.stop_sync();
    const bool condition3{STOP      == t.get_command()};
    const bool condition4{STOPPED   == t.get_state()};
    THREAD_TEST_ASSERT(condition1);
    THREAD_TEST_ASSERT(condition2);
    THREAD_TEST_ASSERT(condition3);
    THREAD_TEST_ASSERT(condition4);
}

#ifdef __linux__
#ifdef __divine__
    __skipcfl
#endif
void ThreadTest::test_start_cancel()
{
    class Xar final: public ADAPRO::Control::Thread
    {
        atomic<bool> flag;
    protected:
        virtual void prepare() override {}
        virtual void execute() override
        {
            this_thread::sleep_for(chrono::milliseconds(100));
        }
        virtual void finish() override
        {
            flag.store(false, memory_order_release);
        }
    public:
        explicit Xar(std::string&& name):
                ADAPRO::Control::Thread
                (
#ifndef VERIFY
                        LOGGER,
#endif
                        std::move(name),
                        nop_cb,
                        -1
                ),
                flag(true) {}
        bool get_flag() {return flag.load(memory_order_consume);}
        virtual ~Xar() { join(); }
    };
    Xar t{std::move("test9")};
    t.start_sync();
    const bool condition1{CONTINUE  == t.get_command()};
    const bool condition2{RUNNING   == t.get_state()};
    t.cancel();
    const bool condition3{CONTINUE  == t.get_command()};
    const bool condition4{RUNNING   == t.get_state()};
    const bool condition5{t.get_flag()};
    t.stop_sync();
    THREAD_TEST_ASSERT(condition1);
    THREAD_TEST_ASSERT(condition2);
    THREAD_TEST_ASSERT(condition3);
    THREAD_TEST_ASSERT(condition4);
    THREAD_TEST_ASSERT(condition5);
}
#endif

#ifdef __divine__
    __skipcfl
#endif
void ThreadTest::test_start_pause_stop()
{
    TestThread t
    {
#ifndef VERIFY
        LOGGER,
#endif
        std::move("test10")
    };
    t.start_sync();
    t.pause_sync();
    const bool condition1 = PAUSE       == t.get_command();
    const bool condition2 = PAUSED      == t.get_state();
    t.stop_sync();
    const bool condition3 = STOP        == t.get_command();
    const bool condition4 = STOPPED     == t.get_state();
    THREAD_TEST_ASSERT(condition1);
    THREAD_TEST_ASSERT(condition2);
    THREAD_TEST_ASSERT(condition3);
    THREAD_TEST_ASSERT(condition4);
}

#ifdef __divine__
    __skipcfl
#endif
void ThreadTest::test_pause_resume()
{
    class Bar final: public ADAPRO::Control::Thread
    {
        atomic<uint64_t> counter;
    protected:
        virtual void prepare() override {}
        virtual void execute() override
        {
            counter.store
            (
                    counter.load(memory_order_consume) + 1,
                    memory_order_release
            );
        }
        virtual void finish() override {}
    public:
        explicit Bar(std::string&& name):
                ADAPRO::Control::Thread
                (
#ifndef VERIFY
                        LOGGER,
#endif
                        std::move(name),
                        nop_cb,
                        -1
                ),
                counter(0) {}
        uint64_t get_count() {return counter.load(memory_order_consume);}
        virtual ~Bar() { join(); }
    };

    Bar bar{std::move("test11")};
    const bool condition1{bar.get_count() == 0};
    bar.start_sync();
#ifndef __divine__
    this_thread::sleep_for(chrono::milliseconds(10));
    uint64_t count;
    bar.pause_sync();
    const uint64_t condition2{count = bar.get_count()};
    this_thread::sleep_for(chrono::milliseconds(10));
    const bool condition3{count     == bar.get_count()};
    bar.resume_sync();
    const bool condition4{CONTINUE  == bar.get_command()};
    const bool condition5{RUNNING   == bar.get_state()};
    this_thread::sleep_for(chrono::milliseconds(100));
    const bool condition6{count < bar.get_count()};
    bar.stop_sync();

    THREAD_TEST_ASSERT(condition1);
    THREAD_TEST_ASSERT(condition2);
    THREAD_TEST_ASSERT(condition3);
    THREAD_TEST_ASSERT(condition4);
    THREAD_TEST_ASSERT(condition5);
    THREAD_TEST_ASSERT(condition6);
#else
    bar.pause_sync();
    bar.resume_sync();
    bar.stop_sync();
#endif
}

#ifdef __divine__
    __skipcfl
#endif
void ThreadTest::test_unhandled_exception_1()
{
    class Fizz final: public ADAPRO::Control::Thread
    {
    protected:
        virtual void prepare() override {}
        virtual void execute() override {throw domain_error("Throw test domain_error.");}
        virtual void finish() override {}
    public:
        explicit Fizz(std::string&& name):
                ADAPRO::Control::Thread
                (
#ifndef VERIFY
        LOGGER,
#endif
                        std::move(name),
                        nop_cb,
                        -1
                ) {}
        virtual ~Fizz() { join(); }
    };
    Fizz t{std::move("test12")};
    t.start_sync();
    t.wait_for_state_mask(ABORTED);
}

#ifdef __divine__
    __skipcfl
#endif
void ThreadTest::test_unhandled_exception_2()
{
    class Buzz final: public ADAPRO::Control::Thread
    {
    protected:
        virtual void prepare() override {}
        virtual void execute() override {throw 4;}
        virtual void finish() override {}
    public:
        explicit Buzz(std::string&& name):
                ADAPRO::Control::Thread
                (
#ifndef VERIFY
                        LOGGER,
#endif
                        std::move(name),
                        nop_cb,
                        1
                ) {}
        virtual ~Buzz() { join(); }
    };
    Buzz t{std::move("test13")};
    t.start_sync();
    t.wait_for_state_mask(ABORTED);
}

#ifdef __divine__
    __skipcfl
#endif
void ThreadTest::test_stop_aborted_thread()
{
    class Foo final: public ADAPRO::Control::Thread
    {
    protected:
        virtual void prepare() override {}
        virtual void execute() override {throw runtime_error("Throw test exception.");}
        virtual void finish() override {}
    public:
        explicit Foo(std::string&& name):
                ADAPRO::Control::Thread
                (
#ifndef VERIFY
        LOGGER,
#endif
                        std::move(name),
                        nop_cb,
                        -1
                ) {}
        virtual ~Foo() { join(); }
    };

    Foo t{std::move("test14")};
    t.start_async();
    t.wait_for_state_mask(HALTING_MASK);
    const bool condition1{ABORT   == t.get_command()};
    t.wait_for_state_mask(HALTED_MASK);
    const bool condition2{ABORTED == t.get_state()};
    t.stop_sync();
    const bool condition3{ABORT   == t.get_command()};
    const bool condition4{ABORTED == t.get_state()};
    THREAD_TEST_ASSERT(condition1);
    THREAD_TEST_ASSERT(condition2);
    THREAD_TEST_ASSERT(condition3);
    THREAD_TEST_ASSERT(condition4);
}

#ifdef __divine__
    __skipcfl
#endif
void ThreadTest::test_wait_for_state()
{
    TestThread t
    {
#ifndef VERIFY
        LOGGER,
#endif
        std::move("test15")
    };
    t.wait_for_state(READY);
    t.start_sync();
    t.wait_for_state(RUNNING);
    t.pause_async();
    t.wait_for_state(PAUSED);
    t.resume_async();
    t.wait_for_state(RUNNING);
    t.stop_async();
    t.wait_for_state(STOPPING);
    t.wait_for_state(STOPPED);
}

void ThreadTest::test_wait_for_state_with_timeout()
{
    class Bor final: public ADAPRO::Control::Thread
    {
        virtual void prepare() override {}
        virtual void execute() override
        {
            this_thread::sleep_for(chrono::seconds(1));
        }
        virtual void finish() override {}
    public:
        explicit Bor(string&& name) noexcept:
                ADAPRO::Control::Thread
                (
#ifndef VERIFY
                        LOGGER,
#endif
                        std::move(name),
                        nop_cb,
                        -1
                ) {}
        virtual ~Bor() noexcept { join(); }
    };

    Bor t{"test16"};
    t.start_sync();
    this_thread::sleep_for(chrono::milliseconds(10));
    t.stop_async();
    bool timeout{false};
    try
    {
        t.wait_for_state(STOPPED, chrono::milliseconds(10));
    }
    catch (const std::runtime_error& e)
    {
//        cout << "    wait_for_state timed out." << '\n';
        timeout = true;
    }
    t.wait_for_state(STOPPED);
    THREAD_TEST_ASSERT(timeout);
}

#ifdef __divine__
    __skipcfl
#endif
void ThreadTest::test_wait_for_state_mask()
{
    TestThread t
    {
#ifndef VERIFY
        LOGGER,
#endif
        std::move("test17")
    };
    t.start_async();
    t.wait_for_state_mask((uint8_t) RUNNING | PAUSED);
    t.pause_async();
    t.wait_for_state_mask((uint8_t) PAUSED);
    t.resume_async();
    t.wait_for_state_mask((uint8_t) RUNNING);
    t.stop_async();
    t.wait_for_state_mask((uint8_t) STOPPED | ABORTED);
}

void ThreadTest::test_wait_for_state_mask_with_timeout()
{
    class Zyx final: public ADAPRO::Control::Thread
    {
        virtual void prepare() override {}
        virtual void execute() override
        {
            this_thread::sleep_for(chrono::seconds(1));
        }
        virtual void finish() override {}
    public:
        explicit Zyx(string&& name) noexcept:
                ADAPRO::Control::Thread
                (
#ifndef VERIFY
                        LOGGER,
#endif
                        std::move(name),
                        nop_cb,
                        -1
                ) {}
        virtual ~Zyx() noexcept { join(); }
    };

    Zyx t{"test18"};
    t.start_sync();
    this_thread::sleep_for(chrono::milliseconds(10));
    t.stop_async();
    bool timeout{false};
    try
    {
        t.wait_for_state_mask((uint8_t) STOPPED | ABORTED, chrono::milliseconds(10));
    }
    catch (const std::runtime_error& e)
    {
//        cout << "    wait_for_state_mask timed out." << '\n';
        timeout = true;
    }
    t.wait_for_state_mask((uint8_t) STOPPED);
    THREAD_TEST_ASSERT(timeout);
}

static atomic_int trans_cb_counter{0};

#ifdef __divine__
    __skipcfl
#endif
static void increment_tras_cb_counter(const State s) noexcept
{
    trans_cb_counter++;
}

#ifdef __divine__
    __skipcfl
#endif
void ThreadTest::test_transition_callback()
{
    class Baz final: public ADAPRO::Control::Thread
    {
    protected:
        virtual void prepare() override {}
        virtual void execute() override {}
        virtual void finish() override {}
    public:
        explicit Baz(std::string&& name) noexcept:
                ADAPRO::Control::Thread
                (
#ifndef VERIFY
                        LOGGER,
#endif
                        std::move(name),
                        increment_tras_cb_counter
                )
        {}
        virtual ~Baz() { join(); }
    };
    const bool condition1{0 == trans_cb_counter.load(memory_order_consume)};
    Baz t{std::move("test19")};
    const bool condition2{1 == trans_cb_counter.load(memory_order_consume)};
    t.start_sync(); // Covariant, may return before the counter is incremented.
    const bool condition3{2 <= trans_cb_counter.load(memory_order_consume)};
    t.pause_sync();
    const bool condition4{4 == trans_cb_counter.load(memory_order_consume)};
    t.resume_sync(); // Covariant, may return before the counter is incremented.
    const bool condition5{4 <= trans_cb_counter.load(memory_order_consume)};
    t.stop_sync();
    const bool condition6{7 == trans_cb_counter.load(memory_order_consume)};

    THREAD_TEST_ASSERT(condition1);
    THREAD_TEST_ASSERT(condition2);
    THREAD_TEST_ASSERT(condition3);
    THREAD_TEST_ASSERT(condition4);
    THREAD_TEST_ASSERT(condition5);
    THREAD_TEST_ASSERT(condition6);
}

#ifdef __divine__
    __skipcfl
#endif
void ThreadTest::test_propagating_state_transition_callback()
{
    class Fuu final: public Thread
    {
    protected:
        virtual void prepare() override {}
        virtual void execute() override
        {
            this_thread::sleep_for(chrono::milliseconds(10));
        }
        virtual void finish() override {}
    public:
        TestThread subordinate;
        Fuu(std::string&& name, std::string&& subordinate_name) noexcept:
                Thread
                (
#ifndef VERIFY
                        LOGGER,
#endif
                        std::move(name),
                        STATE_PROPAGATION_CB({&subordinate})
                ),
                subordinate
                (
#ifndef VERIFY
                        LOGGER,
#endif
                        std::move(subordinate_name)
                ) {};

        virtual string report() noexcept override
        {
            return Thread::report() + "\n        " + subordinate.report();
        }
        virtual ~Fuu() noexcept { join(); }
    };

    Fuu t{"test20", "test21"};
    const bool condition1{t.get_state() == READY};
    const bool condition2{t.get_state() == t.subordinate.get_state()};
    t.start_sync();
    const bool condition3{t.get_state() == RUNNING};
    const bool condition4{t.get_state() == t.subordinate.get_state()};
    t.pause_sync();
    const bool condition5{t.get_state() == PAUSED};
    const bool condition6{t.get_state() == t.subordinate.get_state()};
    t.resume_sync();
    const bool condition7{t.get_state() == RUNNING};
    t.subordinate.wait_for_state_mask(RESUME_MASK);
    const bool condition8{t.get_state() == t.subordinate.get_state()};
    t.stop_sync();
    const bool condition9{t.get_state() == STOPPED};
    const bool condition10{t.get_state() == t.subordinate.get_state()};
//    cout << t.report() << '\n';

    THREAD_TEST_ASSERT(condition1);
    THREAD_TEST_ASSERT(condition2);
    THREAD_TEST_ASSERT(condition3);
    THREAD_TEST_ASSERT(condition4);
    THREAD_TEST_ASSERT(condition5);
    THREAD_TEST_ASSERT(condition6);
    THREAD_TEST_ASSERT(condition7);
    THREAD_TEST_ASSERT(condition8);
    THREAD_TEST_ASSERT(condition9);
    THREAD_TEST_ASSERT(condition10);
}

#ifdef __divine__
    __skipcfl
#endif
void ThreadTest::test_life_cycle()
{
    TestThread t
    {
#ifndef VERIFY
        LOGGER,
#endif
        std::move("test22")
    };
    const bool condition1{CONTINUE == t.get_command()};
    const bool condition2{READY    == t.get_state()};
    t.start_sync();
    const bool condition3{CONTINUE == t.get_command()};
    const bool condition4{RUNNING  == t.get_state()};
    t.stop_sync();
    const bool condition5{STOP     == t.get_command()};
    const bool condition6{STOPPED  == t.get_state()};

    THREAD_TEST_ASSERT(condition1);
    THREAD_TEST_ASSERT(condition2);
    THREAD_TEST_ASSERT(condition3);
    THREAD_TEST_ASSERT(condition4);
    THREAD_TEST_ASSERT(condition5);
    THREAD_TEST_ASSERT(condition6);
}

#ifdef __divine__
    __skipcfl
#endif
void ThreadTest::test_set_affinity()
{
#ifdef __linux__
    mutex mutex_obj;
    using pair_t = pair<const size_t, const size_t>;
    queue<pair_t> threads_and_cores;
    class Boz final: public ADAPRO::Control::Thread
    {
        const size_t number;
        queue<pair_t>& threads_and_cores;
        mutex& mutex_obj;
    protected:
        virtual void prepare() override {}
        virtual void execute() override
        {
            {
                lock_guard<mutex> lock(mutex_obj);
                threads_and_cores.push(
                        pair<const size_t, const size_t>(number, (size_t) sched_getcpu())
                );
            }
            this_thread::sleep_for(chrono::milliseconds(10));
        }
        virtual void finish() override {}
    public:
        Boz(string&& name, const size_t number,
                queue<pair_t>& threads_and_cores, mutex& mutex_obj) noexcept:
                ADAPRO::Control::Thread
                (
#ifndef VERIFY
        LOGGER,
#endif
                        std::move(name),
                        nop_cb,
                        number
                ),
                number(number),
                threads_and_cores(threads_and_cores),
                mutex_obj(mutex_obj) {}
        virtual ~Boz() noexcept { join(); }
    };

    Boz t{std::move("test23"), 0, threads_and_cores, mutex_obj};
    Boz u{std::move("test24"), 1, threads_and_cores, mutex_obj};
    Boz v{std::move("test25"), 2, threads_and_cores, mutex_obj};
    t.start_sync();
    u.start_sync();
    v.start_sync();
    this_thread::sleep_for(chrono::seconds(10));
    t.stop_sync();
    u.stop_sync();
    v.stop_sync();
    unordered_map<size_t, size_t> expected{{0,0}, {1,1}, {2,2}};
    while (!threads_and_cores.empty())
    {
        pair_t actual = threads_and_cores.front();
        THREAD_TEST_ASSERT(expected[actual.first] == actual.second);
//        cout << '(' << actual.first << ',' << actual.second << ')' << '\n';
        threads_and_cores.pop();
    }
#endif
}

#ifdef __divine__
    __skipcfl
#endif
void ThreadTest::test_FSM_consistency()
{
#ifdef VERIFY
#define DERIVE_THREAD(type, starts_paused, prepare_fails, execute_fails, finish_fails) \
    class type: public ADAPRO::Control::Thread                                 \
    {                                                                          \
    protected:                                                                 \
        virtual void prepare() override                                        \
        {                                                                      \
            if (starts_paused) {pause_async();}                                \
            else if (prepare_fails)                                            \
            {                                                                  \
                throw std::runtime_error{"Failure in prepare."};               \
            }                                                                  \
        }                                                                      \
        virtual void execute() override                                        \
        {                                                                      \
            if (execute_fails) {throw std::runtime_error{"Failure in execute."};}\
            else {this_thread::sleep_for(chrono::milliseconds(10));}           \
        }                                                                      \
        virtual void finish() override                                         \
        {                                                                      \
            if (finish_fails) {throw std::runtime_error{"Failure in finish."};}\
        }                                                                      \
    public:                                                                    \
        explicit type(string&& name) noexcept:                                 \
                Thread                                                         \
                (                                                              \
                        std::move(name),                                       \
                        [](const State s){},                                   \
                        -1                                                     \
                )                                                              \
        {}                                                                     \
        virtual ~type() noexcept { join(); }                                   \
    };
#else
#define DERIVE_THREAD(type, starts_paused, prepare_fails, execute_fails, finish_fails) \
    class type: public ADAPRO::Control::Thread                                 \
    {                                                                          \
    protected:                                                                 \
        virtual void prepare() override                                        \
        {                                                                      \
            if (starts_paused) {pause_async();} else {}                        \
            if (prepare_fails)                                                 \
            {                                                                  \
                throw std::runtime_error{"Failure in prepare."};               \
            }                                                                  \
        }                                                                      \
        virtual void execute() override                                        \
        {                                                                      \
            if (execute_fails) {throw std::runtime_error{"Failure in execute."};}\
            else {this_thread::sleep_for(chrono::milliseconds(10));}           \
        }                                                                      \
        virtual void finish() override                                         \
        {                                                                      \
            if (finish_fails) {throw std::runtime_error{"Failure in finish."};}\
        }                                                                      \
    public:                                                                    \
        explicit type(string&& name) noexcept:                                 \
                Thread                                                         \
                (                                                              \
                        LOGGER,                                                \
                        std::move(name),                                       \
                        [](const State s){},                                   \
                        -1                                                     \
                )                                                              \
        {}                                                                     \
        virtual ~type() noexcept { join(); }                                   \
    };
#endif

#define GENERATE_CONDITION(i, thread, state, command)                          \
    const bool condition_##i                                                   \
    {                                                                          \
        thread.get_state() == state && thread.get_command() == command         \
    };

    DERIVE_THREAD(Zarg, true,   false,  false,  false);
    DERIVE_THREAD(Zerg, false,  true,   false,  false);
    DERIVE_THREAD(Zirg, false,  false,  true,   false);
    DERIVE_THREAD(Zorg, false,  false,  false,  true);
    DERIVE_THREAD(Zurg, false,  false,  false,  false);

    Zarg a{"test26"};
    Zerg b{"test27"};
    Zirg c{"test28"};
    Zorg d{"test29"};
    Zurg e{"test30"};

    // READY state:
    e.resume_async();
    GENERATE_CONDITION(1, e, READY, CONTINUE);
    e.pause_async();
    GENERATE_CONDITION(2, e, READY, CONTINUE);
    e.stop_async();
    GENERATE_CONDITION(3, e, READY, CONTINUE);
    e.start_sync();
    GENERATE_CONDITION(4, e, RUNNING, CONTINUE);
    a.start_sync();
    GENERATE_CONDITION(5, a, PAUSED, PAUSE);

    // RUNNING state:
    e.start_sync();
    GENERATE_CONDITION(6, e, RUNNING, CONTINUE);
    e.resume_sync();
    GENERATE_CONDITION(7, e, RUNNING, CONTINUE);
    e.pause_sync();
    GENERATE_CONDITION(8, e, PAUSED, PAUSE);

    // PAUSED state:
    e.start_async();
    GENERATE_CONDITION(9, e, PAUSED, PAUSE);
    e.pause_sync();
    GENERATE_CONDITION(10, e, PAUSED, PAUSE);
    e.resume_sync();
    GENERATE_CONDITION(11, e, RUNNING, CONTINUE);
    a.stop_sync();

    GENERATE_CONDITION(12, a, STOPPED, STOP);
    e.stop_sync();

    // STOPPED state:
    GENERATE_CONDITION(13, e, STOPPED, STOP);
    a.start_async();
    GENERATE_CONDITION(14, a, STOPPED, STOP);
    a.pause_async();
    GENERATE_CONDITION(15, a, STOPPED, STOP);
    a.resume_async();
    GENERATE_CONDITION(16, a, STOPPED, STOP);
    a.stop_async();
    GENERATE_CONDITION(17, a, STOPPED, STOP);

    b.start_async();
    b.wait_for_state_mask(ABORTED);
    GENERATE_CONDITION(18, b, ABORTED, ABORT);
    c.start_async();
    c.wait_for_state_mask(ABORTED);
    GENERATE_CONDITION(19, c, ABORTED, ABORT);
    d.start_sync();
    GENERATE_CONDITION(20, d, RUNNING, CONTINUE);
    d.stop_async();
    d.wait_for_state_mask(ABORTED);
    GENERATE_CONDITION(21, d, ABORTED, ABORT);

    // ABORTED state:
    b.start_async();
    GENERATE_CONDITION(22, b, ABORTED, ABORT);
    b.pause_async();
    GENERATE_CONDITION(23, b, ABORTED, ABORT);
    b.resume_async();
    GENERATE_CONDITION(24, b, ABORTED, ABORT);
    b.stop_async();
    GENERATE_CONDITION(25, b, ABORTED, ABORT);

    THREAD_TEST_ASSERT(condition_1);
    THREAD_TEST_ASSERT(condition_2);
    THREAD_TEST_ASSERT(condition_3);
    THREAD_TEST_ASSERT(condition_4);
    THREAD_TEST_ASSERT(condition_5);
    THREAD_TEST_ASSERT(condition_6);
    THREAD_TEST_ASSERT(condition_7);
    THREAD_TEST_ASSERT(condition_8);
    THREAD_TEST_ASSERT(condition_9);
    THREAD_TEST_ASSERT(condition_10);
    THREAD_TEST_ASSERT(condition_11);
    THREAD_TEST_ASSERT(condition_12);
    THREAD_TEST_ASSERT(condition_13);
    THREAD_TEST_ASSERT(condition_14);
    THREAD_TEST_ASSERT(condition_15);
    THREAD_TEST_ASSERT(condition_16);
    THREAD_TEST_ASSERT(condition_17);
    THREAD_TEST_ASSERT(condition_18);
    THREAD_TEST_ASSERT(condition_19);
    THREAD_TEST_ASSERT(condition_20);
    THREAD_TEST_ASSERT(condition_21);
    THREAD_TEST_ASSERT(condition_22);
    THREAD_TEST_ASSERT(condition_23);
    THREAD_TEST_ASSERT(condition_24);
    THREAD_TEST_ASSERT(condition_25);
}

void ThreadTest::test_spinlock()
{
    chrono::high_resolution_clock::time_point beginning =
            chrono::high_resolution_clock::now();
    Thread::SPINLOCK(chrono::nanoseconds(1000));
    chrono::nanoseconds elapsed = chrono::duration_cast<chrono::nanoseconds>(
            chrono::high_resolution_clock::now() - beginning
    );
#ifndef VERIFY
    cout << "    Slept for " << elapsed.count() << "ns." << '\n';
#endif
    THREAD_TEST_ASSERT(elapsed.count() >= 1000);
}

void ThreadTest::test_spinlock_precision()
{
    chrono::high_resolution_clock::time_point beginning;
    uint64_t min = numeric_limits<uint64_t>::max(), max = 0, sum = 0;
    for (size_t i = 0; i < 1000; ++i)
    {
        beginning = chrono::high_resolution_clock::now();
        Thread::SPINLOCK(chrono::nanoseconds(1000));
        const uint64_t interval = chrono::duration_cast<chrono::nanoseconds>(
                chrono::high_resolution_clock::now() - beginning
        ).count();
//        cout << "Slept for " << interval << " ns." << '\n';
        sum += interval;
        if (interval < min)
        {
            min = interval;
        }
        else if (interval > max)
        {
            max = interval;
        }
    }

#ifndef VERIFY
    double avg = sum / 1000.0;
    cout << "    Minimum spinlock duration: " << min << " ns." << '\n';
    cout << "    Maximum spinlock duration: " << max << " ns." << '\n';
    cout << "    Average spinlock duration: " << avg << " ns." << '\n';
#endif
}
