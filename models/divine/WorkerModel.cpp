#include <string>
#include <map>
#ifdef __divine__
#include <sys/cdefs.h>
#endif

#ifndef EXCLUDE_DIM
#define EXCLUDE_DIM
#endif

#ifndef VERIFY
#include "../../headers/control/Logger.hpp"
#endif
#include "../../headers/control/Thread.hpp"
#include "../../headers/control/Worker.hpp"
#include "../../headers/data/State.hpp"
#include "../../headers/data/Typedefs.hpp"
#include "DummyWorker.hpp"

using namespace std;
using namespace ADAPRO::Data;
using namespace ADAPRO::Control;

/**
 * The main procedure of this model.
 */
#ifdef __divine__
    __skipcfl
#endif
int main(int argc, char** argv)
{
#ifndef VERIFY
    Logger logger{0xff, "", true};
#endif
    DummyWorker worker
    {
#ifndef VERIFY
        logger,
#endif
        map<string,string>{}
        , 1
    };
    worker.start_async();
    worker.wait_for_state_mask(HALTED_MASK);
//    return worker.get_state() == STOPPED ? 0 : 1;
    return 0;
}