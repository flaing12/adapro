#include <list>
#include <map>
#include <string>
#include <memory>
#include <functional>
#include <stdexcept>
#ifdef __divine__
#include <sys/cdefs.h>
#endif

#ifndef EXCLUDE_DIM
#define EXCLUDE_DIM
#endif

#ifndef VERIFY
#include "../../headers/control/Logger.hpp"
#endif
#include "../../headers/control/Thread.hpp"
#include "../../headers/control/Worker.hpp"
#include "../../headers/control/Supervisor.hpp"
#include "../../headers/library/ConfigurationUtils.hpp"
#include "../../headers/data/Context.hpp"
#include "../../headers/data/State.hpp"
#include "../../headers/data/LoggingLevel.hpp"
#include "../../headers/data/Parameters.hpp"
#include "../../headers/data/Typedefs.hpp"
#include "DummyWorker.hpp"

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::Data;
using namespace ADAPRO::Library;

/**
 * The main procedure of this model.
 */
#ifdef __divine__
    __skipcfl
#endif
int main(int argc, char** argv)
{
    const config_t configuration{make_default_configuration()};
#ifndef VERIFY
    Logger logger{Logger::make_logger(configuration)};
#endif
    Supervisor supervisor
    (
#ifndef VERIFY
        logger,
#endif
        configuration
        , std::move(list<worker_factory_t> {DUMMY_WORKER_FACTORY(1)})
    );
    supervisor.start_async();
    supervisor.wait_for_state_mask(HALTED_MASK);
    return supervisor.get_state() == STOPPED ? 0 : 1;
}