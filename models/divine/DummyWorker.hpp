/*
 * File:   DummyWorker.hpp
 * Author: John Lång
 *
 * Created on 17 January 2019, 18:19
 */

#ifndef DUMMY_WORKER_HPP
#define DUMMY_WORKER_HPP

#include <string>
#include <map>
#include <stdexcept>
#include "../../headers/control/Thread.hpp"
#include "../../headers/control/Worker.hpp"
#include "../../headers/data/Typedefs.hpp"

#ifndef VERIFY
namespace ADAPRO
{
namespace Control
{
    class Logger;
}
}
#endif

using namespace std;
using namespace ADAPRO::Data;
using namespace ADAPRO::Control;

/**
 * Creates a worker factory function for a DummyWorker.
 */

#ifdef VERIFY
#define DUMMY_WORKER_FACTORY(i)                                                \
    [](const config_t& configuration)                                          \
    {                                                                          \
        return unique_ptr<DummyWorker>                                         \
        (                                                                      \
                new DummyWorker(configuration, i)                              \
        );                                                                     \
    }
#else
#define DUMMY_WORKER_FACTORY(i)                                                \
    [](Logger& logger, const config_t& configuration)                          \
    {                                                                          \
        return unique_ptr<DummyWorker>                                         \
        (                                                                      \
                new DummyWorker(logger, configuration, i)                      \
        );                                                                     \
    }
#endif

/**
 * A simplistic Worker model.
 */
class DummyWorker final : public Worker
{

protected:

#ifdef __divine__
    __skipcfl
#endif
    virtual void prepare() override
    {
#ifdef __divine__
        switch (__vm_choose(4))
        {
            case 0:
                pause_async();
                break;
            case 1:
                stop_async();
                break;
            case 2:
                throw std::runtime_error{"Failure in prepare."};
            default:
                break;
        }
#endif
    }

#ifdef __divine__
    __skipcfl
#endif
    virtual void execute() override
    {
#ifdef __divine__
        switch (__vm_choose(3))
        {
            case 0:
                stop_async();
                break;
            case 1:
                throw std::runtime_error{"Failure in execute."};
                break;
            default:
                break;
        }
#endif
    }

#ifdef __divine__
    __skipcfl
#endif
    virtual void finish() override
    {
#ifdef __divine__
        if (__vm_choose(2) == 0)
        {
                throw std::runtime_error{"Failure in finish."};
        }
#endif
    }

public:
    DummyWorker
    (
#ifndef VERIFY
            Logger& logger,
#endif
            const config_t& configuration,
            const int id
    )
    noexcept:
            Worker(
#ifndef VERIFY
                    logger,
#endif
                    std::move("Worker " + to_string(id)),
                    configuration
            )
    {}

    virtual ~DummyWorker() noexcept { join(); }
};


#endif /* DUMMY_WORKER_HPP */