#include <list>
#include <map>
#include <string>
#include <memory>
#include <functional>
#include <stdexcept>
#ifdef __divine__
#include <iostream>
#include <sys/divm.h>
#include <sys/cdefs.h>
#endif

#ifndef EXCLUDE_DIM
#define EXCLUDE_DIM
#endif

#include "../../headers/control/Session.hpp"
#include "../../headers/library/ConfigurationUtils.hpp"
#include "../../headers/data/Context.hpp"
#include "../../headers/data/State.hpp"
#include "../../headers/data/LoggingLevel.hpp"
#include "../../headers/data/Parameters.hpp"
#include "../../headers/data/Typedefs.hpp"
#include "DummyWorker.hpp"

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::Data;
using namespace ADAPRO::Library;

/**
 * The main procedure of this model.
 */
#ifdef __divine__
    __skipcfl
#endif
int main(int argc, char** argv)
{
//#ifdef __divine__
//    __VERIFIER_atomic_begin();
//#endif
    const list<string> configuration_paths;

    const list<worker_factory_t> factories
    {
            DUMMY_WORKER_FACTORY(1), DUMMY_WORKER_FACTORY(2)
    };

    map<string, string> configuration(make_default_configuration());
    configuration.erase("ADAPRO_DIM_SERVER_NAME");
    configuration.erase("ADAPRO_DIM_DNS_NODE");
    configuration.erase("ADAPRO_DIM_DNS_PORT");
    configuration.erase("ADAPRO_DIM_CALLBACK_CORE");
    configuration.erase("ADAPRO_DIM_CONTROLLER_CORE");
    configuration.erase("ADAPRO_DAEMON_LOGFILE");
    configuration.at("ADAPRO_LOGGING_MASK")         = "WIFE";
    configuration.at("ADAPRO_COLOURS_ENABLED")      = "TRUE";
    configuration.at("ADAPRO_DIM_SERVER_ENABLED")   = "FALSE";
#ifdef __divine__
    if (__vm_choose(2) == 0)
    {
        configuration.at("ADAPRO_SERIALIZE_COMMANDS") = "FALSE";
    }
//    __VERIFIER_atomic_end();
#endif
    Session::INITIALIZE(std::move(Context{
            "ADAPRO DIVINE Model"   // Name of the application
            , "5.0.0"               // Version of the application
            , argc                  // Number of command-line arguments
            , argv                  // Actual command-line arguments
            , factories             // Worker factories
            , configuration_paths   // Configuration path candidates
            , configuration         // Configuration
            , false                 // Not allowing configuration to be updated
    }));

#ifdef __divine__
    thread environment([](){
        while (Session::GET_SUPERVISOR_STATE() != RUNNING) {}
        Session::PRINT("Environment started.", SPECIAL_2);
        bool environment_continue(true);
        while (environment_continue)
        {
            switch (__vm_choose(5))
            {
                case 0:
                    Session::PAUSE_ASYNC();
                    break;
                case 1:
                    Session::RESUME_ASYNC();
                    break;
                case 2:
                    Session::STOP_ASYNC();
                    break;
                case 3:
                    environment_continue = false;
                default:;
            }
            this_thread::yield();
        }
    });
    environment.detach();
#endif

    return (int) Session::RUN();
}


