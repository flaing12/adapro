/*
 * File:   Context.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 17 January 2017, 17:24
 */

#ifndef ADAPRO_CONTEXT_HPP
#define ADAPRO_CONTEXT_HPP

#include <cstdint>
#include <string>
#include <list>
#include <map>
#include <utility>
#include <memory>
#include <functional>
#include <atomic>
#include "../data/Typedefs.hpp"
#ifndef EXCLUDE_DIM
#include "../DIM/Typedefs.hpp"
#endif

namespace ADAPRO
{
#ifndef EXCLUDE_DIM
namespace DIM
{
namespace Wrapper
{
    void default_dim_error_handler(int severity, int code, char* message)
    noexcept;
}
}
#endif

namespace Control
{
    class Session;
}

namespace Data
{
    /**
     * <tt>Context</tt> contains the immutable data needed for running an ADAPRO
     * Session.
     *
     * @see ADAPRO::Control::Session
     */
    struct Context final
    {
        /**
         * Name of the ADAPRO application that is shown in the environment
         * report.
         */
        std::string application_name;

        /**
         * A string representing the version of the ADAPRO application (e.g.
         * <tt>"5.0.0"</tt>).
         */
        std::string application_version;

        /**
         * Count of command-line arguments provided to the main procedure of the
         * ADAPRO application.
         */
        int argument_count;

        /**
         * The values of the command-line arguments. If <tt>argument_count</tt>
         * is greater than zero, then the first argument is interpreted as a
         * configuration path with the highest priority.
         */
        char** const argument_values;

        /**
         * Worker factories are functions returning
         * <tt>unique_ptr&ltADAPRO::Control::Worker;&gt;</tt> objects.
         * They are used by <tt>ADAPRO::Control::Supervisor</tt> to initialize
         * the user-implemented <tt>Worker</tt>s.
         *
         * @see ADAPRO::Control::Worker
         * @see ADAPRO::Control::Supervisor
         */
        std::list<worker_factory_t> worker_factories;

        /**
         * List of the runtime search paths for the configuration used for
         * creating the <tt>Worker</tt> Threads implemented by the ADAPRO
         * application.
         *
         * @see ADAPRO::Library::import_configuration
         */
        std::list<std::string> configuration_paths;

        /**
         * The configuration for the ADAPRO Session.
         *
         * @see ADAPRO::Library::import_configuration
         * @see ADAPRO::Data::Context::configuration_paths
         * @see ADAPRO::Control::Session
         */
        ADAPRO::Data::config_t configuration;

        /**
         * <p>If <tt>true</tt>, during startup sequence, Session will attempt
         * updating its configuration using the first accessible configuration
         * file found using <tt>configuration_paths</tt>.</p>
         * <p>If the application doesn't use a configuration file, then it must
         * provide at least all the parameters included in
         * <tt>ADAPRO::Data::DEFAULT_CONFIGURATION</tt> as the
         * <tt>initial_configuration</tt> and set this field to <tt>false</tt>
         * (by using the Context constructor). Renamed from
         * <tt>update_configuration</tt> to <tt>allow_update_configuration</tt>
         * in ADAPRO 5.0.0.
         *
         * @see ADAPRO::Data::Contrext::configuration_paths
         * @see ADAPRO::Data::Contrext::initial_configuration
         */
        bool allow_update_configuration;

        /**
         * If <tt>true</tt>, Session will exit abnormally if none of the
         * configuration files provided in <tt>configuration_paths</tt> were
         * accessible, otherwise an information message will be printed and
         * Session will proceed without updating the configuration, i.e. by
         * using <tt>default_configuration</tt> only. Has only effect if
         * <tt>allow_update_configuration</tt> was set to <tt>true</tt>.
         *
         * @since 4.2.0
         * @see ADAPRO::Data::Contrext::allow_update_configuration
         * @see ADAPRO::Data::Contrext::configuration_paths
         * @see ADAPRO::Data::Contrext::initial_configuration
         */
        bool require_update_configuration;

        /**
         * If <tt>false</tt>, then a key found in the configuration file, but
         * not in the default configuration, generates a runtime exception,
         * which then causes <tt>ADAPRO::Control::main</tt> to abort with a
         * status code indicating the scenario.
         *
         * @see ADAPRO::Library::import_configuration
         */
        bool allow_new_keys;

        /**
         * The relation that the configuration file needs to satisfy. By
         * default, this is simply the binary cartesian product of strings,
         * implying that every key-value pair is accepted.
         *
         * @see ADAPRO::Library::import_configuration
         */
        relation_t relation;

        /**
         * <p>A signal callback function that maps a signal to a boolean. It
         * must deliver <strong>noexcept</strong> guarantee. ADAPRO signal
         * handler invokes this callback if it catches a supported signal and
         * initiates abnormal shutdown sequence if the return value was
         * <tt>false</tt>. If this return value was <tt>true</tt>, then the
         * ADAPRO Session is allowed to continue. <em>The signal callback
         * function must not terminate the application, nor free any memory that
         * might be still used during a normal shutdown sequence. The signal
         * callback will be invoked synchronously, i.e. it blocks the shutdown
         * sequence. </em></p>
         * <p>Currently, the following signals are supported: <tt>SIGSEGV</tt>,
         * <tt>SIGABRT</tt>, <tt>SIGTERM</tt>, <tt>SIGINT</tt>, <tt>SIGILL</tt>,
         * and <tt>SIGFPE</tt>. On POSIX-compliant systems, <tt>SIGHUP</tt> is
         * also supported. For more information on signal handling (on POSIX
         * systems), see <tt>man signal</tt>.</p>
         */
        signal_callback_t signal_callback;

#ifndef EXCLUDE_DIM
        /**
         * <p>A list of callback functions returning tuples containing
         * information of user-defined DIM command services that ADAPRO will
         * publish. The components of the tuples are:</p>
         * <ol>
         *     <li>Name of the service</li>
         *     <li>DIM service description (e.g. <tt>"I:1"</tt>)</li>
         *     <li>The callback</li>
         * </ol>
         */
        std::list<ADAPRO::DIM::command_factory_t> dim_command_factories;

        /**
         * The DIM client error callback.
         */
        ADAPRO::DIM::error_callback_t dim_client_error_handler;

        /**
         * The DIM server error callback.
         */
        ADAPRO::DIM::error_callback_t dim_server_error_handler;
#endif
    };
}
}

#endif /* ADAPRO_CONTEXT_HPP */

