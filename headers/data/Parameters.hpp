/*
 * File:   Parameter.hpp
 * Author: john Lång (john.larry.lang@cern.ch)
 *
 * Created on 2 February 2017, 11:32
 */

#ifndef ADAPRO_PARAMETER_HPP
#define ADAPRO_PARAMETER_HPP

#include <string>
#include <map>
#include <regex>
#include <stdexcept>
#include "../data/Typedefs.hpp"
#include "../library/GenericFunctions.hpp"

namespace ADAPRO
{
namespace Data
{
    using namespace ADAPRO::Library;

    /**
     * A type enumerating all the reserved ADPARO configuration keys. The string
     * representations for these values are prefixed with <tt>"ADAPRO_"</tt> to
     * distinguish them from possible user-defined application parameters.
     *
     * @since ADAPRO 5.0.0
     */
    enum Parameter
    {

        /**
         * <p>This parameter is used for enabling the use of CSI SGR colour
         * codes in the <tt>Logger</tt> output. The range of this parameter
         * consists of <tt>"TRUE"</tt> and <tt>"FALSE"</tt> (case sensitive).
         * Coloured output will be enabled if and only if the value of this
         * parameter is <tt>"TRUE"</tt>. The default value is <tt>"FALSE"</tt>.
         * </p>
         * <p>
         * The colours
         * may or may not be supported by the output terminal. In the latter
         * case, the output may have garbage characters between the log
         * messages. When running in <em>daemon mode</em>, setting this
         * parameter to <tt>"FALSE"</tt> might make automated processing of the
         * log file easier.
         * </p>
         *
         * @since ADAPRO 5.0.0
         * @see ADAPRO::Control::Logger
         * @see https://en.wikipedia.org/wiki/ANSI_escape_code#SGR
         */
        COLOURS_ENABLED,

        /**
         * This Linux-only parameter is used for enabling or disabling
         * <em>daemon  mode</em>. In daemon mode, an ADAPRO process will run as
         * a Linux daemon, i.e. as an background process detached from a virtual
         * terminal. In daemon mode, the output goes to a log file. The default
         * value of this parameter is <tt>"FALSE"</tt>.
         */
        DAEMON_ENABLED,

        /**
         * This Linux-only parameter is used for setting the logfile for the
         * daemon mode. In daemon mode, the ADAPRO Logger prints its output into
         * the logfile instead of <tt>stdout</tt> or <tt>stderr</tt>. The
         * default value is <tt>"/var/log/adapro.log"</tt>.
         */
        DAEMON_LOGFILE,

        /**
         * This parameter sets the affinity for the DIM callback thread. If the
         * value is a negative integer (represented as a string), then no
         * affinity will be set.  The range of this parameter is integers,
         * though the affinity can't be set if the value of this parameter is
         * greater than, or equal to the number of CPU cores available (since
         * their indexing starts  from zero). The default value of this
         * parameter is <tt>"-1"</tt>. <em>Currently, this parameter has no
         * effect.</em> <em>NB: Setting the affinities of thread might require
         * special privileges or capabilities from the process.</em> Please
         * consult the relevant manuals for more information.
         */
        DIM_CALLBACK_CORE,

        /**
         * This parameter sets the affinity for the ADAPRO Controller Thread. If
         * the value is a negative integer (represented as a string), then no
         * affinity will be set. The range of this parameter is integers, though
         * the affinity can't be set if the value of this parameter is greater
         * than, or equal to the number of CPU cores available (since their
         * indexing starts from zero). The default value of this parameter is
         * <tt>"-1"</tt>. <em>NB: Setting the affinities of thread might require
         * special privileges or capabilities from the process.</em> Please
         * consult the relevant manuals for more information.
         */
        DIM_CONTROLLER_CORE,

        /**
         * This parameter is used for setting the <tt>DIM_DNS_NODE</tt> when
         * using the DIM server mode. The value range is DNS hostnames and the
         * default value is <tt>"localhost"</tt>.
         */
        DIM_DNS_NODE,

        /**
         * This parameter is used for setting the <em>DIM DNS port</em> (see DIM
         * documentation) when using the DIM server mode. The value range is
         * natural numbers greater than 0 and less than 65536 (represented as
         * strings in the usual decimal  notation). The default value is
         * <tt>"2505"</tt>.
         */
        DIM_DNS_PORT,

        /**
         * This parameter is used for enabling or disabling the DIM server mode.
         * The range of this parameter consists of <tt>"TRUE"</tt> and
         * <tt>"FALSE"</tt> (case sensitive). DIM server mode will be enabled if
         * and only if the value of this parameter is <tt>"TRUE"</tt>, which is
         * the default value.
         */
        DIM_SERVER_ENABLED,

        /**
         * The name of the DIM server when running in <em>DIM server mode</em>.
         * The default value is <tt>"ADAPRO_Application"</tt>. <em>The maximum
         * length of the value is 124 (ASCII) characters.</em>
         */
        DIM_SERVER_NAME,

        /**
         * This parameter is used for enabling or disabling memory locking.
         * Locking the process' virtual memory prevents the (Linux) operating
         * system from swapping it to disk. The default value for this parameter
         * is <tt>"FALSE"</tt>. Locking the process memory requires
         * <tt>CAP_IPC_LOCK</tt> capability on Linux.
         */
        LOCK_MEMORY,

        /**
         * <p>
         * This parameter can be used for filtering events in log output. Events
         * with a logging level not included in the logging mask, constructed
         * from the value of this parameter, are discarded. The default logging
         * mask includes all logging levels, except <tt>ADAPRO_DEBUG</tt>, which
         * corresponds with the value <tt>"DEFISWZ"</tt> of this parameter.
         * </p>
         * <p>
         * The value of this parameter is expected to be a non-empty string of
         * the following alphabet:
         * </p>
         * <table>
         *     <tr><th>Symbol</th><th>LoggingLevel</th></tr>
         *     <tr><td><tt>A</tt></td><td><tt>ADAPRO_DEBUG</tt></td></tr>
         *     <tr><td><tt>D</tt></td><td><tt>USER_DEBUG</tt></td></tr>
         *     <tr><td><tt>I</tt></td><td><tt>INFO</tt></td></tr>
         *     <tr><td><tt>S</tt></td><td><tt>SPECIAL_1</tt></td></tr>
         *     <tr><td><tt>W</tt></td><td><tt>WARNING</tt></td></tr>
         *     <tr><td><tt>E</tt></td><td><tt>ERROR</tt></td></tr>
         *     <tr><td><tt>F</tt></td><td><tt>FATAL</tt></td></tr>
         *     <tr><td><tt>F</tt></td><td><tt>SPECIAL_2</tt></td></tr>
         * </table>
         * <p>The symbols can occur in any order in the string, but duplicates
         * are not allowed. Permutations are considered equal (e.g.
         * <tt>"FEW"</tt> is the same as <tt>"EFW"</tt>, etc.). For example,</p>
         * <ul>
         *     <li><tt>"DISWEFZ"</tt> (the default logging mask)</li>
         *     <li><tt>"FWISE"</tt></li>
         *     <li><tt>"F"</tt></li>
         * </ul>
         * <p>are well-formed, whereas</p>
         * <ul>
         *     <li><tt>""</tt></li>
         *     <li><tt>"AA"</tt></li>
         *     <li><tt>"XYZ"</tt></li>
         * </ul>
         * <p>are not.</p>
         *
         * @see ADAPRO::Control::Logger
         * @see ADAPRO::Data::LoggingLevel
         */
        LOGGING_MASK,

        /**
         * This parameter sets the affinity for the main thread (i.e. the one
         * executing <tt>ADAPRO::Control::Session::run</tt>). If the value is a
         * negative integer (represented as a string), then no affinity will be
         * set. The range of this parameter is integers, though the affinity
         * can't be set if the value of this parameter is greater than, or equal
         * to the number of CPU cores available (since their indexing starts
         * from zero). The default value of this parameter is <tt>"-1"</tt>.
         * <em>NB: Setting the affinities of thread might require special
         * privileges or capabilities from the process.</em> Please consult the
         * relevant manuals for more information.
         */
        MAIN_CORE,

        /**
         * This parameter is used for setting the nice value of the Linux
         * process of the ADAPRO application. The value range is the string
         * representations of integers between -20 and 19 (inclusive). The
         * default value is <tt>"0"</tt>. For more information, see
         * <tt>man nice</tt>. Setting a negative nice value requires
         * <tt>CAP_SYS_NICE</tt> capability on Linux.
         */
        NICE,

        /**
         * This parameter is used for enabling or disabling serial command
         * propagation mode. In serial mode, Supervisor propagates commands to
         * workers serially, i.e. waiting for each worker to complete its state
         * transition before sending a command to the next worker. The strategy
         * for propagating the commands follows stack logic: The worker that was
         * started first, will be stopped last. This also applies to pausing and
         * resuming: The thread that was started first, will be paused last, but
         * resumed first. The default value is <tt>"TRUE"</tt>.
         */
        SERIALIZE_COMMANDS,

        /**
         * This parameter sets the affinity for the ADAPRO Supervisor Thread. If
         * the value is a negative integer (represented as a string), then no
         * affinity will be set. The range of this parameter is integers, though
         * the affinity can't be set if the value of this parameter is greater
         * than, or equal to the number of CPU cores available (since their
         * indexing starts from zero). The default value of this parameter is
         * <tt>"-1"</tt>. <em>NB: Setting the affinities of thread might require
         * special privileges or capabilities from the process.</em> Please
         * consult the relevant manuals for more information.
         */
        SUPERVISOR_CORE
    };
}

namespace Library
{
    using namespace ADAPRO::Data;

    /**
     * Checks with a regular expression, whether or not the given string
     * represents an integer quantity. Accepts any number of digits.
     *
     * @param s The string to be examined.
     * @return <tt>true</tt> if and only if the given string represents an
     * integer.
     */
    inline bool is_integer(const std::string& s) noexcept
    {
        return std::regex_match(s, std::regex{"^-?[0-9]+$"});
    }

    /**
     * Checks with a regular expression, whether or not the given string
     * represents an unsigneld integer quantity. Accepts any number of digits.
     *
     * @param s The string to be examined.
     * @return <tt>true</tt> if and only if the given string represents an
     * unsigned (i.e. non-negative) integer.
     */
    inline bool is_unsigned(const std::string& s) noexcept
    {
        return std::regex_match(s, std::regex{"^[0-9]+$"});
    }

    /**
     * Checks with a regular expression, whether or not the given string
     * represents an IPv4 address.
     *
     * @param s The string to be examined
     * @return <tt>true</tt> if and only if the given string represents an IPv4
     * address.
     */
    inline bool is_IPv4_address(const std::string& s) noexcept
    {
        return std::regex_match(s, std::regex{
            "^(25[0-4]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]|[0-9])"
            "(\\.(25[0-4]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]|[0-9])){3}$"
        });
    }

    /**
     * Checks with a regular expression, whether or not the given string
     * represents a hostname (in a format supported by ADAPRO).
     *
     * @param s The string to be examined
     * @return <tt>true</tt> if and only if the given string a hostname
     * supported by ADAPRO.
     */
    inline bool is_hostname(const std::string& s) noexcept
    {
        return std::regex_match(s, std::regex{"^(\\w|-|\\.)+$"});
    }

    /**
     * Checks with a regular expression, whether or not the given string
     * represents a DIM service name (in a format supported by ADAPRO).
     *
     * @param s The string to be examined
     * @return <tt>true</tt> if and only if the given string a DIM service name
     * supported by ADAPRO.
     */
    inline bool is_DIM_service_name(const std::string& s) noexcept
    {
        return std::regex_match(s, std::regex{"^(\\w|-|/|:|&)+$"});
    }

    /**
     * Checks with a regular expression, whether or not the given string
     * represents a DIM service description string.
     *
     * @param s The string to be examined
     * @return <tt>true</tt> if and only if the given string a DIM service
     * description string.
     * @see ADAPRO::DIM::Wrapper
     */
    inline bool is_DIM_service_description(const std::string& s) noexcept
    {
        return std::regex_match(s, std::regex{"^([CSILXFD]|"
            "[CSILXFD]:[1-9]\\d*(;[CSILXFD]:[1-9]\\d*)*(;[CSILXFD])?)$"});
    }

    /**
     * Checks with a regular expression, whether or not the given string
     * contains only word characters (<tt>^[a-zA-Z0-9_]$</tt>).
     *
     * @param s The string to be examined
     * @return <tt>true</tt> if and only if the given string is a word.
     */
    inline bool is_word(const std::string& s) noexcept
    {
        return std::regex_match(s, std::regex{"^(\\w)+$"});
    }

    /**
     * Checks with a regular expression, whether or not the given non-empty
     * string contains only whitespace characters (excluding non-breaking
     * spaces).
     *
     * @param s The string to be examined
     * @return <tt>true</tt> if and only if the given string is whitespace.
     */
    inline bool is_whitespace(const std::string& s) noexcept
    {
        return std::regex_match(s, std::regex{"[[:space:]]+"});
    }

    /**
     * Checks with a regular expression, whether or not the given string
     * is a Linux filename. May produce false negatives, but not false
     * positives.
     *
     * @param s The string to be examined
     * @return <tt>true</tt> if the given string is a Linux filename.
     */
    inline bool is_filename(const std::string& s) noexcept
    {
        return std::regex_match(s, std::regex{"^(.?.?/?(\\w|-)+)+$"});
    }

    /**
     * Checks with a regular expression, whether or not the given string
     * is a Linux directory name ending with a '/'. May produce false negatives,
     * but not false positives.
     *
     * @param s The string to be examined
     * @return <tt>true</tt> if the given string is a Linux directory name.
     */
    inline bool is_directory_name(const std::string& s) noexcept
    {
        return std::regex_match(s, std::regex{"^/?"
            "(\\.?(([^ \\s!$`&*()+\\.\\\\/]+|"
            "(\\\\(\\ |\\!|\\$|\\`|\\&|\\*|\\(|\\)|\\+)))"
            "(\\.([^ \\s!$`&*()+\\.\\\\/]+|"
            "(\\\\(\\ |\\!|\\$|\\`|\\&|\\*|\\(|\\)|\\+))))?|"
            "\\.)/)+$"});
    }

    /**
     * Checks whether or not the given string is an ADAPRO logging mask string
     * representation.
     *
     * @param s The string to be examined
     * @return <tt>true</tt> if the given string is an ADAPRO logging mask.
     */
    inline bool is_logging_mask(const std::string& s) noexcept
    {
        if (s.empty()) { return false; }
        bool    A_found{false}, M_found{false}, D_found{false}, I_found{false},
                S_found{false}, W_found{false}, E_found{false}, F_found{false};
        for (const char& c : s)
        {
            switch (c)
            {
                case 'A': if (!A_found) { A_found = true; continue; } else {return false;}
                case 'M': if (!M_found) { M_found = true; continue; } else {return false;}
                case 'D': if (!D_found) { D_found = true; continue; } else {return false;}
                case 'I': if (!I_found) { I_found = true; continue; } else {return false;}
                case 'S': if (!S_found) { S_found = true; continue; } else {return false;}
                case 'W': if (!W_found) { W_found = true; continue; } else {return false;}
                case 'E': if (!E_found) { E_found = true; continue; } else {return false;}
                case 'F': if (!F_found) { F_found = true; continue; } else {return false;}
                default: return false;
            }
        }
        return true;
    }

    /**
     * Returns <tt>true</tt> if and only if the argument equals to
     * <tt>"TRUE"</tt> or <tt>"FALSE"</tt> (case sensitive).
     */
    inline bool is_boolean(const std::string& s) noexcept
    {
        return s == "TRUE" || s == "FALSE";
    }

    /**
     * Converts a <tt>Parameter</tt> into its canonical string representation.
     *
     * @since   ADAPRO 5.0.0
     * @param parameter A parameter.
     * @return          The corresponding string.
     * @throws std::domain_error if given an invalid argument.
     * @see ADAPR::Data::Parameter
     */
    template<> inline std::string show(const Parameter parameter)
    {
        switch (parameter)
        {
            case COLOURS_ENABLED:       return "ADAPRO_COLOURS_ENABLED";
            case DAEMON_ENABLED:        return "ADAPRO_DAEMON_ENABLED";
            case DAEMON_LOGFILE:        return "ADAPRO_DAEMON_LOGFILE";
            case DIM_CALLBACK_CORE:     return "ADAPRO_DIM_CALLBACK_CORE";
            case DIM_CONTROLLER_CORE:   return "ADAPRO_DIM_CONTROLLER_CORE";
            case DIM_DNS_NODE:          return "ADAPRO_DIM_DNS_NODE";
            case DIM_DNS_PORT:          return "ADAPRO_DIM_DNS_PORT";
            case DIM_SERVER_ENABLED:    return "ADAPRO_DIM_SERVER_ENABLED";
            case DIM_SERVER_NAME:       return "ADAPRO_DIM_SERVER_NAME";
            case LOCK_MEMORY:           return "ADAPRO_LOCK_MEMORY";
            case LOGGING_MASK:          return "ADAPRO_LOGGING_MASK";
            case MAIN_CORE:             return "ADAPRO_MAIN_CORE";
            case NICE:                  return "ADAPRO_NICE";
            case SERIALIZE_COMMANDS:    return "ADAPRO_SERIALIZE_COMMANDS";
            case SUPERVISOR_CORE:       return "ADAPRO_SUPERVISOR_CORE";
            default: throw std::domain_error("Invalid Parameter.");
        }
    }

    /**
     * Converts a string to its corresponding <tt>Parameter</tt> value.
     *
     * @since   ADAPRO 5.0.0
     * @param s The string to be converted.
     * @return  The corresponding <tt>Parameter</tt> value.
     * @throws  std::domain_error if given a string which doesn't represent a
     * <tt>Parameter</tt> value.
     * @see     ADAPRO::Data::Parameter
     */
    template<> inline Parameter read(const std::string& s)
    {
             if (s == "ADAPRO_COLOURS_ENABLED"    ) return COLOURS_ENABLED;
        else if (s == "ADAPRO_DAEMON_ENABLED"     ) return DAEMON_ENABLED;
        else if (s == "ADAPRO_DAEMON_LOGFILE"     ) return DAEMON_LOGFILE;
        else if (s == "ADAPRO_DIM_CALLBACK_CORE"  ) return DIM_CALLBACK_CORE;
        else if (s == "ADAPRO_DIM_CONTROLLER_CORE") return DIM_CONTROLLER_CORE;
        else if (s == "ADAPRO_DIM_DNS_NODE"       ) return DIM_DNS_NODE;
        else if (s == "ADAPRO_DIM_DNS_PORT"       ) return DIM_DNS_PORT;
        else if (s == "ADAPRO_DIM_SERVER_ENABLED" ) return DIM_SERVER_ENABLED;
        else if (s == "ADAPRO_DIM_SERVER_NAME"    ) return DIM_SERVER_NAME;
        else if (s == "ADAPRO_LOCK_MEMORY"        ) return LOCK_MEMORY;
        else if (s == "ADAPRO_LOGGING_MASK"       ) return LOGGING_MASK;
        else if (s == "ADAPRO_MAIN_CORE"          ) return MAIN_CORE;
        else if (s == "ADAPRO_NICE"               ) return NICE;
        else if (s == "ADAPRO_SERIALIZE_COMMANDS" ) return SERIALIZE_COMMANDS;
        else if (s == "ADAPRO_SUPERVISOR_CORE"    ) return SUPERVISOR_CORE;
        else throw std::domain_error("Invalid Parameter.");
    }

    /**
     * Maps a <tt>Parameter</tt> to its default value.
     *
     * @since           ADAPRO 5.0.0
     * @param parameter The parameter.
     * @return          The default value of the given <tt>Parameter</tt>.
     * @throws std::domain_error if given an invalid argument.
     * @see ADAPR::Data::Parameter
     */
    inline std::string default_value(const Parameter parameter)
    {
        switch (parameter)
        {
            case COLOURS_ENABLED:       return "FALSE";
            case DAEMON_ENABLED:        return "FALSE";
            case DAEMON_LOGFILE:        return "/var/log/adapro.log";
            case DIM_CALLBACK_CORE:     return "-1";
            case DIM_CONTROLLER_CORE:   return "-1";
            case DIM_DNS_NODE:          return "localhost";
            case DIM_DNS_PORT:          return "2505";
            case DIM_SERVER_ENABLED:    return "TRUE";
            case DIM_SERVER_NAME:       return "ADAPRO_application";
            case LOCK_MEMORY:           return "FALSE";
            case LOGGING_MASK:          return "DEFIW";
            case MAIN_CORE:             return "-1";
            case NICE:                  return "0";
            case SERIALIZE_COMMANDS:    return "TRUE";
            case SUPERVISOR_CORE:       return "-1";
            default: throw std::domain_error("Invalid Parameter.");
        }
    }

    /**
     * The default relation used by ADAPRO to verify configuration entries. For
     * ADAPRO parameters, the appropriate sanity checks are performed. For other
     * entries, the default relation acts like the <em>Cartesian product</tt>,
     * accepting them as they are.
     */
    inline bool default_relation
    (
            const std::string& key,
            const std::string& value
    )
    noexcept
    {
        if (key.substr(0, 7) == "ADAPRO_")
        {
            const std::string k{key.substr(7)};
            if (k == "COLOURS_ENABLED")
            {
                return is_boolean(value);
            }
            else if (k == "DAEMON_ENABLED")
            {
                return is_boolean(value);
            }
            else if (k == "DAEMON_LOGFILE")
            {
                return is_filename(value);
            }
            else if (k == "DIM_CALLBACK_CORE")
            {
                return is_integer(value);
            }
            else if (k == "DIM_CONTROLLER_CORE")
            {
                return is_integer(value);
            }
            else if (k == "DIM_DNS_NODE")
            {
                return value.length() < 256 && is_hostname(value);
            }
            else if (k == "DIM_DNS_PORT")
            {
                if (is_unsigned(value))
                {
                    // cppcheck complains if I use brace initialization here:
                    const int v = std::stoi(value);
                    return 0 < v && v < 65536;
                }
                else
                {
                    return false;
                }
            }
            else if (k == "DIM_SERVER_ENABLED")
            {
                return is_boolean(value);
            }
            else if (k == "DIM_SERVER_NAME")
            {
                // 132 is the hard-coded maximum DIM service name length. (In
                // DIM server mode,) ADAPRO publishes the service suffixed with
                // "/COMMAND", so the name must then be at most 132 - 8 = 124
                // characters:
                return value.length() < 125 && is_hostname(value);
            }
            else if (k == "LOCK_MEMORY")
            {
                return is_boolean(value);
            }
            else if (k == "LOGGING_MASK")
            {
                return is_logging_mask(value);
            }
            else if (k == "NICE")
            {
                if (is_integer(value))
                {
                    // cppcheck complains if I use brace initialization here:
                    const int v = std::stoi(value);
                    return -20 <= v && v < 20;
                }
                else
                {
                    return false;
                }
            }
            else if (k == "MAIN_CORE")
            {
                return is_integer(value);
            }
            else if (k == "SERIALIZE_COMMANDS")
            {
                return is_boolean(value);
            }
            else if (k == "SUPERVISOR_CORE")
            {
                return is_integer(value);
            }
        }
        return true;
    };

    /**
     * Returns a map containing the default configuration of ADAPRO.
     */
    inline ADAPRO::Data::config_t make_default_configuration() noexcept
    {
        return config_t
        {
              {"ADAPRO_COLOURS_ENABLED"     , "FALSE"               }
            , {"ADAPRO_DAEMON_ENABLED"      , "FALSE"               }
            , {"ADAPRO_DAEMON_LOGFILE"      , "/var/log/adapro.log" }
            , {"ADAPRO_DIM_CALLBACK_CORE"   , "-1"                  }
            , {"ADAPRO_DIM_CONTROLLER_CORE" , "-1"                  }
            , {"ADAPRO_DIM_DNS_NODE"        , "localhost"           }
            , {"ADAPRO_DIM_DNS_PORT"        , "2505"                }
            , {"ADAPRO_DIM_SERVER_ENABLED"  , "TRUE"                }
            , {"ADAPRO_DIM_SERVER_NAME"     , "ADAPRO_application"  }
            , {"ADAPRO_LOCK_MEMORY"         , "FALSE"               }
            , {"ADAPRO_LOGGING_MASK"        , "DEFIW"               }
            , {"ADAPRO_MAIN_CORE"           , "-1"                  }
            , {"ADAPRO_NICE"                , "0"                   }
            , {"ADAPRO_SERIALIZE_COMMANDS"  , "TRUE"                }
            , {"ADAPRO_SUPERVISOR_CORE"     , "-1"                  }
        };
    }

    /**
     * The default signal callback. Instructs ADAPRO to initiate abnormal
     * shutdown sequence on any signal.
     */
    inline bool default_signal_callback(const uint8_t signal) noexcept
    {
        return false;
    };
}
}

#endif /* ADAPRO_PARAMETER_HPP */

