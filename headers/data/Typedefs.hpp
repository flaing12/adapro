/*
 * File:   Typedefs.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * This file contains the common typedefs used in this project.
 *
 * Created on 20 July 2015, 9:15
 */

#ifndef ADAPRO_DATA_TYPEDEFS_HPP
#define ADAPRO_DATA_TYPEDEFS_HPP

#include <cstdint>
#include <functional>
#include <memory>
#include <map>
#include <utility>
#include <string>
#include "../control/AlignedAllocator.hpp"
#include "../data/State.hpp"
#ifndef EXCLUDE_DIM
#include <dim/dim_common.h>
#include "../DIM/SubscriptionType.hpp"
#endif

namespace ADAPRO
{
namespace Control
{
#ifndef VERIFY
    class Logger;
#endif
    class Worker;
}
}

/**
 * This is the main namespace of the Alice Data Point Processing (ADAPRO)
 * application framework.
 */
namespace ADAPRO
{
/**
 * This namespace contains the data types of ADAPRO.
 */
namespace Data
{
    /**
     * Type of the transition callback used by <tt>ADAPRO::Control::Thread</tt>.
     * @see ADAPRO::Control::Thread
     */
    using trans_cb_t = std::function<void(const ADAPRO::Data::State)>;

    /**
     * Type of the configuration that Worker and some library procedures
     * accept. Configuration is essentially a key-value map of pairs of strings
     * (<tt>std::map&lt;std::string, std::string&gt;</tt>).
     */
    using config_t = std::map<std::string, std::string>;

    /**
     * The type of a relation used for sanity checking configuration entries.
     */
    using relation_t = std::function<bool(const std::string&, const std::string&)>;

    /**
     * The type of a signal callback, invoked by the ADAPRO signal handler.
     *
     * @see ADAPRO::Data::InitializationContext::signal_callback
     */
    using signal_callback_t = std::function<bool(const uint8_t)>;

    /**
     * Type of a worker factory, i.e. a function taking a reference to a
     * <tt>ADAPRO::Control::Logger</tt> and a const reference to a
     * <tt>config_t</tt> (that is
     * <tt>std::map&lt;std::string, std::string&gt;</tt>), and returning a
     * <tt>std::unique_ptr&lt;Worker&gt;</tt>
     *
     * @see ADAPRO::Control::Logger
     */
#ifndef VERIFY
    using worker_factory_t = std::function<std::unique_ptr<ADAPRO::Control::Worker>(
            ADAPRO::Control::Logger&, const config_t&)>;
#else
    using worker_factory_t = std::function<std::unique_ptr<ADAPRO::Control::Worker>(
            const config_t&)>;
#endif


    /**
     * Type of a function (pure or impure) from <tt>T</tt> to <tt>U</tt>.
     * <em> The type </em><tt>U</tt><em> must have a (non-deleted) default
     * constructor</em>.
     */
    template<typename T, typename U> using action_t = std::function<U(T)>;

    /**
     * Type of a predicate, i.e. a function from a value of type <tt>V</tt> to
     * bool.
     */
    template<typename V> using predicate_t = std::function<bool(const V&)>;
}
}

#endif /* ADAPRO_DATA_TYPEDEFS_HPP */

