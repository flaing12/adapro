
/*
 * File:   GenericFunctions.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 9 June 2017, 17:29
 */

#ifndef ADAPRO_GENERIC_FUNCTIONS_HPP
#define ADAPRO_GENERIC_FUNCTIONS_HPP

#include <string>

namespace ADAPRO
{
namespace Library
{
    /**
     * This function template is used for converting ADAPRO enumerated values
     * into strings in a fashion similar to the function show in the Show type
     * class in Haskell. The exact implementation depends on the specialization.
     *
     * @param input A T value to be converted.
     * @return      A string representation of the given value.
     * @throws std::domain_error The specialized function may throw a domain
     * error if applied with an invalid input value.
     */
    template<typename T> std::string show(const T input);

    /**
     * Returns a single-character or abbreviated string representing the given
     * value. Used for representing logging masks, states, and commands as
     * strings.
     *
     * @param input A T value to be converted.
     * @return      A string representation of the given value.
     * @throws std::domain_error The specialized function may throw a domain
     * error if applied with an invalid input value.
     * @since ADAPRO 5.0.0
     * @see ADAPRO::Data::LoggingMask
     * @see ADAPRO::Data::State
     * @see ADAPRO::Data::Command
     */
    template<typename T> std::string symbol(const T input);

    /**
     * This function template is used for parsing strings as ADAPRO enumerated
     * values. The exact implementation depends on the specialization.
     *
     * @param input A string to be interpreted as a T value.
     * @return      The T value corresponding with the input.
     * @throws std::domain_error The specialized function may throw a domain
     * error if the given string couldn't be converted into a T value.
     */
    template<typename T> T read(const std::string& input);
}
}

#endif /* ADAPRO_GENERIC_FUNCTIONS_HPP */

