/*
 * File:   ThreadGuard.hpp
 * Author: John Lång
 *
 * Created on 26 January 2019, 18:10
 */

#ifndef ADAPRO_THREAD_GUARD_HPP
#define ADAPRO_THREAD_GUARD_HPP

#include <string>
#include <functional>
#include "Thread.hpp"
#include "../data/State.hpp"
#include "../data/Typedefs.hpp"

namespace ADAPRO
{
namespace Control
{
#ifndef VERIFY
    class Logger;
#endif

    template<typename T>
    class ThreadGuard final : public T, public Thread
    {
        ThreadGuard() noexcept = delete;

    protected:
        virtual void prepare() override { this->T::prepare(); }
        virtual void execute() override { this->T::execute(); }
        virtual void finish() override { this->T::finish(); }

    public:
        template< typename... Args > ThreadGuard
        (
#ifndef VERIFY
                Logger& logger,
#endif
                std::string&& name
                , const ADAPRO::Data::trans_cb_t transition_callback
                , const int preferred_core
                , Args&&... args
        )
        noexcept:
                Thread
                (
#ifndef VERIFY
                        logger,
#endif
                        std::move(name),
                        transition_callback,
                        preferred_core
                ),
                T(std::forward< Args >( args )...)
        {}

        ~ThreadGuard() noexcept { this->Thread::join(); }
    };
}
}

#endif /* ADAPRO_THREAD_GUARD_HPP */

