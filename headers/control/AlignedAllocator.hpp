/*
 * File:   AlignedAllocator.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 22 February 2017, 12:41
 */

#ifndef ADAPRO_ALIGNED_ALLOCATOR_HPP
#define ADAPRO_ALIGNED_ALLOCATOR_HPP

#include <cstddef>
#include <cstdlib>
#include <type_traits>

namespace ADAPRO
{
namespace Control
{
    /**
     * <p>AlignedAllocator is a stateless allocator that allocates objects
     * honouring (over)alignment. It is intended, for performance reasons, to be
     * used with STL storing the following ADAPRO types:</p>
     * <ul>
     *     <li><tt>ADAPRO::ADAPOS::DataPointIdentifier</tt></li>
     *     <li><tt>ADAPRO::ADAPOS::DataPointValue</tt></li>
     *     <li><tt>ADAPRO::ADAPOS::DataPointCompositeObject</tt></li>
     *     <li><tt>ADAPRO::ADAPOS::DataPointEventRecord</tt></li>
     * </ul>
     */
    template<typename T> struct AlignedAllocator
    {
        using value_type = T;

        AlignedAllocator() noexcept {}

        template <class U> AlignedAllocator(const AlignedAllocator<U>& other)
        noexcept {}

        inline T* allocate(const size_t count) const noexcept
        {
            return (T*) aligned_alloc(alignof(T), count * sizeof(T));
        }

        inline void deallocate(T* begin, const size_t count) const noexcept
        {
            return free(begin);
        }
    };

    template <typename T, typename U>
    inline bool operator==(const AlignedAllocator<T>&, const AlignedAllocator<U>&)
    {
        return std::is_same<T,U>::value;
    }

    template <typename T, typename U>
    inline bool operator!=(const AlignedAllocator<T>&, const AlignedAllocator<U>&)
    {
        return !(std::is_same<T,U>::value);
    }
}
}

#endif /* ADAPRO_ALIGNED_ALLOCATOR_HPP */

