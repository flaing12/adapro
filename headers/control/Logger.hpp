
/*
 * File:   Logger.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 30 August 2016, 15:19
 */

#ifndef ADAPRO_LOGGER_HPP
#define ADAPRO_LOGGER_HPP

#include <cstdint>
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <mutex>
#include <memory>
#include "../library/ConfigurationUtils.hpp"
#include "../data/LoggingLevel.hpp"
#include "../data/Parameters.hpp"
#include "../data/Typedefs.hpp"

class LoggerTest;

namespace ADAPRO
{

namespace Library
{
    const std::string& query
    (
            const ADAPRO::Data::config_t& configuration,
            const ADAPRO::Data::Parameter parameter
    );
}
namespace Control
{
    class Thread;

    /**
     * Logger is the ADAPRO class responsible for printing formatted output in a
     * <tt>std::ostrem</tt>. The output stream can be <tt>std::cout</tt> or a
     * <tt>std::ofstream</tt>. <em>Logger doesn't perform any error checking,
     * other than ensuring that the output stream is open. </em> If a
     * <tt>std::ofstream</tt> cannot be opened (e.g. due to lacking file system
     * access rights), then Logger reverts to <tt>std::cout</tt>. If the output
     * is directed to a file stream, it will be closed when <tt>~Logger</tt> is
     * executed.
     */
    class Logger final
    {
        friend LoggerTest;
        friend Thread;

        /**
         * An empty string that can be used as the output path in the
         * constructor of Logger.
         */
        static const std::string EMPTY_STRING;

        /**
         * <tt>"\e[0m"</tt>. Used for resetting the output colour to default.
         */
        static const std::string COLOUR_ZERO;

        /**
         * <tt>"\e[0;30m"</tt>. Used for printing coloured messages.
         */
        static const std::string COLOUR_GRAY;

        /**
         * <tt>"\e[0;31m"</tt>. Used for printing coloured messages.
         */
        static const std::string COLOUR_RED;

        /**
         * <tt>"\e[0;32m"</tt>. Used for printing coloured messages.
         */
        static const std::string COLOUR_GREEN;

        /**
         * <tt>"\e[0;33m"</tt>. Used for printing coloured messages.
         */
        static const std::string COLOUR_YELLOW;

        /**
         * <tt>"\e[0;34m"</tt>. Used for printing coloured messages.
         */
        static const std::string COLOUR_BLUE;

        /**
         * Gets the colour code associated with the given <tt>LoggingLevel</tt>,
         * if coloured messages were enabled for this <tt>Logger</tt>.
         * Otherwise, returns an empty string.
         *
         * @param logging_level The <tt>LoggingLevel</tt> instance whose colour
         * is to be queried.
         * @return The appropriate colour code.
         */
        inline const std::string& get_colour
        (
                const ADAPRO::Data::LoggingLevel logging_level
        )
        noexcept
        {
            if (!use_colours) { return EMPTY_STRING; }
            switch (logging_level)
            {
                case ADAPRO::Data::ADAPRO_DEBUG:
                case ADAPRO::Data::USER_DEBUG:
                    return COLOUR_GRAY;
                case ADAPRO::Data::FSM:
                    return COLOUR_BLUE;
                case ADAPRO::Data::WARNING:
                    return COLOUR_YELLOW;
                case ADAPRO::Data::ERROR:
                case ADAPRO::Data::FATAL:
                    return COLOUR_RED;
                case ADAPRO::Data::SPECIAL:
                    return COLOUR_GREEN;
                default:
                    return EMPTY_STRING;
            }
        }

        /**
         * If this Logger was configured to use coloured output, a colour reset
         * code will be returned. Otherwise, returns an empty string.
         *
         * @return A string appropriate for the situation
         */
        inline const std::string& reset_colour() noexcept
        {
            return use_colours ? COLOUR_ZERO : EMPTY_STRING;
        }

        /**
         * The stream used for constructing Logger in daemon mode. In normal
         * operation, this field is unused.
         */
        std::ofstream file_stream; // TODO: Add a flush method.

        /**
         * The output stream where Logger prints all messages.
         */
        std::ostream& output_stream;

        /**
         * A mutex object used for guarding output stream access.
         */
        std::mutex output_mutex;

        /**
         * This bitmask is used for filtering messages for output. Only messages
         * with their logging level contained in this bitmask, will be printed.
         */
        const uint8_t logging_mask;

        /**
         * If this value is set to <tt>true</tt>, then CSI SGR colour codes will
         * be used to emphasize the logging levels of the printed messages.
         */
        const bool use_colours;
        /**
         * <p>Prints the given message with the given logging level (if equal to
         * or higher than Logger's logging level), and the given Thread as the
         * origin, to the output stream. The output format is:</p>
         * <tt>
         * &lt;log-entry&gt;   ::= &lt;timestamp&gt; " " &lt;level&gt; " "
         *                         &lt;thread-info&gt; " " &lt;message&gt; "\\n"\n
         * &lt;thread-info&gt; ::= "&lt;" &lt;name&gt; "," &lt;state&gt; ","
         *                         &lt;command&gt; &lt;core&gt; "&gt;"\n
         * &lt;core&gt;        ::= "," &lt;sched_getcpu&gt;
         * </tt>
         * <p>where <tt>&lt;timestamp&gt;</tt> is the return value of
         * <tt>ADAPRO::Library::Clock::timestamp</tt>, level is the return value
         * of <tt>ADAPRO::Data::LoggingLevel::symbol</tt> applied with the
         * given LoggingLevel, and <tt>&lt;message&gt;</tt> is the given
         * message, <tt>&lt;name</tt> is the name of the given Thread,
         * <tt>&lt;state&gt;</tt> is the Thread's current state,
         * <tt>&lt;command&gt;</tt> is the Thread's current command, and
         * if <tt>&lt;sched_getcpu&gt;</tt> is a string containing the output of
         * the function <tt>sched_getcpu</tt> on GNU-compliant systems (where C
         * preprocessor has defined <tt>GNU_SOURCE</tt>. For example, this
         * procedure might print a line like
         * <tt>"[2017-05-18 14:23:45] (I) &lt;HelloWorldThread,STARTING,CONTINUE,3&gt; Hello,_World!"</tt>
         * when applied with a HelloWorldThread instance (see the ADAPRO example
         * application), <tt>"Hello, World!"</tt>, and
         * <tt>ADAPRO::Data::LoggingLevel::INFO</tt>.</p>
         *
         * @param origin    A Thread whose name, command, state, and CPU core
         * number will be printed as the origin of the message. If the pointer
         * is <tt>nullptr</tt>, then the origin part of the message is omitted.
         * @param message   The message to be printed to the output stream.
         * @param severity  Logging level of the message.
         * @see ADAPRO::Data::LoggingLevel
         * @see ADAPRO::Library::Clock::timestamp
         * @see ADAPRO::Data::LoggingLevel::symbol
         * @see ADAPRO::Control::Thread
         * @see ADAPRO::Data::State
         * @see ADAPRO::Data::Command
         */
        void print
        (
                const ADAPRO::Control::Thread& origin,
                const std::string& message,
                const ADAPRO::Data::LoggingLevel severity =
                        ADAPRO::Data::LoggingLevel::USER_DEBUG
        )
        noexcept;

    public:

        /**
         * Makes a new Logger instance with the given configuration. If the
         * configuration entry <tt>DAEMON_ENABLED</tt> is set to <tt>"TRUE</tt>,
         * then the logger will write output into a log file specified in the
         * configuration. Otherwise, the output will go to <tt>stdout</tt> and
         * <tt>stderr</tt> streams.
         *
         * @param configuration The configuration.
         * @return              A logger.
         * @throws std::out_of_range if one of the following mandatory
         * configuration entries were undefined: <tt>COLOURS_ENABLED</tt>,
         * <tt>LOGGING_MASK</tt>, <tt>DAEMON_ENABLED</tt>, and if the value of
         * <tt>DAEMON_ENABLED</tt> was <tt>"TRUE"</tt> then also
         * <tt>DAEMON_LOGFILE</tt>.
         */
        #ifdef __divine__
            __skipcfl
        #endif
        inline static Logger make_logger
        (
                const ADAPRO::Data::config_t& configuration
        )
        {
            const uint8_t logging_mask{ADAPRO::Library::logging_mask(
                    ADAPRO::Library::query(configuration, ADAPRO::Data::Parameter::LOGGING_MASK))};
            return ADAPRO::Library::query(configuration, ADAPRO::Data::Parameter::DAEMON_ENABLED) == "TRUE"
                    ? Logger(
                            logging_mask
                            , ADAPRO::Library::query(configuration, ADAPRO::Data::Parameter::DAEMON_LOGFILE)
                            , (ADAPRO::Library::query(configuration, ADAPRO::Data::Parameter::COLOURS_ENABLED) == "TRUE")
                    )
                    : Logger(
                            logging_mask
                            , EMPTY_STRING
                            , (ADAPRO::Library::query(configuration, ADAPRO::Data::Parameter::COLOURS_ENABLED) == "TRUE")
                    );
        };

        /**
         * Constructor for Logger. Uses the given <tt>LoggingLevel</tt> mask and
         * output file.
         *
         * @param logging_mask  A bitmask of accepted <tt>LoggingLevel</tt>.
         * Only messages having <tt>LoggingLevel</tt> values contained in this
         * mask will be printed. By default, all <tt>LoggingLevel</tt>s, except
         * for <tt>ADAPRO_DEBUG</tt> will be accepted.
         * @param output_file   The output file. If this string is empty or
         * refers to an inaccessible path, then<tt>std::cout</tt> will be used
         * for output instead of a file stream.
         * @param use_colours   Added in ADAPRO 5.0.0. The logger instance will
         * add CSI SGR colour codes to messages to emphasize their logging level
         * if and only if this argument is <tt>true</tt>.
         *
         * @see ADAPRO::Data::LoggingLevel
         */
        Logger
        (
                const uint8_t logging_mask = ADAPRO::Library::logging_mask(
                        ADAPRO::Library::default_value(
                        ADAPRO::Data::Parameter::LOGGING_MASK))
                , const std::string& output_file = EMPTY_STRING
                , const bool use_colours = false
        )
        noexcept:
                file_stream(output_file)
                , output_stream(file_stream.is_open() ? file_stream : std::cout)
                , output_mutex()
                , logging_mask(logging_mask)
                , use_colours(use_colours)
        {}

        /**
         * A move constructor. Moves the file stream from the other
         * <tt>Logger</tt>.
         *
         * @param other The <tt>Logger</tt> whose streams are to be moved.
         */
        Logger(Logger&& other) noexcept:
                file_stream(std::move(other.file_stream))
                , output_stream(other.output_stream)
                , output_mutex()
                , logging_mask(other.logging_mask)
                , use_colours(other.use_colours)
        {}

        /**
         * <p>Prints the given message with the given logging level (if equal to
         * or higher than Logger's logging level) to the output stream. The
         * output format is:</p>
         * <tt>
         * &lt;log-entry&gt; ::= &lt;timestamp&gt; " " &lt;level&gt; " "
         *                       &lt;message&gt; "\\n"
         * </tt>
         * <p>where <tt>&lt;timestamp&gt;</tt> is the return value of
         * <tt>ADAPRO::Library::Clock::timestamp</tt>, level is the return value
         * of <tt>ADAPRO::Data::LoggingLevel::symbol</tt> applied with the
         * given LoggingLevel, and <tt>&lt;message&gt;</tt> is the given
         * message. For example, this procedure might print a line like
         * <tt>"[2017-05-18 14:18:24] (I) Hello, World!"</tt> when applied with
         * <tt>"Hello, World!"</tt> and
         * <tt>ADAPRO::Data::LoggingLevel::INFO</tt>.</p>
         *
         * @param message   The message to be printed to the output stream.
         * @param severity  Logging level of the message.
         * @see ADAPRO::Data::LoggingLevel
         * @see ADAPRO::Library::Clock::timestamp
         * @see ADAPRO::Data::LoggingLevel::symbol
         */
        void print
        (
                const std::string& message,
                const ADAPRO::Data::LoggingLevel severity =
                        ADAPRO::Data::LoggingLevel::USER_DEBUG
        )
        noexcept;

        /**
         * The trivial virtual destructor of Logger, which implicitly closes the
         * output stream.
         */
        ~Logger() noexcept {};
    };
}
}

#endif /* ADAPRO_LOGGER_HPP */