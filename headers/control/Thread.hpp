/*
 * File:   Thread.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 23 July 2015, 17:25
 */

#ifndef ADAPRO_THREAD_HPP
#define	ADAPRO_THREAD_HPP

#include <stdint.h>
#include <thread>
#include <memory>
#include <functional>
#include <mutex>
#include <chrono>
#include <condition_variable>
#include <atomic>
#include <string>
#include <list>
#include <stdexcept>
#ifndef VERIFY
#include "../control/Logger.hpp"
#endif
#include "../data/LoggingLevel.hpp"
#include "../data/Command.hpp"
#include "../data/Typedefs.hpp"

using namespace std;

class ThreadTest;
class SupervisorTest;

namespace ADAPRO
{
namespace Data
{
    enum State;
}

namespace Control
{
    class Session;
    class Supervisor;

    /**
     * <p>
     * Thread is a finite state machine executing independently and
     * asynchronously a predefined task of indeterminate duration. For a simple
     * one-time execution of arbitrary code, consider using standard library
     * threads instead. The behaviour of the Thread depends on its task (i.e.
     * implementing class), state, and command. A Thread must not throw
     * exceptions, but abort execution instead and set the state to
     * <tt>ABORTED</tt> should an unrecoverable runtime error occur.
     * </p>
     * <p>
     * The life cycle of a Thread in terms of states typically goes as follows:
     * <tt>READY</tt>, <tt>STARTING</tt>, <tt>RUNNING</tt>, <tt>STOPPING</tt>
     * (<tt>ABORTING</tt>), <tt>STOPPED</tt> (<tt>ABORTED</tt>). Immediately
     * after construction, the Thread is in the state <tt>READY</tt>, which
     * means it's waiting for the command <tt>START</tt>. Invoking the method
     * <tt>start</tt> sends the command <tt>START</tt> and sets the state to
     * <tt>STARTING</tt>. In this state, the Thread is executing some
     * preliminary actions (like memory allocation) that need to be done before
     * starting the actual computational task. These preliminary actions are
     * defined in the method <tt>prepare</tt>. When the Thread is executing its
     * computational task (the actual algorithm producing the desired side
     * effects on memory), it's in the state <tt>RUNNING</tt> with command set
     * to <tt>CONTINUE</tt>. This computational task is defined in the method
     * <tt>execute</tt>. The execution (i.e. invoking <tt>execute</tt>)
     * continues in a loop for as long as the command remains unchanged. When
     * the command is set to <tt>STOP</tt> by invoking the method stop, the
     * Thread begins a shutdown sequence which in state <tt>STOPPING</tt>. When
     * all of the computation has ended, the Thread is in state
     * <tt>STOPPED</tt> (or <tt>ABORTED</tt>). At this point, the Thread is
     * ready to be deleted.
     * </p>
     * <p>
     * Any state, other than <tt>READY</tt>, <tt>STOPPED</tt>, and
     * <tt>ABORTED</tt>, can also be aborted by a runtime exception or call to
     * the <tt>abort</tt> by another thread, or <tt>trap</tt> from inside one of
     * the virtual hook methods (<tt>prepare</tt>, <tt>execute</tt>, and
     * <tt>finish</tt> of the Thread itself. When the abortion sequence begins,
     * Thread sets it state to <tt>ABORTING</tt> and prints an error message,
     * after which it halts in the state <tt>ABORTED</tt>.
     * </p>
     * <p>
     * <em>Known issue: The destructors for <tt>Thread</tt> or any of its
     * inheriting classes are only safe to call when the Thread is in one of the
     * states <tt>READY</tt>, <tt>STOPPED</tt>, or <tt>ABORTED</tt>.</em> In
     * other cases, a race between the destructor and the backend thread might
     * occur. Because the inheriting class' destructor will be called before
     * <tt>~Thread()</tt>, this issue cannot be prevented by the <tt>Thread</tt>
     * class. <em>However, this problem can be avoided by calling <tt>join</tt>
     * in the destructor of the final inheriting class.</em> Another option is
     * to use <tt>ADAPRO::Control::ThreadGuard</tt>.
     * </p>
     * <p>
     * <em>The framework user should not create Threads before the ADAPRO
     * session has been started.</em> It is recommended to use
     * <tt>ADAPRO::Control::Worker</tt> as the basis of user-defined Threads,
     * and register Worker instances to the framework via Worker factories given
     * to <tt>ADAPRO::Data::Context</tt>. A Worker registered to the framework
     * can safely create more Threads. In this case, it's recommended to use
     * <tt>STATE_PROPAGATION_CB</tt> to create a transition callback to
     * propagate state transitions to the Threads owned by the Worker.
     * </p>
     * <p>
     * For more information on Thread, please consult the Technical
     * Documentation.
     * </p>
     * @see ADAPRO::Control::Thread::join
     * @see ADAPRO::Control::Thread::STATE_PROPAGATION_CB
     * @see ADAPRO::Control::Worker
     * @see ADAPRO::Control::Session
     * @see ADAPRO::Control::ThreadGuard
     * @see ADAPRO::Data::Context
     */
    class Thread
    {
        friend ThreadTest;
        friend Session;
        friend Supervisor;
        friend SupervisorTest;

#ifndef VERIFY
        /**
         * Reference to the <tt>ADAPRO::Control::Logger</tt> instance used in
         * the current ADAPRO session. It is valid during the whole lifetime of
         * the Thread.
         */
        Logger& logger;
#endif

        /**
         * The mutex object used for ensuring mutual exclusion to <tt>state</tt>
         * and <tt>mutex</tt> during transitions and setters.
         */
        mutex transition_mutex;

        /**
         * The internal condition variable of the Thread used for barrier
         * synchronization on the valued of <tt>state</tt>.
         */
        condition_variable state_semaphore;

        /**
         * The state of the Thread.
         */
        atomic<ADAPRO::Data::State> state;

        /**
         * The command of the Thread.
         */
        atomic<ADAPRO::Data::Command> command;

        /**
         * This callback is executed every time the Thread enters into a new
         * state. It <emph>must not</emph> fail when called with
         * <tt>ADAPRO::Data::State::ABORTING</tt> or
         * <tt>ADAPRO::Data::State::ABORTED</tt>. <em>This callback must not
         * trigger another transition.</em> OTherwise, a deadlock will occur.
         */
        const ADAPRO::Data::trans_cb_t transition_callback;

#ifndef VERIFY
        /**
         * This field is used for a watchdog mechanism for the Worker class. For
         * technical reasons, it's included in Thread class, even if it's
         * redundant for non-Worker Threads. The value is <tt>true</tt> if and
         * only if the Worker is alive. This value will be set to <tt>false</tt>
         * periodically by the Supervisor.
         *
         * @see ADAPRO::Control::Suprevisor
         */
        atomic_bool responsive;
#endif

        /**
         * Unique pointer to the <tt>std::thread</tt> backend object.
         */
        unique_ptr<thread> backend_ptr;

#if defined _GNU_SOURCE && !defined(VERIFY)
        /**
         * Sets the name and affinity for the Thread.
         */
        void set_name_and_affinity() noexcept;
#endif

        /**
         * This function is passed as a callback to the std::thread constructor.
         * It carries out the internal Thread control logic.
         */
        void run() noexcept;

#ifdef __linux__
        /**
         * Cancels the backend POSIX thread.
         */
        void cancel() noexcept;
#endif

        /**
         * Thread's internal setter for state.
         *
         * @param new_state     The new state value.
         */
        void set_state(const ADAPRO::Data::State new_state) noexcept;

        /**
         * <tt>Thread</tt>'s internal setter for command. If permitted by the
         * FSM model (see manual), overwrites the <tt>Thread</tt>'s current
         * <tt>Command</tt> with the given value and returns <tt>true</tt>.
         * Otherwise, the current <tt>Command</tt> remains unchanged and the
         * return value will be <tt>false</tt>.
         *
         * @param new_command   The new command value.
         * @return <tt>true</tt> if and only if changing the <tt>Command</tt>
         * succeeded.
         */
        bool set_command(const ADAPRO::Data::Command new_command) noexcept;

        /**
         * Performs a <em>covariant</em> state transition. In a covariant state
         * transition, the state changes <emph>before</emph> <tt>transition</tt>
         * will be invoked.
         *
         * @param message   A message to be printed with the <tt>FSM</tt>
         * logging level.
         * @param state     The state after the transition.
         * @since           Introduced in ADAPRO 5.0.0.
         */
        inline void covariant_transition(
            const string& message, const ADAPRO::Data::State& state) noexcept
        {
            set_state(state);
            print(message, ADAPRO::Data::LoggingLevel::FSM);
            transition_callback(state);
        }

        /**
         * Performs a <em>contravariant</em> state transition. In a
         * contravariant state transition, the state changes <emph>after</emph>
         * <tt>transition</tt> has been invoked.
         *
         * @param message   A message to be printed with the <tt>FSM</tt>
         * logging level.
         * @param state     The state after the transition.
         * @since           Introduced in ADAPRO 5.0.0.
         */
        inline void contravariant_transition(
            const string& message, const ADAPRO::Data::State& state) noexcept
        {
            transition_callback(state);
            set_state(state);
            print(message, ADAPRO::Data::LoggingLevel::FSM);
        }

        /**
         * The exception handler of <tt>Thread</tt>. Prints an error message and
         * halts.
         *
         * @param message An error message as a null-terminated <tt>char</tt>
         * array.
         */
        void handle_exception(const char* message) noexcept;

    protected:

        /**
         * Returns a transition callback that propagates the state of the Thread
         * to the subordinate Threads synchronously. <em>The callback function
         * assumes that the subordinates and the calling Thread have all
         * synchronized states and does no state checking. The behaviour of the
         * callback in case of any error is undefined.</em>
         *
         * @param subordinates  The subordinate Threads.
         * @return              A transition callback.
         */
        static ADAPRO::Data::trans_cb_t STATE_PROPAGATION_CB(
                std::list<Thread*>&& subordinates) noexcept;

        /**
         * Constructor for Thread.
         *
         * @param logger                The logger used for the ADAPRO Session
         * that this Thread will be part of.
         * @param name              The name (identifier) of the Thread.
         * @param preferred_core    <p>Number of the preferred CPU core.</p>
         * <p>If set to a non-negative value that is less than the number of
         * available CPU cores, then Thread attempts to set its affinity to this
         * core number when starting. Also, in this case Thread attempts to set
         * its scheduling policy to FIFO with maximum priority to ensure maximal
         * CPU time.</p>
         * <p>
         * If the value of this parameter is outside the range specified above,
         * then the Thread object gets the standard treatment from the Operating
         * System.
         * </p>
         */
        [[deprecated("Please use the other constructor instead.")]]
        Thread
        (
#ifndef VERIFY
                Logger& logger,
#endif
                string&& name
                , const int preferred_core = -1
        )
        noexcept;

        /**
         * Constructor for Thread.
         *
         * @param logger                The logger used for the ADAPRO Session
         * that this Thread will be part of.
         * @param name                  The name (identifier) of the Thread.
         * @param transition_callback   A callback to be executed every time the
         * state of the Thread changes. The argument of the callback will be the
         * new state of the Thread. A <tt>nofail</tt> guarantee must be provided
         * with arguments <tt>ABORTING</tt> or <tt>ABORTED</tt>.
         * @param preferred_core        <p>Number of the preferred CPU core.</p>
         * <p>If set to a non-negative value that is less than the number of
         * available CPU cores, then Thread attempts to set its affinity to this
         * core number when starting. Also, in this case Thread attempts to set
         * its scheduling policy to FIFO with maximum priority to ensure maximal
         * CPU time.</p>
         * <p>
         * If the value of this parameter is outside the range specified above,
         * then the Thread object gets the standard treatment from the Operating
         * System.
         * @since 0.7.0
         */
        Thread
        (
#ifndef VERIFY
                Logger& logger,
#endif
                string&& name
                , const ADAPRO::Data::trans_cb_t transition_callback
                , const int preferred_core = -1
        )
        noexcept;

        /**
         * <p>Prints the given message with the given logging level, if it is
         * greater than or equal to the configured logging level of the ADAPRO
         * session. The Logger instance of the ADAPRO session is used for
         * output.</p>
         * <p><em>This function is meant to be used only from inside the Thread,
         * that is, from a method, whose call stack includes <tt>prepare</tt>,
         * <tt>execute</tt>, <tt>finish</tt>, or <tt>handle_exception</tt>.</em>
         * The purpose of this arrangment is to have the Thread shown as the
         * origin of the message, to correspond with the actual runtime thread
         * calling this method. This semantic condition cannot be enforced by
         * the C++ type system, so it's on user's responsibility to honour it.
         * </p>
         *
         * @param message   The message.
         * @param severity  The logging level.
         * @see ADAPRO::Control::Logger
         */
        inline void print
        (
                const string& message,
                ADAPRO::Data::LoggingLevel severity =
                        ADAPRO::Data::LoggingLevel::USER_DEBUG
        )
        noexcept
        {
#ifndef VERIFY
            logger.print(*this, message, severity);
#endif
        }

        /**
         * Performs the required preparations before starting to execute the
         * main task of the Thread.
         */
        virtual void prepare() = 0;

        /**
         * Performs one computational step. This method will be called
         * repeatedly as long as the command of the Thread remains CONTINUE.
         *
         * @see Thread::abort
         */
        virtual void execute() = 0;

        /**
         * Executes the required actions after the execution of the main task
         * has ceased. <em>This method is allowed to fail by throwing an
         * exception, but must not attempt to trigger an FSM state transition
         * otherwise.</em> This method will not be called if the <tt>Thread</tt>
         * aborts during <tt>prepare</tt> or <tt>execute</tt>.
         */
        virtual void finish() = 0;

        /**
         * Blocks until the backend thread has halted. If this <tt>Thread</tt>
         * was not in one of the states <tt>STOPPING</tt>, <tt>STOPPED</tt>,
         * <tt>ABORTING</tt>, or <tt>ABORTED</tt>, an ADAPRO debug message will
         * be printed and this <tt>Thread</tt> will be stopped. <em>This methods
         * is meant to be called from a destructor of an inheriting class only,
         * to ensure that the background thread stops before the objects owned
         * by this <tt>Thread</tt> are deallocated.</em>
         *
         * @since ADAPRO 5.0.0
         */
        void join() noexcept;

    public:

        /**
         * Name of the Thread. Used for logging.
         */
        const string name;

        /**
         * Number of the preferred CPU core. If this number is between zero
         * (inclusive) and the number of available CPU cores (exclusive), Thread
         * attempts to set its affinity to this core when starting. Also, the
         * scheduling policy for the Thread will be set to FIFO for ensuring
         * maximal CPU time.
         */
        const int preferred_core;

        /**
         * The deleted default constructor.
         */
        Thread() noexcept = delete;

        /**
         * This blocking method returns control to the caller once the Thread
         * has entered to the given state or a later state. Note that there's no
         * guarantee that this will ever happen.
         *
         * @deprecated  As of ADAPRO 5.0.0, this method has been declared
         * deprecated, since it is prone to subtle unexpected behaviour in some
         * corner cases. Please consider using <tt>wait_for_state_mask</tt>
         * instead.
         */
        [[deprecated("Please consider using wait_for_state_mask instead.")]]
        void wait_for_state(const ADAPRO::Data::State target_state) noexcept;

        /**
         * This blocking method returns control to the caller once the Thread
         * has entered to the given state or a later state. If the Thread fails
         * to do so before the timeout is reached, an exception is thrown.
         *
         * @param timeout Maximum time for blocking.
         * @throws std::runtime_error If the Thread failed to perform the
         * desired state transition before timeout.
         * @deprecated  As of ADAPRO 5.0.0, this method has been declared
         * deprecated, since it is prone to subtle unexpected behaviour in some
         * corner cases. Please consider using <tt>wait_for_state_mask</tt>
         * instead.
         */
        [[deprecated("Please consider using wait_for_state_mask instead.")]]
        void wait_for_state(const ADAPRO::Data::State target_state,
                const chrono::milliseconds timeout);

        /**
         * Like <tt>wait_for_state</tt> for a single state, but accepts a state
         * mask. This method returns control to the caller only after the Thread
         * has entered a state <tt>s</tt> for which <tt>state_in(s)</tt> returns
         * <tt>true</tt> (which might never happen).
         *
         * @param state_mask    A bit mask representing a selection of
         * <tt>ADAPRO::Data::State</tt> values. When (or if) the Thread enters
         * one of these states, then this method returns control to the caller.
         *
         * @see ADAPRO::Data::State
         * @see ADAPRO::Control::Thread::state_in
         */
        void wait_for_state_mask(const uint8_t state_mask) noexcept;

        /**
         * Like <tt>wait_for_state</tt> for a single state, but accepts a state
         * mask. This method returns control to the caller only after the Thread
         * has entered a state <tt>s</tt> for which <tt>state_in(s)</tt> returns
         * <tt>true</tt> (which might never happen). This method also has a
         * timeout parameter for guarding against infinite waiting.
         *
         * @param state_mask    A bit mask representin a selection of
         * <tt>ADAPRO::Data::State</tt> values. When (or if) the Thread enters
         * one of these states, then this method returns control to the caller.
         * @param timeout       A timeout period, after which an exception will
         * be thrown if the Thread hasn't moved to any of the masked states.
         * @throws std::runtime_error If the Thread failed to perform the
         * desired state transition before timeout.
         */
        void wait_for_state_mask(const uint8_t state_mask,
                const chrono::milliseconds timeout);

        /**
         * Returns the current state of the Thread.
         *
         * @return A state.
         * @see State.hpp
         */
        inline ADAPRO::Data::State get_state() const noexcept
        {
            return state.load(memory_order_consume);
        }

        /**
         * Returns the current command of the Thread.
         *
         * @return A command.
         * @see Command.hpp
         */
        inline ADAPRO::Data::Command get_command() const noexcept
        {
            return command.load(memory_order_consume);
        }

        /**
         * Compares the current state of the Thread against the given bit mask
         * using bitwise AND. For example, for Thread <tt>t</tt> in state
         * <tt>RUNNING</tt>, it holds that
         * <tt>t.state_in(READY | STOPPED) == false</tt>, but
         * <tt>t.state_in(RUNNING) == true</tt>.
         *
         * @param state_mask    A bit mask of states.
         * @return              <tt>true</tt> if and only if this Thread is in
         * one of the masked states.
         */
        inline bool state_in(const uint8_t state_mask) const noexcept
        {
            return (get_state() & state_mask) > 0;
        }

        /**
         * Compares the current command of the Thread against the given bit mask
         * using bitwise AND. For example, for Thread <tt>t</tt> with command
         * <tt>START</tt>, it holds that
         * <tt>t.command_in(CONTINUE | PAUSE) == false</tt>, but
         * <tt>t.command_in(START) == true</tt>.
         *
         * @param state_mask    A bit mask of commands.
         * @return              <tt>true</tt> if and only if the current command
         * of this Thread is one of the masked commands.
         */
        inline bool command_in(const uint8_t command_mask) const noexcept
        {
            return (get_command() & command_mask) > 0;
        }

        /**
         * Starts the execution of the Thread. The behaviour of the Thread
         * depends on its task and parameter given in the constructor. This
         * method has only effect when called on a Thread that is in the state
         * <tt>READY</tt> with the command <tt>CONTINUE</tt>, i.e. the initial
         * configuration.
         *
         * @param wait Whether or not to wait until the Thread has moved to one
         * of the states specified in <tt>ADAPRO::Data::START_MASK</tt>.
         * @deprecated Since ADAPRO 5.0.0, this method has become deprecated.
         * Please use <tt>start_sync</tt> or <tt>start_async</tt> instead.
         * @see ADAPRO::Data::State::START_MASK
         * @see ADAPRO::Control::Thread::start_async
         * @see ADAPRO::Control::Thread::start_sync
         */
        [[deprecated("This method is unncessary. Please use start_async or "
                "start_sync instead.")]]
        inline void start(const bool wait = false) noexcept
        {
            if (wait) { start_sync(); } else { start_async(); }
        }

        /**
         * Starts this <tt>Thread</tt>. This method is asynchronous, the new
         * (native) thread will run independently from the calling thread.
         *
         * @since ADAPRO 5.0.0
         * @see ADAPRO::Control::Thread::start_sync
         */
        void start_async() noexcept;

        /**
         * Starts this <tt>Thread</tt> like <tt>start_async</tt>, but blocks
         * until this <tt>Thread</tt> has moved into a state contained in the
         * state mask <tt>ADAPRO::DATA::START_MASK</tt>.
         *
         * @since ADAPRO 5.0.0
         * @see ADAPRO::Control::Thread::start_async
         * @see ADAPRO::Data::State::START_MASK
         */
        inline void start_sync() noexcept
        {
            start_async();
            wait_for_state_mask(ADAPRO::Data::START_MASK);
        }

        /**
         * Causes the Thread to temporarily stop executing its task and move to
         * the state <tt>PAUSED</tt>. This method has only effect on a Thread
         * that is in the state <tt>RUNNING</tt>, or if a Thread chooses to
         * pause itself during <tt>prepare</tt>. In the latter case, this method
         * <emph>must</emph> be called with an argument that evaluates to
         * <tt>false</tt> in order to avoid a deadlock.
         *
         * @param wait Whether or not to wait until the Thread has moved to one
         * of the states specified in <tt>ADAPRO::Data::PAUSE_MASK</tt>.
         * @deprecated Since ADAPRO 5.0.0, this method has become deprecated.
         * Please use <tt>pause_sync</tt> or <tt>pause_async</tt> instead.
         *
         * @see ADAPRO::Control::Thread::resume
         * @see ADAPRO::Control::Thread::pause_async
         * @see ADAPRO::Control::Thread::pause_sync
         * @see ADAPRO::Data::State::PAUSE_MASK
         */
        [[deprecated("This method is unncessary. Please use pause_async or "
                "pause_sync instead.")]]
        void pause(const bool wait = false) noexcept
        {
            if (wait) { pause_sync(); } else { pause_async(); }
        }

        /**
         * Instructs this <tt>Thread</tt> to move into state <tt>PAUSED</tt> (by
         * sending the command <tt>PAUSE</tt>. During the <tt>PAUSED</tt> state,
         * this <tt>Thread</tt> temporarily ceases calling <tt>execute</tt>.
         * This method is asynchronous, so it will not block until this
         * <tt>Thread</tt> has made its state transition. This method cannot
         * violate the <tt>Thread</tt> FSM constraints. If called during an
         * inappropriate state, a warning message will be printed (via a
         * <tt>Logger</tt>) and the command will be ignored. <em>It's on the
         * caller's responsibilility to use this method only when permitted by
         * the FSM model.</em>
         *
         * @since ADAPRO 5.0.0
         * @see ADAPRO::Control::Thread::pause_sync
         * @see ADAPRO::Control::Thread::wait_for_state
         * @see ADAPRO::Control::Thread::resume_async
         * @see ADAPRO::Control::Thread::resume_sync
         * @see ADAPRO::Control::Logger
         */
        inline void pause_async() noexcept
        {
            if (!set_command(ADAPRO::Data::Command::PAUSE))
            {
                print("Unable to pause.", ADAPRO::Data::LoggingLevel::FSM);
            }
        }

        /**
         * Like <tt>pause_async</tt> (with the same FSM constraints applying),
         * but waits for this Thread to move into a state in <tt>PAUSE_MASK</tt>
         * before unblocking.
         *
         * @since ADAPRO 5.0.0
         * @see ADAPRO::Data::State::PAUSE_MASK
         * @see ADAPRO::Control::Thread::pause_async
         */
        inline void pause_sync() noexcept
        {
            if (set_command(ADAPRO::Data::Command::PAUSE))
            {
                wait_for_state_mask(ADAPRO::Data::PAUSE_MASK);
            }
            else
            {
                print("Unable to pause.", ADAPRO::Data::LoggingLevel::FSM);
            }
        }

        /**
         * Continues the execution of this <tt>Thread</tt>. This command only
         * has effect when called on a <tt>Thread</tt> that is <tt>PAUSED</tt>,
         * in which case the Thread moves back to <tt>RUNNING</tt> and resumes
         * its compuational task.
         *
         * @param wait Whether or not to wait until the Thread has moved to one
         * of the states specified in <tt>ADAPRO::Data::RESUME_MASK</tt>.
         * @deprecated Since ADAPRO 5.0.0, this method has become deprecated.
         * Please use the non-blocking version and a waiting method instead.
         *
         * @see ADAPRO::Control::Thread::pause
         * @see ADAPRO::Data::State::RESUME_MASK
         */
        [[deprecated("This method is unncessary. Please use resume_async or "
                "resume_sync instead.")]]
        inline void resume(const bool wait = false) noexcept
        {
            if (wait) { resume_sync(); } else { resume_async(); }
        }

        /**
         * Instructs this <tt>Thread</tt> to move from state <tt>PAUSED</tt>
         * into state <tt>RUNNING</tt> (by sending the command
         * <tt>CONTINUE</tt>. This method is asynchronous, so it will not block
         * until this <tt>Thread</tt> has made its state transition. This method
         * cannot violate the <tt>Thread</tt> FSM constraints. If called during
         * an inappropriate state, a warning message will be printed (via a
         * <tt>Logger</tt>) and the command will be ignored. <em>It's on the
         * caller's responsibilility to use this method only when permitted by
         * the FSM model.</em>
         *
         * @since ADAPRO 5.0.0
         * @see ADAPRO::Thread::Control::resume_sync
         * @see ADAPRO::Control::Thread::wait_for_state
         * @see ADAPRO::Control::Thread::pause_async
         * @see ADAPRO::Control::Thread::pause_sync
         * @see ADAPRO::Control::Logger
         */
        inline void resume_async() noexcept
        {
            if (!set_command(ADAPRO::Data::Command::CONTINUE))
            {
                print("Unable to resume.", ADAPRO::Data::LoggingLevel::FSM);
            }
        }

        /**
         * Like <tt>resume_async</tt> (with the same FSM constraints applying),
         * but waits for this Thread to move into a state in
         * <tt>RESUME_MASK</tt> before unblocking.
         *
         * @since ADAPRO 5.0.0
         * @see ADAPRO::Data::State::RESUME_MASK
         * @see ADAPRO::Control::Thread::resume_async
         */
        inline void resume_sync() noexcept
        {
            if (set_command(ADAPRO::Data::Command::CONTINUE))
            {
                wait_for_state_mask(ADAPRO::Data::RESUME_MASK);
            }
            else
            {
                print("Unable to resume.", ADAPRO::Data::LoggingLevel::FSM);
            }
        }

        /**
         * Signals the Thread to stop executing its task if it hadn't received
         * this signal already. <em>The behaviour of this method is
         * undefined if the Thread tries to call this itself.</em> In this
         * scenario, there will probably be a deadlock or a fatal runtime error.
         *
         * @param join This procedure will be blocking if and only if this
         * variable is set <tt>true</tt>.
         * @deprecated Since ADAPRO 5.0.0, this method has become deprecated.
         * Please use the non-blocking version and a waiting method instead.
         * @see ADAPRO::Data::State::HALT_MASK
         */
        [[deprecated("This method is unncessary. Please use stop_async or "
                "stop_sync instead.")]]
        inline void stop(const bool join = true) noexcept
        {
            if (join) { stop_sync(); } else { stop_async(); }
        }

        /**
         * Instructs this <tt>Thread</tt> to start its shutdown sequence (by
         * sending the command <tt>STOP</tt>. This method is asynchronous, so it
         * will not block until this <tt>Thread</tt> has made its state
         * transition. This method cannot violate the <tt>Thread</tt> FSM
         * constraints. If called during an inappropriate state, a warning
         * message will be printed (via a <tt>Logger</tt>) and the command will
         * be ignored. <em>It's on the caller's responsibilility to use this
         * method only when permitted by the FSM model.</em>
         *
         * @since ADAPRO 5.0.0
         * @see ADAPRO::Thread::Control::resume_sync
         * @see ADAPRO::Control::Thread::wait_for_state
         * @see ADAPRO::Control::Logger
         */
        inline void stop_async() noexcept
        {
            if (!set_command(ADAPRO::Data::Command::STOP))
            {
                print("Unable to stop.", ADAPRO::Data::LoggingLevel::FSM);
            }
        }

        /**
         * Like <tt>stop_async</tt> (with the same FSM constraints applying),
         * but waits for this Thread to move into a state in <tt>STOP_MASK</tt>
         * before unblocking.
         *
         * @since ADAPRO 5.0.0
         * @see ADAPRO::Data::State::HALTED_MASK
         * @see ADAPRO::Control::Thread::resume_async
         */
        inline void stop_sync() noexcept
        {
            if (set_command(ADAPRO::Data::Command::STOP))
            {
                wait_for_state_mask(ADAPRO::Data::HALTED_MASK);
            }
            else
            {
                print("Unable to stop.", ADAPRO::Data::LoggingLevel::FSM);
            }
        }

        /**
         * Returns a report that can be used for debugging purposes. The default
         * report contains the name, state, and command of the Thread.
         *
         * @return A string.
         */
        virtual string report() noexcept;

        /**
         * Virtual destructor for the Thread. Note that calling a destructor for
         * a Thread that is not in any of the states <tt>READY</tt>,
         * <tt>STOPPED</tt>, or <tt>ABORTED</tt>, leads to a <tt>SIGSEGV</tt>,
         * <tt>SIGABRT</tt> or other fatal runtime situation. This is because
         * the memory allocations belonging to Thread are not available anymore
         * to the backend <tt>std::thread</tt> object. <em>It is user's
         * responsibility to make sure that the destructor for <tt>Thread</tt>
         * (or any of its subclasses) never gets called during a non-safe state.
         * </em>
         *
         * @see ADAPRO::Control::Thread::join
         */
        virtual ~Thread() noexcept {}

        /**
         * Causes the caller to spend at least the given number of nanoseconds
         * in a spinlock. The actual performance of this function will depend on
         * the runtime environment, naturally. <em>Waiting in a spinlock
         * consumes CPU cycles that could be allocated to other threads.</em>
         * This function should only be used in situations where the granularity
         * of <tt>std::this_thread::sleep_for</tt> is insufficient.
         *
         * @param interval  The minimum amount of time to be spent in a
         * spinlock.
         */
        static void SPINLOCK(const chrono::nanoseconds interval) noexcept;

        /**
         * On supported platforms, this static method sets the CPU core affinity
         * for the thread invoking it. On other platforms, this procedure does
         * nothing. Currently, only systems with POSIX threads are supported
         * (e.g. Linux OS family). <em>This procedure works for all threads, not
         * only for ADAPRO Threads.</em>
         *
         * @param thread_name   Name of the thread. Used for printing
         * debug/warning/error messages.
         * @param affinity      Number of the preferred CPU core of the thread.
         * If the value of this argument is negative, then this returns without
         * changing affinity.
         * @param logger        A logger instance.
         * @param thread_name   An optional thread name argument. May be empty.
         * @throws std::runtime_error If there were no CPU core number matching
         * the argument value, or if setting the affinity or scheduling
         * parameters failed for some other reason.
         */
#ifndef VERIFY
        static void SET_AFFINITY(const int affinity, Logger& logger, const string& thread_name);
#endif /* VERIFY */
    };
}
}

#endif	/* ADAPRO_THREAD_HPP */

