/*
 * File:   Version.hpp
 * Author: John Lång
 *
 * Created on 6 January 2019, 15:17
 */

#ifndef ADAPRO_VERSION_HPP
#define ADAPRO_VERSION_HPP

#include <string>

namespace ADAPRO
{
    /**
     * Returns the current versio of the ADAPRO framework (i.e. 5.0.0).
     */
     inline std::string get_version() noexcept {return "5.0.0";};
     // Note to self: Keep this up-to-date!
}

#endif /* ADAPRO_VERSION_HPP */

