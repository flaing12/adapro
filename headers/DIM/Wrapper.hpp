/*
 * File:   Wrapper.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 2 November 2016, 16:54
 */

#ifndef EXCLUDE_DIM
#ifndef ADAPRO_DIM_WRAPPER_HPP
#define ADAPRO_DIM_WRAPPER_HPP

#include <cstdint>
#include <string>
#include <vector>
#include "../DIM/Typedefs.hpp"
#include "../data/LoggingLevel.hpp"

// Copied from <dim/dim_common.h>:
typedef long dim_long;

namespace ADAPRO
{
namespace Control
{
    class Logger;
}

namespace DIM
{
/**
 * <p>Wrapper can be used for ensuring mutual exclusion when accessing DIM
 * functions. Also, Wrapper uses a modern exception-based approach in error
 * handling. As its name suggests, Wrapper is merely a simple wrapper, and
 * its logic follows that of the DIM C library. Wrapper doesn't implement the
 * whole API, but the functionality needed by ADAPOS. For compatibility, the
 * functions of Wrapper have same <tt>const</tt> qualifications for their
 * parameters as the corresponding DIM procedures. For general DIM
 * documentation, please consult the DIM web page.</p>
 * <p><em>Wrapper is known to go into deadlock (at least) in the following
 * situation (altough this particular deadlock scenario has been fixed in
 * ADAPRO 4.0.0):
 * </em></p>
 * <ol>
 *     <li>A service subscription is made using
 *         <tt>client_subscribe_stamped</tt>;</li>
 *     <li>DIM immediately executes <tt>user_routine</tt>, while Wrapper is
 *         still holding its internal lock for DIM API access; and</li>
 *     <li><tt>user_routine</tt> calls <tt>client_write_timestamp</tt>, which
 *         then waits forever for the internal lock.</li>
 * </ol>
 * <p>It is possible, if not likely, that other deadlock scenarios also exist.
 * Therefore, using <tt>Controller</tt> is recommended instead of directly using
 * Wrapper.</p>
 *
 * @see <a href="https://dim.web.cern.ch/dim/">https://dim.web.cern.ch/dim/</a>
 * @see <a href="https://dim.web.cern.ch/dim/dimC.html">https://dim.web.cern.ch/dim/dimC.html</a>
 * @see ADAPRO::DIM::Controller
 */
namespace Wrapper
{
    /**
     * Converts a DIM error severity code into a LoggingLevel object.
     *
     * @param severity  DIM severity code.
     * @return          LoggingLevel.
     * @see DIM_SEVERITIES
     * @see ADAPRO::Data::LoggingLevel
     */
    inline ADAPRO::Data::LoggingLevel dim_severity_to_logging_level
    (
            const int severity
    )
    noexcept
    {
        switch (severity)
        {
            case DIM_INFO:
                return ADAPRO::Data::LoggingLevel::ADAPRO_DEBUG;
            case DIM_WARNING:
                return ADAPRO::Data::LoggingLevel::WARNING;
            case DIM_ERROR:
                return ADAPRO::Data::LoggingLevel::ERROR;
            case DIM_FATAL:
            default: // default should be unreachable.
                return ADAPRO::Data::LoggingLevel::FATAL;
        }
    }

    /**
     * The default DIM error handler of ADAPRO. Prints the error message using
     * Logger. Aborts the Session if the severity of the DIM error was
     * <tt>DIM_ERROR</tt> or <tt>DIM_FATAL</tt>.
     *
     * @param severity  Severity of the error (one of the following:
     * <tt>DIM_INFO</tt>, <tt>DIM_WARNING</tt>, <tt>DIM_ERROR</tt>,
     * <tt>DIM_FATAL</tt>).
     * @param code      An error code, that will be ignored.
     * @param message   The error message to be printed.
     * @see ADAPRO::Control::Session
     * @see ADAPRO::Control::Logger
     */
    void default_dim_error_handler(int severity, int code, char* message)
    noexcept;

    /**
     * <p>Sets the DIM DNS node. <em>Maximum length for this string is 255
     * characters</em>.</p>
     *
     * @param node The new value for DIM DNS node.
     * @throws std::length_error If the given string is longer than 255
     * characters.
     */
    void common_set_dim_dns_node(const std::string& node = "localhost");

    /**
     * Returns the DIM DNS node. <em>This procedure assumes that the length
     * of DIM DNS node hostname/address including the terminating null byte
     * is no more than 256 chars</em>.
     *
     * @return Hostname or address of the DIM DNS node.
     * @throws std::runtime_error If retrieving the DIM DNS node failed.
     */
    std::string* common_get_dim_dns_node();

    /**
     * Sets the TCP port used for connecting to DIM DNS node. The default
     * DIM DNS port is 2505.
     *
     * @param port TCP port number. Must be positive.
     * @throws std::invalid_argument If the argument was zero.
     */
    void common_set_dim_dns_port(const uint16_t port = 2505);

    /**
     * Gets the TCP port number that the DIM DNS node should be listening
     * to. The default DIM DNS port is 2505.
     *
     * @return Port number of DIM DNS node.
     * @throws std::runtime_error If retrieving the DIM DNS port failed.
     */
    uint16_t common_get_dim_dns_port();

    /**
     * <p>Adds a new DIM command Service. This procedure should be invoked
     * <em>before</em> <tt>server_start</tt>.</p>
     *
     * @param command_name  Name of the service.
     *
     * @param command_description <p>DIM description of the service. In case of
     * a command service (i.e. when passed to this procedure), it represents a
     * typing constraint on the command  argument, whereas in cse of a normal
     * service (i.e. when passed to <tt>server_add_service</tt>), it determines
     * the structure of the published value. The format of this string is:</p>
     * <pre>
     *     &lt;description&gt;   ::= "" | &lt;segment&gt; | &lt;description&gt; ";" &lt;description&gt; | &lt;type-id&gt; | &lt;description&gt; ";" &lt;type-id&gt;
     *     &lt;segment&gt;       ::= &lt;type-id&gt; ":" &lt;count&gt; | &lt;C&gt;
     *     &lt;type-id&gt;       ::= "C" | "S" | "I" | "L" | "X" | "F" | "D"
     *     &lt;count&gt;         ::= "1" | "2" | "3" | ...
     * </pre>
     * <p>The <tt>type-id</tt> values correspond with the following types (on
     * 64-bit Linux systems):
     * </p>
     * <table>
     *     <tr><td><tt>C</tt></td><td>8-bit integer (char)</td></tr>
     *     <tr><td><tt>S</tt></td><td>16-bit integer (short)</td></tr>
     *     <tr><td><tt>I</tt></td><td>32-bit integer (int)</td></tr>
     *     <tr><td><tt>L</tt></td><td>64-bit integer (long)</td></tr>
     *     <tr><td><tt>X</tt></td><td>64-bit integer (long long)</td></tr>
     *     <tr><td><tt>F</tt></td><td>32-bit IEEE 754 single-precision floating point value</td></tr>
     *     <tr><td><tt>D</tt></td><td>64-bit IEEE 754 double-precision floating point value</td></tr>
     * </table>
     * <p>when <tt>type-id</tt> is used without count, DIM interprets that all
     * consecutive values have the same type. Hence, lone <tt>type-id</tt>s are
     * allowed only at the end of the description string. For example,
     * <tt>"S:2;I:1;C"</tt> is allowed, but <tt>"S:2;C;I:1"</tt> is not.
     * <em>Using this feature is not recommended, since it might result in
     * buffer overflows and other memory problems if the size of the data
     * segment changes dynamically.</em> Resolving these problems is left for
     * the user.</p>
     * <p>The purpose of the type string is to inform DIM and the clients of
     * the expected type of the arguments of the command. For Example, a
     * command expecting a single <tt>int</tt> as an argument would have
     * <tt>service_type</tt> <tt>"I:1"</tt>, where a command expecting an
     * <tt>int</tt> argument, two <tt>float</tt> arguments, and a
     * <tt>uint64_t</tt> argument would have
     * <tt>service_type</tt> <tt>"I:1;F:2;X:1"</tt>.</p>
     * <p>Since ADAPRO 4.0.0, DIM service description strings are tested against
     * the following ECMAScript regular expression:
     * <tt>^([CSILXFD]|(([CSILXFD]:[1-9]\\d*(;[CSILXFD]:[1-9]\\d*)*)+(;[CSILXFD])?))$</tt>.
     * If this parameter fails to match with the regular expression, a
     * <tt>std::invalid_argument</tt> is thrown.</p>
     *
     * @param user_routine <p>A function pointer that will be called when a
     * client requests the execution of the command service.</p>
     * <p>
     *     The first argument of this function pointer is a pointer to a
     *     <tt>dim_long</tt> type variable that has the value of the
     *     <tt>tag</tt> parameter given in this procedure's call.
     * </p>
     * <p>
     *     The second argument of this function pointer is a pointer to a
     *     buffer containing the arguments given by the DIM client when
     *     calling the command service.
     * </p>
     * <p>
     *     The third argument of this function pointer is a pointer to the
     *     size of the buffer given as the second argument.
     * </p>
     *
     * @param tag An arbitrary integer value that will be passed by
     * reference to the <tt>user_routine</tt>.
     *
     * @return An unsigned integer identifying the added command service. It
     * can be used for updating or removing the service.
     * @throws std::invalid_argument If the DIM (command) service description
     * string was malformed.
     * @throws std::length_error If the command service name was longer than 132
     * characters.
     *
     * @see ADAPRO::DIM::Controller
     * @see ADAPRO::DIM::Wrapper::server_update_service
     * @see ADAPRO::DIM::Wrapper::server_remove_service
     * @see ADAPRO::DIM::Wrapper::server_start
     */
    service_id_t server_add_command
    (
            const std::string& command_name,
            const std::string& command_description,
            command_callback_t user_routine,
            dim_long tag = 0
    );

    /**
     * Like <tt>server_add_command</tt>, but not thread-safe. More specifically,
     * access to this procedure by multiple threads is not mutually excluded.
     * <em>This procedudre should only be invoked inside a user routine given to
     * DIM, in order to prevent deadlocks caused by cyclic locking.</em>
     *
     * @param command_name          Name for the DIM command service.
     * @param command_description   DIM service description string, that applies
     * to the arguments of the command.
     * @param user_routine          The user routine that will be exdcuted when
     * the command is invoked.
     * @param tag                   An integer that will be passed to the user
     * routine.
     * @return                      DIM service id of the command service.
     *
     * @throws std::invalid_argument If the DIM (command) service description
     * string was malformed.
     * @throws std::length_error If the command service name was longer than 132
     * characters.
     *
     * @see ADAPRO::DIM:Wrapper::server_add_command
     */
    service_id_t server_add_command_unsafe
    (
            const std::string& command_name,
            const std::string& command_description,
            command_callback_t user_routine,
            dim_long tag = 0
    );

    /**
     * Adds a new DIM service. This procedure should be invoked <em>before</em>
     * <tt>server_start</tt>.
     *
     * @param service_name      Name of the service. Maximum length is 132.
     * @param service_description Description of the service. Uses the same
     * format as <tt>ADAPRO::DIM::Wrapper::server_add_command</tt>.
     * @param buffer            Address of the buffer containing the data
     * that is being published.
     * @param buffer_size       Size of the buffer in bytes.
     * @param user_routine      This parameter is required by DIM, but its
     * semantics are unknown.
     * @param tag               A user-provided number identifying the service,
     * that will be passed on to the callback. (This is not the DIM service id.)
     * @return An unsigned integer identifying the added service. It can be
     * used for updating or removing the service.
     *
     * @see ADAPRO::DIM::Controller
     * @see ADAPRO::DIM::Wrapper::server_add_command
     * @see ADAPRO::DIM::Wrapper::server_update_service
     * @see ADAPRO::DIM::Wrapper::server_remove_service
     * @see ADAPRO::DIM::Wrapper::server_start
     * @throws std::length_error If the service name was longer than 132
     * characters.
     * @throws std::invalid_argument If the DIM (command) service description
     * string was malformed.
     */
    service_id_t server_add_service
    (
            const std::string& service_name,
            const std::string& service_description,
            void* buffer,
            int buffer_size,
            void (*user_routine)(void*, void**, int*, int*) = nullptr,
            dim_long tag = 0
    );

    /**
     * Like <tt>server_add_service</tt>, but not thread-safe. More specifically,
     * access to this procedure by multiple threads is not mutually excluded.
     * <em>This procedudre should only be invoked inside a user routine given to
     * DIM, in order to prevent deadlocks caused by cyclic locking.</em>
     *
     * @param service_name          Name for the DIM service.
     * @param service_description   DIM description string for the service data.
     * @param buffer                A buffer containing the service data.
     * @param buffer_size           Size of the buffer.
     * @param user_routine          User routine.
     * @param tag                   Tag probably given to the user routine.
     * @return                      DIM service id.
     *
     * @see ADAPRO::DIM::server_add_service
     */
    service_id_t server_add_service_unsafe
    (
            const std::string& service_name,
            const std::string& service_description,
            void* buffer,
            int buffer_size,
            void (*user_routine)(void*, void**, int*, int*) = nullptr,
            dim_long tag = 0
    );

    /**
     * <p>Adds a DIM server error handler. The parameters of this callback
     * function are the severity of the error, an error code, and an error
     * message as a null-terminated character array.</p>
     * <p>The following severity values are enumerated in <tt>dim_common.h</tt>:
     * </p>
     * <ul>
     *     <li>DIM_INFO</li>
     *     <li>DIM_WARNING</li>
     *     <li>DIM_ERROR</li>
     *     <li>DIM_FATAL</li>
     * </ul>
     * <p>The error codes are defined as macros in <tt>dim_common.h</tt>:
     * <table>
     *     <tr><th>Macro</th><th>Expansion</th><th>Description</th><th>Severity</th></tr>
     *     <tr>
     *         <td><tt>DIMDNSUNDEF</tt></td>
     *         <td><tt>0x1</tt></td>
     *         <td>DIM_DNS_NODE undefined</td>
     *         <td><tt>DIM_FATAL</tt></td>
     *     </tr>
     *     <tr>
     *         <td><tt>DIMDNSREFUS</tt></td>
     *         <td><tt>0x2</tt></td>
     *         <td>DIM_DNS refuses connection</td>
     *         <td><tt>DIM_FATAL</tt></td>
     *     </tr>
     *     <tr>
     *         <td><tt>DIMDNSDUPLC</tt></td>
     *         <td><tt>0x3</tt></td>
     *         <td>Service already exists in DNS</td>
     *         <td><tt>DIM_FATAL</tt></td>
     *     </tr>
     *     <tr>
     *         <td><tt>DIMDNSEXIT</tt></td>
     *         <td><tt>0x4</tt></td>
     *         <td>DNS requests server to EXIT</td>
     *         <td><tt>DIM_FATAL</tt></td>
     *     </tr>
     *     <tr>
     *         <td><tt>DIMDNSTMOUT</tt></td>
     *         <td><tt>0x5</tt></td>
     *         <td>Server failed sending Watchdog</td>
     *         <td><tt>DIM_WARNING</tt></td>
     *     </tr>
     *     <tr>
     *         <td><tt>DIMSVCDUPLC</tt></td>
     *         <td><tt>0x10</tt></td>
     *         <td>Service already exists in Server</td>
     *         <td><tt>DIM_ERROR</tt></td>
     *     </tr>
     *     <tr>
     *         <td><tt>DIMSVCFORMT</tt></td>
     *         <td><tt>0x11</tt></td>
     *         <td>Bad format string for service</td>
     *         <td><tt>DIM_ERROR</tt></td>
     *     </tr>
     *     <tr>
     *         <td><tt>DIMSVCINVAL</tt></td>
     *         <td><tt>0x12</tt></td>
     *         <td>Service ID invalid</td>
     *         <td><tt>DIM_ERROR</tt></td>
     *     </tr>
     *     <tr>
     *         <td><tt>DIMSVCTOOLG</tt></td>
     *         <td><tt>0x13</tt></td>
     *         <td>Service name too long (maximum is 132 characters)</td>
     *         <td><tt>DIM_ERROR</tt></td>
     *     </tr>
     *     <tr>
     *         <td><tt>DIMTCPRDERR</tt></td>
     *         <td><tt>0x20</tt></td>
     *         <td>TCP/IP read error</td>
     *         <td><tt>DIM_ERROR</tt></td>
     *     </tr>
     *     <tr>
     *         <td><tt>DIMTCPWRRTY</tt></td>
     *         <td><tt>0x21</tt></td>
     *         <td>TCP/IP write	error - Retrying</td>
     *         <td><tt>DIM_WARNING</tt></td>
     *     </tr>
     *     <tr>
     *         <td><tt>DIMTCPWRTMO</tt></td>
     *         <td><tt>0x22</tt></td>
     *         <td>TCP/IP write error - Disconnect</td>
     *         <td><tt>DIM_ERROR</tt></td>
     *     </tr>
     *     <tr>
     *         <td><tt>DIMTCPLNERR</tt></td>
     *         <td><tt>0x23</tt></td>
     *         <td>TCP/IP listen error</td>
     *         <td><tt>DIM_ERROR</tt></td>
     *     </tr>
     *     <tr>
     *         <td><tt>DIMTCPOPERR</tt></td>
     *         <td><tt>0x24</tt></td>
     *         <td>TCP/IP open server error</td>
     *         <td><tt>DIM_ERROR</tt></td>
     *     </tr>
     *     <tr>
     *         <td><tt>DIMTCPCNERR</tt></td>
     *         <td><tt>0x25</tt></td>
     *         <td>TCP/IP connection error</td>
     *         <td><tt>DIM_ERROR</tt></td>
     *     </tr>
     *     <tr>
     *         <td><tt>DIMTCPCNEST</tt></td>
     *         <td><tt>0x26</tt></td>
     *         <td>TCP/IP connection established</td>
     *         <td><tt>DIM_INFO</tt></td>
     *     </tr>
     *     <tr>
     *         <td><tt>DIMDNSCNERR</tt></td>
     *         <td><tt>0x30</tt></td>
     *         <td>Connection to DNS failed</td>
     *         <td><tt>DIM_ERROR</tt></td>
     *     </tr>
     *     <tr>
     *         <td><tt>DIMDNSCNEST</tt></td>
     *         <td><tt>0x31</tt></td>
     *         <td>Connection to DNS established</td>
     *         <td><tt>DIM_INFO</tt></td>
     *     </tr>
     * </table>
     * @param user_routine The custom error handler. Default is
     * <tt>DEFAULT_DIM_ERROR_HANDLER</tt>.
     * @see ADAPRO::DIM::Wrapper::DEFAULT_DIM_ERROR_HANDLER
     */
    void server_add_error_handler
    (
            error_callback_t dim_error_handler = default_dim_error_handler
    )
    noexcept;

    /**
     * <p>Informs DIM that the given service has been updated. The DIM clients
     * that have subscribed to this service using subscrption type
     * <tt>MONITORED</tt> will receive their updates after this procedure has
     * been carried out. This procedure has <em>noexcept</em> guarantee. It
     * should be invoked <em>after</em> <tt>server_start</tt>.</p>
     *
     * @param service_id Identifier of the service returned when calling
     * <tt>server_add_service</tt>.
     *
     * @see ADAPRO::DIM::Controller
     * @see ADAPRO::DIM::Wrapper::server_add_service
     * @see ADAPRO::DIM::SubscriptionType
     */
    void server_update_service(const service_id_t service_id) noexcept;

    /**
     * Like <tt>server_update_service</tt>, but not thread-safe. More
     * specifically, access to this procedure by multiple threads is not
     * mutually excluded. <em>This procedudre should only be invoked inside a
     * user routine given to DIM, in order to prevent deadlocks caused by cyclic
     * locking.</em>
     *
     * @param service_id    DIM service ID for the service to be updated.
     */
    void server_update_service_unsafe(const service_id_t service_id) noexcept;

    /**
     * Removes the service with the given service ID.
     *
     * @param service_id Identifier of the service returned when calling
     * <tt>server_add_service</tt>.
     * @throws std::runtime_error If removing the service failed.
     * @see ADAPRO::DIM::Controller
     * @see ADAPRO::DIM::Wrapper::server_add_service
     */
    void server_remove_service(const service_id_t service_id);

    /**
     * Same as <tt>server_remove_service</tt>.
     *
     * @param service_id Identifier of the service to be removed.
     *
     * @see ADAPRO::DIM::Wrapper::server_remove_service
     */
    inline void server_remove(const service_id_t service_id)
    {
        server_remove_service(service_id);
    }

    /**
     * Starts the DIM server with the given name. If the name is too long,
     * it will be truncated. This procedure has <em>noexcept</em> guarantee.
     *
     * @param server_name Name of the server. Maximum length is 40 characters.
     * @throws std::lenth_error If the server name was longer than 40
     * characters.
     * @see ADAPRO::DIM::Controller
     */
    void server_start(const std::string& server_name);

    /**
     * Stops the DIM server. This procedure has <em>noexcept</em> guarantee.
     * @see ADAPRO::DIM::Controller
     */
    void server_stop() noexcept;

    /**
     * <p>Returns a reference to a vector of strings containing all the DIM
     * service names, and their respective descriptions and types, registered to
     * the current DIM DNS node, excluding the services provided by DIM DNS node
     * itself. The format of these strings is the following:
     * <tt>&lt;service-entry&gt; ::= &lt;name&gt; "|" &lt;description&gt; "|"
     * &lt;service-type&gt;</tt>, where <tt>&lt;service-entry&gt</tt> is the
     * name of the DIM service, <tt>&lt;description&gt</tt> is a DIM service
     * description string (e.g. <tt>"S:2;I:1;D:1"</tt>), and
     * <tt>&lt;service-type&gt;</tt> can be an empty string for normal sercices,
     * <tt>"CMD"</tt> for DIM command services, or "<tt>RPC</tt> for DIM RPC
     * services.</p>
     * <p>For example, <tt>"ADAPOS_LG/TST_000005|S:2;I:1;C:4|"</tt> is the entry
     * for the service <tt>"ADAPOS_LG/TST_000005"</tt> (which happens to follow
     * the ALICE DIM service naming convention, meaning that the name of the DIM
     * server providing this sercice is <tt>"ADAPOS_LG"</tt>). The DIM
     * description string of this service is <tt>"S:2;I:1;C:4"</tt>, which means
     * that, from the lowest to highest byte address, this service provides two
     * shorts (two-byte integers), one int (4 byte integer), and four
     * consecutive <tt>char</tt>s. This service is not a command or RPC service,
     * hence the entry ends with a bar (<tt>'|'</tt>).</p>
     * <p><em>This procedure is considered successful when retrieving DIM
     * server names from DIM DNS node is successful.</em> On failure, this
     * procedure throws a <tt>std::runtime_error</tt>. Getting the service
     * lists from individual services might, in theory, fail. This unlikely
     * event will not generate an exception, but it will be indicated with a
     * warning message (i.e. a message with
     * <tt>ADAPRO::Data::LoggingLevel::WARNING</tt>) using the
     * <tt>ADAPRO::Control::Logger</tt> instance provided as the argument.
     * </p>
     *
     * @return          List of DIM service names.
     * @throws std::runtime_error if retrieving DIM service names failed.
     * @see ADAPRO::DIM::Controller
     */
    std::vector<std::string>& client_get_services();

    /**
     * <p>Subscribes to a DIM service. This procedure has a <em>noexcept</em>
     * guarantee. If timestamps are needed for the updates, then the
     * subscription must be done through <tt>client_subscribe_stamped</tt>.</p>
     *
     * @param service_name      Name of the service to subscribe to.
     * @param subscription_type <p>Type of the subscription. The types are:</p>
     * <table>
     *     <tr><td><tt>1</tt></td><td>The client only connects once to the
     *         server, gets the data, and then disconnects. This means that
     *         only one state of the service will be received.</td></tr>
     *     <tr><td><tt>2</tt></td><td>The client will receive data
     *         periodically from the service with an interval specified in
     *         seconds.</td></tr>
     *     <tr><td><tt>4</tt></td><td>The client will receive data from the
     *         server whenever it's updated. Additionally, a time interval
     *         for periodic updates can be specified in seconds. If the
     *         interval is <tt>0</tt>, then there will be no periodic
     *         updates.</td></tr>
     * </table>
     * @param interval          Time interval between updates in seconds.
     * @param buffer            Buffer where the incoming data will be
     * stored in.
     * @param buffer_size       Size of the buffer in bytes.
     * @param user_routine      A callback function for handling the
     * incoming data. This callback function takes the same arguments as the
     * one used in <tt>DIMWrapper::server_add_command</tt>.
     * @param tag               An arbitrary integer, that will be passed
     * by reference to the user routine.
     * @param error_object      An error object, that will be written in the
     * buffer (repeatedly until the whole buffer is filled) in case of an
     * error, if the parameter <tt>error_object_size<tt> was positive.
     * @param error_object_size Size of the error object. If this parameter is
     * set to zero, then <tt>buffer</tt> won't be overwritten in case of error,
     * but the third argument of the callback will 0 instead.
     * @return                  An unsigned integer identifying the
     * subscribed service. It can be used for unsubscribing.
     *
     * @see ADAPRO::DIM::Controller
     * @see ADAPRO::DIM::Wrapper::server_add_command
     * @see ADAPRO::DIM::Wrapper::client_unsubscribe
     */
    service_id_t client_subscribe
    (
            const std::string& service_name,
            int subscription_type,
            int interval,
            void* buffer,
            int buffer_size,
            command_callback_t user_routine = nullptr,
            dim_long tag = 0,
            void* error_object = nullptr,
            int error_object_size = 0
    )
    noexcept;

    /**
     * Like <tt>client_subscribe</tt>, but not thread-safe. More
     * specifically, access to this procedure by multiple threads is not
     * mutually excluded. <em>This procedudre should only be invoked inside a
     * user routine given to DIM, in order to prevent deadlocks caused by cyclic
     * locking.</em>
     *
     * @param service_name          Name of the service to be subscribed to.
     * @param subscription_type     Type of subscription (<tt>ONCE_ONLY</tt>,
     * <tt>TIMED</tt>, or <tt>MONITORED</tt>.
     * @param interval              Interval in seconds for <tt>TIMED</tt> or
     * <tt>MONITORED</tt> subscriptions.
     * @param buffer                Buffer for receiving the updates.
     * @param buffer_size           Size of the buffer.
     * @param user_routine          User routine that will be called when the
     * service is updated.
     * @param tag                   An integer that will be passed to the user
     * routine.
     * @param error_object          An error object that will be written into
     * the buffer instead of data in case of error (such as connection failure).
     * @param error_object_size     Size of the error object.
     * @return                      A DIM service ID identifying this service
     * subscription.
     *
     * @see ADAPRO::DIM::Wrapper::client_subscribe
     */
    service_id_t client_subscribe_unsafe
    (
            const std::string& service_name,
            int subscription_type,
            int interval,
            void* buffer,
            int buffer_size,
            command_callback_t user_routine = nullptr,
            dim_long tag = 0,
            void* error_object = nullptr,
            int error_object_size = 0
    )
    noexcept;

    /**
     * <p>Like <tt>ADAPRO::DIM::Wrapper::client_subscribe</tt>, but
     * subscribing to services with this procedure enables timestamp queries.
     * </p>
     *
     * @param service_name      Name of the service to subscribe to.
     * @param type              <p>Type of the subscription. The types are:</p>
     * <table>
     *     <tr><td><tt>1</tt></td><td>The client only connects once to the
     *         server, gets the data, and then disconnects. This means that
     *         only one state of the service will be received.</td></tr>
     *     <tr><td><tt>2</tt></td><td>The client will receive data
     *         periodically from the service with an interval specified in
     *         seconds.</td></tr>
     *     <tr><td><tt>4</tt></td><td>The client will receive data from the
     *         server whenever it's updated. Additionally, a time interval
     *         for periodic updates can be specified in seconds. If the
     *         interval is <tt>0</tt>, then there will be no periodic
     *         updates.</td></tr>
     * </table>
     * @param interval          Time interval between updates in seconds.
     * @param buffer            Buffer where the incoming data will be
     * stored in.
     * @param buffer_size       Size of the buffer in bytes.
     * @param user_routine      A callback function for handling the
     * incoming data. This callback function takes the same arguments as the
     * one used in <tt>DIMWrapper::server_add_command</tt>.
     * @param tag               An arbitrary integer, that will be passed
     * by reference to the user routine.
     * @param error_object      An error object, that will be written in the
     * buffer (repeatedly until the whole buffer is filled) in case of an
     * error, if the parameter <tt>error_object_size</tt> was positive.
     * @param error_object_size Size of the error object. If this parameter is
     * set to zero, then <tt>buffer</tt> won't be changed in case of error, but
     * the third (i.e. size) argument of the callback will 0 instead.
     * @return                  An unsigned integer identifying the
     * subscribed service. It can be used for unsubscribing.
     *
     * @see ADAPRO::DIM::Controller
     * @see ADAPRO::DIM::Wrapper::client_subscribe
     */
    service_id_t client_subscribe_stamped
    (
            const std::string& service_name,
            int type,
            int interval,
            void* buffer,
            int buffer_size,
            command_callback_t user_routine = nullptr,
            dim_long tag = 0,
            void* error_object = nullptr,
            int error_object_size = 0
    )
    noexcept;

    /**
     * <p>Requests the execution of a DIM command service.</p>
     *
     * @param service_name  Name of the service.
     * @param buffer        A buffer containing the arguments for the
     * service.
     * @param buffer_size   Size of the given buffer.
     * @throws std::runtime_exception If the command service call failed.
     */
    void client_call_command
    (
            const std::string& service_name,
            void* buffer,
            int buffer_size
    );

    /**
     * Like <tt>client_call_command</tt>, but not thread-safe. More
     * specifically, access to this procedure by multiple threads is not
     * mutually excluded. <em>This procedudre should only be invoked inside a
     * user routine given to DIM, in order to prevent deadlocks caused by cyclic
     * locking.</em>
     *
     * @param service_name  Name of the service.
     * @param buffer        A buffer containing the arguments for the
     * service.
     * @param buffer_size   Size of the given buffer.
     * @throws std::runtime_exception If the command service call failed.
     * @see client_call_command
     */
    void client_call_command_unsafe
    (
            const std::string& service_name,
            void* buffer,
            int buffer_size
    );

    /**
     * <p>Writes the (UNIX) time stamp of the DIM service, identified with the
     * given DIM service ID, to the given output addresses. If this procedure is
     * called from a DIM client callback, then the service ID can be set to 0.
     * <em>In order to be able to use this procedure, the service subscription
     * needs to be performed with <tt>client_subscribe_service_stamped</tt>.
     * </em></p>
     *
     * @param service_id        Identifier of the DIM service. Can be 0 if
     * this procedure will be called from a DIM client callback.
     * @param milliseconds_ptr  Output pointer for the milliseconds part of the
     * timestamp.
     * @param seconds_ptr       Output pointer for the seconds part of the
     * timestamp.
     * @throws std::runtime_error if the timestamp couldn't be written.
     * @see ADAPRO::DIM::Controller
     * @see ADAPRO::DIM::Wrapper::client_subscribe_service_stamped
     */
    void client_write_timestamp
    (
            service_id_t service_id,
            int* milliseconds_ptr,
            int* seconds_ptr
    );

    /**
     * Like <tt>client_write_timestamp</tt>, but not thread-safe. More
     * specifically, access to this procedure by multiple threads is not
     * mutually excluded. <em>This procedudre should only be invoked inside a
     * user routine given to DIM, in order to prevent deadlocks caused by cyclic
     * locking.</em>
     *
     * @param milliseconds_ptr  Output pointer for seconds.
     * @param seconds_ptr       Output pointer for milliseconds.
     *
     * @see client_write_timestamp
     */
    void client_write_timestamp_unsafe
    (
            int* milliseconds_ptr,
            int* seconds_ptr
    );

    /**
     * Adds a user-defined error handler for the DIM client.
     *
     * @param user_routine  The custom error handler.
     *
     * @see ADAPRO::DIM::Controller
     * @see ADAPRO::DIM::Wrapper::server_add_error_handler
     * @see ADAPRO::DIM::Wrapper::DEFAULT_DIM_ERROR_HANDLER
     */
    void client_add_error_handler
    (
            error_callback_t dim_error_handler = default_dim_error_handler
    )
    noexcept;

    /**
     * Asks to DIM remove the subscription for the service with the given
     * ID. This procedure has <em>noexcept</em> guarantee.
     *
     * @param service_id ID of the service for unsubscription.
     * @see ADAPRO::DIM::Controller
     */
    void client_unsubscribe(service_id_t service_id) noexcept;
}
}
}

#endif /* ADAPRO_DIM_WRAPPER_HPP */
#endif /* EXCLUDE_DIM */
