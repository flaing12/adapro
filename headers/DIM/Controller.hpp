/*
 * File:   Controller.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 31 May 2017, 12:14
 */

#ifndef ADAPRO_DIM_CONTROLLER_HPP
#define ADAPRO_DIM_CONTROLLER_HPP

#include <cstdint>
#include <string>
#include <memory>
#include <unordered_set>
#include <map>
#include <utility>
#include <functional>
#include <queue>
#include <mutex>
#include "../control/Worker.hpp"
#include "../data/State.hpp"
#include "../data/Typedefs.hpp"
#include "../DIM/SubscriptionType.hpp"
#include "../DIM/TaskType.hpp"
#include "../DIM/TaskArguments.hpp"
#include "../DIM/Typedefs.hpp"

namespace ADAPRO
{
namespace Library
{
    template<> inline std::string show(const TaskType task_type);
    template<> inline std::string show(const State state);
}
namespace Control
{
    class Logger;
    class Supervisor;
}
namespace DIM
{
    /**
     * <p>ADAPRO DIM Controller is a <tt>ADAPRO::Control::Worker</tt> reponsible
     * for thread-safe serialization of DIM C API (currently through
     * <tt>ADAPRO::DIM::Wrapper</tt>) access.</p>
     * <p>When Controller starts, it sets the DIM DNS node using the
     * configuration provided (by looking for the value of the configuration
     * keys <tt>"ADAPRO_DIM_DNS_NODE"</tt> and <tt>"ADAPRO_DIM_DNS_PORT"</tt>)
     * and starts a DIM server with the name also provided in the configuration
     * (as the value of <tt>"ADAPRO_DIM_SERVER_NAME"</tt>). Also, the CPU
     * affinity of Controller is configurable (using the configuration parameter
     * <tt>"ADAPRO_DIM_CONTROLLER_CORE"</tt>).</p>
     * <p>When in state <tt>ADAPRO::Data::State::RUNNING</tt> or
     * <tt>ADAPRO::Data::State::PAUSED</tt>, controller accepts <tt>tasks</tt>,
     * or pairs of <tt>ADAPRO::Data::TaskType</tt> and
     * <tt>std::shared_ptr&lt;ADAPRO::DIM::Controller::TaskArgument&gt;</tt>
     * objects, storing them in a queue. During its <tt>execute</tt> method,
     * controller takes one task at a time from the queue and performs the
     * appropriate action using <tt>ADAPRO::DIM::Wrapper</tt>. <em>Because
     * Controller is a Thread, it only executes <tt>execute</tt> when in state
     * <tt>RUNNING</tt>, even though tasks can be appended to the queue also
     * when Controller is <tt>PAUSED</tt>.</em></p>
     * <p>Controller automatically takes care of releasing DIM (server) services
     * and (client) subscriptions when shutting down, during <tt>finish</tt>.
     * This happens also when Controller aborts.
     * </p>
     *
     */
    class Controller final: public ADAPRO::Control::Worker
    {
        friend ADAPRO::Control::Supervisor;

        /**
         * A helper function that dereferences the given shared pointer, for
         * reducing the amount of boilerplate.
         *
         * @param pointer   The shared pointer to the task argument.
         * @return          The task argument.
         */
        template<typename T> T& derefer(const task_argument_t pointer)
        {
            return *static_pointer_cast<T>(pointer);
        }

        /**
         * Used for ensuring mutual exclusion for the task queue access.
         */
        std::mutex task_mutex;

        /**
         * Used for signaling Controllre that there are tasks waiting.
         */
        std::condition_variable tasks_waiting;

        /**
         * A queue containing tasks to be executed following FIFO principle.
         */
        std::queue<task_t> task_queue;

        /**
         * The set of the IDs of DIM services published by Controller.
         */
        std::unordered_set<service_id_t> services;

        /**
         * The set of the IDs of DIM command services published by Controller.
         */
        std::unordered_set<service_id_t> commands;

        /**
         * The set of the IDs of DIM services subscribed by Controller.
         */
        std::unordered_set<service_id_t> subscriptions;

        /**
         * Carries out the given task.
         *
         * @param task  Task to be carried out.
         */
        void carry_out(task_t&& task);

        /**
         * Carries out a service publishing task.
         *
         * @param argument The service to be published.
         * @throws std::runtime_error
         */
        void publish_service(ServicePublication& argument);

        /**
         * Carries out a command service publishing task.
         *
         * @param argument The command service to be published.
         */
        void publish_command(CommandPublication& argument);

        /**
         * Carries out a service update task.
         *
         * @param argument The service to be updated.
         */
        void update_service(ServiceID& argument);

        /**
         * Carries out a service subscription task.
         *
         * @param argument The service to be subscribed to.
         */
        void subscribe_to_service(ServiceSubscription& argument);

        /**
         * Carries out a command invocation task.
         *
         * @param argument The command to be invoked.
         */
        void call_command(CommandInvocation& argument);

        /**
         * Carries out a timestamp retrieval task.
         *
         * @param argument  The timestamp argument.
         */
        void overwrite_timestamp(TimestampRequest& argument);

        /**
         * Emplaces a new task into the task queue.
         *
         * @param task_type     Type of the task.
         * @param task_argument Argument of the task.
         */
        inline void add_to_queue
        (
                TaskType&& task_type,
                task_argument_t&& task_argument
        )
        {
            if (state_in(ADAPRO::Library::state_mask(task_type)))
            {
                unique_lock<mutex> lock(task_mutex);
                task_queue.emplace(task_type, task_argument);
                tasks_waiting.notify_one();
            }
            else
            {
                throw std::runtime_error
                (
                        "DIM Controller cannot accept " +
                        ADAPRO::Library::show(task_type) + " while " +
                        ADAPRO::Library::show(get_state()) + "."
                );
            }
        }

    protected:
        void prepare() override;
        void execute() override;
        void finish() override;

    public:

        /**
         * Constructor for DIM Controller. <em>NB: The arguments related to DIM
         * server mode require ADAPRO to be built with DIM support and the
         * configuration parameter <tt>DIM_SERVER_ENABLED</tt> to have value
         * <tt>TRUE</tt>. Otherwise, they have no effect.</em> This constructor
         * sets the DIM DNS node and port, enques ADAPRO services for
         * publishing, but does not start the DIM server. This constructor
         * delivers <tt>noexcept</tt> guarantee. If DIM server mode parameters
         * were not properly verified, this thread may abort when started.
         *
         * @param logger        Logger of the ADAPRO Session.
         * @param supervisor    Supervisor of the ADAPRO Session.
         * @param configuration Configuration of the ADAPRO Session.
         * @param dim_command_factories List of the factory functions providing
         * user-defined DIM commands. <em>Requires DIM server mode.</em>
         * @param dim_client_error_handler The DIM client error handler. <em>
         * Requires DIM server mode.</em>
         * @param dim_server_error_handler The DIM server error handler. <em>
         * Requires DIM server mode.</em>
         * @param state_ptr             Pointer to the memory address used for
         * storing the state of this Supervisor. <em>Requires DIM server mode.
         * </em>
         */
        Controller
        (
                ADAPRO::Control::Logger& logger
                , ADAPRO::Control::Supervisor& supervisor
                , const ADAPRO::Data::config_t& configuration
#ifndef EXCLUDE_DIM
                , list<command_factory_t>&& dim_command_factories
                , error_callback_t dim_client_error_handler
                , error_callback_t dim_server_error_handler
                , uint32_t* state_ptr
#endif
        )
        noexcept;

        /**
         * Appends a DIM service publishing task to the internal queue. The task
         * will be executed asynchronously. <em>The pointer <tt>buffer</tt> in
         * the <tt>ServicePublication</tt> object will be given directly to DIM
         * (i.e. without copying the contents).</em> As far as the mutual
         * exclusion to DIM API calls is concerned, this method is thread-safe.
         * This method must be invoked <em>before</em> starting the Controller
         * Thread. For more information on publishing DIM services, see the
         * documentation for <tt>ADAPRO::DIM::Wrapper</tt>.
         *
         * @param ptr A shared pointer to the
         * <tt>ADAPRO::DIM::ServicePublication</tt> object containing the
         * parameters for the DIM C API call that will be done using
         * <tt>ADAPRO::DIM::Wrapper::server_add_service</tt>.
         * @see ADAPRO::DIM::Wrapper::server_add_service
         * @throws std::runtime error if controller was not <tt>READY</tt> when
         * this method was invoked.
         */
        inline void publish(std::shared_ptr<ServicePublication> ptr)
        {
            add_to_queue(PUBLISH_SERVICE, std::move(ptr));
        }

        /**
         * Appends a DIM command service publishing task to the internal queue.
         * The task will be executed asynchronously. As far as the mutual
         * exclusion to DIM API calls is concerned, this method is thread-safe.
         * This method must be invoked <em>before</em> starting the Controller
         * Thread. For more information on publishing DIM command services, see
         * the documentation for <tt>ADAPRO::DIM::Wrapper</tt>.
         *
         * @param ptr A shared pointer to the
         * <tt>ADAPRO::DIM::CommandPublication</tt> object containing the
         * parameters for the DIM C API call that will be done using
         * <tt>ADAPRO::DIM::Wrapper::server_add_command</tt>.
         * @see ADAPRO::DIM::Wrapper::server_add_command
         * @throws std::runtime error if controller was not <tt>READY</tt> when
         * this method was invoked.
         */
        inline void publish(std::shared_ptr<CommandPublication> ptr)
        {
            add_to_queue(PUBLISH_COMMAND, std::move(ptr));
        }

        /**
         * Appends a DIM service update task to the internal queue. This task
         * will be executed asynchronously. As far as the mutual exclusion to
         * DIM API calls is concerned, this method is thread-safe. This method
         * must be invoked when the Controller is <tt>RUNNING</tt> or
         * <tt>PAUSED</tt>. For more information on updating DIM services, see
         * the documentation for <tt>ADAPRO::DIM::Wrapper</tt>.
         *
         * @param ptr A shared pointer to the boxed DIM service ID value
         * identifying the service that needs to be updated.
         * @see ADAPRO::DIM::Wrapper::server_update_service
         * @throws std::runtime error if controller was not <tt>RUNNING</tt> or
         * <tt>PAUSED</tt> when this method was invoked.
         */
        inline void update(std::shared_ptr<ServiceID> ptr)
        {
            add_to_queue(UPDATE_SERVICE, std::move(ptr));
        }

        /**
         * Appends a DIM service subscription task to the internal queue. This
         * task will be executed asynchronously. <em>The pointer <tt>buffer</tt>
         * in the <tt>ServiceSubscription</tt> object will be given directly to
         * DIM (i.e. without copying the contents).</em> As far as the mutual
         * exclusion to DIM API calls is concerned, this method is thread-safe.
         * This method must be invoked when the controller is <tt>READY</tt>,
         * <tt>RUNNING</tt>, or <tt>PAUSED</tt>. For more information on
         * subscribing to DIM services, see the documentation for
         * <tt>ADAPRO::DIM::Wrapper</tt>.
         *
         * @param ptr A shared pointer to the
         * <tt>ADAPRO::DIM::ServiceSubscription</tt> object containing the
         * parameters for the DIM C API call that will be done using
         * <tt>ADAPRO::DIM::Wrapper::client_subscribe</tt> or
         * <tt>ADAPRO::DIM::Wrapper::client_subscribe_stamped</tt>. The former
         * Wrapper procedure will be used if <tt>!(ptr->with_timestamps)</tt>,
         * and the latter will be used otherwise.
         * @throws std::runtime error if controller was not <tt>READY</tt>,
         * <tt>RUNNING</tt>, or <tt>PAUSED</tt> when this method was invoked.
         */
        inline void subscribe(std::shared_ptr<ServiceSubscription> ptr)
        {
            add_to_queue(SUBSCRIBE_TO_SERVICE, std::move(ptr));
        }

        /**
         * Appends a DIM command service call task to the internal queue. This
         * task will be executed asynchronously. <em>The pointer <tt>buffer</tt>
         * in the <tt>CommandInvocation</tt> object will be given directly to
         * DIM (i.e. without copying the contents).</em> As far as the mutual
         * exclusion to DIM API calls is concerned, this method is thread-safe.
         * This method must be invoked when the controller is <tt>READY</tt>,
         * <tt>RUNNING</tt>, or <tt>PAUSED</tt>. For more information on
         * calling DIM command services, see the documentation for
         * <tt>ADAPRO::DIM::Wrapper</tt>.
         *
         * @param ptr A shared pointer to the
         * <tt>ADAPRO::DIM::CommandInvocation</tt> object containing the
         * parameters for the DIM C API call that will be done using
         * <tt>ADAPRO::DIM::Wrapper::client_call_command</tt>.
         * @throws std::runtime error if controller was not <tt>READY</tt>,
         * <tt>RUNNING</tt>, or <tt>PAUSED</tt> when this method was invoked.
         */
        inline void call(std::shared_ptr<CommandInvocation> ptr)
        {
            add_to_queue(CALL_COMMAND, std::move(ptr));
        }

        /**
         * Appends a DIM timestamp retrieval task to the internal queue. This
         * task will be executed asynchronously. <em>The pointers
         * <tt>milliseconds_ptr</tt> and <tt>seconds_ptr</tt> will be given
         * directly to DIM (i.e. without copying the contents).</em> As far as
         * the mutual exclusion to DIM API calls is concerned, this method is
         * thread-safe. This method must be invoked when the controller is
         * <tt>RUNNING</tt> or <tt>PAUSED</tt>. For more information on
         * retrieving DIM timestamps, see the documentation for
         * <tt>ADAPRO::DIM::Wrapper</tt>.
         *
         * @param ptr A shared pointer to the <tt>TimestampRequest</tt> object
         * containing the parameters for the IDM C API call that will be done
         * using <tt>ADAPRO::DIM::Wrapper::client_write_timestamp</tt>.
         * @see ADAPRO::DIM::Wrapper::client_write_timestamp
         * @throws std::runtime error if controller was not <tt>RUNNING</tt> or
         * <tt>PAUSED</tt> when this method was invoked.
         */
        inline void overwrite(std::shared_ptr<TimestampRequest> ptr)
        {
            add_to_queue(OVERWRITE_TIMESTAMP, std::move(ptr));
        }

        virtual ~Controller() noexcept { join(); };
    };
}
}

#endif /* ADAPRO_DIM_CONTROLLER_HPP */