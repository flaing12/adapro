/*
 * File:   SubscriptionType.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 31 May 2017, 10:22
 */

#ifndef ADAPRO_DIM_SUBSCRIPTION_TYPE_HPP
#define ADAPRO_DIM_SUBSCRIPTION_TYPE_HPP

//#ifndef EXCLUDE_DIM
//#undef ONCE_ONLY
//#undef TIMED
//#undef MONITORED
//#endif /* EXCLUDE_DIM */

namespace ADAPRO
{
namespace DIM
{
    /**
     * SubscriptionType is a simple enum corresponding with the relevant DIM
     * service type definitions in <tt>&lt;dim_common.h&gt;</tt>. It is used for
     * subscribing to services as a DIM client.
     */
    enum SubscriptionType
    {
        /**
         * When subscribing to a service with <tt>ONCE_ONLY</tt>, the client
         * subscribes and unsubscribes immediately. <em>The service shouldn't be
         * unsubscribed by the user.</em>
         */
        ONCE_ONLY    = 0x01,

        /**
         * When subscribing to a service with <tt>TIMED</tt>, the client will
         * recieve periodical updates to the service, independent of the
         * possible changes in the state of the service.
         */
        TIMED        = 0x02,

        /**
         * When subscribing to a service with <tt>TIMED</tt>, the client will
         * receive updates to the service, when the state of the service
         * changes, and optionally also on regular intervals.
         */
        MONITORED    = 0x04
    };
}
}

//#ifndef EXCLUDE_DIM
//#define ONCE_ONLY 0x01
//#define TIMED 0x02
//#define MONITORED 0x04
//#endif /* EXCLUDE_DIM */
#endif /* ADAPRO_DIM_SUBSCRIPTION_TYPE_HPP */