#!/bin/bash

wget https://dim.web.cern.ch/dim/dimNoWebDID_v20r20.zip > /dev/null && \
unzip dimNoWebDID_v20r20.zip > /dev/null && \
cd dimNoWebDID_v20r20 && \
mkdir -p /usr/lib/dim/ && \
cp linux/lib* /usr/lib/dim/ && \
chmod -R a+x /usr/lib/dim/ && \
mkdir -p /usr/include/dim/ && \
cp dim/* /usr/include/dim
