# ADAPRO

This repository is forked from https://gitlab.cern.ch/adapos/adapro.

ADAPRO stands for *ALICE Data Point Processing Framework*, for it originates
from a software architecture used as a part of the ALICE O² project at CERN [1]
(see also http://alice-collaboration.web.cern.ch/). It is a C++14 software
framework, licenced under the Apache Licence version 2.0 (see `LICENSE`),
producing multi-threaded applications with optional support for configuration
file access, remote control and monitoring, and running an application as a
daemon process.

Remote control and monitoring is provided through the Distributed Information
Management (DIM) protocol and daemon mode support by Systemd. The framework has
been developed in a Debian GNU/Linux environment with the usual GNU toolchain
(https://www.gnu.org/) and the NetBeans IDE (see https://netbeans.org/). It
should be portable to at least other Linux distributions and POSIX-compliant
systems. This readme is a mere quick start guide. More information can be found
in the API documentation and the manual.

The main concepts of the framework are *Finite State Machine* (FSM), *Thread*,
*Worker*, *Supervisor*, and *Session*, which are represented as C++ classes.
Essentially, ADAPRO is just a piece of machinery doing the bookkeeping necessary
for managing Workers. Workers are responsible for the application's domain
logic. See the manual for in-depth discussion on these topics. The API
documentation for the latest tagged release is available online at:
https://jllang.gitlab.io/adapro/

## 1. Programming Contract

The specification and implementation of ADAPRO Session mechanism has been
partially formally verified using the Spin
(see http://spinroot.com/spin/whatispin.html) and DIVINE (see
https://divine.fi.muni.cz/) model checkers. The results of this verification
effort will be published in the Proceedings of the The 27th ACM Joint European
Software Engineering Conference and Symposium on the Foundations of Software
Engineering [2]. The key assumptions for the verification project, which can
also be thought of as the *preconditions* of the (non-legally binding)
programming contract offered by ADAPRO. Here, the term *user-defined code* means
any C++ statements or expressions. The preconditions are the following:
1.  There will be no sabotage or force majeur incidents of any kind whatsoever.
2.  The C++ compiler, standard library and runtime system, the operating system
    and the hardware platform function correctly with respect to their
    specifications.
3.  The operating system scheduler satisfies weak fairness, i.e. the property
    that every native thread that is eventually always executable, will be run
    on a processor unboundedly many times.
4.  Allocating memory for new objects always succeeds, i.e. the machine will not
    run out of (virtual) memory.
5.  User-defined code always terminates.
6.  Unless explicitly permitted to do so, by ADAPRO manual or the API
    documentation, user-defined code will not modify or delete any object owned
    by the framework.
7.  User-defined code never
    * calls `std::abort`;
    * raises a signal that can't handled;
    * causes deadlocks (or livelocks);
    * uses Thread trigger methods inappropriately; or
    * triggers a known issue in the framework.
8.  User-defined code conforms to the well-known *Resource Aqcuisition is
    Initialization* (RAII) principle, so that all memory and resources acquired
    by user-defined code will be automatically released when ownership expires
    (e.g. a destructor of a user-defined object, holding a resource, is
    invoked).
9.  In DIM server mode or daemon mode, the user-defined code does not directly
    interact with DIM or Systemd libraries respectively.

Assuming these nine preconditions, ADAPRO aims to satisfy the following nine
*postconditions* (don't try to understand them on the first reading):

1.  At all times, every Thread satisfies its FSM constraints.
2.  When a Session is running, Supervisor is the first Thread to start and the
    last one to halt.
3.  Supervisor constructs and destroys Workers.
4.  Supervisor never aborts.
5.  Supervisor stops if either all Workers have stopped or if there are Workers
    that have aborted.
6.  After being started during the Session startup sequence, Supervisor enters
    into a state transition if and only if postcondition 5 above applies or if a
    signal or DIM command triggers a Session-wide state transition. In these
    cases, Supervisor shall propagate the appropriate command to all Workers and
    wait until they have finished their own respective state transitions.
7.  An ADAPRO application shall be responsive in the sense of *weak fairness*
    and *liveness*. These kind of properties express the ideas that an
    application will not get stuck and that every command triggers the correct
    action eventually. Real time constraints haven't been considered yet,
    though.
8.  In addition to these properties, ADAPRO shall satisfy usual (type) safety
    properties, such as absence of runtime errors and illegal memory accesses.
9.  An ADAPRO application shall not produce memory leaks.

These postconditions have been formalized into a theory of *Linear
Temporal Logic* (LTL) (with first-order quantifiers ranging over Threads). The
Promela model in `models/promela` has been verified to satisfy this theory, with
the Spin model checker. The C++ implementation has not yet been fully verified
with the DIVINE model checker.

## 2. Warnings

1.  There are known issues. Consult the manual and the issue tracker.
2.  Backwards compatibility is often broken in updates. *Caveat emptor*!
3.  The API documentation and comments in the source code or example application
    configuration files may be outdated. The manual should always have the
    highest authority (except when it also contains outdated parts).
4.  It seems that NetBeans likes to use absolute paths in the makefiles by
    default.
    This behaviour can be corrected by setting *Tools -> Options -> C/C++ ->
    Project Options -> File Path Mode* to `Always Relative`. *Using absolute
    paths in makefiles breaks the build on other machines.*
5.  NetBeans seems to like automatically adding include entries such as
    `#include <adapro/data/StatusFlags.hpp>` that tend to break the builds in
    systems which don't have ADAPRO headers installed in a system include
    directory (see also the `g++ -I` option). Includes in the source code should
    use relative paths instead (e.g. `#include "../../data/StatusFlags.hpp"`
    depending on where the including file is).

## 3. Dependencies

This project depends on
* `g++` with C++14 support (version 6.2 or higher is recommended)
  * Alternatively, `clang++` works also. To use the LLVM toolchain with
    Netbeans, right-click the name of the project, select
    *Properties* -> *Build* -> *Tool Collection* -> *LLVM*.
* `git` (and GitLab, of course)
* All the usual tools for building C++ projects (standard headers, `make`, etc.)
* `pkg-config`
* systemd headers. The required dpkg package on Debian is `libsystemd-dev`, and
  the required rpm package on CentOS is `systemd-devel`.

Without these pieces of software, it's not possible to obtain and compile ADAPRO
from the source code on Linux. Precompiled shared objects are available at the
*Pipelines* section of this repository. Compiling the source code is the
recommended way for obtaining the shared object.

Additionally, the project has the following *optional* dependencies:
* `bash` (version 4 or higher) to run various scripts
* `cppcheck` for static analysis
* `cppunit` for tests
* `dia` for document generation
* `dim` for distributed messaging and remote procedure calls (core version
  without webDID suffices)
* `divine` for model checking the implementation
* `doxygen` (for document generation; *version 1.8.13 doesn't work*)
  * depends on `graphviz` (`dot`)
* `gdb` for debugging
* `gnuplot` for plotting memory and CPU time consumption when running DIVINE
* `NetBeans` IDE (version 8.1 or higher recommended, with C++ support, duh)
* `pdflatex` (for document generation)
  * many (La)TeX packages are needed, see `Dockerfile`
* `pgrep` for DIVINE runner script (in Debian, this is contained in the package
  `procps`)
* `spin` for model checking the specification
* `time` for getting some stats when running DIVINE
* `valgrind` for memory debugging

Optional dependencies are used for developing ADAPRO, and they're not needed for
building/installation/deployment.

The following versions, for example, are known to work:
* `bash 4.4.12(1)-release (x86_64-pc-linux-gnu)`
* `cppcheck 1.76.1`
* `dia 0.97+git`
* `dim 20r19`
* `divine 4.1.20+2018.12.17`
* `doxygen 1.7`
* `g++ 6.3.0` (or `clang++ 7`)
* `gdb 7.12`
* `git 2.11.0`
* `gnuplot 5.0 patchlevel 5`
* `graphviz 2.38.0-17`
* `LaTeX 2016.20170`
* `lcov 1.13`
* `libcppunit-dev 1.13.2`
* `libsystemd-dev 232-25+deb9u1`
* `make 4.1`
* `NetBeans 8.1`
* `pdflatex 3.14159265-2.6-1.40.17`
* `pgrep 3.3.12`
* `pkg-config 0.2.9`
* `spin 6.4.5`
* `time 1.7`
* `valgrind 3.12.0.SVN`

Your mileage may vary on the names of the software packages, depending on the
operating system and distribution.

## 4. Compiling and Installing

This section assumes that the (non-optional) dependencies are installed.

As mentioned in the previous section, precompiled shared objects are available
at the *Pipelines* section of the ADAPRO GitLab repository. They may or may not
work on any particular setup. Tagged releases in general should be more stable
and trustworthy than random commits. Therefore, *tagged commits are recommended
for the users of this framework*, while others represent the bleeding edge of
the development of ADAPRO and may thus be fragile or broken.

Alternatively, the project can be built by simply running `make` at project root
directory.
  * Optionally, it can be built with the `Debug` (or `No_DIM`) configuration by
  running `make CONF=Debug`. In debug mode, ADAPRO prints debug messages into
  `stdout` in addition to the error messages that are always printed into
  `stderr`.

The shared object containing ADAPRO runtime will be created at
  * `dist/Release/GNU-Linux/libadapro.so` for `Release` configuration;
  * `dist/Debug/GNU-Linux/libadapro.so` for `Debug` configuration; or
  * `dist/No_DIM/GNU-Linux/libadapro-no-dim.so` for `No_DIM` configuration.

A system-wide installation of ADAPRO can be done by invoking `make install` with
the *super user* (root) rights, or by performing the equivalent actions by hand:
  * The makefile target will copy the headers to `/usr/include/adapro`.
  * The shared object will be copied to `/usr/lib/adapro/libadapro.so`
    * `Release` configuration has priority over `Debug`. `Release` and `Debug`
      shared objects can't both be installed at the same time.
    * Optionally, a shared object compiled with the `No_DIM` configuration can
      be also installed at `/usr/lib/adapro/libadapro-no-dim.so`.
  * A symbolic link pointing to `/usr/lib/adapro/libadapro.so` will be created
    with the name `/usr/lib/libadapro.so`.
    * Also, a symbolic link pointing to `/usr/lib/adapro/libadapro-no-dim.so`
      with the name `/usr/lib/libadapro-no-dim.so` will be created if the
      `No_DIM` version of the shared object existed.

## 5. Testing the Installation

The installation can be tested by compiling and running the example applications
located in `examples/`. For instance, the `Toy Application` can be compiled by
running `cd examples/ToyApplication/ && make && ./toy_application`. A successful
execution on a Debian GNU/Linux system might look like the following:
```
g++ -g -std=c++14 -Werror=pedantic -DEXCLUDE_DIM -I../../headers \
ToyApplication.cpp -o toy_application -lpthread -latomic -ladapro-no-dim
[2019-01-21 13:56:01] (A) <main> Affinity set to core 1.
[2019-01-21 13:56:01] (W) <main> Couldn't set scheduling parameters:
    "Operation not permitted"
[2019-01-21 13:56:01] (I) ENVIRONMENT
-------------------------------------

    ADAPRO Version          5.0.0 pre-release
    Application             Toy Application
    Application Version     5.0.0
    Command Line Arguments  [none]
    Configuration File      ToyApplication.conf
    Host                    my_computer
    PID                     12345
    User                    johndoe

================================================================================

[2019-01-21 13:56:01] (I) CONFIGURATION
---------------------------------------

    ADAPRO_COLOURS_ENABLED      TRUE
    ADAPRO_DAEMON_ENABLED       FALSE
    ADAPRO_DAEMON_LOGFILE       example.log
    ADAPRO_DIM_CALLBACK_CORE    -1
    ADAPRO_DIM_CONTROLLER_CORE  1
    ADAPRO_DIM_DNS_NODE         localhost
    ADAPRO_DIM_DNS_PORT         2505
    ADAPRO_DIM_SERVER_ENABLED   FALSE
    ADAPRO_DIM_SERVER_NAME      ADAPRO_Example_Application
    ADAPRO_LOCK_MEMORY          FALSE
    ADAPRO_LOGGING_MASK         ADISWEF
    ADAPRO_MAIN_CORE            1
    ADAPRO_NICE                 10
    ADAPRO_SERIALIZE_COMMANDS   TRUE
    ADAPRO_SUPERVISOR_CORE      1
    DUMMY                       PARAMETER
    USER_GREETING               Hello,_World!
    USER_TARGET                 3

================================================================================

[2019-01-21 13:56:01] (A) <Metronome<1000>-0> Affinity set to core 2.
[2019-01-21 13:56:01] (W) <Metronome<1000>-0,READY,START,2> Couldn't set scheduling parameters:
    "Operation not permitted"
[2019-01-21 13:56:01] (A) <Metronome<1000>-0,STARTING,START,2> Starting...
[2019-01-21 13:56:01] (A) <Metronome<1000>-0,RUNNING,CONTINUE,2> Started.
[2019-01-21 13:56:01] (A) <AOFStream<DPCOM>-0> Ignoring negative affinity -1.
[2019-01-21 13:56:01] (A) <AOFStream<DPCOM>-0,STARTING,START,1> Starting...
[2019-01-21 13:56:01] (A) <AOFStream<DPCOM>-0,RUNNING,CONTINUE,1> Started.
[2019-01-21 13:56:01] (S) <Session> Ready.
[2019-01-21 13:56:01] (S) <Session> Starting...
[2019-01-21 13:56:01] (A) <Supervisor> Affinity set to core 1.
[2019-01-21 13:56:01] (W) <Supervisor,READY,START,1> Couldn't set scheduling parameters:
    "Operation not permitted"
[2019-01-21 13:56:01] (A) <Supervisor,STARTING,START,1> Starting...
[2019-01-21 13:56:01] (A) <HelloWorldThread> Ignoring negative affinity -1.
[2019-01-21 13:56:01] (A) <HelloWorldThread,STARTING,START,1> Starting...
[2019-01-21 13:56:01] (D) HelloWorldThread goes to STARTING...
[2019-01-21 13:56:01] (I) <HelloWorldThread,STARTING,CONTINUE,1> Hello,_World!
[2019-01-21 13:56:01] (A) <HelloWorldThread,RUNNING,CONTINUE,1> Started.
[2019-01-21 13:56:01] (D) HelloWorldThread goes to RUNNING...
[2019-01-21 13:56:01] (I) <HelloWorldThread,RUNNING,CONTINUE,1> Working hard...
[2019-01-21 13:56:01] (A) <Supervisor,RUNNING,CONTINUE,1> Started.
[2019-01-21 13:56:01] (A) <Supervisor,RUNNING,CONTINUE,1> Not propagating the first CONTINUE command.
[2019-01-21 13:56:01] (S) <Session> Started.
[2019-01-21 13:56:02] (I) <HelloWorldThread,RUNNING,CONTINUE,1> Working hard...
[2019-01-21 13:56:03] (I) <HelloWorldThread,RUNNING,CONTINUE,1> Working hard...
[2019-01-21 13:56:04] (I) <HelloWorldThread,RUNNING,CONTINUE,1> Enough for today.
[2019-01-21 13:56:04] (A) <HelloWorldThread,STOPPING,STOP,1> Stopping...
[2019-01-21 13:56:04] (D) HelloWorldThread goes to STOPPING...
[2019-01-21 13:56:04] (I) <HelloWorldThread,STOPPING,STOP,1> Cleaning up...
[2019-01-21 13:56:04] (A) <AOFStream<DPCOM>-0,STOPPING,STOP,1> Stopping...
[2019-01-21 13:56:04] (A) <AOFStream<DPCOM>-0,STOPPING,STOP,1> Closing file "example.csv"...
[2019-01-21 13:56:04] (A) <AOFStream<DPCOM>-0,STOPPED,STOP,1> Stopped.
[2019-01-21 13:56:04] (D) HelloWorldThread goes to STOPPED...
[2019-01-21 13:56:04] (A) <HelloWorldThread,STOPPED,STOP,1> Stopped.
[2019-01-21 13:56:05] (I) <Supervisor,RUNNING,CONTINUE,1> Stopping because all workers have stopped.
[2019-01-21 13:56:05] (A) <Supervisor,STOPPING,STOP,1> Stopping...
[2019-01-21 13:56:05] (A) <Supervisor,STOPPING,STOP,1> Didn't stop "HelloWorldThread", that was already STOPPED.
[2019-01-21 13:56:05] (A) <Supervisor,STOPPING,STOP,1> Waiting for HelloWorldThread to stop gracefully...
[2019-01-21 13:56:05] (A) <Supervisor,STOPPING,STOP,1> HelloWorldThread stopped in time.
[2019-01-21 13:56:05] (A) <Supervisor,STOPPED,STOP,1> Stopped.
[2019-01-21 13:56:05] (S) <Session> Stopping...
[2019-01-21 13:56:05] (S) <Session> Stopped.
[2019-01-21 13:56:05] (I) <HelloWorldThread,STOPPED,STOP,1> Hopefully I have finished my computations by now...
[2019-01-21 13:56:06] (A) <Metronome<1000>-0,STOPPING,STOP,2> Stopping...
```
Again, your mileage may vary. For discussion on this example application, see
the comments in source files, API documentation, and the manual.

## 6. Building Documentation

This section assumes that the optional dependencies needed for building the
documentation are intalled.

To build the API Documentation, run `make apidoc` at the project root directory.
The HTML version of the API Documentation will appear at `dist/apidoc/html`.
The PDF version generated with LaTeX will appear at
`dist/apidoc/pdf/refman.pdf`.

To build the Technical Documentation, run `make techdoc` at the project root
directory. The PDF file, will appear at `dist/doc/TechnicalDocumentation.pdf`.

## 7. Running Unit Tests and Analysis Tools

This section assumes that the optional dependencies for unit testing and static
analysis are intalled.

To run the unit tests, run `make test CONF=No_DIM` at the project root
directory. The output of the tests should go to `stdout` and/or `stderr`.
Running the tests will take some time.

Optionally, a code coverage report can be generated after the tests by running
`make lcov`. The results will appear in `dist/reports/lcov`.

Static analysis can be performed on the code by running `make cppcheck`. The
results will appear in `dist/reports/cppcheck`.

## 8. Running the Model Checkers

This section assumes that the model checkers are installed. The model checker
Spin can be run with `make spin` on the root directory.

The DIVINE model checker can be used for verifying the toy application in
`examples/ToyApplication/`, or the three programs specifically crafted for
DIVINE at `models/divine/`.

On Linux system, the models in `models/divine/` can be verified with the script
`run_divine.sh` in a GNU BASH shell. It requires the following five arguments:
1.  `check` or `verify` to select between `divine check` and `divine verify`
    respectively. See the DIVINE manual for details.
2.  `worker`, `supervisor`, or `session` to select the model. These models can
    be used for verifying the corresponding ADAPRO classes.
3.  Number of threads for model checking (a positive integer).
4.  Memory limit, in GiB as a positive integer.
5.  A polling delay in seconds, used for updating a graph of CPU time and memory
    consumption during model checking. (During the verification run, a temporary
    graph can be found at `/tmp/divine/graph.png`.)

Report files will emerge in `dist/divine/<name-of-the-model>`.

## 9. References

[1] J.L. Lång et al., “ADAPOS: An Architecture for Publishing ALICE DCS
    Conditions Data”, in Proc. 16th Int. Conf. on Accelerator and Large
    Experimental Physics Control Systems (ICALEPCS'17), Barcelona, Spain, Oct.
    2017, paper TUPHA042, pp. 482-485, ISBN: 978-3-95450-193-9,
    https://doi.org/10.18429/JACoW-ICALEPCS2017-TUPHA042, 2018.

[2] John Lång and I.S.W.B. Prasetya. 2019. Model Checking a C++ Software
    Framework, a Case Study. In Proceedings of the 27th ACM Joint European
    Software Engineering Conference and Symposium on the Foundations of Software
    Engineering (ESEC/FSE ’19), August 26–30, 2019, Tallinn, Estonia. ACM, New
    York, NY, USA, 11 pages. https://doi.org/10.1145/3338906.3340453
