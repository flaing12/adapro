#
#  There exist several targets which are by default empty and which can be 
#  used for execution of your targets. These targets are usually executed 
#  before and after some main targets. They are: 
#
#     .build-pre:              called before 'build' target
#     .build-post:             called after 'build' target
#     .clean-pre:              called before 'clean' target
#     .clean-post:             called after 'clean' target
#     .clobber-pre:            called before 'clobber' target
#     .clobber-post:           called after 'clobber' target
#     .all-pre:                called before 'all' target
#     .all-post:               called after 'all' target
#     .help-pre:               called before 'help' target
#     .help-post:              called after 'help' target
#
#  Targets beginning with '.' are not intended to be called on their own.
#
#  Main targets can be executed directly, and they are:
#  
#     build                    build a specific configuration
#     clean                    remove built files from a configuration
#     clobber                  remove all built files
#     all                      build all configurations
#     help                     print help mesage
#  
#  Targets .build-impl, .clean-impl, .clobber-impl, .all-impl, and
#  .help-impl are implemented in nbproject/makefile-impl.mk.
#
#  Available make variables:
#
#     CND_BASEDIR                base directory for relative paths
#     CND_DISTDIR                default top distribution directory (build artifacts)
#     CND_BUILDDIR               default top build directory (object files, ...)
#     CONF                       name of current configuration
#     CND_PLATFORM_${CONF}       platform name (current configuration)
#     CND_ARTIFACT_DIR_${CONF}   directory of build artifact (current configuration)
#     CND_ARTIFACT_NAME_${CONF}  name of build artifact (current configuration)
#     CND_ARTIFACT_PATH_${CONF}  path to build artifact (current configuration)
#     CND_PACKAGE_DIR_${CONF}    directory of package (current configuration)
#     CND_PACKAGE_NAME_${CONF}   name of package (current configuration)
#     CND_PACKAGE_PATH_${CONF}   path to package (current configuration)
#
# NOCDDL


# Environment 
MKDIR=mkdir
CP=cp
CCADMIN=CCadmin
LANG=en

# build
build: .build-post

.build-pre:
# Add your pre 'build' code here...

.build-post: .build-impl

# clean
clean: .clean-post

.clean-pre:
# Add your pre 'clean' code here...

.clean-post: .clean-impl
# Add your post 'clean' code here...


# clobber
clobber: .clobber-post

.clobber-pre:
# Add your pre 'clobber' code here...

.clobber-post: .clobber-impl
# Add your post 'clobber' code here...


# all
all: .all-post

.all-pre:
# Add your pre 'all' code here...

.all-post: .all-impl
# Add your post 'all' code here...


# build tests
build-tests: .build-tests-post

.build-tests-pre:
# Add your pre 'build-tests' code here...

.build-tests-post: .build-tests-impl
# Add your post 'build-tests' code here...


# run tests
test: .test-post

.test-pre: build-tests
# Add your pre 'test' code here...

.test-post: .test-impl
# Add your post 'test' code here...

# Check the SPIN model
spin:
	cd ./models/promela/ && make

# help
help: .help-post

.help-pre:
# Add your pre 'help' code here...

.help-post: .help-impl
# Add your post 'help' code here...



# include project implementation makefile
include nbproject/Makefile-impl.mk

# include project make variables
include nbproject/Makefile-variables.mk

apidoc:
	export DOT_PATH=/usr/local/bin
	mkdir -p dist/apidoc
	doxygen
	#cd dist/apidoc/latex && make && mkdir -p ../pdf && mv refman.pdf ../pdf/

manual:
	cd doc && ./topdf.sh && cd ..

cppcheck:
	mkdir -p dist/reports/cppcheck
	cppcheck --enable=all . --xml 1> /dev/null 2> dist/reports/cppcheck/results.xml

install:
	mkdir -p /usr/lib/adapro/
	cp dist/Release/GNU-Linux/libadapro.so /usr/lib/adapro/ 2> /dev/null || \
	cp dist/Debug/GNU-Linux/libadapro.so /usr/lib/adapro/ 2> /dev/null ||  \
	echo "Either Release of Debug build must exist."
	if [ -e "dist/No_DIM/GNU-Linux/libadapro-no-dim.so" ];                 \
	then cp dist/No_DIM/GNU-Linux/libadapro-no-dim.so /usr/lib/adapro/ 2> /dev/null; fi
	if [ -e "/usr/lib/adapro/libadapro.so" -a                              \
		! -e "/usr/lib/libadapro.so" ];                                \
	then ln -s /usr/lib/adapro/libadapro.so /usr/lib/libadapro.so; fi
	if [ -e "/usr/lib/adapro/libadapro-no-dim.so" -a                       \
		! -e "/usr/lib/libadapro-no-dim.so" ];                         \
	then ln -s /usr/lib/adapro/libadapro-no-dim.so /usr/lib/libadapro-no-dim.so; fi
	if [ -e "/usr/include/adapro" ];                                       \
	then rm -rf /usr/include/adapro/*;                                     \
	else mkdir -p /usr/include/adapro/; fi
	cp -r headers/* /usr/include/adapro/

lcov:
	mkdir -p dist/reports/lcov
	lcov --capture                                                         \
	    --directory build/Debug/ --directory headers --directory src       \
	    --no-external --output-file build/Debug/coverage.info > /dev/null
	genhtml build/Debug/coverage.info --output-directory dist/reports/lcov \
	    > /dev/null

adapro_files = src/control/Logger.cpp                                          \
        src/control/Metronome.cpp                                              \
        src/control/Thread.cpp                                                 \
        src/control/Supervisor.cpp                                             \
        src/control/Session.cpp                                                \
        src/library/ConfigurationUtils.cpp                                     \
        src/library/MetronomeFactory.cpp                                       \
        src/library/StringUtils.cpp

clang_session_model:
	mkdir -p dist/divine/SessionModel
	clang++ -std=c++14 -DEXCLUDE_DIM -o dist/divine/SessionModel/SessionModel \
	$(adapro_files) models/divine/SessionModel.cpp -lm -lpthread -lsystemd

clang_supervisor_model:
	mkdir -p dist/divine/SupervisorModel
	clang++ -std=c++14 -DEXCLUDE_DIM -o dist/divine/SupervisorModel/SupervisorModel \
	$(adapro_files) models/divine/SupervisorModel.cpp -lm -lpthread -lsystemd

clang_worker_model:
	mkdir -p dist/divine/WorkerModel
	clang++ -std=c++14 -DEXCLUDE_DIM -o dist/divine/WorkerModel/WorkerModel \
	$(adapro_files) models/divine/WorkerModel.cpp -lm -lpthread -lsystemd

divine_cc_toy_app:
	divine cc -std=c++14 -DEXCLUDE_DIM -DVERIFY -Iheaders                  \
	-o examples/ToyApplication/ToyApplication.bc                           \
	$(adapro_files) examples/ToyApplication/ToyApplication.cpp -lm

divine_check_toy_app:
	divine check --max-memory 16G --leakcheck state,exit                   \
	--capture examples/ToyApplication/data/:nofollow:./                    \
	--report-filename examples/ToyApplication/ToyApplication.report	       \
	examples/ToyApplication/ToyApplication.bc

divine_verify_toy_app:
	divine verify --max-memory 16G -o nofail:malloc                        \
	--capture examples/ToyApplication/data/:nofollow:./                    \
	--report-filename examples/ToyApplication/ToyApplication.report        \
	examples/ToyApplication/ToyApplication.bc

divine_exec_toy_app:
	divine sim -o nofail:malloc                                            \
	--capture examples/ToyApplication/data/:follow:./                      \
	examples/ToyApplication/ToyApplication.bc

divine_check_thread_unit_test:
	mkdir -p build/test/divine
	mkdir -p dist/reports/divine
	divine cc -std=c++14 -DEXCLUDE_DIM -DVERIFY -Iheaders                  \
	-o build/test/divine/ThreadTest.bc                                     \
	src/control/Thread.cpp tests/control/ThreadTest/ThreadTest.cpp         \
	tests/control/ThreadTest/ThreadTestDivineRunner.cpp
	divine check --report-filename dist/reports/divine/ThreadTest.report   \
	--leakcheck state,exit build/test/divine/ThreadTest.bc

divine_check_session_unit_test:
	mkdir -p build/test/divine
	mkdir -p dist/reports/divine
	divine cc -std=c++14 -DEXCLUDE_DIM -Iheaders                           \
	-o build/test/divine/SessionTest.bc                                    \
	src/control/Logger.cpp src/control/Thread.cpp                          \
	src/control/Supervisor.cpp src/control/Session.cpp                     \
	src/library/StringUtils.cpp src/library/ConfigurationUtils.cpp         \
	tests/control/SessionTest/SessionTest.cpp                              \
	tests/control/SessionTest/SessionTestDivineRunner.cpp -lm
	divine check --report-filename dist/reports/divine/SessionTest.report  \
	--leakcheck state,exit build/test/divine/SessionTest.bc