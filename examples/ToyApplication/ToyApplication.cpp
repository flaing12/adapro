
#include <cstdlib>
#include <cstdint>
#include <csignal>
#include <list>
#include <map>
#include <utility>
#include <string>
#include <memory>
#include <functional>
#include <stdexcept>
#ifdef __divine__
#include <sys/cdefs.h>
#include "../../headers/control/Thread.hpp"
#include "../../headers/control/Worker.hpp"
#include "../../headers/control/Session.hpp"
#include "../../headers/control/Metronome.hpp"
#include "../../headers/control/AOFStream.hpp"
#ifndef VERIFY
#include "../../headers/control/Logger.hpp"
#endif
#include "../../headers/library/Clock.hpp"
#include "../../headers/library/GenericFunctions.hpp"
#include "../../headers/data/Context.hpp"
#include "../../headers/data/State.hpp"
#include "../../headers/data/LoggingLevel.hpp"
#include "../../headers/data/Parameters.hpp"
#include "../../headers/data/Typedefs.hpp"
#else
#include <adapro/control/Thread.hpp>
#include <adapro/control/Worker.hpp>
#include <adapro/control/Session.hpp>
#include <adapro/control/Metronome.hpp>
#include <adapro/control/AOFStream.hpp>
#ifndef VERIFY
#include <adapro/control/Logger.hpp>
#endif
#include <adapro/library/Clock.hpp>
#include <adapro/library/GenericFunctions.hpp>
#include <adapro/data/Context.hpp>
#include <adapro/data/State.hpp>
#include <adapro/data/LoggingLevel.hpp>
#include <adapro/data/Parameters.hpp>
#include <adapro/data/Typedefs.hpp>
#endif

/**
 * This application does not require DIM support:
 */
#ifndef EXCLUDE_DIM
#define EXCLUDE_DIM
#endif

/**
 * This is a simple worker Thread demonstrating some of the basic functionality
 * of the ADAPRO framework. The only virtual methods that are allowed to fail
 * (by throwing eceptions), are prepare, execute, and finish.
 */
class HelloWorldThread final: public ADAPRO::Control::Worker
{
    size_t counter;
    const size_t target;
    ADAPRO::Control::Metronome metronome;
    ADAPRO::Control::AOFStream<std::string> file_output;

    protected:

        /**
         * This virtual procedure will be invoked when this worker Thread
         * starts.
         */
#ifdef __divine__
    __skipcfl
#endif
        virtual void prepare() override
        {
            print
            (
                    configuration.at("USER_GREETING"),
                    ADAPRO::Data::LoggingLevel::INFO
            );
            print
            (
                    "A piece of trivia: My size is " + to_string(sizeof(*this)) +
                    " bytes.",
                    ADAPRO::Data::LoggingLevel::USER_DEBUG
            );
            print
            (
                    "More trivia: My base class object size is " +
                    to_string(sizeof(ADAPRO::Control::Worker)) + " bytes.",
                    ADAPRO::Data::LoggingLevel::USER_DEBUG
            );
        }

        /**
         * This virtual procedure will be invoked repeatedly as long as the
         * current command of the worker Thread is <tt>CONTINUE</tt>.
         */
#ifdef __divine__
    __skipcfl
#endif
        virtual void execute() override
        {
            if (++counter <= target)
            {
                print("Working hard...", ADAPRO::Data::LoggingLevel::INFO);
                uint64_t timestamp(ADAPRO::Library::epoch_time());
                file_output << (to_string(counter) + ", " + to_string(timestamp)
                        + ", " + ADAPRO::Library::timestamp());
                metronome.synchronize(); // Sleeping until the next second.
            }
            else
            {
                print("Enough for today.", ADAPRO::Data::LoggingLevel::INFO);
                stop_async(); // Always send commands to self asynchronously!
            }
        }

        /**
         * This virtual procedure will be invoked when the worker stops. Always
         * remember to stop all delegated Threads (if any).
         */
#ifdef __divine__
    __skipcfl
#endif
        virtual void finish() override
        {
            print("Cleaning up...", ADAPRO::Data::LoggingLevel::INFO);
            // The destructor would be a more suitable place for closing the
            // stream, but it's here just to demonstrate that some final action
            // might be performed by this method:
            file_output.close();
        }

    public:

        /**
         * <p>The constructor of HelloWorldThread that will be used by the
         * Supervisor Thread of ADAPRO framework indirectly, by invoking the
         * factory function.</p>
         * <p>An instance of this thread has the name
         * <tt>"HelloWorldThread"</tt>, no affinity to any specific CPU core,
         * and a simple transition callback that prints the name of the new
         * state.</p>
         *
         * @param logger        Logger of the ADAPRO Session.
         * @param configuration The configuration fetched from the configuration
         * file.
         */
#ifdef VERIFY
        explicit
#endif
        HelloWorldThread
        (
#ifndef VERIFY
                ADAPRO::Control::Logger& logger,
#endif
                const std::map<std::string,std::string>& configuration
        )
        noexcept:
                ADAPRO::Control::Worker
                (
#ifndef VERIFY
                        logger,
#endif
                        std::move("HelloWorldThread"),
                        configuration,
                        -1,
                        [this](const ADAPRO::Data::State s)
                        {
                            ADAPRO::Control::Session::PRINT
                            (
                                    name + " goes to " +
                                    ADAPRO::Library::show(s) + "...",
                                    ADAPRO::Data::LoggingLevel::USER_DEBUG
                            );
                        }
                ),
                counter(0),
                target(stoi(configuration.at("USER_TARGET"))),
                metronome
                (
#ifndef VERIFY
                        logger,
#endif
                        1000,
                        2
                ),
                file_output
                (
#ifndef VERIFY
                        logger,
#endif
                        std::move("DPCOM"),
                        std::move("data/example.csv")
                )
        {}

        virtual ~HelloWorldThread() noexcept
        {
            // This method was added in ADAPRO 5.0.0. It blocks until the Thread
            // has been fully stopped, preventing a race condition between the
            // native backend thread and this destructor. Always remember to
            // call join in the destructor of the final inheriting class:
            join();

            if (get_state() == ABORTED)
            {
                // AOFStream follows the RAII principle and closes it self when
                // destroyed. The point here is to demonstrate that whatever
                // the method finish was supposed to do, does not happen if the
                // Thread aborts (due to unhandled exception) during prepare or
                // execute. Thus, the destructor should check if the Thread
                // ended up in state ABORTED afer joining and perform the
                // cleanup in that case as well:
                file_output.close();
            }
        }
};

/**
 * This relation will be used for sanity checking configuration file input. If
 * the configuration file has (a) key-value pair(s) not satisfying the relation,
 * an exception will be thrown. (The relation would be allowed to throw
 * exceptions as well.)
 */
#ifdef __divine__
    __skipcfl
#endif
const bool relation(const std::string& key, const std::string& value) noexcept
{
    if (key == "USER_TARGET")
    {
        if (!ADAPRO::Data::is_unsigned(value))
        {
            ADAPRO::Control::Session::PRINT
            (
                    "User target must be a (small) unsigned integer!",
                    ADAPRO::Data::LoggingLevel::ERROR
            );
            return false;
        }
        else if (std::stoi(value) >= 10)
        {
            ADAPRO::Control::Session::PRINT
            (
                    "Too much work!",
                    ADAPRO::Data::LoggingLevel::ERROR
            );
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        // DEFAULT_RELATION verifies every ADAPRO parameter and accepts all
        // other configuration entries as they are:
        return ADAPRO::Library::default_relation(key, value);
    }
}

/**
 * A simple signal callback function for the ADAPRO framework. The return value
 * determines whether or not the Session is allowed to continue after catching
 * a signal. NB: This callback is not allowed to exit or free memory in a way
 * that would interfere with the normal shutdown sequence.
 */
#ifdef __divine__
    __skipcfl
#endif
bool signal_callback(const uint8_t signal) noexcept
{
    string signal_name;
    ADAPRO::Data::LoggingLevel logging_level;
    switch (signal)
    {
        case SIGSEGV:
            signal_name = "SIGSEGV";
            logging_level = ADAPRO::Data::LoggingLevel::FATAL;
            break;
        case SIGABRT:
            signal_name = "SIGABRT";
            logging_level = ADAPRO::Data::LoggingLevel::FATAL;
            break;
        case SIGTERM:
            signal_name = "SIGTERM";
            logging_level = ADAPRO::Data::LoggingLevel::INFO;
            break;
        case SIGINT:
            signal_name = "SIGINT";
            logging_level = ADAPRO::Data::LoggingLevel::INFO;
            break;
        case SIGILL:
            signal_name = "SIGILL";
            logging_level = ADAPRO::Data::LoggingLevel::FATAL;
            break;
        case SIGFPE:
            signal_name = "SIGFPE";
            logging_level = ADAPRO::Data::LoggingLevel::FATAL;
            break;
#ifdef _POSIX_C_SOURCE
        case SIGHUP:
            signal_name = "SIGHUP";
            logging_level = ADAPRO::Data::LoggingLevel::INFO;
            break;
#endif
    }
    ADAPRO::Control::Session::PRINT(signal_name + " caught.", logging_level);
    return false;
}

/**
 * The main function of this example program.
 *
 * @param argc  Number of command-line arguments.
 * @param argv  Values of the command-line arguments. ADAPRO interprets the
 * first argument as the location of the configuration file. It will be added in
 * the head of the list of configuration paths.
 * @return      The exit status code.
 */
#ifdef __divine__
    __skipcfl
#endif
int main(int argc, char** argv)
{
    /**
     * List of the possible configuration file locations (the correct one being
     * <tt>"example.conf</tt>).
     */
    const std::list<std::string> configuration_paths
    {
        "nonexistent.conf", "data/ToyApplication.conf", "unreachable.conf"
    };

    /**
     * Default configuration. The values will be overwritten with the ones found
     * in the configuration file. When disallowing new keys, the default
     * configuration must contain non-empty string values for each allowed key.
     */
    std::map<std::string, std::string> configuration
    {
            ADAPRO::Library::make_default_configuration()
    };
    configuration.insert({  "DUMMY",          "PARAMETER"     });
    configuration.insert({  "USER_TARGET",    "0"             });
    configuration.insert({  "USER_GREETING",  "Hello,_Mars!"  });

    /**
     * The Supervisor Thread of ADAPRO framework will use this factory
     * function for creating a HelloWorldThread instance.
     */
    const std::list<ADAPRO::Data::worker_factory_t> factories
    {
            []
            (
#ifndef VERIFY
                    ADAPRO::Control::Logger& logger,
#endif
                    const std::map<std::string, std::string>& configuration
            )
            {
                return std::unique_ptr<HelloWorldThread>
                (
                        new HelloWorldThread
                        (
#ifndef VERIFY
                                logger,
#endif
                                configuration
                        )
                );
            }
    };

    /**
     * Initializes the ADAPRO Session. May throw exceptions for several reasons:
     */
    ADAPRO::Control::Session::INITIALIZE(std::move(ADAPRO::Data::Context{
            string("Toy Application")   // Name of the application
            , string("5.0.0")           // Version of the application
            , argc                      // Number of command-line arguments
            , argv                      // Actual command-line arguments
            , factories                 // Worker factories
            , configuration_paths       // Configuration path candidates
            , configuration             // Default configuration
            , true                      // Enabling configuration file reading
            , true                      // Crash if configuration file not found
            , false                     // Disallowing new keys
            , relation                  // The relation for sanity-checking cfg
            , signal_callback           // Signal callback
    }));

    /**
     * Runs the Session. Will not throw. Returns a non-zero bit mask of error
     * flags in case of error(s). See the documentation for
     * <adapro/data/StatusFlags.hpp> (i.e.
     * "../../../headers/data/StatusFlags.hpp).
     */
    return ADAPRO::Control::Session::RUN();
}
