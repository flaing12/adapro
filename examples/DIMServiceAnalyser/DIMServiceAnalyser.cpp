
#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <csignal>
#include <list>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <map>
#include <utility>
#include <string>
#include <sstream>
#include <iomanip>
#include <memory>
#include <functional>
#include <iostream>
#include <adapro/control/Thread.hpp>
#include <adapro/control/Worker.hpp>
#include <adapro/control/Session.hpp>
#include <adapro/control/Logger.hpp>
#include <adapro/DIM/Wrapper.hpp>
#include <adapro/DIM/Typedefs.hpp>
#include <adapro/library/Clock.hpp>
#include <adapro/library/StringUtils.hpp>
#include <adapro/library/GenericFunctions.hpp>
#include <adapro/data/Context.hpp>
#include <adapro/data/State.hpp>
#include <adapro/data/LoggingLevel.hpp>
#include <adapro/data/Parameters.hpp>
#include <adapro/data/Typedefs.hpp>

/**
 * DIMServiceAnalyser is responsible for carrying out the business logic of this
 * application, i.e. retrieving the service names and calculating their
 * alphabet.
 */
class DIMServiceAnalyser final: public ADAPRO::Control::Worker
{
    /**
     * Gets the services registered to the DIM DNS node determined by the ADAPRO
     * configuration parameters <tt>ADAPRO_DIM_DNS_NODE</tt> and
     * <tt>ADAPRO_DIM_DNS_PORT</tt>. The results will be in a vector of strings.
     * The strings use the format as DIM (see API documentation for
     * <tt>ADARPO::DIM::Wrapper::client_get_services</tt>). When the ADAPRO
     * configuration parameter <tt>ADAPRO_LOGGING_MASK</tt> is set to
     * <tt>USER_DEBUG</tt>, then the list of services will be printed to
     * <tt>stdout</tt> also.
     *
     * @return A vector containing every DIM service registered on the DIM DNS
     * node, excluding services provided by DIM DNS node itself.
     * @see ADAPRO::DIM::Wrapper::client_get_services
     */
    vector<string>& get_services()
    {
        print
        (
                "Generating the list of DIM services on " +
                configuration.at(ADAPRO::Library::show(ADAPRO::Data::Parameter::DIM_DNS_NODE)) +
                ":" +
                configuration.at(ADAPRO::Library::show(ADAPRO::Data::Parameter::DIM_DNS_PORT)) +
                "...",
                ADAPRO::Data::LoggingLevel::INFO
        );
        vector<string>& services(ADAPRO::DIM::Wrapper::client_get_services());
        print
        (
                "Results (" + to_string(services.size()) + " services):",
                ADAPRO::Data::LoggingLevel::INFO
        );
        if (ADAPRO::Library::logging_mask(
                configuration.at(ADAPRO::Library::show(ADAPRO::Data::Parameter::LOGGING_MASK))) &
                ADAPRO::Data::LoggingLevel::USER_DEBUG)
        {
            for (const string& service_name : services)
            {
                cout << "    " << service_name << '\n';
            }
        }
        return services;
    }

    /**
     * Prints the alphabet of the service names.
     *
     * @param services  Services to be analysed.
     */
    void analyse(vector<string>& services) noexcept
    {
        unordered_map<char, size_t> symbol_frequencies;
        stringstream alphabet;
        size_t total_symbol_count(0);
        size_t invalid_name_count(0);
        for (const string& service : services)
        {
            const string name((ADAPRO::Library::split(service, '|'))[0]);
            if (!ADAPRO::Data::is_DIM_service_name(name))
            {
                print
                (
                        "Service name \"" + name + "\" contains illegal "
                        "character(s).",
                        ADAPRO::Data::LoggingLevel::WARNING
                );
                ++invalid_name_count;
            }
            for (const char c : name)
            {
                auto iterator(symbol_frequencies.find(c));
                if (iterator != symbol_frequencies.end())
                {
                    ++(iterator->second);
                }
                else
                {
                    symbol_frequencies.emplace(c, 1);
                }
                ++total_symbol_count;
            }
        }
        map<size_t, char, greater<size_t>> ordered_frequencies;
        for (auto x : symbol_frequencies)
        {
            ordered_frequencies[x.second] = x.first;
        }
        size_t i(0);
        for (auto x : ordered_frequencies)
        {
            alphabet << "    " << ++i << '.' << '\t' << x.second << '\t' <<
                    x.first << " times \t" << setprecision(3) << fixed <<
                    (100.0 * x.first / total_symbol_count) << " % frequency\n";
        }
        print
        (
                "DIM service name alphabet:\n" + alphabet.str(),
                ADAPRO::Data::LoggingLevel::INFO
        );
        print
        (
                "Number of invalid DIM service names: " +
                to_string(invalid_name_count),
                ADAPRO::Data::LoggingLevel::INFO
        );
    }

protected:

    virtual void prepare() override
    {
        ADAPRO::DIM::Wrapper::common_set_dim_dns_node(
                configuration.at(ADAPRO::Library::show(ADAPRO::Data::Parameter::DIM_DNS_NODE)));
        ADAPRO::DIM::Wrapper::common_set_dim_dns_port(
                stoi(configuration.at(ADAPRO::Library::show(ADAPRO::Data::Parameter::DIM_DNS_PORT))));
    }

    virtual void execute() override
    {
        analyse(get_services()); // Nothing to do after this.
        stop_async(); // Always send commands to self asynchronously!
    }

    virtual void finish() override {}

public:

    /**
     * Constructor for DIMServiceAnalyser.
     *
     * @param logger        Logger of the ADAPRO Session.
     * @param configuration Configuration of the ADAPRO Session.
     */
    DIMServiceAnalyser
    (
            ADAPRO::Control::Logger& logger,
            const std::map<std::string,std::string>& configuration
    )
    noexcept:
            ADAPRO::Control::Worker
            (
                    logger,
                    std::move("DIMServiceAnalyser"),
                    configuration,
                    -1
            )
    {}

    virtual ~DIMServiceAnalyser() noexcept {}
};

/**
 * The main function of this application. This application retrieves list of
 * services on a DIM DNS node and performs some analysis on them.
 *
 * @param argc  Number of command-line arguments.
 * @param argv  Values of the command-line arguments. The first argument will be
 * interpreted as DIM_DNS_NODE and the second argument as DIM_DNS_PORT.
 * @return      The exit status code.
 */
int main(int argc, char** argv)
{
    ADAPRO::Data::config_t configuration{ADAPRO::Library::make_default_configuration()};
    if (argc >= 2)
    {
        if (strcmp(argv[1], "--help") == 0)
        {
            cout << "Usage: dim_service_analyser [<DIM_DNS_NODE> "
                    "[<DIM_DNS_PORT>]]" << '\n';
            return EXIT_SUCCESS;
        }
        configuration[ADAPRO::Library::show(ADAPRO::Data::Parameter::DIM_DNS_NODE)] = string(argv[1]);
    }
    if (argc >= 3)
    {
        configuration[ADAPRO::Library::show(ADAPRO::Data::Parameter::DIM_DNS_PORT)] = string(argv[2]);
    }
    configuration[ADAPRO::Library::show(ADAPRO::Data::Parameter::LOGGING_MASK)] = "ADISWEF";
    configuration[ADAPRO::Library::show(ADAPRO::Data::Parameter::COLOURS_ENABLED)] = "TRUE";
    configuration[ADAPRO::Library::show(ADAPRO::Data::Parameter::DIM_SERVER_ENABLED)] = "FALSE";

    const std::list<ADAPRO::Data::worker_factory_t> factories
    {
            []
            (
                    ADAPRO::Control::Logger& logger,
                    const std::map<std::string, std::string>& configuration
            )
            {
                return std::unique_ptr<DIMServiceAnalyser>
                (
                        new DIMServiceAnalyser(logger, configuration)
                );
            }
    };

    // This application handles its arguments itself, so they will not be
    // forwarded to the ADAPRO framework:
    ADAPRO::Control::Session::INITIALIZE(ADAPRO::Data::Context{
            "DIMServiceAnalyzer",   // Name of this application
            "5.0.0",                // Version of this application
            0,                      // Number of command-line arguments
            nullptr,                // Values of the command-line arguments
            factories,              // Used by Supervisor to create the worker
            {},                     // Empty list of configuration file paths
            configuration,          // The initial configuration
            false                   // Disabling configuration file access
    });

    return ADAPRO::Control::Session::RUN();
}
