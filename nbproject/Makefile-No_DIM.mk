#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=No_DIM
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/src/DIM/Controller.o \
	${OBJECTDIR}/src/DIM/Wrapper.o \
	${OBJECTDIR}/src/control/Logger.o \
	${OBJECTDIR}/src/control/Metronome.o \
	${OBJECTDIR}/src/control/Session.o \
	${OBJECTDIR}/src/control/Supervisor.o \
	${OBJECTDIR}/src/control/Thread.o \
	${OBJECTDIR}/src/library/ConfigurationUtils.o \
	${OBJECTDIR}/src/library/MetronomeFactory.o \
	${OBJECTDIR}/src/library/StringUtils.o

# Test Directory
TESTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}/tests

# Test Files
TESTFILES= \
	${TESTDIR}/TestFiles/f1/f1 \
	${TESTDIR}/TestFiles/f1/f2 \
	${TESTDIR}/TestFiles/f1/f3 \
	${TESTDIR}/TestFiles/f1/f9 \
	${TESTDIR}/TestFiles/f1/f4 \
	${TESTDIR}/TestFiles/f1/f6 \
	${TESTDIR}/TestFiles/f1/f7 \
	${TESTDIR}/TestFiles/f1/f5 \
	${TESTDIR}/TestFiles/f1/f8 \
	${TESTDIR}/TestFiles/f2/f8 \
	${TESTDIR}/TestFiles/f2/f1 \
	${TESTDIR}/TestFiles/f2/f11 \
	${TESTDIR}/TestFiles/f2/f7 \
	${TESTDIR}/TestFiles/f2/f9 \
	${TESTDIR}/TestFiles/f2/f10 \
	${TESTDIR}/TestFiles/f4/f1 \
	${TESTDIR}/TestFiles/f4/f2 \
	${TESTDIR}/TestFiles/f3/f1 \
	${TESTDIR}/TestFiles/f3/f2 \
	${TESTDIR}/TestFiles/f3/f5 \
	${TESTDIR}/TestFiles/f3/f6

# Test Object Files
TESTOBJECTFILES= \
	${TESTDIR}/tests/DIM/ControllerTest/ControllerTest.o \
	${TESTDIR}/tests/DIM/ControllerTest/ControllerTestRunner.o \
	${TESTDIR}/tests/DIM/TaskTypeTest/TaskTypeTest.o \
	${TESTDIR}/tests/DIM/TaskTypeTest/TaskTypeTestRunner.o \
	${TESTDIR}/tests/control/AOFStreamTest/AOFStreamTest.o \
	${TESTDIR}/tests/control/AOFStreamTest/AOFStreamTestRunner.o \
	${TESTDIR}/tests/control/AlignedAllocatorTest/AlignedAllocatorTest.o \
	${TESTDIR}/tests/control/AlignedAllocatorTest/AlignedAllocatorTestRunner.o \
	${TESTDIR}/tests/control/Either/EitherTest.o \
	${TESTDIR}/tests/control/Either/EitherTestRunner.o \
	${TESTDIR}/tests/control/LoggerTest/LoggerTest.o \
	${TESTDIR}/tests/control/LoggerTest/LoggerTestRunner.o \
	${TESTDIR}/tests/control/MetronomeTest/MetronomeTest.o \
	${TESTDIR}/tests/control/MetronomeTest/MetronomeTestRunner.o \
	${TESTDIR}/tests/control/SessionTest/SessionTest.o \
	${TESTDIR}/tests/control/SessionTest/SessionTestDivineRunner.o \
	${TESTDIR}/tests/control/SessionTest/SessionTestRunner.o \
	${TESTDIR}/tests/control/SupervisorTest/SupervisorTest.o \
	${TESTDIR}/tests/control/SupervisorTest/SupervisorTestRunner.o \
	${TESTDIR}/tests/control/ThreadGuardTest/ThreadGuardTest.o \
	${TESTDIR}/tests/control/ThreadGuardTest/ThreadGuardTestRunner.o \
	${TESTDIR}/tests/control/ThreadTest/ThreadTest.o \
	${TESTDIR}/tests/control/ThreadTest/ThreadTestDivineRunner.o \
	${TESTDIR}/tests/control/ThreadTest/ThreadTestRunner.o \
	${TESTDIR}/tests/data/CommandTest/CommandTest.o \
	${TESTDIR}/tests/data/CommandTest/CommandTestRunner.o \
	${TESTDIR}/tests/data/ContextTest/ContextTest.o \
	${TESTDIR}/tests/data/ContextTest/ContextTestRunner.o \
	${TESTDIR}/tests/data/LoggingLevelTest/LoggingLevelTest.o \
	${TESTDIR}/tests/data/LoggingLevelTest/LoggingLevelTestRunner.o \
	${TESTDIR}/tests/data/ParametersTest/ParametersTest.o \
	${TESTDIR}/tests/data/ParametersTest/ParametersTestRunner.o \
	${TESTDIR}/tests/data/ReportTest/ReportTest.o \
	${TESTDIR}/tests/data/ReportTest/ReportTestRunner.o \
	${TESTDIR}/tests/data/StateTest/StateTest.o \
	${TESTDIR}/tests/data/StateTest/StateTestRunner.o \
	${TESTDIR}/tests/library/ClockTest/ClockTest.o \
	${TESTDIR}/tests/library/ClockTest/ClockTestRunner.o \
	${TESTDIR}/tests/library/ConfigurationUtilsTest/ConfigurationUtilsTest.o \
	${TESTDIR}/tests/library/ConfigurationUtilsTest/ConfigurationUtilsTestRunner.o \
	${TESTDIR}/tests/library/MetronomeFactoryTest/MetronomeFactoryTest.o \
	${TESTDIR}/tests/library/MetronomeFactoryTest/MetronomeFactoryTestRunner.o \
	${TESTDIR}/tests/library/StringUtilsTest/StringUtilsTest.o \
	${TESTDIR}/tests/library/StringUtilsTest/StringUtilsTestRunner.o

# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-m64 --coverage
CXXFLAGS=-m64 --coverage

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lpthread `pkg-config --libs libsystemd`  

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libadapro-no-dim.${CND_DLIB_EXT}

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libadapro-no-dim.${CND_DLIB_EXT}: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libadapro-no-dim.${CND_DLIB_EXT} ${OBJECTFILES} ${LDLIBSOPTIONS} --coverage -shared -fPIC

${OBJECTDIR}/src/DIM/Controller.o: src/DIM/Controller.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/DIM
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM `pkg-config --cflags libsystemd` -std=c++14  -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/DIM/Controller.o src/DIM/Controller.cpp

${OBJECTDIR}/src/DIM/Wrapper.o: src/DIM/Wrapper.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/DIM
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM `pkg-config --cflags libsystemd` -std=c++14  -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/DIM/Wrapper.o src/DIM/Wrapper.cpp

${OBJECTDIR}/src/control/Logger.o: src/control/Logger.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/control
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM `pkg-config --cflags libsystemd` -std=c++14  -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/control/Logger.o src/control/Logger.cpp

${OBJECTDIR}/src/control/Metronome.o: src/control/Metronome.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/control
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM `pkg-config --cflags libsystemd` -std=c++14  -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/control/Metronome.o src/control/Metronome.cpp

${OBJECTDIR}/src/control/Session.o: src/control/Session.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/control
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM `pkg-config --cflags libsystemd` -std=c++14  -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/control/Session.o src/control/Session.cpp

${OBJECTDIR}/src/control/Supervisor.o: src/control/Supervisor.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/control
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM `pkg-config --cflags libsystemd` -std=c++14  -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/control/Supervisor.o src/control/Supervisor.cpp

${OBJECTDIR}/src/control/Thread.o: src/control/Thread.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/control
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM `pkg-config --cflags libsystemd` -std=c++14  -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/control/Thread.o src/control/Thread.cpp

${OBJECTDIR}/src/library/ConfigurationUtils.o: src/library/ConfigurationUtils.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/library
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM `pkg-config --cflags libsystemd` -std=c++14  -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/library/ConfigurationUtils.o src/library/ConfigurationUtils.cpp

${OBJECTDIR}/src/library/MetronomeFactory.o: src/library/MetronomeFactory.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/library
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM `pkg-config --cflags libsystemd` -std=c++14  -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/library/MetronomeFactory.o src/library/MetronomeFactory.cpp

${OBJECTDIR}/src/library/StringUtils.o: src/library/StringUtils.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/library
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM `pkg-config --cflags libsystemd` -std=c++14  -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/library/StringUtils.o src/library/StringUtils.cpp

# Subprojects
.build-subprojects:

# Build Test Targets
.build-tests-conf: .build-tests-subprojects .build-conf ${TESTFILES}
.build-tests-subprojects:

${TESTDIR}/TestFiles/f1/f1: ${TESTDIR}/tests/control/AlignedAllocatorTest/AlignedAllocatorTest.o ${TESTDIR}/tests/control/AlignedAllocatorTest/AlignedAllocatorTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f1
	${LINK.cc} -latomic --coverage   -o ${TESTDIR}/TestFiles/f1/f1 $^ ${LDLIBSOPTIONS} `pkg-config --libs libsystemd` `pkg-config --libs cppunit`   

${TESTDIR}/TestFiles/f1/f2: ${TESTDIR}/tests/control/AOFStreamTest/AOFStreamTest.o ${TESTDIR}/tests/control/AOFStreamTest/AOFStreamTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f1
	${LINK.cc} -latomic --coverage   -o ${TESTDIR}/TestFiles/f1/f2 $^ ${LDLIBSOPTIONS} `pkg-config --libs libsystemd` `pkg-config --libs cppunit`   

${TESTDIR}/TestFiles/f1/f3: ${TESTDIR}/tests/control/Either/EitherTest.o ${TESTDIR}/tests/control/Either/EitherTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f1
	${LINK.cc} -latomic --coverage   -o ${TESTDIR}/TestFiles/f1/f3 $^ ${LDLIBSOPTIONS} `pkg-config --libs libsystemd` `pkg-config --libs cppunit`   

${TESTDIR}/TestFiles/f1/f9: ${TESTDIR}/tests/control/LoggerTest/LoggerTest.o ${TESTDIR}/tests/control/LoggerTest/LoggerTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f1
	${LINK.cc} -latomic --coverage   -o ${TESTDIR}/TestFiles/f1/f9 $^ ${LDLIBSOPTIONS} `pkg-config --libs libsystemd` `pkg-config --libs cppunit`   

${TESTDIR}/TestFiles/f1/f4: ${TESTDIR}/tests/control/MetronomeTest/MetronomeTest.o ${TESTDIR}/tests/control/MetronomeTest/MetronomeTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f1
	${LINK.cc} -latomic --coverage   -o ${TESTDIR}/TestFiles/f1/f4 $^ ${LDLIBSOPTIONS} `pkg-config --libs libsystemd` `pkg-config --libs cppunit`   

${TESTDIR}/TestFiles/f1/f6: ${TESTDIR}/tests/control/SessionTest/SessionTest.o ${TESTDIR}/tests/control/SessionTest/SessionTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f1
	${LINK.cc} -latomic --coverage   -o ${TESTDIR}/TestFiles/f1/f6 $^ ${LDLIBSOPTIONS} `pkg-config --libs libsystemd` `pkg-config --libs cppunit`   

${TESTDIR}/TestFiles/f1/f7: ${TESTDIR}/tests/control/SupervisorTest/SupervisorTest.o ${TESTDIR}/tests/control/SupervisorTest/SupervisorTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f1
	${LINK.cc} -latomic --coverage   -o ${TESTDIR}/TestFiles/f1/f7 $^ ${LDLIBSOPTIONS} `pkg-config --libs libsystemd` `pkg-config --libs cppunit`   

${TESTDIR}/TestFiles/f1/f5: ${TESTDIR}/tests/control/ThreadGuardTest/ThreadGuardTest.o ${TESTDIR}/tests/control/ThreadGuardTest/ThreadGuardTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f1
	${LINK.cc} -latomic --coverage   -o ${TESTDIR}/TestFiles/f1/f5 $^ ${LDLIBSOPTIONS} `pkg-config --libs libsystemd` `pkg-config --libs cppunit`   

${TESTDIR}/TestFiles/f1/f8: ${TESTDIR}/tests/control/ThreadTest/ThreadTest.o ${TESTDIR}/tests/control/ThreadTest/ThreadTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f1
	${LINK.cc} -latomic --coverage   -o ${TESTDIR}/TestFiles/f1/f8 $^ ${LDLIBSOPTIONS} `pkg-config --libs libsystemd` `pkg-config --libs cppunit`   

${TESTDIR}/TestFiles/f2/f8: ${TESTDIR}/tests/data/CommandTest/CommandTest.o ${TESTDIR}/tests/data/CommandTest/CommandTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f2
	${LINK.cc} -latomic --coverage   -o ${TESTDIR}/TestFiles/f2/f8 $^ ${LDLIBSOPTIONS} `pkg-config --libs libsystemd` `pkg-config --libs cppunit`   

${TESTDIR}/TestFiles/f2/f1: ${TESTDIR}/tests/data/ContextTest/ContextTest.o ${TESTDIR}/tests/data/ContextTest/ContextTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f2
	${LINK.cc} -latomic --coverage   -o ${TESTDIR}/TestFiles/f2/f1 $^ ${LDLIBSOPTIONS} `pkg-config --libs libsystemd` `pkg-config --libs cppunit`   

${TESTDIR}/TestFiles/f2/f11: ${TESTDIR}/tests/data/LoggingLevelTest/LoggingLevelTest.o ${TESTDIR}/tests/data/LoggingLevelTest/LoggingLevelTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f2
	${LINK.cc} -latomic --coverage   -o ${TESTDIR}/TestFiles/f2/f11 $^ ${LDLIBSOPTIONS} `pkg-config --libs libsystemd` `pkg-config --libs cppunit`   

${TESTDIR}/TestFiles/f2/f7: ${TESTDIR}/tests/data/ParametersTest/ParametersTest.o ${TESTDIR}/tests/data/ParametersTest/ParametersTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f2
	${LINK.cc} -latomic --coverage   -o ${TESTDIR}/TestFiles/f2/f7 $^ ${LDLIBSOPTIONS} `pkg-config --libs libsystemd` `pkg-config --libs cppunit`   

${TESTDIR}/TestFiles/f2/f9: ${TESTDIR}/tests/data/ReportTest/ReportTest.o ${TESTDIR}/tests/data/ReportTest/ReportTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f2
	${LINK.cc} -latomic --coverage   -o ${TESTDIR}/TestFiles/f2/f9 $^ ${LDLIBSOPTIONS} `pkg-config --libs libsystemd` `pkg-config --libs cppunit`   

${TESTDIR}/TestFiles/f2/f10: ${TESTDIR}/tests/data/StateTest/StateTest.o ${TESTDIR}/tests/data/StateTest/StateTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f2
	${LINK.cc} -latomic --coverage   -o ${TESTDIR}/TestFiles/f2/f10 $^ ${LDLIBSOPTIONS} `pkg-config --libs libsystemd` `pkg-config --libs cppunit`   

${TESTDIR}/TestFiles/f4/f1: ${TESTDIR}/tests/DIM/ControllerTest/ControllerTest.o ${TESTDIR}/tests/DIM/ControllerTest/ControllerTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f4
	${LINK.cc} -latomic --coverage   -o ${TESTDIR}/TestFiles/f4/f1 $^ ${LDLIBSOPTIONS} `pkg-config --libs libsystemd` `pkg-config --libs cppunit`   

${TESTDIR}/TestFiles/f4/f2: ${TESTDIR}/tests/DIM/TaskTypeTest/TaskTypeTest.o ${TESTDIR}/tests/DIM/TaskTypeTest/TaskTypeTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f4
	${LINK.cc} -latomic --coverage   -o ${TESTDIR}/TestFiles/f4/f2 $^ ${LDLIBSOPTIONS} `pkg-config --libs libsystemd` `pkg-config --libs cppunit`   

${TESTDIR}/TestFiles/f3/f1: ${TESTDIR}/tests/library/ClockTest/ClockTest.o ${TESTDIR}/tests/library/ClockTest/ClockTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f3
	${LINK.cc} -latomic --coverage   -o ${TESTDIR}/TestFiles/f3/f1 $^ ${LDLIBSOPTIONS} `pkg-config --libs libsystemd` `pkg-config --libs cppunit`   

${TESTDIR}/TestFiles/f3/f2: ${TESTDIR}/tests/library/ConfigurationUtilsTest/ConfigurationUtilsTest.o ${TESTDIR}/tests/library/ConfigurationUtilsTest/ConfigurationUtilsTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f3
	${LINK.cc} -latomic --coverage   -o ${TESTDIR}/TestFiles/f3/f2 $^ ${LDLIBSOPTIONS} `pkg-config --libs libsystemd` `pkg-config --libs cppunit`   

${TESTDIR}/TestFiles/f3/f5: ${TESTDIR}/tests/library/MetronomeFactoryTest/MetronomeFactoryTest.o ${TESTDIR}/tests/library/MetronomeFactoryTest/MetronomeFactoryTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f3
	${LINK.cc} -latomic --coverage   -o ${TESTDIR}/TestFiles/f3/f5 $^ ${LDLIBSOPTIONS} `pkg-config --libs libsystemd` `pkg-config --libs cppunit`   

${TESTDIR}/TestFiles/f3/f6: ${TESTDIR}/tests/library/StringUtilsTest/StringUtilsTest.o ${TESTDIR}/tests/library/StringUtilsTest/StringUtilsTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f3
	${LINK.cc} -latomic --coverage   -o ${TESTDIR}/TestFiles/f3/f6 $^ ${LDLIBSOPTIONS} `pkg-config --libs libsystemd` `pkg-config --libs cppunit`   


${TESTDIR}/tests/control/AlignedAllocatorTest/AlignedAllocatorTest.o: tests/control/AlignedAllocatorTest/AlignedAllocatorTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests/control/AlignedAllocatorTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/control/AlignedAllocatorTest/AlignedAllocatorTest.o tests/control/AlignedAllocatorTest/AlignedAllocatorTest.cpp


${TESTDIR}/tests/control/AlignedAllocatorTest/AlignedAllocatorTestRunner.o: tests/control/AlignedAllocatorTest/AlignedAllocatorTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/control/AlignedAllocatorTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/control/AlignedAllocatorTest/AlignedAllocatorTestRunner.o tests/control/AlignedAllocatorTest/AlignedAllocatorTestRunner.cpp


${TESTDIR}/tests/control/AOFStreamTest/AOFStreamTest.o: tests/control/AOFStreamTest/AOFStreamTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests/control/AOFStreamTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/control/AOFStreamTest/AOFStreamTest.o tests/control/AOFStreamTest/AOFStreamTest.cpp


${TESTDIR}/tests/control/AOFStreamTest/AOFStreamTestRunner.o: tests/control/AOFStreamTest/AOFStreamTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/control/AOFStreamTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/control/AOFStreamTest/AOFStreamTestRunner.o tests/control/AOFStreamTest/AOFStreamTestRunner.cpp


${TESTDIR}/tests/control/Either/EitherTest.o: tests/control/Either/EitherTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests/control/Either
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/control/Either/EitherTest.o tests/control/Either/EitherTest.cpp


${TESTDIR}/tests/control/Either/EitherTestRunner.o: tests/control/Either/EitherTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/control/Either
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/control/Either/EitherTestRunner.o tests/control/Either/EitherTestRunner.cpp


${TESTDIR}/tests/control/LoggerTest/LoggerTest.o: tests/control/LoggerTest/LoggerTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests/control/LoggerTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/control/LoggerTest/LoggerTest.o tests/control/LoggerTest/LoggerTest.cpp


${TESTDIR}/tests/control/LoggerTest/LoggerTestRunner.o: tests/control/LoggerTest/LoggerTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/control/LoggerTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/control/LoggerTest/LoggerTestRunner.o tests/control/LoggerTest/LoggerTestRunner.cpp


${TESTDIR}/tests/control/MetronomeTest/MetronomeTest.o: tests/control/MetronomeTest/MetronomeTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests/control/MetronomeTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/control/MetronomeTest/MetronomeTest.o tests/control/MetronomeTest/MetronomeTest.cpp


${TESTDIR}/tests/control/MetronomeTest/MetronomeTestRunner.o: tests/control/MetronomeTest/MetronomeTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/control/MetronomeTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/control/MetronomeTest/MetronomeTestRunner.o tests/control/MetronomeTest/MetronomeTestRunner.cpp


${TESTDIR}/tests/control/SessionTest/SessionTest.o: tests/control/SessionTest/SessionTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests/control/SessionTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/control/SessionTest/SessionTest.o tests/control/SessionTest/SessionTest.cpp


${TESTDIR}/tests/control/SessionTest/SessionTestRunner.o: tests/control/SessionTest/SessionTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/control/SessionTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/control/SessionTest/SessionTestRunner.o tests/control/SessionTest/SessionTestRunner.cpp


${TESTDIR}/tests/control/SupervisorTest/SupervisorTest.o: tests/control/SupervisorTest/SupervisorTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests/control/SupervisorTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/control/SupervisorTest/SupervisorTest.o tests/control/SupervisorTest/SupervisorTest.cpp


${TESTDIR}/tests/control/SupervisorTest/SupervisorTestRunner.o: tests/control/SupervisorTest/SupervisorTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/control/SupervisorTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/control/SupervisorTest/SupervisorTestRunner.o tests/control/SupervisorTest/SupervisorTestRunner.cpp


${TESTDIR}/tests/control/ThreadGuardTest/ThreadGuardTest.o: tests/control/ThreadGuardTest/ThreadGuardTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests/control/ThreadGuardTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/control/ThreadGuardTest/ThreadGuardTest.o tests/control/ThreadGuardTest/ThreadGuardTest.cpp


${TESTDIR}/tests/control/ThreadGuardTest/ThreadGuardTestRunner.o: tests/control/ThreadGuardTest/ThreadGuardTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/control/ThreadGuardTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/control/ThreadGuardTest/ThreadGuardTestRunner.o tests/control/ThreadGuardTest/ThreadGuardTestRunner.cpp


${TESTDIR}/tests/control/ThreadTest/ThreadTest.o: tests/control/ThreadTest/ThreadTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests/control/ThreadTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/control/ThreadTest/ThreadTest.o tests/control/ThreadTest/ThreadTest.cpp


${TESTDIR}/tests/control/ThreadTest/ThreadTestRunner.o: tests/control/ThreadTest/ThreadTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/control/ThreadTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/control/ThreadTest/ThreadTestRunner.o tests/control/ThreadTest/ThreadTestRunner.cpp


${TESTDIR}/tests/data/CommandTest/CommandTest.o: tests/data/CommandTest/CommandTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests/data/CommandTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/data/CommandTest/CommandTest.o tests/data/CommandTest/CommandTest.cpp


${TESTDIR}/tests/data/CommandTest/CommandTestRunner.o: tests/data/CommandTest/CommandTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/data/CommandTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/data/CommandTest/CommandTestRunner.o tests/data/CommandTest/CommandTestRunner.cpp


${TESTDIR}/tests/data/ContextTest/ContextTest.o: tests/data/ContextTest/ContextTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests/data/ContextTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/data/ContextTest/ContextTest.o tests/data/ContextTest/ContextTest.cpp


${TESTDIR}/tests/data/ContextTest/ContextTestRunner.o: tests/data/ContextTest/ContextTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/data/ContextTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/data/ContextTest/ContextTestRunner.o tests/data/ContextTest/ContextTestRunner.cpp


${TESTDIR}/tests/data/LoggingLevelTest/LoggingLevelTest.o: tests/data/LoggingLevelTest/LoggingLevelTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests/data/LoggingLevelTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/data/LoggingLevelTest/LoggingLevelTest.o tests/data/LoggingLevelTest/LoggingLevelTest.cpp


${TESTDIR}/tests/data/LoggingLevelTest/LoggingLevelTestRunner.o: tests/data/LoggingLevelTest/LoggingLevelTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/data/LoggingLevelTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/data/LoggingLevelTest/LoggingLevelTestRunner.o tests/data/LoggingLevelTest/LoggingLevelTestRunner.cpp


${TESTDIR}/tests/data/ParametersTest/ParametersTest.o: tests/data/ParametersTest/ParametersTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests/data/ParametersTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/data/ParametersTest/ParametersTest.o tests/data/ParametersTest/ParametersTest.cpp


${TESTDIR}/tests/data/ParametersTest/ParametersTestRunner.o: tests/data/ParametersTest/ParametersTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/data/ParametersTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/data/ParametersTest/ParametersTestRunner.o tests/data/ParametersTest/ParametersTestRunner.cpp


${TESTDIR}/tests/data/ReportTest/ReportTest.o: tests/data/ReportTest/ReportTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests/data/ReportTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/data/ReportTest/ReportTest.o tests/data/ReportTest/ReportTest.cpp


${TESTDIR}/tests/data/ReportTest/ReportTestRunner.o: tests/data/ReportTest/ReportTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/data/ReportTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/data/ReportTest/ReportTestRunner.o tests/data/ReportTest/ReportTestRunner.cpp


${TESTDIR}/tests/data/StateTest/StateTest.o: tests/data/StateTest/StateTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests/data/StateTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/data/StateTest/StateTest.o tests/data/StateTest/StateTest.cpp


${TESTDIR}/tests/data/StateTest/StateTestRunner.o: tests/data/StateTest/StateTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/data/StateTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/data/StateTest/StateTestRunner.o tests/data/StateTest/StateTestRunner.cpp


${TESTDIR}/tests/DIM/ControllerTest/ControllerTest.o: tests/DIM/ControllerTest/ControllerTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests/DIM/ControllerTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/DIM/ControllerTest/ControllerTest.o tests/DIM/ControllerTest/ControllerTest.cpp


${TESTDIR}/tests/DIM/ControllerTest/ControllerTestRunner.o: tests/DIM/ControllerTest/ControllerTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/DIM/ControllerTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/DIM/ControllerTest/ControllerTestRunner.o tests/DIM/ControllerTest/ControllerTestRunner.cpp


${TESTDIR}/tests/DIM/TaskTypeTest/TaskTypeTest.o: tests/DIM/TaskTypeTest/TaskTypeTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests/DIM/TaskTypeTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/DIM/TaskTypeTest/TaskTypeTest.o tests/DIM/TaskTypeTest/TaskTypeTest.cpp


${TESTDIR}/tests/DIM/TaskTypeTest/TaskTypeTestRunner.o: tests/DIM/TaskTypeTest/TaskTypeTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/DIM/TaskTypeTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/DIM/TaskTypeTest/TaskTypeTestRunner.o tests/DIM/TaskTypeTest/TaskTypeTestRunner.cpp


${TESTDIR}/tests/library/ClockTest/ClockTest.o: tests/library/ClockTest/ClockTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests/library/ClockTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/library/ClockTest/ClockTest.o tests/library/ClockTest/ClockTest.cpp


${TESTDIR}/tests/library/ClockTest/ClockTestRunner.o: tests/library/ClockTest/ClockTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/library/ClockTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/library/ClockTest/ClockTestRunner.o tests/library/ClockTest/ClockTestRunner.cpp


${TESTDIR}/tests/library/ConfigurationUtilsTest/ConfigurationUtilsTest.o: tests/library/ConfigurationUtilsTest/ConfigurationUtilsTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests/library/ConfigurationUtilsTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/library/ConfigurationUtilsTest/ConfigurationUtilsTest.o tests/library/ConfigurationUtilsTest/ConfigurationUtilsTest.cpp


${TESTDIR}/tests/library/ConfigurationUtilsTest/ConfigurationUtilsTestRunner.o: tests/library/ConfigurationUtilsTest/ConfigurationUtilsTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/library/ConfigurationUtilsTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/library/ConfigurationUtilsTest/ConfigurationUtilsTestRunner.o tests/library/ConfigurationUtilsTest/ConfigurationUtilsTestRunner.cpp


${TESTDIR}/tests/library/MetronomeFactoryTest/MetronomeFactoryTest.o: tests/library/MetronomeFactoryTest/MetronomeFactoryTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests/library/MetronomeFactoryTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/library/MetronomeFactoryTest/MetronomeFactoryTest.o tests/library/MetronomeFactoryTest/MetronomeFactoryTest.cpp


${TESTDIR}/tests/library/MetronomeFactoryTest/MetronomeFactoryTestRunner.o: tests/library/MetronomeFactoryTest/MetronomeFactoryTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/library/MetronomeFactoryTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/library/MetronomeFactoryTest/MetronomeFactoryTestRunner.o tests/library/MetronomeFactoryTest/MetronomeFactoryTestRunner.cpp


${TESTDIR}/tests/library/StringUtilsTest/StringUtilsTest.o: tests/library/StringUtilsTest/StringUtilsTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests/library/StringUtilsTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/library/StringUtilsTest/StringUtilsTest.o tests/library/StringUtilsTest/StringUtilsTest.cpp


${TESTDIR}/tests/library/StringUtilsTest/StringUtilsTestRunner.o: tests/library/StringUtilsTest/StringUtilsTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/library/StringUtilsTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM -DDEBUG -DEXCLUDE_DIM -I. `pkg-config --cflags libsystemd` -std=c++14 --coverage -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/library/StringUtilsTest/StringUtilsTestRunner.o tests/library/StringUtilsTest/StringUtilsTestRunner.cpp


${OBJECTDIR}/src/DIM/Controller_nomain.o: ${OBJECTDIR}/src/DIM/Controller.o src/DIM/Controller.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/DIM
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/DIM/Controller.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM `pkg-config --cflags libsystemd` -std=c++14  -fPIC  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/DIM/Controller_nomain.o src/DIM/Controller.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/src/DIM/Controller.o ${OBJECTDIR}/src/DIM/Controller_nomain.o;\
	fi

${OBJECTDIR}/src/DIM/Wrapper_nomain.o: ${OBJECTDIR}/src/DIM/Wrapper.o src/DIM/Wrapper.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/DIM
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/DIM/Wrapper.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM `pkg-config --cflags libsystemd` -std=c++14  -fPIC  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/DIM/Wrapper_nomain.o src/DIM/Wrapper.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/src/DIM/Wrapper.o ${OBJECTDIR}/src/DIM/Wrapper_nomain.o;\
	fi

${OBJECTDIR}/src/control/Logger_nomain.o: ${OBJECTDIR}/src/control/Logger.o src/control/Logger.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/control
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/control/Logger.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM `pkg-config --cflags libsystemd` -std=c++14  -fPIC  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/control/Logger_nomain.o src/control/Logger.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/src/control/Logger.o ${OBJECTDIR}/src/control/Logger_nomain.o;\
	fi

${OBJECTDIR}/src/control/Metronome_nomain.o: ${OBJECTDIR}/src/control/Metronome.o src/control/Metronome.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/control
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/control/Metronome.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM `pkg-config --cflags libsystemd` -std=c++14  -fPIC  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/control/Metronome_nomain.o src/control/Metronome.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/src/control/Metronome.o ${OBJECTDIR}/src/control/Metronome_nomain.o;\
	fi

${OBJECTDIR}/src/control/Session_nomain.o: ${OBJECTDIR}/src/control/Session.o src/control/Session.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/control
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/control/Session.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM `pkg-config --cflags libsystemd` -std=c++14  -fPIC  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/control/Session_nomain.o src/control/Session.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/src/control/Session.o ${OBJECTDIR}/src/control/Session_nomain.o;\
	fi

${OBJECTDIR}/src/control/Supervisor_nomain.o: ${OBJECTDIR}/src/control/Supervisor.o src/control/Supervisor.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/control
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/control/Supervisor.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM `pkg-config --cflags libsystemd` -std=c++14  -fPIC  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/control/Supervisor_nomain.o src/control/Supervisor.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/src/control/Supervisor.o ${OBJECTDIR}/src/control/Supervisor_nomain.o;\
	fi

${OBJECTDIR}/src/control/Thread_nomain.o: ${OBJECTDIR}/src/control/Thread.o src/control/Thread.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/control
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/control/Thread.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM `pkg-config --cflags libsystemd` -std=c++14  -fPIC  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/control/Thread_nomain.o src/control/Thread.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/src/control/Thread.o ${OBJECTDIR}/src/control/Thread_nomain.o;\
	fi

${OBJECTDIR}/src/library/ConfigurationUtils_nomain.o: ${OBJECTDIR}/src/library/ConfigurationUtils.o src/library/ConfigurationUtils.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/library
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/library/ConfigurationUtils.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM `pkg-config --cflags libsystemd` -std=c++14  -fPIC  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/library/ConfigurationUtils_nomain.o src/library/ConfigurationUtils.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/src/library/ConfigurationUtils.o ${OBJECTDIR}/src/library/ConfigurationUtils_nomain.o;\
	fi

${OBJECTDIR}/src/library/MetronomeFactory_nomain.o: ${OBJECTDIR}/src/library/MetronomeFactory.o src/library/MetronomeFactory.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/library
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/library/MetronomeFactory.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM `pkg-config --cflags libsystemd` -std=c++14  -fPIC  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/library/MetronomeFactory_nomain.o src/library/MetronomeFactory.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/src/library/MetronomeFactory.o ${OBJECTDIR}/src/library/MetronomeFactory_nomain.o;\
	fi

${OBJECTDIR}/src/library/StringUtils_nomain.o: ${OBJECTDIR}/src/library/StringUtils.o src/library/StringUtils.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/library
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/library/StringUtils.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -DDEBUG -DEXCLUDE_DIM `pkg-config --cflags libsystemd` -std=c++14  -fPIC  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/library/StringUtils_nomain.o src/library/StringUtils.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/src/library/StringUtils.o ${OBJECTDIR}/src/library/StringUtils_nomain.o;\
	fi

# Run Test Targets
.test-conf:
	@if [ "${TEST}" = "" ]; \
	then  \
	    ${TESTDIR}/TestFiles/f1/f1 || true; \
	    ${TESTDIR}/TestFiles/f1/f2 || true; \
	    ${TESTDIR}/TestFiles/f1/f3 || true; \
	    ${TESTDIR}/TestFiles/f1/f9 || true; \
	    ${TESTDIR}/TestFiles/f1/f4 || true; \
	    ${TESTDIR}/TestFiles/f1/f6 || true; \
	    ${TESTDIR}/TestFiles/f1/f7 || true; \
	    ${TESTDIR}/TestFiles/f1/f5 || true; \
	    ${TESTDIR}/TestFiles/f1/f8 || true; \
	    ${TESTDIR}/TestFiles/f2/f8 || true; \
	    ${TESTDIR}/TestFiles/f2/f1 || true; \
	    ${TESTDIR}/TestFiles/f2/f11 || true; \
	    ${TESTDIR}/TestFiles/f2/f7 || true; \
	    ${TESTDIR}/TestFiles/f2/f9 || true; \
	    ${TESTDIR}/TestFiles/f2/f10 || true; \
	    ${TESTDIR}/TestFiles/f4/f1 || true; \
	    ${TESTDIR}/TestFiles/f4/f2 || true; \
	    ${TESTDIR}/TestFiles/f3/f1 || true; \
	    ${TESTDIR}/TestFiles/f3/f2 || true; \
	    ${TESTDIR}/TestFiles/f3/f5 || true; \
	    ${TESTDIR}/TestFiles/f3/f6 || true; \
	else  \
	    ./${TEST} || true; \
	fi

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libadapro-no-dim.${CND_DLIB_EXT}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
