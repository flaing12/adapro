#ifndef EXCLUDE_DIM

#include <cstdint>
#include <memory>
#include <vector>
#include <iterator>
#include <deque>
#include <string>
#include <mutex>
#include <atomic>
#include <stdexcept>
#include <condition_variable>
#include <thread>
#include <dim/dim_common.h>
#include <dim/dis.h>
#include <dim/dic.h>
#include "../../headers/DIM/Wrapper.hpp"
#include "../../headers/DIM/Typedefs.hpp"
#include "../../headers/library/StringUtils.hpp"
#include "../../headers/control/Session.hpp"
#include "../../headers/data/LoggingLevel.hpp"
#include "../../headers/data/StatusFlags.hpp"
#include "../../headers/data/Typedefs.hpp"

using namespace std;
using namespace ADAPRO::DIM;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

static const regex PIPE("|");
static mutex SEMAPHORE;
static condition_variable SERVER_VECTOR_RETRIEVED;
static atomic_bool SERVER_VECTOR_RETRIEVAL_CONFIRMATION;
static atomic_bool SERVER_VECTOR_RETRIEVAL_FAILED;
static atomic<uint32_t> SUBVECTOR_SUCCESS_COUNT;
static atomic<uint32_t> SUBVECTOR_FAILURE_COUNT;
static vector<string> SERVER_VECTOR;
static vector<string> SERVICE_VECTOR;
static vector<vector<string>> SERVICE_SUBVECTORS;

static void SERVICE_SUBVECTOR_CALLBACK(void* tag, void* buffer, int* size)
noexcept
{
    if (*size > 0)
    {
        SERVICE_SUBVECTORS[*((vector<string>::size_type*) tag)] =
                std::move(split(string((char*) buffer), '\n'));
        ++SUBVECTOR_SUCCESS_COUNT;
    }
    else
    {
        ++SUBVECTOR_FAILURE_COUNT;
    }
}

static void SERVICE_VECTOR_CALLBACK(void* tag, void* buffer, int* size) noexcept
{
    if (*size > 0)
    {
        SERVER_VECTOR = std::move(split(string((char*) buffer), '|'));
        const vector<string>::size_type server_count(SERVER_VECTOR.size());
        if (server_count > 0) // Server list includes DIM DNS itself.
        {
            SERVICE_SUBVECTORS.reserve(server_count - 1);
            for (vector<string>::size_type i = 1; i < server_count; ++i)
            {
                SERVICE_SUBVECTORS.push_back({});
                const string service_name((split(SERVER_VECTOR[i], '@'))[0] +
                        "/SERVICE_LIST");
                Wrapper::client_subscribe_unsafe
                (
                        service_name,
                        ADAPRO::DIM::ONCE_ONLY,
                        10,
                        nullptr,
                        0,
                        SERVICE_SUBVECTOR_CALLBACK,
                        i - 1
                );
            }
        }
        else
        {
            ADAPRO::Control::Session::PRINT
            (
                    "No servers found.",
                    ADAPRO::Data::LoggingLevel::ADAPRO_DEBUG
            );
        }
    }
    else
    {
        SERVER_VECTOR_RETRIEVAL_FAILED.store(true, memory_order_release);
    }
    SERVER_VECTOR_RETRIEVAL_CONFIRMATION.store(true, memory_order_release);
    SERVER_VECTOR_RETRIEVED.notify_one();
}

void Wrapper::default_dim_error_handler(int severity, int code, char* message)
noexcept
{
    ADAPRO::Data::LoggingLevel logging_level(
            dim_severity_to_logging_level(severity));
    ADAPRO::Control::Session::PRINT
    (
            std::string(message),
            logging_level
    );
    if (logging_level >= ADAPRO::Data::LoggingLevel::ERROR)
    {
        ADAPRO::Control::Session::ABORT_ASYNC(ADAPRO::Data::FLAG_DIM_ERROR);
    }
}

void Wrapper::common_set_dim_dns_node(const string& node)
{
    lock_guard<mutex> lock(SEMAPHORE);
    if (node.length() > 255)
    {
        throw length_error
        (
                "The maximum length of 255 characters for DIM DNS node was "
                "exceeded. The given string \"" + node + "\" had length of " +
                std::to_string(node.length()) + " characters."
        );
    }
    dim_set_dns_node(node.c_str());
}

string* Wrapper::common_get_dim_dns_node()
{
    lock_guard<mutex> lock(SEMAPHORE);
    char buffer[256];
    int code = dim_get_dns_node(buffer);
    if (!code)
    {
        throw runtime_error("Error in retrieving DIM DNS node.");
    }
    return new string(buffer);
}

void Wrapper::common_set_dim_dns_port(const uint16_t port)
{
    lock_guard<mutex> lock(SEMAPHORE);
    if (port == 0)
    {
        throw invalid_argument("DIM DNS port can't be zero.");
    }
    dim_set_dns_port(port);
}

uint16_t Wrapper::common_get_dim_dns_port()
{
    lock_guard<mutex> lock(SEMAPHORE);
    int port = dim_get_dns_port();
    if (port <= 0 || port > 65535)
    {
        throw runtime_error("Error in retrieving DIM DNS port.");
    }
    return (uint16_t) port;
}

service_id_t Wrapper::server_add_command
(
        const string& command_name,
        const string& command_description,
        command_callback_t user_routine,
        dim_long tag
)
{
    if (command_name.length() > 132)
    {
        throw length_error
        (
                "Maximum DIM service name length of 132 characters exceeded for"
                " \"" + command_name + "\"."
        );
    }
    else if (ADAPRO::Data::is_DIM_service_description(command_description))
    {
        lock_guard<mutex> lock(SEMAPHORE);
        return dis_add_cmnd(command_name.c_str(), command_description.c_str(),
                user_routine, tag);
    }
    else
    {
        throw invalid_argument
        (
                "Malformed DIM command description string \"" +
                command_description + "\" for command service \"" +
                command_name + "\"."
        );
    }
}

service_id_t Wrapper::server_add_command_unsafe
(
        const string& command_name,
        const string& command_description,
        command_callback_t user_routine,
        dim_long tag
)
{
    if (command_name.length() > 132)
    {
        throw length_error
        (
                "Maximum DIM command service name length of 132 characters "
                "exceeded for \"" + command_name + "\"."
        );
    }
    else if (ADAPRO::Data::is_DIM_service_description(command_description))
    {
        return dis_add_cmnd(command_name.c_str(), command_description.c_str(),
                user_routine, tag);
    }
    else
    {
        throw invalid_argument
        (
                "Malformed DIM command description string \"" +
                command_description + "\" for command service \"" +
                command_name + "\"."
        );
    }
}

service_id_t Wrapper::server_add_service
(
        const string& service_name,
        const string& service_description,
        void* buffer,
        int buffer_size,
        void(*user_routine)(void*, void**, int*, int*),
        dim_long tag
)
{
    if (service_name.length() > 132)
    {
        throw length_error
        (
                "Maximum DIM service name length of 132 characters exceeded for"
                " \"" + service_name + "\"."
        );
    }
    else if (!ADAPRO::Data::is_DIM_service_description(service_description))
    {
        throw invalid_argument
        (
                "Malformed DIM service description string \"" +
                service_description + "\" for service \"" + service_name + "\"."
        );
    }
    else
    {
        lock_guard<mutex> lock(SEMAPHORE);
        return dis_add_service(service_name.c_str(), service_description.c_str(),
                buffer, buffer_size, user_routine, tag);
    }
}

service_id_t Wrapper::server_add_service_unsafe
(
        const string& service_name,
        const string& service_description,
        void* buffer,
        int buffer_size,
        void(*user_routine)(void*, void**, int*, int*),
        dim_long tag
)
{
    if (service_name.length() > 132)
    {
        throw length_error
        (
                "Maximum DIM service name length of 132 characters exceeded for"
                " \"" + service_name + "\"."
        );
    }
    else if (!ADAPRO::Data::is_DIM_service_description(service_description))
    {
        throw invalid_argument
        (
                "Malformed DIM service description string \"" +
                service_description + "\" for service \"" + service_name + "\"."
        );
    }
    else
    {
        return dis_add_service(service_name.c_str(), service_description.c_str(),
                buffer, buffer_size, user_routine, tag);
    }
}

void Wrapper::server_add_error_handler(error_callback_t user_routine)
noexcept
{
    lock_guard<mutex> lock(SEMAPHORE);
    dis_add_error_handler(user_routine);
}

void Wrapper::server_update_service(const service_id_t service_id) noexcept
{
    lock_guard<mutex> lock(SEMAPHORE);
    dis_update_service(service_id);
}

void Wrapper::server_update_service_unsafe(const service_id_t service_id)
noexcept
{
    dis_update_service(service_id);
}

void Wrapper::server_remove_service(const service_id_t service_id)
{
    lock_guard<mutex> lock(SEMAPHORE);
    dis_remove_service(service_id);
    // It's not clear what dis_remove_service is actually supposed to return.
    // The documentation states that it returns 1 on success, but the source
    // code suggests that the return value is always 0. For the time being,
    // I'll ignore the return value.
//    if(!dis_remove_service(service_id))
//    {
//        throw runtime_error("Error in removing DIM service number " +
//                std::to_string(service_id) + ".");
//    }
}

void Wrapper::server_start(const string& server_name)
{
    if (server_name.length() > 40)
    {
        throw length_error
        (
                "Maximum DIM server name of 40 characters exceeded for \"" +
                server_name + "\"."
        );
    }
    lock_guard<mutex> lock(SEMAPHORE);
    dis_start_serving(server_name.c_str());
}

void Wrapper::server_stop() noexcept
{
    lock_guard<mutex> lock(SEMAPHORE);
    dis_stop_serving();
}

vector<string>& Wrapper::client_get_services()
{
    lock_guard<mutex> lock(SEMAPHORE);
    mutex local_mutex;
    unique_lock<mutex> local_lock(local_mutex);
    SERVER_VECTOR_RETRIEVAL_CONFIRMATION.store(false, memory_order_release);
    SERVER_VECTOR_RETRIEVAL_FAILED.store(false, memory_order_release);
    SUBVECTOR_SUCCESS_COUNT.store(0, memory_order_release);
    SUBVECTOR_FAILURE_COUNT.store(0, memory_order_release);
    SERVER_VECTOR.clear();
    SERVICE_VECTOR.clear();
    SERVICE_SUBVECTORS.clear();
    Wrapper::client_subscribe_unsafe
    (
            "DIS_DNS/SERVER_LIST",
            ONCE_ONLY,
            10,
            nullptr,
            0,
            SERVICE_VECTOR_CALLBACK,
            0
    );
    SERVER_VECTOR_RETRIEVED.wait(local_lock, [](){
        return SERVER_VECTOR_RETRIEVAL_CONFIRMATION.load(memory_order_consume);
    });
    if (!(SERVER_VECTOR_RETRIEVAL_FAILED.load(memory_order_consume)))
    {
        const vector<string>::size_type server_count(SERVICE_SUBVECTORS.size());
        while (SUBVECTOR_SUCCESS_COUNT.load(memory_order_consume) +
                SUBVECTOR_FAILURE_COUNT.load(memory_order_consume)
                < server_count)
        {}

        if (SUBVECTOR_FAILURE_COUNT.load(memory_order_consume) > 0)
        {
            ADAPRO::Control::Session::PRINT
            (
                    "Failed to retrieve service list(s) from one or more DIM "
                    "servers.",
                    WARNING
            );
        }
        for (const vector<string>& subvector : SERVICE_SUBVECTORS)
        {
            SERVICE_VECTOR.insert
            (
                    SERVICE_VECTOR.end(),
                    make_move_iterator(subvector.begin()),
                    make_move_iterator(subvector.end())
            );
        }
        return SERVICE_VECTOR;
    }
    else
    {
        throw runtime_error("Failed to retrieve the list of DIM servers.");
    }
}

service_id_t Wrapper::client_subscribe
(
        const std::string& service_name,
        int subscription_type,
        int interval,
        void* buffer,
        int buffer_size,
        command_callback_t user_routine,
        dim_long tag,
        void* error_object,
        int error_object_size
)
noexcept
{
    lock_guard<mutex> lock(SEMAPHORE);
    return dic_info_service
    (
            service_name.c_str(),
            subscription_type,
            interval,
            buffer,
            buffer_size,
            user_routine,
            tag,
            error_object,
            error_object_size
    );
}

service_id_t Wrapper::client_subscribe_unsafe
(
        const std::string& service_name,
        int subscription_type,
        int interval,
        void* buffer,
        int buffer_size,
        command_callback_t user_routine,
        dim_long tag,
        void* error_object,
        int error_object_size
)
noexcept
{
    return dic_info_service
    (
            service_name.c_str(),
            subscription_type,
            interval,
            buffer,
            buffer_size,
            user_routine,
            tag,
            error_object,
            error_object_size
    );
}

service_id_t Wrapper::client_subscribe_stamped
(
        const std::string& service_name,
        int subscription_type,
        int interval,
        void* buffer,
        int buffer_size,
        command_callback_t user_routine,
        dim_long tag,
        void* error_object,
        int error_object_size
)
noexcept
{
    lock_guard<mutex> lock(SEMAPHORE);
    return dic_info_service_stamped
    (
            service_name.c_str(),
            subscription_type,
            interval,
            buffer,
            buffer_size,
            user_routine,
            tag,
            error_object,
            error_object_size
    );
}

void Wrapper::client_call_command
(
        const string& service_name,
        void* buffer,
        int buffer_size
)
{
    lock_guard<mutex> lock(SEMAPHORE);
    if (dic_cmnd_service(service_name.c_str(), buffer, buffer_size) == -1)
    {
        throw runtime_error("Calling DIM command \"" + service_name +
                "\" failed.");
    }
}

void Wrapper::client_call_command_unsafe
(
        const string& service_name,
        void* buffer,
        int buffer_size
)
{
    if (dic_cmnd_service(service_name.c_str(), buffer, buffer_size) == -1)
    {
        throw runtime_error("Calling DIM command \"" + service_name +
                "\" failed.");
    }
}

void Wrapper::client_write_timestamp
(
        service_id_t service_id,
        int* milliseconds_ptr,
        int* seconds_ptr
)
{
    lock_guard<mutex> lock(SEMAPHORE);
    if (dic_get_timestamp(0, seconds_ptr, milliseconds_ptr) != 1)
    {
        throw runtime_error("Couldn't get timestamp for DIM service " +
                std::to_string(service_id) + ".");
    }
}

void Wrapper::client_write_timestamp_unsafe
(
        int* milliseconds_ptr,
        int* seconds_ptr
)
{
    if (dic_get_timestamp(0, seconds_ptr, milliseconds_ptr) != 1)
    {
        throw runtime_error("Couldn't get timestamp for the current DIM "
                "service.");
    }
}

void Wrapper::client_add_error_handler(error_callback_t user_routine)
noexcept
{
    lock_guard<mutex> lock(SEMAPHORE);
    dic_add_error_handler(user_routine);
}

void Wrapper::client_unsubscribe(service_id_t service_id) noexcept
{
    lock_guard<mutex> lock(SEMAPHORE);
    dic_release_service(service_id);
}

#endif /* EXCLUDE_DIM */