
#include <string>
#include <memory>
#include <map>
#include <unordered_set>
#include <utility>
#include <functional>
#include <queue>
#include <mutex>
#include <thread>
#include <chrono>
#include <stdexcept>
#include "../../headers/control/Thread.hpp"
#include "../../headers/control/Worker.hpp"
#include "../../headers/control/Supervisor.hpp"
#include "../../headers/control/Session.hpp"
#include "../../headers/control/Logger.hpp"
#include "../../headers/data/State.hpp"
#include "../../headers/data/Report.hpp"
#include "../../headers/data/LoggingLevel.hpp"
#include "../../headers/data/Typedefs.hpp"
#include "../../headers/DIM/Controller.hpp"
#ifndef EXCLUDE_DIM
#include <dim/dim_common.h>
#include "../../headers/DIM/SubscriptionType.hpp"
#include "../../headers/DIM/TaskType.hpp"
#include "../../headers/DIM/TaskArguments.hpp"
#include "../../headers/DIM/Typedefs.hpp"
#include "../../headers/DIM/Wrapper.hpp"
using namespace ADAPRO::DIM;
#endif

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

Controller::Controller
(
        Logger& logger
        , Supervisor& supervisor
        , const config_t& configuration
#ifndef EXCLUDE_DIM
        , list<command_factory_t>&& dim_command_factories
        , error_callback_t dim_client_error_handler
        , error_callback_t dim_server_error_handler
        , uint32_t* supervisor_state_ptr
#endif
)
noexcept:
        Worker
        (
                logger,
                std::move("DIMController"),
                configuration,
                stoi(query(configuration, DIM_CONTROLLER_CORE))
        ),
        task_mutex(),
        task_queue(),
        services(),
        commands(),
        subscriptions()
{
#ifdef EXCLUDE_DIM
    print("DIM support is disabled. Ignoring all commands.", INFO);
#else
    dim_init();
    Wrapper::common_set_dim_dns_node
    (
            query(configuration, DIM_DNS_NODE)
    );
    Wrapper::common_set_dim_dns_port
    (
            stoi(query(configuration, DIM_DNS_PORT))
    );
    Wrapper::server_add_error_handler(dim_client_error_handler);
    Wrapper::client_add_error_handler(dim_server_error_handler);
    dim_long tag(0);
#ifdef _GNU_SOURCE
    dtq_start_timer
    (
            1,
            [](void* tag_ptr)
            {
                pthread_setname_np(pthread_self(), "DIM callback");
//                try // TODO: We need static access to the configuration here:
//                {
//                    Thread::SET_AFFINITY
//                    (
//                            stoi(query(configuration, DIM_CALLBACK_CORE))
//                    );
//                }
//                catch (const std::runtime_error& e)
//                {
//                    Session::PRINT("<DIM callback> " + string(e.what()), WARNING);
//                }
                Session::PRINT
                (
                        "Setting DIM callback thread affinity is currently not supported.",
                        WARNING
                );
            },
            (void*) &tag
    );
#endif /* _GNU_SOURCE */

    publish(make_shared<ServicePublication>(
            query(configuration, DIM_SERVER_NAME) + "/STATE",
            "I:1",
            (void*) &supervisor_state_ptr,
            4,
            nullptr,
            tag++
    ));
    publish(make_shared<CommandPublication>(
            query(configuration, DIM_SERVER_NAME) + "/COMMAND",
            "I:1",
            [](void* tag, void* buffer, int* size)
            {
                const Command command(*((Command*) buffer));
                if (*size == 4)
                {
                    switch (command)
                    {
                        case PAUSE:
                            Session::PRINT
                            (
                                    "Received a PAUSE command through DIM.",
                                    ADAPRO_DEBUG
                            );
                            Session::PAUSE_ASYNC();
                            return;
                        case CONTINUE:
                            Session::PRINT
                            (
                                    "Received a CONTINUE command through DIM.",
                                    ADAPRO_DEBUG
                            );
                            Session::RESUME_ASYNC();
                            return;
                        case STOP:
                            Session::PRINT
                            (
                                    "Received a STOP command through DIM.",
                                    ADAPRO_DEBUG
                            );
                            Session::STOP_ASYNC();
                            return;
                        default:
                            Session::PRINT
                            (
                                    "Ignoring command " + show(command) +
                                    " received through DIM.", WARNING
                            );
                            return;
                    }
                }
                Session::PRINT("Ignoring unsupported command " +
                        std::to_string(*((int*) buffer)) + ".", WARNING);
            },
            tag++
    ));
//    publish(make_shared<CommandPublication>(
//            configuration.at(get<0>(DIM_SERVER_NAME)) + "/REPORT",
//            "I:1",
//            [](void* tag, void* buffer, int* size)
//            {
//                if (*size == 4)
//                {
//                    const Report report(*((Report*) buffer));
//                    switch (report)
//                    {
//                        case THREADS:
//                            Session::PRINT(supervisor.report());
//                            break;
//                        case CONFIGURATION:
//                            print_configuration
//                            (
//                                    logger,
//                                    "CONFIGURATION",
//                                    configuration
//                            );
//                            break;
//                        case ENVIRONMENT:
//                            session.print_environment_report();
//                            break;
//                        default:
//                            Session::PRINT("Ignoring report " +
//                                    show(report) + ".", WARNING);
//                    }
//                }
//            },
//            tag++
//    ));
    for (auto callback : dim_command_factories)
    {
        auto tuple{callback(configuration)};
        publish(make_shared<CommandPublication>(
                get<0>(tuple), get<1>(tuple), get<2>(tuple), tag++
        ));
    }
#endif /* EXCLUDE_DIM */
}

void Controller::prepare()
{
#ifndef EXCLUDE_DIM
#ifdef _GNU_SOURCE
    dim_long tag{0};
    dtq_start_timer
    (
            1,
            [](void* tag_ptr)
            {
                errno = pthread_setname_np(pthread_self(), "DIM callback");
                if (errno != 0)
                {
                    Session::PRINT
                    (
                            "<DIM callback> pthread_setname failed ("
                            "errno " + to_string(errno) + "):\n    \n" +
                            string(strerror(errno)) + "\"",
                            WARNING
                    );
                }
                else
                {
                    Session::PRINT
                    (
                            "<DIM callback> pthread_setname_np success.",
                            ADAPRO_DEBUG
                    );
                }
            },
            (void*) &tag
    );
    this_thread::sleep_for(chrono::milliseconds(1100));
#endif /* _GNU_SOURCE */
    Wrapper::common_set_dim_dns_node(query(configuration, DIM_DNS_NODE));
    Wrapper::common_set_dim_dns_port(stoi(query(configuration, DIM_DNS_PORT)));
    Wrapper::server_start(query(configuration, DIM_SERVER_NAME));
#endif /* EXCLUDE_DIM */
}

void Controller::execute()
{
#ifdef EXCLUDE_DIM
    print("Going to stop now, since DIM support is disabled.", ADAPRO_DEBUG);
    stop_async();
#else
    unique_lock<mutex> lock{task_mutex};
    if (!task_queue.empty())
    {
        task_t task(task_queue.front());
        task_queue.pop();
        lock.unlock();
        carry_out(std::move(task));
    }
    else
    {
        tasks_waiting.wait_for
        (
                lock,
                chrono::milliseconds(100),
                [this](){return get_state() == RUNNING && task_queue.empty();}
        );
    }
#endif /* EXCLUDE_DIM */
}

void Controller::finish()
{
#ifndef EXCLUDE_DIM
    for (const service_id_t service_id : services)
    {
        Wrapper::server_remove(service_id);
    }
    for (const service_id_t service_id : commands)
    {
        Wrapper::server_remove(service_id);
    }
    for (const service_id_t service_id : subscriptions)
    {
        Wrapper::client_unsubscribe(service_id);
    }
    Wrapper::server_stop();
#endif
}

void Controller::carry_out(task_t&& task)
{
    switch (task.first)
    {
        case PUBLISH_SERVICE:
            publish_service(derefer<ServicePublication>(task.second));
            break;
        case PUBLISH_COMMAND:
            publish_command(derefer<CommandPublication>(task.second));
            break;
        case UPDATE_SERVICE:
            update_service(derefer<ServiceID>(task.second));
            break;
        case SUBSCRIBE_TO_SERVICE:
            subscribe_to_service(derefer<ServiceSubscription>(task.second));
            break;
        case CALL_COMMAND:
            call_command(derefer<CommandInvocation>(task.second));
            break;
        case OVERWRITE_TIMESTAMP:
            overwrite_timestamp(derefer<TimestampRequest>(task.second));
            break;
        default: // Should be unreachable:
            throw std::runtime_error{"Unknown command " +
                    std::to_string((int) task.first) + "."};
    }
}

void Controller::publish_service(ServicePublication& argument)
{
    int buffer_size{(int) argument.buffer_size};
    if (buffer_size >= 0)
    {
#ifndef EXCLUDE_DIM
        dim_long tag((dim_long) argument.tag); // Ignoring the unlikely overflow.
        services.emplace(Wrapper::server_add_service
        (
                argument.service_name,
                argument.service_description,
                argument.buffer,
                buffer_size,
                argument.user_routine,
                tag
        ));
#endif
    }
    else
    {
        throw std::runtime_error{"Unable to publish DIM service \"" +
                argument.service_name + "\": Buffer size overflow."};
    }
}

void Controller::publish_command(CommandPublication& argument)
{
#ifndef EXCLUDE_DIM
    command_callback_t user_routine{argument.user_routine};
    commands.emplace(Wrapper::server_add_command
    (
            argument.service_name,
            argument.service_description,
            user_routine,
            argument.tag
    ));
#endif
}

void Controller::update_service(ServiceID& argument)
{
    if (services.find(argument.service_id) != services.end())
    {
#ifndef EXCLUDE_DIM
    Wrapper::server_update_service(argument.service_id);
#endif
    }
    else
    {
        throw std::runtime_error
        {
                "Unable to update DIM service with ID " +
                std::to_string(argument.service_id) + ": The service doesn't "
                "exist."
        };
    }
}

void Controller::subscribe_to_service(ServiceSubscription& argument)
{
    int buffer_size{(int) argument.buffer_size};
    int error_object_size{(int) argument.error_object_size};
    if (buffer_size >= 0 && error_object_size >= 0)
    {
#ifndef EXCLUDE_DIM
        int subscription_type((int) argument.subscription_type);
        int interval((int) argument.interval);
        command_callback_t user_routine(argument.user_routine);
        void* error_object(const_cast<void*>(argument.error_object));
        dim_long tag((dim_long) argument.tag);
        subscriptions.emplace(Wrapper::client_subscribe
        (
                argument.service_name,
                subscription_type,
                interval,
                argument.buffer,
                buffer_size,
                user_routine,
                tag,
                error_object,
                error_object_size
        ));
#endif
    }
    else
    {
        throw std::runtime_error{"Unable to subscribe to DIM service \"" +
                argument.service_name + "\": Buffer/error object size overflow."};
    }
}

void Controller::call_command(CommandInvocation& argument)
{
    int buffer_size{(int) argument.buffer_size};
    if (buffer_size >= 0)
    {
#ifndef EXCLUDE_DIM
        Wrapper::client_call_command
        (
                argument.service_name,
                argument.buffer,
                buffer_size
        );
#endif
    }
    else
    {
        throw std::runtime_error{"Unable to call DIM command \"" +
                argument.service_name + "\": Buffer size overflow."};
    }
}

void Controller::overwrite_timestamp(TimestampRequest& argument)
{
    if (subscriptions.find(argument.service_id) != subscriptions.end())
    {
#ifndef EXCLUDE_DIM
        Wrapper::client_write_timestamp
        (
                argument.service_id,
                argument.milliseconds_ptr,
                argument.seconds_ptr
        );
#endif
    }
    else
    {
        throw std::runtime_error
        {
                "Unable to retrieve timestamp for DIM service with ID " +
                std::to_string(argument.service_id) + ": The service doesn't "
                "exist."
        };
    }
}