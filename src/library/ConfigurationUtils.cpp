
#include <string>
#include <memory>
#include <fstream>
#include <sstream>
#include <map>
#include <list>
#include <functional>
#ifdef __divine__
#include <sys/cdefs.h>
#endif
#include "../../headers/library/ConfigurationUtils.hpp"
#include "../../headers/library/StringUtils.hpp"
#include "../../headers/library/GenericFunctions.hpp"
#include "../../headers/data/LoggingLevel.hpp"
#include "../../headers/data/Parameters.hpp"
#include "../../headers/data/Typedefs.hpp"

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

#ifndef VERIFY
namespace ADAPRO
{
namespace Control
{
    class Logger;
}
}
#endif

#ifdef __divine__
    __skipcfl
#endif
void ADAPRO::Library::check_mandatory_entries(const config_t& configuration)
{
    list<string> required_keys
    {
        show(LOGGING_MASK)
        , show(DAEMON_ENABLED)
        , show(DIM_SERVER_ENABLED)
        , show(COLOURS_ENABLED)
        , show(SERIALIZE_COMMANDS)
#if defined (_XOPEN_SOURCE) && !defined(__divine__)
        , show(NICE)
#endif
#ifdef _POSIX_MEMLOCK
        , show(LOCK_MEMORY)
#endif
#ifdef _GNU_SOURCE
        , show(MAIN_CORE)
        , show(SUPERVISOR_CORE)
#endif
#if defined(__linux__) || defined(__divine__)
#endif
    };
    try
    {
        if (query(configuration, DAEMON_ENABLED) == "TRUE") // segfault
        {
            required_keys.push_back(show(DAEMON_LOGFILE));
        }
    }
    catch (const out_of_range& e) {}
    try
    {
        if (query(configuration, DIM_SERVER_ENABLED) == "TRUE")
        {
            required_keys.push_back(show(DIM_DNS_NODE));
            required_keys.push_back(show(DIM_DNS_PORT));
            required_keys.push_back(show(DIM_SERVER_NAME));
#ifdef _GNU_SOURCE
            required_keys.push_back(show(DIM_CONTROLLER_CORE));
            required_keys.push_back(show(DIM_CALLBACK_CORE));
#endif
        }
    }
    catch (const out_of_range& e) {}
    list<string> missing_keys;
    for (string& key : required_keys)
    {
        if (configuration.find(key) == configuration.end())
        {
            missing_keys.push_back(key);
        }
    }
    if (!missing_keys.empty())
    {
        stringstream ss;
        auto it = missing_keys.begin();
        ss << "The following mandatory configuration entries were unspecified:\n    ";
        ss << *it++;
        while (it != missing_keys.end()) { ss << ", " << *it++; }
        throw std::invalid_argument(ss.str());
    }
}

#ifdef __divine__
    __skipcfl
#endif
const string& ADAPRO::Library::import_configuration
(
        config_t& key_value_map,
        const list<string>& filenames,
        const bool allow_new_keys,
        const function<bool(const string&, const string&)>& relation
)
{
    if (filenames.empty())
    {
        throw invalid_argument("No configuration file specified.");
    }
    for (const string& filename : filenames)
    {
        ifstream file(filename);
        if (file.is_open())
        {
            map<string, size_t> first_definitions;
            string error_message;
            string line;
            size_t line_number(0), error_number(0);
            while (getline(file, line))
            {
                ++line_number;
                vector<string> tokens(std::move(split_by_whitespace(line)));
                if (tokens.size() >= 1)
                {
                    const string& key = tokens[0];
                    if (key[0] != '#')
                    {
                        switch (tokens.size())
                        {
                            case 1:
                                error_message += "Error " +
                                        to_string(++error_number) + ":\n    Line " +
                                        to_string(line_number) + " in file \"" +
                                        filename + "\" is missing the value after"
                                        " key \"" + key + "\".\n";
                                continue;
                            case 2: break;
                            default:
                                if (tokens[2][0] != '#')
                                {
                                    error_message += "Error " +
                                            to_string(++error_number) +
                                            ":\n    Line " +
                                            to_string(line_number) + " in file \"" +
                                            filename + "\" contains too many tokens"
                                            " (e.g. \"" + tokens[2] + "\").\n";
                                    continue;
                                }
                        }
                        if (first_definitions[key] != 0)
                        {
                            error_message += "Error " + to_string(++error_number) +
                                    ":\n    Line " + to_string(line_number) +
                                    " in file \"" + filename + "\" contains a"
                                    " redefinition for the key \"" + key + "\""
                                    " (first defined in line " +
                                    to_string(first_definitions[key]) + ").\n";
                            continue;
                        }
                        if (!allow_new_keys && key_value_map[key] == "")
                        {
                            error_message += "Error " + to_string(++error_number) +
                                    ":\n    Line " + to_string(line_number) +
                                    " in file \"" + filename + "\" contains an"
                                    " unknown key \"" + key + "\".\n";
                            continue;
                        }
                        const string& value = tokens[1];
                        if (!relation(key, value))
                        {
                            error_message += "Error " +
                                    to_string(++error_number) + ":\n    Line " +
                                    to_string(line_number) + " in file \"" +
                                    filename + "\" failed to satisfy the "
                                    "relation provided by the application.\n";
                            continue;
                        }
                        key_value_map[key]      = value;
                        first_definitions[key]  = line_number;
                    }
                }
            }
            if (error_message != "")
            {
                throw domain_error(error_message.c_str());
            }
            else
            {
                return filename;
            }
        }
    }
    auto it = filenames.begin();
    stringstream ss;
    ss << "Coudln't access (any of) the following configuration file(s):\n    ";
    ss << *it++;
    while (it != filenames.end()) { ss << ", " << *it++; }
    throw ios_base::failure(ss.str());
}

#ifdef __divine__
    __skipcfl
#endif
void ADAPRO::Library::print_configuration
(
#ifndef VERIFY
        Logger& logger,
#endif
        const string& title,
        const config_t& key_value_map
)
noexcept
{
#ifndef VERIFY
    map<string,string> tmp(key_value_map);
    list<pair<string, string>> pairs;
    size_t maximum_length{0};
    for (map<string, string>::iterator it = tmp.begin(); it != tmp.end(); ++it)
    {
        pairs.push_back(pair<string, string>(it->first, it->second));
        if (it->first.length() > maximum_length)
        {
            maximum_length = it->first.length();
        }
    }
    print_k_v_list(logger, title, pairs, maximum_length + 4 - (maximum_length % 4));
#endif
}

#ifdef __divine__
    __skipcfl
#endif
void ADAPRO::Library::export_configuration
(
        const config_t& key_value_map,
        const string& filename,
        const bool tabulate,
        const function<bool(const string&, const string&)>& relation
)
{
    ofstream file(filename);
    size_t largest_key_length = 0;
    for (const pair<string, string>& k_v : key_value_map)
    {
        if (relation(k_v.first, k_v.second))
        {
            if (is_whitespace(k_v.first) || is_whitespace(k_v.second))
            {
                throw invalid_argument
                (
                        "Configuration entry (\"" + k_v.first + "\", \"" +
                        k_v.second + "\") contains whitespace characters."
                );
            }
            else if (k_v.first.length() > largest_key_length)
            {
                largest_key_length = k_v.first.length();
            }
        }
        else
        {
            throw invalid_argument("Malformed key \"" + k_v.first + "\".");
        }
    }
    for (const pair<string, string>& k_v : key_value_map)
    {
        file << k_v.first << string(
                tabulate ? (largest_key_length + 1) - k_v.first.length() : 1,
                ' ') << k_v.second << '\n';
    }
}