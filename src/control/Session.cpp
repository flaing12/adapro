#include <cstdint>
#include <csignal>
#include <cstring>
#include <cerrno>
#include <string>
#include <thread>
#include <atomic>
#include <mutex>
#include <chrono>
#include <map>
#include <utility>
#include <list>
#include <algorithm>
#include <stdexcept>
#include <sstream>
#ifndef VERIFY
#include <iostream>
#endif
#ifdef _POSIX_C_SOURCE
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#endif
#ifdef __divine__
#include <sys/cdefs.h>
#endif
#include "../../headers/Version.hpp"
#ifndef VERIFY
#include "../../headers/control/Logger.hpp"
#endif
#include "../../headers/control/Either.hpp"
#include "../../headers/control/Session.hpp"
#include "../../headers/library/GenericFunctions.hpp"
#include "../../headers/library/ConfigurationUtils.hpp"
#include "../../headers/library/StringUtils.hpp"
#include "../../headers/data/Context.hpp"
#include "../../headers/data/LoggingLevel.hpp"
#include "../../headers/data/Parameters.hpp"
#include "../../headers/data/State.hpp"
#include "../../headers/data/StatusFlags.hpp"
#include "../../headers/data/Typedefs.hpp"

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

static atomic_bool ABORT_FLAG;
static timed_mutex ABORT_MUTEX;
static Session* SESSION_PTR;
static signal_callback_t SIGNAL_CALLBACK;

struct ADAPRO_exception final : public runtime_error
{
    explicit ADAPRO_exception(const char* what) noexcept : runtime_error(what) {}

    virtual ~ADAPRO_exception() {}
};

#define ADAPRO_EXCEPTION_HANDLER(type)                                         \
    catch (const type& e)                                                      \
    {                                                                          \
        Session::PRINT("Unhandled exception (of type "#type"): \n    \"" +     \
                string(e.what()) + "\"", FATAL);                               \
    }

#define ADAPRO_DEFAULT_EXCEPTION_HANDLER                                       \
    catch (...)                                                                \
    {                                                                          \
        Session::PRINT("Unknown thrown object caught!", FATAL);                \
    }

/**
 * This macro checks if <tt>ABORT_FLAG</tt> was set. If it was,
 * <tt>std::runtime_error</tt> will be thrown.
 */
#define CHECKPOINT                                                             \
    if (ABORT_FLAG.load(memory_order_consume))                                 \
    {                                                                          \
        throw ADAPRO_exception("Abnormal shutdown sequence initiated.");       \
    }

#ifdef __divine__
    __skipcfl
#endif
static void ADAPRO_terminate_handler() noexcept
{
    try
    {
        exception_ptr e_ptr{current_exception()};
        if (e_ptr)
        {
            std::rethrow_exception(e_ptr);
        }
        else
        {
            Session::PRINT("Terminate handler called without active exception.", FATAL);
        }
    }
    ADAPRO_EXCEPTION_HANDLER(ios_base::failure)
    ADAPRO_EXCEPTION_HANDLER(system_error)
    ADAPRO_EXCEPTION_HANDLER(regex_error)
    ADAPRO_EXCEPTION_HANDLER(underflow_error)
    ADAPRO_EXCEPTION_HANDLER(overflow_error)
    ADAPRO_EXCEPTION_HANDLER(range_error)
    ADAPRO_EXCEPTION_HANDLER(runtime_error)
    ADAPRO_EXCEPTION_HANDLER(out_of_range)
    ADAPRO_EXCEPTION_HANDLER(length_error)
    ADAPRO_EXCEPTION_HANDLER(domain_error)
    ADAPRO_EXCEPTION_HANDLER(invalid_argument)
    ADAPRO_EXCEPTION_HANDLER(logic_error)
    ADAPRO_EXCEPTION_HANDLER(bad_weak_ptr)
    ADAPRO_EXCEPTION_HANDLER(bad_typeid)
    ADAPRO_EXCEPTION_HANDLER(bad_function_call)
    ADAPRO_EXCEPTION_HANDLER(bad_exception)
    ADAPRO_EXCEPTION_HANDLER(bad_cast)
    ADAPRO_EXCEPTION_HANDLER(bad_array_new_length)
    ADAPRO_EXCEPTION_HANDLER(bad_alloc)
    ADAPRO_EXCEPTION_HANDLER(exception)
    ADAPRO_DEFAULT_EXCEPTION_HANDLER
//    Session::ABORT_SYNC(FLAG_UNHANDLED_EXCEPTION);
    Session::ABORT_ASYNC(FLAG_UNHANDLED_EXCEPTION);
};

#ifdef __divine__
    __skipcfl
#endif
static void ADAPRO_signal_handler(int signal) noexcept
{
    uint8_t code;
    switch (signal)
    {
        case SIGSEGV:
            Session::PRINT("A segmentation violation occured (SIGSEGV).", FATAL);
            code = FLAG_SEGV;
            break;
        case SIGABRT:
            Session::PRINT("An abort signal (SIGABRT) was raised.", FATAL);
            code = FLAG_SIGNAL;
            break;
        case SIGTERM:
            Session::PRINT("A terminate signal (SIGTERM) was raised.", INFO);
            code = FLAG_SIGNAL;
            break;
        case SIGINT:
            Session::PRINT("An interrupt signal (SIGINT) was raised.", INFO);
            code = FLAG_SIGNAL;
            break;
        case SIGILL:
            Session::PRINT("An illegal instruction signal (SIGILL) was raised.", FATAL);
            code = FLAG_SIGNAL;
            break;
        case SIGFPE:
            Session::PRINT("A floating point exception signal (SIGFPE) was raised.", FATAL);
            code = FLAG_SIGNAL;
            break;
#ifdef _POSIX_C_SOURCE
        case SIGHUP:
            Session::PRINT("A hang up signal (SIGHUP) was raised.", INFO);
            code = FLAG_SIGNAL;
            break;
#endif
        default:
            // This case is unreachable. It seems that a handler for every
            // single possible signal would need to be registered separately
            // through sigaction:
            Session::PRINT("Unsupported signal " + to_string(signal) +
                    " was raised.", FATAL);
            code = FLAG_SIGNAL;
    }
    if (!SIGNAL_CALLBACK(signal))
    {
        Session::ABORT_ASYNC(code);
//        Session::ABORT_SYNC(code);
    }
    else
    {
        Session::PRINT("Signal ignored.", ADAPRO_DEBUG);
    }
}

#if defined(__divine__) || !defined(_POSIX_C_SOURCE)
// ANSI C fallback section.
#ifdef __divine__
    __skipcfl
#endif
static void SIGSEGV_handler(int dummy) noexcept
{
    ADAPRO_signal_handler(SIGSEGV);
}

#ifdef __divine__
    __skipcfl
#endif
static void SIGABRT_handler(int dummy) noexcept
{
    ADAPRO_signal_handler(SIGABRT);
}

#ifdef __divine__
    __skipcfl
#endif
static void SIGTERM_handler(int dummy) noexcept
{
    ADAPRO_signal_handler(SIGTERM);
}

#ifdef __divine__
    __skipcfl
#endif
static void SIGINT_handler(int dummy) noexcept
{
    ADAPRO_signal_handler(SIGINT);
}

#ifdef __divine__
    __skipcfl
#endif
static void SIGILL_handler(int dummy) noexcept
{
    ADAPRO_signal_handler(SIGILL);
}

#ifdef __divine__
    __skipcfl
#endif
static void SIGFPE_handler(int dummy) noexcept
{
    ADAPRO_signal_handler(SIGFPE);
}
#endif

ADAPRO::Data::State Session::GET_SUPERVISOR_STATE() noexcept
{
    // I'm not sure if the C++ memory model guarantees this to ever become true:
    if (SESSION_PTR != nullptr)
    {
        return SESSION_PTR->supervisor.get_state();
    }
    else
    {
        return READY;
    }
}

// TODO: Figure out what to do in the "else" case in the following four methods:

void Session::PAUSE_ASYNC() noexcept
{
    if (SESSION_PTR != nullptr)
    {
        SESSION_PTR->supervisor.pause_async();
    }
}

void Session::RESUME_ASYNC() noexcept
{
    if (SESSION_PTR != nullptr)
    {
        SESSION_PTR->supervisor.pause_async();
    }
}

void Session::STOP_ASYNC() noexcept
{
    if (SESSION_PTR != nullptr)
    {
        SESSION_PTR->supervisor.pause_async();
    }
}

void Session::ABORT_ASYNC(const uint8_t flags) noexcept
{
    if (SESSION_PTR != nullptr)
    {
        SESSION_PTR->mask_status_code(flags);
        ABORT_FLAG.store(true, memory_order_release);
    }
}

void Session::ABORT_SYNC(const uint8_t flags) noexcept
{
    if (SESSION_PTR != nullptr)
    {
        SESSION_PTR->mask_status_code(flags);
        bool success{true};
        ABORT_FLAG.store(true, memory_order_release);
        unique_lock<timed_mutex> lock{ABORT_MUTEX, defer_lock};
        try
        {
            // A spurious wake-up is possible here:
            success = lock.try_lock_for(chrono::minutes(1));
        }
        catch (const system_error& e)
        {
            success = false;
        }
        if (!success)
        {
#ifndef VERIFY
            PRINT
            (
                    "Session failed to halt in one minute.", WARNING
            );
#endif
        }
    }
}

bool Session::IS_ABORTING() noexcept
{
    return ABORT_FLAG.load(memory_order_consume);
}

void Session::PRINT
(
        const std::string& message,
        const ADAPRO::Data::LoggingLevel severity
)
noexcept
{
#ifndef VERIFY
    if (SESSION_PTR != nullptr)
    {
        SESSION_PTR->logger.print(message, severity);
    }
    else
    {
        cerr << "<Session not initialized> " << message << '\n';
    }
#endif
}

/**
 * Registers the signal handlers and callbacks.
 *
 * @param context The <tt>InitializationContext</tt> providing the handlers and
 * callbacks.
 */
#ifdef __divine__
    __skipcfl
#endif
static void prepare_handlers(const Context& context) noexcept
{
    set_terminate(ADAPRO_terminate_handler);
    SIGNAL_CALLBACK = context.signal_callback;
#if defined(_POSIX_C_SOURCE) && !defined(__divine__)
    struct sigaction action;
    action.sa_handler   = ADAPRO_signal_handler;
    action.sa_flags     = 0;
//    action.sa_mask      = SA_RESTART;
//    sigemptyset(&action.sa_mask);
    sigfillset(&action.sa_mask);
    sigaction(SIGSEGV, &action, nullptr);
    sigaction(SIGABRT, &action, nullptr);
    sigaction(SIGTERM, &action, nullptr);
    sigaction(SIGINT, &action, nullptr);
    sigaction(SIGILL, &action, nullptr);
    sigaction(SIGFPE, &action, nullptr);
    sigaction(SIGHUP, &action, nullptr);
#else
    // ANSI C fallback:
    signal(SIGSEGV, SIGSEGV_handler);
    signal(SIGABRT, SIGABRT_handler);
    signal(SIGTERM, SIGTERM_handler);
    signal(SIGINT,  SIGINT_handler);
    signal(SIGILL,  SIGILL_handler);
    signal(SIGFPE,  SIGFPE_handler);
#endif
}

/**
 * Parses the command line arguments of the context. Returns a configuration
 * determined by the command-line arguments. As a side effect, updates
 * configuration paths in reverse order.
 *
 * @param context   The <tt>Context</tt>.
 * @return          A configuration generated from the command-line arguments.
 * @throws std::invalid_argument if parsing the command-line arguments failed.
 */
static config_t parse_command_line_entries(Context& context)
{
    config_t command_line_entries;
    if (context.argument_count < 2)
    {
        return command_line_entries;
    }
    // The first argument should be the name of the executable, so it will be
    // skipped:
    list<string> command_line_arguments
    {
            context.argument_values + 1,
            context.argument_values + ((size_t) context.argument_count) - 1
    };
    context.configuration_paths.reverse();
    for (const string& argument : command_line_arguments)
    {
        vector<string> tokens{split(argument, '=')};
        switch (tokens.size())
        {
            case 1:
                if (argument == "--version")
                {
#ifndef VERIFY
                    cout << context.application_name << " " <<
                            context.application_version <<
                            " (running ADAPRO " << ADAPRO::get_version() << ")."
                            << '\n';
#endif
                    exit(0);
                }
                // Now the argument is assumed to be a valid filename referring
                // to a possible configuration file. import_configuration will
                // throw if all the filenames are invalid or refer to
                // inaccessible files:
                context.configuration_paths.push_back(std::move(tokens[0]));
                break;
            case 2:
                if (context.allow_update_configuration)
                {
                    // A configuration key-pair argument must be of the form
                    // "--<key>=<value>" with both <key> and <value> non-null:
                    string& key = tokens[0];
                    const string& value = tokens[1];
                    if (key.length() >= 3 && '-' == key[0] && '-' == key[1] &&
                            !value.empty())
                    {
                        key.erase(0, 2);
                        to_upper_case(key);
                        if (!context.allow_new_keys && context.configuration[key] == "")
                        {
                            throw std::invalid_argument
                            (
                                    "Unknown configuration key \"" + key +
                                    "\" provided in command line."
                            );
                        }
                        if (context.relation(key, value))
                        {
                            command_line_entries[key] = value;
                        }
                        else
                        {
                            throw std::invalid_argument
                            (
                                    "Invalid configuration entry \"" + key +
                                    " " + value + "\" provided in command line."
                            );
                        }
                    }
                    else
                    {
                        throw std::invalid_argument
                        (
                                "Malformed configuration entry \"" + argument +
                                "\" provided in command line. (Syntax: "
                                "\"--<key>=<value>\")"
                        );
                    }
                }
                break;
            default:
                throw std::invalid_argument
                (
                        "Malformed command-line argument \"" + argument + "\"."
                );
        }
    }
    context.configuration_paths.reverse();
    return command_line_entries;
}

/**
 * <p>Updates the configuration of the context to its final state. If allowed by
 * the <tt>Context</tt>, tries to read configuration keys from a configuration
 * file, using <tt>import_configuration</tt> with the relation of the context,
 * after which the configuration will be updated with possible command-line
 * entries. Finally, the mandatory ADAPRO configuration entries are verified
 * with respect to the default relation.</p>
 * <p><em>As a second side-effect, the field <tt>configuration_paths</tt> is
 * modified to contain only the name of the accessed configuration file, if any.
 * </em><p>
 *
 * @param context The <tt>Context</tt> instance.
 *
 * @see ADAPRO::Data::Context
 * @see ADAPRO::Library::make_default_configuration
 * @throws std::invalid_argument if parsing the command-line arguments failed.
 */
#ifdef __divine__
    __skipcfl
#endif
static void finalize_configuration(Context& context)
{
    if(context.allow_update_configuration)
    {
        config_t command_line_entries{parse_command_line_entries(context)};
        string filename;
        if (!context.configuration_paths.empty() ||
                context.require_update_configuration /* We'll get exception */)
        {
            filename = import_configuration
            (
                    context.configuration,
                    context.configuration_paths,
                    context.allow_new_keys,
                    context.relation
            );
        }
        for (pair<string,string>&& entry : command_line_entries)
        {
            context.configuration[entry.first] = entry.second;
        }
        check_mandatory_entries(context.configuration);
        context.configuration_paths.clear();
        if (!filename.empty())
        {
            context.configuration_paths.push_front(filename);
        }
    }
}

#if _POSIX_C_SOURCE >= 200112L && !defined(VERIFY)
#ifdef __divine__
    __skipcfl
#endif
static void handle_POSIX_parameters
(
        const config_t& configuration,
        Logger& logger
)
noexcept
{
#if defined (_XOPEN_SOURCE) && !defined(__divine__)
    errno = 0;
    int nice_val{stoi(query(configuration, NICE))};
    nice(nice_val);
    if (errno != 0)
    {
        logger.print
        (
                "<main> Couldn't set process nice value (errno " +
                to_string(errno) + "):\n    \"" + string(strerror(errno)) + "\"",
                WARNING
        );
        if (errno == EPERM)
        {
            logger.print
            (
                    "<main> Try adding the capability CAP_SYS_NICE to the "
                    "executable, or rerunning with super user privileges.",
                    INFO
            );
        }
        errno = 0;
    }
#endif /* _XOPEN_SOURCE */
#ifdef _POSIX_MEMLOCK
    if (query(configuration, LOCK_MEMORY) == "TRUE")
    {
        if (mlockall(MCL_CURRENT | MCL_FUTURE) != 0)
        {
            logger.print
            (
                    "<main> Couldn't lock process virtual memory (errno " +
                    to_string(errno) + "):\n    \"" + string(strerror(errno)) +
                    "\"",
                    WARNING
            );
            if (errno == EPERM)
            {
                logger.print
                (
                        "<main> Try adding the capability CAP_IPC_LOCK to the "
                        "executable, or rerunning with super user privileges.",
                        INFO
                );
            }
            errno = 0;
        }
    }
#endif /* _POSIX_MEMLOCK */
#ifdef _GNU_SOURCE
    try
    {
        Thread::SET_AFFINITY(
                stoi(query(configuration, MAIN_CORE)), logger, "main");
    }
    catch (const std::runtime_error& e)
    {
        logger.print(string{"<main> "} + e.what(), WARNING);
    }
#endif /* _GNU_SOURCE */
};

/**
 * Prints the environment report containing various pieces of runtime
 * information.
 *
 * @param context   The context, whose field <tt>configuration_paths</tt> should
 * be a single-item list containing only the name of the accessed configuration
 * file, if any.
 * @param logger
 */
#ifdef __divine__
    __skipcfl
#endif
static void print_environment_report
(
        const Context& context,
        Logger& logger
)
{
#ifndef __divine__
    errno = 0;
    char* host = new char[256];
    gethostname(host, 256);
    if (errno != 0)
    {
        logger.print
        (
                "Retieving hostname failed (errno " + to_string(errno) +
                "):\n    \"" + string(strerror(errno)) + "\"",
                WARNING
        );
    }
#endif
    char* user{getenv("USER")};
    string command_line_arguments{std::move(char_ptr_ptr_to_string(
            context.argument_count, context.argument_values, 28))};
    // TODO: Add process nice value and virtual memory locking status here:
    map<string, string> environment
    {
        {"Application", context.application_name},
        {"Application Version", context.application_version},
        {"ADAPRO Version", ADAPRO::get_version()},
        {"User", string{user == nullptr ? "[unknown]" : user}},
#ifndef __divine__
        {"Host", string{host}},
#endif
#if defined _POSIX_VERSION && _POSIX_VERSION >= 200112L
        {"PID", std::to_string(getpid())},
#endif
        {"Command Line Arguments", command_line_arguments},
        {"Configuration File", context.configuration_paths.empty()
                ? "[none]"
                : context.configuration_paths.front()}
    };
    print_configuration(logger, "ENVIRONMENT", environment);
#ifndef __divine__
    delete[] host;
#endif
};
#endif /* _POSIX_C_SOURCE && !defined(VERIFY) */

#ifdef __divine__
    __skipcfl
#endif
void Session::prepare()
{
#ifndef VERIFY
    logger.print("<Session> Starting...", SPECIAL);               // noexcept
#endif
    CHECKPOINT                                                      // may throw
    supervisor.start_sync();                                        // noexcept
#ifndef VERIFY
    logger.print("<Session> Started.", SPECIAL);                  // noexcept
#endif
}

void Session::execute()
{
    do
    {
        CHECKPOINT                                                  // may throw
        this_thread::sleep_for(chrono::seconds(1));                 // noexcept
    }
    while (!supervisor.state_in(HALTING_MASK));                     // noexcept
}

void Session::finish() noexcept
{
#ifndef VERIFY
    logger.print("<Session> Stopping...", SPECIAL);                 // noexcept
#endif
    if (!supervisor.state_in(HALTING_MASK))
    {
        supervisor.stop_async();
    }
    try
    {
        supervisor.wait_for_state_mask(HALTED_MASK, chrono::milliseconds(10000));
    }
    catch (const runtime_error& e)
    {
#ifndef VERIFY
        logger.print("Supervisor failed to stop in 10 seconds.", WARNING);
#endif
#ifdef __linux__
        supervisor.cancel();
#endif
        this_thread::sleep_for(chrono::seconds(10));
    }
    if (supervisor.some_workers_dead)
    {
        mask_status_code(FLAG_WORKER_ERROR);
    }
#ifndef VERIFY
    logger.print("<Session> Stopped.", SPECIAL);                    // noexcept
#endif
}

Session::Session
(
        config_t&& configuration
#ifndef VERIFY
        , Logger&& logger
#endif
        , list<worker_factory_t>&& worker_factories
#ifndef EXCLUDE_DIM
        , list<ADAPRO::DIM::command_factory_t>&& dim_command_factories
        , ADAPRO::DIM::error_callback_t dim_client_error_handler
        , ADAPRO::DIM::error_callback_t dim_server_error_handler
#endif
)
noexcept:
    configuration(std::move(configuration))
    , status_code(0)
#ifndef EXCLUDE_DIM
    , supervisor_state(0)
#endif
#ifndef VERIFY
    , logger(std::move(logger))
#endif
    , supervisor (
#ifndef VERIFY
        this->logger,
#endif
        this->configuration
        , std::move(worker_factories)
#ifndef EXCLUDE_DIM
        , std::move(dim_command_factories)
        , dim_client_error_handler
        , dim_server_error_handler
        , &supervisor_state
#endif
    )
{
#ifndef VERIFY
    logger.print("<Session> Ready.", SPECIAL);                      // noexcept
#endif
}

#ifdef __divine__
    __skipcfl
#endif
void Session::INITIALIZE(ADAPRO::Data::Context&& context)
{
    lock_guard<timed_mutex> lock{ABORT_MUTEX};                      // may throw
    ABORT_FLAG.store(false, memory_order_release);                  // noexcept
    prepare_handlers(context);                                      // noexcept
    CHECKPOINT                                                      // may throw
    finalize_configuration(context);                                // may throw
    CHECKPOINT                                                      // may throw
#ifndef VERIFY
    Logger logger{Logger::make_logger(context.configuration)};      // noexcept
#endif
    CHECKPOINT                                                      // may throw

#ifndef VERIFY
#if _POSIX_C_SOURCE >= 200112L
    handle_POSIX_parameters(context.configuration, logger);         // noexcept
#endif
    print_environment_report(context, logger);                      // noexcept
    print_configuration(logger, "CONFIGURATION", context.configuration); // noexcept
    CHECKPOINT                                                      // may throw
#endif /* VERIFY */

    SESSION_PTR = new Session{                                      // may throw
            std::move(context.configuration)
#ifndef VERIFY
            , std::move(logger)
#endif
            , std::move(context.worker_factories)
#ifndef EXCLUDE_DIM
            , std::move(context.dim_command_factories)
            , context.dim_client_error_handler
            , context.dim_server_error_handler
#endif
    };
}

#ifdef __divine__
    __skipcfl
#endif
uint8_t Session::RUN() noexcept
{
    lock_guard<timed_mutex> lock{ABORT_MUTEX};
    if (SESSION_PTR == nullptr) // We should maybe also check if the Session has been run once already
    {
#ifndef VERIFY
        cerr << "Session not initialized." << '\n';
#endif
        return FLAG_UNHANDLED_EXCEPTION;
    }
    uint8_t code{0};
    try
    {
        SESSION_PTR->prepare();
        SESSION_PTR->execute();
    }
    catch (const system_error& e)
    {
        code |= FLAG_UNHANDLED_EXCEPTION;
    }
    catch (const ADAPRO_exception& e)
    {
        code |= FLAG_UNHANDLED_EXCEPTION;
    }
    SESSION_PTR->finish();
    code = SESSION_PTR->get_status_code();
    delete SESSION_PTR;
    return code;
}