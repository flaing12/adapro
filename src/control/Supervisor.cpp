#include <csignal>
#include <cassert>
#include <string>
#include <map>
#include <utility>
#include <iostream>
#include <sstream>
#include <memory>
#include <atomic>
#include <list>
#include <thread>
#include <chrono>
#include <mutex>
#ifdef _POSIX_C_SOURCE
#include <pthread.h>
#endif
#ifdef __divine__
#include <sys/cdefs.h>
#endif
#include "../../headers/control/Thread.hpp"
#include "../../headers/control/Thread.hpp"
#include "../../headers/control/Worker.hpp"
#include "../../headers/control/Supervisor.hpp"
#if defined(__linux__) && !defined(VERIFY)
#include "../../headers/systemd/Wrapper.hpp"
#endif
#ifndef EXCLUDE_DIM
#include "../../headers/DIM/Controller.hpp"
#include "../../headers/DIM/Typedefs.hpp"
#endif
#include "../../headers/library/GenericFunctions.hpp"
#include "../../headers/library/ConfigurationUtils.hpp"
#include "../../headers/data/Typedefs.hpp"
#include "../../headers/data/State.hpp"
#include "../../headers/data/Command.hpp"
#include "../../headers/data/StatusFlags.hpp"
#include "../../headers/data/Parameters.hpp"
#include "../../headers/data/Typedefs.hpp"

#ifndef VERIFY
namespace ADAPRO
{
namespace Control
{
    class Logger;
}
}
#endif

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

#ifdef __divine__
    __skipcfl
#endif
void Supervisor::send_command
(
        unique_ptr<Worker>& worker_ptr,
        const ADAPRO::Data::Command command,
        const bool wait
)
noexcept
{
    const State worker_state = worker_ptr->get_state();
    switch (command)
    {
        case START:
            if (worker_state == READY)
            {
                if (wait) worker_ptr->start_sync(); else worker_ptr->start_async();
            }
            else
            {
                print("Didn't start \"" + worker_ptr->name + "\", that was "
                        "already " + show(worker_state) + ".", ADAPRO_DEBUG);
            }
            break;
        case PAUSE:
            if (worker_state == RUNNING)
            {
                if (wait) worker_ptr->pause_sync(); else worker_ptr->pause_async();
            }
            else
            {
                print("Didn't pause \"" + worker_ptr->name + "\", that was "
                        "already " + show(worker_state) + ".", ADAPRO_DEBUG);
            }
            break;
        case CONTINUE:
            if (worker_state == PAUSED)
            {
                if (wait) worker_ptr->resume_sync(); else worker_ptr->resume_async();
            }
            else
            {
                print("Didn't resume \"" + worker_ptr->name + "\", that was "
                        "already " + show(worker_state) + ".", ADAPRO_DEBUG);
            }
            break;
        case STOP:
            if ((worker_state & (STOPPED | ABORTED)) == 0)
            {
                if (wait) worker_ptr->stop_sync(); else worker_ptr->stop_async();
            }
            else
            {
                print("Didn't stop \"" + worker_ptr->name + "\", that was "
                        "already " + show(worker_state) + ".", ADAPRO_DEBUG);
            }
            break;
        default:;
//            throw std::runtime_error {"Assertion error."}; // Unreachable
    }
}

#ifdef __divine__
    __skipcfl
#endif
void Supervisor::wait_or_kill(unique_ptr<Worker>& worker_ptr) noexcept
{
    // These different kind of forceful shutdown mechanisms should be moved to
    // Thread probably...
    try
    {
        print("Waiting for " + worker_ptr->name + " to stop gracefully...",
                ADAPRO_DEBUG);
        worker_ptr->wait_for_state_mask(HALTED_MASK, chrono::seconds(10));
        print(worker_ptr->name + " stopped in time.", ADAPRO_DEBUG);
    }
    catch (const std::runtime_error& e)
    {
        print("Worker \"" + worker_ptr->name + "\" failed to stop in 10 "
                "seconds.", WARNING);

#ifdef __linux__
        worker_ptr->cancel();
#endif
// This thing might kill the whole process:
//#ifdef _POSIX_C_SOURCE >= 199506L || _XOPEN_SOURCE >= 500
//        pthread_kill(worker_ptr->backend_ptr->native_handle(), SIGKILL);
//#endif

    }
}

#ifdef __divine__
    __skipcfl
#endif
void Supervisor::propagate_command
(
        const ADAPRO::Data::Command command
)
noexcept
{
    if (command == CONTINUE && !started)
    {
        started = true;
        print("Not propagating the first CONTINUE command.", ADAPRO_DEBUG);
        return;
    }
    if (query(configuration, SERIALIZE_COMMANDS) == "TRUE") // Maybe a boolean field could be added for this
    {
        propagate_command_in_lifo(command);
    }
    else
    {
        propagate_command_in_parallel(command);
    }
}

#ifdef __divine__
    __skipcfl
#endif
void Supervisor::propagate_command_in_parallel
(
        const Command command
)
noexcept
{
    unique_lock<mutex>{command_mutex};
    covariant_propagation(command, false);
    for (unique_ptr<Worker>& worker_ptr : workers)
    {
        switch (command)
        {
            case START:
                worker_ptr->wait_for_state_mask(START_MASK);
                break;
            case CONTINUE:
                worker_ptr->wait_for_state_mask(RESUME_MASK);
                break;
            case PAUSE:
                worker_ptr->wait_for_state_mask(PAUSE_MASK);
                break;
            case STOP:
                wait_or_kill(worker_ptr);
                break;
            default:;
                // Should be unreachable:
//                throw std::runtime_error{"Assertion error in "
//                        "Supervisor::propagate_command_in_parallel!"};
        }
    }
}

#ifdef __divine__
    __skipcfl
#endif
void Supervisor::propagate_command_in_lifo
(
        const Command command
)
noexcept
{
    unique_lock<mutex>{command_mutex};
    if (command == START || command == CONTINUE)
    {
        covariant_propagation(command, true);
    }
    else
    {
        contravariant_propagation(command);
    }
}

#ifdef __divine__
    __skipcfl
#endif
void Supervisor::covariant_propagation(const ADAPRO::Data::Command command,
        const bool serialize) noexcept
{
    for (unique_ptr<Worker>& worker_ptr : workers)
    {
        send_command(worker_ptr, command, serialize);
    }
}

#ifdef __divine__
    __skipcfl
#endif
void Supervisor::contravariant_propagation(const ADAPRO::Data::Command command)
noexcept
{
    for (auto iterator = workers.rbegin(); iterator != workers.rend(); ++iterator)
    {
        const bool stopping(command == ADAPRO::Data::Command::STOP ||
            command == ADAPRO::Data::Command::ABORT);
        send_command(*iterator, command, !stopping);
        if (stopping)
        {
            wait_or_kill(*iterator);
        }
    }
}

Supervisor::Supervisor
(
#ifndef VERIFY
        Logger& logger,
#endif
        const config_t& configuration
        , list<worker_factory_t>&& worker_factories
#ifndef EXCLUDE_DIM
        , list<ADAPRO::DIM::command_factory_t>&& dim_command_factories
        , ADAPRO::DIM::error_callback_t dim_client_error_handler
        , ADAPRO::DIM::error_callback_t dim_server_error_handler
        , uint32_t* state_ptr
#endif
)
noexcept:
        Worker
        (
#ifndef VERIFY
                logger,
#endif
                std::move("Supervisor"),
                configuration,
                stoi(query(configuration, SUPERVISOR_CORE)),
                [
                    this
#ifndef EXCLUDE_DIM
                    , state_ptr
#endif
                ]
                (const State s)
                {
                    switch (s)
                    {
                        case RUNNING:
                            propagate_command(CONTINUE);
#if defined(__linux__) && !defined(VERIFY)
                            if (daemon_mode_enabled)
                            {
                                Systemd::enable_watchdog();
                            }
#endif
                            break;
                        case PAUSED:
                            propagate_command(PAUSE);
#if defined(__linux__) && !defined(VERIFY)
                            if (daemon_mode_enabled)
                            {
                                Systemd::disable_watchdog();
                            }
#endif
                            break;
                        case ABORTING: case STOPPING:
                            propagate_command(STOP);
#if defined(__linux__) && !defined(VERIFY)
                            if (daemon_mode_enabled)
                            {
                                Systemd::disable_watchdog();
                            }
#endif
                        default:;
                    }
#ifndef EXCLUDE_DIM
                    *state_ptr = (uint32_t) s;
#endif
#if defined(__linux__) && !defined(VERIFY)
                    if (daemon_mode_enabled)
                    {
                        ADAPRO::Systemd::set_process_state(s);
                    }
#endif
                }
        )
        , started(false)
        , all_workers_stopped(false)
        , some_workers_dead(false)
#ifdef __linux__
        , daemon_mode_enabled(query(configuration, Parameter::DAEMON_ENABLED) == "TRUE")
#endif
        , workers()
        , command_mutex()
{
#ifndef EXCLUDE_DIM
    if (query(configuration, DIM_SERVER_ENABLED) == "TRUE")
    {
        workers.emplace_front(make_unique<ADAPRO::DIM::Controller>(
#ifndef VERIFY
                logger,
#endif
                *this
                , configuration
                , std::move(dim_command_factories)
                , dim_client_error_handler
                , dim_server_error_handler
                , state_ptr
        ));
    }
#endif
    for (const worker_factory_t& factory: worker_factories)
    {
        workers.push_back(factory(
#ifndef VERIFY
                logger,
#endif
                configuration
        ));
    }
}

#ifdef __divine__
    __skipcfl
#endif
void Supervisor::prepare()
{
    propagate_command(START);
}

#ifdef __divine__
    __skipcfl
#endif
void Supervisor::execute()
{
#if defined(__linux__) && !defined(VERIFY)
    if (daemon_mode_enabled)
    {
        ADAPRO::Systemd::notify_watchdog();
    }
#endif
    all_workers_stopped = true;
    for (unique_ptr<Worker>& worker_ptr : workers)
    {
        const State worker_state = worker_ptr->get_state();
        if (worker_state != STOPPED)
        {
            all_workers_stopped = false;
        }
        if (worker_state == ABORTED)
        {
            some_workers_dead = true;
        }
#ifndef VERIFY
        if (worker_state & (RUNNING | PAUSED))
        {
            Worker& worker(*worker_ptr);
            if (!worker.responsive.load(memory_order_consume) &&
                    ++worker.missed_checkpoints
                    % worker.missed_checkpoints_treshold == 0)
            {
                print
                (       worker.name + " has missed " +
                        to_string(worker.missed_checkpoints) + " checkpoint" +
                        (worker.missed_checkpoints == 1 ? "" : "s") + " so far.",
                        ADAPRO_DEBUG
                );
                worker.missed_checkpoints_treshold *= 2;
            }
            else if (worker.missed_checkpoints_treshold >= 20)
            {
                worker.missed_checkpoints_treshold -= 10;
            }
            worker.responsive.store(false, memory_order_release);
        }
#endif
    }
    if (some_workers_dead)
    {
        print("Stopping because one or more workers have aborted.", INFO);
        print(report(), INFO);
        this->stop_async();
    }
    else if (all_workers_stopped)
    {
        print("Stopping because all workers have stopped.", INFO);
        this->stop_async();
    }
    else
    {
        this_thread::sleep_for(chrono::seconds(1));
    }
}

#ifdef __divine__
    __skipcfl
#endif
void Supervisor::finish()
{
//    workers.clear();
}

#ifdef __divine__
    __skipcfl
#endif
string Supervisor::report() noexcept
{
    stringstream ss;
    ss << Thread::report() << "; supervising" << '\n';
    for (unique_ptr<Worker>& worker_ptr : workers)
    {
        ss << "    " << worker_ptr->report() << '\n';
    }
    return ss.str();
}

#ifdef __divine__
    __skipcfl
#endif
Supervisor::~Supervisor() noexcept
{
    join();
    // The Workers should be stopped already, but we want to be absolutely sure:
    for (unique_ptr<Worker>& worker_ptr : workers)
    {
        if (worker_ptr->backend_ptr->joinable())
        {
            worker_ptr->backend_ptr->join();
        }
    }
}