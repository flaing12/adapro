#include <string>
#include <mutex>
#include <iostream>
#include <ostream>
#include <fstream>
#ifdef _GNU_SOURCE
#include <sched.h>
#endif
#include "../../headers/control/Thread.hpp"
#include "../../headers/control/Logger.hpp"
#include "../../headers/library/Clock.hpp"
#include "../../headers/library/GenericFunctions.hpp"
#include "../../headers/data/State.hpp"
#include "../../headers/data/Command.hpp"
#include "../../headers/data/LoggingLevel.hpp"

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

const string Logger::EMPTY_STRING;
const string Logger::COLOUR_ZERO    = "\e[0m";
const string Logger::COLOUR_GRAY    = "\e[0;30m";
const string Logger::COLOUR_RED     = "\e[0;31m";
const string Logger::COLOUR_GREEN   = "\e[0;32m";
const string Logger::COLOUR_YELLOW  = "\e[0;33m";
const string Logger::COLOUR_BLUE    = "\e[0;34m";

void Logger::print
(
        const Thread& origin,
        const string& message,
        const LoggingLevel severity
)
noexcept
{
#ifndef VERIFY
    if (logging_mask & severity)
    {
        lock_guard<mutex> lock{output_mutex};
        output_stream.flush();
        output_stream <<
                get_colour(severity) <<
                "[" << timestamp() << "] (" <<
                symbol(severity) << ")" <<
                " <" << origin.name <<
                "," << symbol(origin.get_state()) <<
                "," << symbol(origin.get_command()) <<
#ifdef _GNU_SOURCE
                "," << to_string(sched_getcpu()) <<
#endif
                "> " << message <<
                reset_colour() <<
                '\n';
    }
#endif
}

void Logger::print
(
        const std::string& message,
        const ADAPRO::Data::LoggingLevel severity
)
noexcept
{
#ifndef VERIFY
    if (logging_mask & severity)
    {
        lock_guard<mutex> lock{output_mutex};
        output_stream.flush();
        output_stream <<
                get_colour(severity) <<
                "[" << timestamp() << "] (" << symbol(severity) << ") " <<
                message <<
                reset_colour() <<
                '\n';
    }
#endif
}
