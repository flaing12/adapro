
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <string>
#include <functional>
#include <memory>
#include <thread>
#include <chrono>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <list>
#include <vector>
#include <stdexcept>
#ifdef _GNU_SOURCE
#include <pthread.h>
#include <unistd.h>
#include <sched.h>
#include <signal.h>
#endif
#ifdef __divine__
#include <sys/cdefs.h>
#endif
#include "../../headers/control/Thread.hpp"
#include "../../headers/control/Session.hpp"
#include "../../headers/control/Logger.hpp"
#include "../../headers/library/StringUtils.hpp"
#include "../../headers/library/GenericFunctions.hpp"
#include "../../headers/data/State.hpp"
#include "../../headers/data/Command.hpp"
#include "../../headers/data/Typedefs.hpp"

/**
 * The purpose of this macro is to call <tt>handle_exception</tt> with a proper
 * error message, bypassing problems that are caused by object slicing when
 * exploiting polymorphism of C++ exception classes.
 */
#define THREAD_META_HANDLER(e)                                                 \
    lock.lock();                                                               \
    handle_exception(e.what());                                                \
    lock.unlock();

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::Data;

const chrono::milliseconds DELAY{10};

Thread::Thread
(
#ifndef VERIFY
        Logger& logger,
#endif
        string&& name,
        const int preferred_core
)
noexcept:
#ifndef VERIFY
        logger(logger),
#endif
        transition_mutex(),
        state_semaphore(),
        state(READY),
        command(CONTINUE),
        transition_callback([](const State s){}),
#ifndef VERIFY
        responsive(false),
#endif
        backend_ptr(),
        name(std::move(name)),
        preferred_core(preferred_core)
{
    contravariant_transition("Ready.", READY);
}

Thread::Thread
(
#ifndef VERIFY
        Logger& logger,
#endif
        string&& name,
        const trans_cb_t transition_callback,
        const int preferred_core
)
noexcept:
#ifndef VERIFY
        logger(logger),
#endif
        transition_mutex(),
        state_semaphore(),
        state(READY),
        command(CONTINUE),
        transition_callback(transition_callback),
#ifndef VERIFY
        responsive(false),
#endif
        backend_ptr(),
        name(std::move(name)),
        preferred_core(preferred_core)
{
    contravariant_transition("Ready.", READY);
}

#if defined(_GNU_SOURCE) && !defined(VERIFY)
#ifdef __divine__
    __skipcfl
#endif
void Thread::set_name_and_affinity() noexcept
{
    pthread_t worker{pthread_self()};
    try
    {
        SET_AFFINITY(preferred_core, logger, name);
    }
    catch (const std::runtime_error& e)
    {
        print(e.what(), WARNING);
    }
    char worker_name[16];
    if (name.length() >= 16)
    {
        strncpy(worker_name, name.c_str(), 11);
        worker_name[11] = '.';
        worker_name[12] = '.';
        worker_name[13] = '.';
        worker_name[14] = name[name.length() - 1];
        worker_name[15] = '\0';
    }
    else
    {
        strncpy(worker_name, name.c_str(), name.length() + 1);
    }
    errno = pthread_setname_np(worker, worker_name);
    if (errno != 0)
    {
        print
        (
                "pthread_setname failed (errno " + to_string(errno) +
                "):\n    \n" + string(strerror(errno)) + "\"",
                WARNING
        );
    }
}
#endif /* _GNU_SOURCE */

void Thread::run() noexcept
{
    // Problem: Thread is not responsive to commands before it enters the switch
    // below for the first time. If stop_(a)sync is called immediately after
    // start_async, it's possible that the STOP command is lost, because
    // set_state refused to set it prematurely. All commands arriving before
    // the switch statement is reached are in danger of being ignored. According
    // to the FSM model, the startup sequence should be indeed atomic. One
    // option would be to make set_command bock until the switch is reached.
    unique_lock<mutex> lock{transition_mutex, defer_lock};
#if defined(_GNU_SOURCE) && !defined(VERIFY)
    set_name_and_affinity();
#endif /* _GNU_SOURCE */

    try
    {
        bool shutdown{false};
        // Startup:
        lock.lock();
        covariant_transition("Starting...", STARTING);
        command.store(CONTINUE, memory_order_release);
        lock.unlock();
        prepare();
        lock.lock();
        switch (get_command()) // Fist branching point.
        {
            case CONTINUE:
                covariant_transition("Started.", RUNNING);
                break;
            case PAUSE:
                contravariant_transition("Started.", PAUSED);
                break;
            default:
                shutdown = true;
        }
        lock.unlock();

        // Execution:
        while (!shutdown)
        {
#ifndef VERIFY
            responsive.store(true, memory_order_release);
#endif
            lock.lock();
            switch (get_command())
            {
                case CONTINUE:
                    if (get_state() == PAUSED)
                    {
                        covariant_transition("Resuming...", RUNNING);
                    }
                    lock.unlock();
                    execute();
                    break;
                case PAUSE:
                    if (get_state() == RUNNING)
                    {
                        contravariant_transition("Paused.", PAUSED);
                    }
                    lock.unlock();
                    this_thread::sleep_for(DELAY);
                    break;
                default:
                    lock.unlock();
                    shutdown = true;
            }
        }

        // Shutdown:
        if (get_command() == ABORT)
        {
            throw runtime_error{"Unknown error."}; // Should be unreachable.
        }
        lock.lock();
        covariant_transition("Stopping...", STOPPING);
        lock.unlock();
        finish();
        lock.lock();
        contravariant_transition("Stopped.", STOPPED);
        lock.unlock();
    }
    catch (const ios_base::failure& e)     {THREAD_META_HANDLER(e);}
    catch (const system_error& e)          {THREAD_META_HANDLER(e);}
    catch (const regex_error& e)           {THREAD_META_HANDLER(e);}
    catch (const underflow_error& e)       {THREAD_META_HANDLER(e);}
    catch (const overflow_error& e)        {THREAD_META_HANDLER(e);}
    catch (const range_error& e)           {THREAD_META_HANDLER(e);}
    catch (const runtime_error& e)         {THREAD_META_HANDLER(e);}
    catch (const out_of_range& e)          {THREAD_META_HANDLER(e);}
    catch (const length_error& e)          {THREAD_META_HANDLER(e);}
    catch (const domain_error& e)          {THREAD_META_HANDLER(e);}
    catch (const invalid_argument& e)      {THREAD_META_HANDLER(e);}
    catch (const logic_error& e)           {THREAD_META_HANDLER(e);}
    catch (const bad_weak_ptr& e)          {THREAD_META_HANDLER(e);}
    catch (const bad_typeid& e)            {THREAD_META_HANDLER(e);}
    catch (const bad_function_call& e)     {THREAD_META_HANDLER(e);}
    catch (const bad_exception& e)         {THREAD_META_HANDLER(e);}
    catch (const bad_cast& e)              {THREAD_META_HANDLER(e);}
    catch (const bad_array_new_length& e)  {THREAD_META_HANDLER(e);}
    catch (const bad_alloc& e)             {THREAD_META_HANDLER(e);}
    catch (const exception& e)             {THREAD_META_HANDLER(e);}
    catch (...)
    {
        // This is a really bad situation.
        lock.lock();
        handle_exception("\u2620 Unknown thrown object!!! \u2620");
        lock.unlock();
    }
}

#ifdef __linux__
#ifdef __divine__
    __skipcfl
#endif
void Thread::cancel() noexcept
{
    pthread_cancel(backend_ptr->native_handle());
}
#endif

#ifdef __divine__
    __skipcfl
#endif
void Thread::set_state(const ADAPRO::Data::State new_state) noexcept
{
    state.store(new_state, memory_order_release);
    state_semaphore.notify_all();
}

#ifdef __divine__
    __skipcfl
#endif
bool Thread::set_command(const ADAPRO::Data::Command new_command) noexcept
{
    bool success{false};
    lock_guard<mutex> lock(transition_mutex);
    switch (new_command)
    {
        case START:
            success = get_state() == READY && get_command() == CONTINUE;
            break;
        case PAUSE:
            success = state_in(STARTING | RUNNING) && get_command() == CONTINUE;
            break;
        case CONTINUE:
            success = command_in(START | PAUSE);
            break;
        case STOP:
            success = state_in(STARTING | RUNNING | PAUSED) &&
                    command_in(CONTINUE | PAUSE);
            break;
        default:
            success = state_in(STARTING | RUNNING | STOPPING);
    }
    if (success)
    {
        command.store(new_command, memory_order_release);
    }
    return success;
}

#ifdef __divine__
    __skipcfl
#endif
void Thread::start_async() noexcept
{
    if (state_in(READY) && backend_ptr == nullptr)
    {
        set_command(START);
        backend_ptr = make_unique<thread>(&Thread::run, this);
    }
    else
    {
        print("Unable to start.", ADAPRO::Data::LoggingLevel::FSM);
    }
}

#ifdef __divine__
    __skipcfl
#endif
string Thread::report() noexcept
{
    // Perhaps I could use new lines and indentation...
    return name + ": state = " + ADAPRO::Library::show(get_state()) +
            ", command = " + ADAPRO::Library::show(get_command());
}

#ifdef __divine__
    __skipcfl
#endif
void Thread::wait_for_state(const State target_state) noexcept
{
    if (get_state() < target_state)
    {
        unique_lock<mutex> lock(transition_mutex);
        state_semaphore.wait(lock, [this, target_state](){
            return get_state() < target_state;});
    }
}

#ifdef __divine__
    __skipcfl
#endif
void Thread::wait_for_state(const State target_state,
        const chrono::milliseconds timeout)
{
    if (get_state() < target_state)
    {
        unique_lock<mutex> lock(transition_mutex);
        state_semaphore.wait_for
        (
                lock,
                timeout,
                [this, target_state](){return get_state() >= target_state;}
        );
        lock.unlock();
        if (get_state() < target_state)
        {
            throw runtime_error("State transition timed out.");
        }
    }
}

#ifdef __divine__
    __skipcfl
#endif
void Thread::wait_for_state_mask(const uint8_t state_mask) noexcept
{
    if (!state_in(state_mask))
    {
        unique_lock<mutex> lock(transition_mutex);
        state_semaphore.wait(lock, [this, state_mask](){
            return state_in(state_mask);});
    }
}

#ifdef __divine__
    __skipcfl
#endif
void Thread::wait_for_state_mask(const uint8_t state_mask,
        const chrono::milliseconds timeout)
{
    if (!state_in(state_mask))
    {
        unique_lock<mutex> lock(transition_mutex);
        state_semaphore.wait_for
        (
                lock,
                timeout,
                [this, state_mask](){return state_in(state_mask);}
        );
        lock.unlock();
        if (!state_in(state_mask))
        {
            throw runtime_error("State transition timed out.");
        }
    }
}

#ifdef __divine__
    __skipcfl
#endif
void Thread::handle_exception(const char* message) noexcept
{
    command.store(ABORT, memory_order_release);
    covariant_transition("Aborting...", ABORTING);
    print
    (
            string{"Unhandled exception:\n    \""} + message + "\"",
            FATAL
    );
    contravariant_transition("Aborted.", ABORTED);
}

#ifdef __divine__
    __skipcfl
#endif
ADAPRO::Data::trans_cb_t Thread::STATE_PROPAGATION_CB(list<Thread*>&& subordinates) noexcept
{
    return [subordinates](const ADAPRO::Data::State s)
    {
        for (Thread* subordinate : subordinates)
        {
            switch (s)
            {
                case STARTING:
                    subordinate->start_sync();
                    break;
                case RUNNING:
                    if (subordinate->get_state() == PAUSED)
                    {
                        subordinate->resume_sync();
                    }
                    break;
                case PAUSED:
                    subordinate->pause_sync();
                    break;
                case STOPPING:
                case ABORTING:
                    subordinate->stop_sync();
                default:;
            }
        }
    };
}

#ifdef __divine__
    __skipcfl
#endif
void Thread::join() noexcept
{
    if (backend_ptr == nullptr) { return; }
    if (!state_in(HALTING_MASK))
    {
        print("Backend thread is still running.", FSM);
        stop_async();
    }
    wait_for_state_mask(HALTED_MASK);
    if (backend_ptr->joinable())
    {
        backend_ptr->join();
    }
}

#ifndef VERIFY
#ifdef __divine__
    __skipcfl
#endif
void Thread::SET_AFFINITY(const int affinity, Logger& logger, const string& name)
{
#ifdef _GNU_SOURCE
    if (affinity < 0)
    {
        logger.print
        (
                (name.empty() ? name : "<" + name + "> ") +
                "Ignoring negative affinity " + to_string(affinity) + ".",
                FSM
        );
        return;
    }
    const size_t core_count(thread::hardware_concurrency());
    pthread_t backend(pthread_self());
    int x = 0;
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &x);
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &x);
    if ((size_t) affinity < core_count)
    {
        cpu_set_t cpu_set;
        CPU_ZERO(&cpu_set);
        CPU_SET(affinity, &cpu_set);
        int code = pthread_setaffinity_np
        (
                backend,
                sizeof(cpu_set_t),
                &cpu_set
        );
        if (code != 0)
        {
            throw std::runtime_error
            (
                    "Couldn't set affinity:\n    \"" + string(strerror(code)) +
                    "\""
            );
        }
        else
        {
            logger.print
            (
                    (name.empty() ? name : "<" + name + "> ") +
                    "Affinity set to core " + to_string(affinity) + ".",
                    FSM
            );
        }
#ifdef _POSIX_PRIORITY_SCHEDULING
        const struct sched_param sp
        {
            sched_get_priority_max(SCHED_FIFO)
        };
        code = pthread_setschedparam(backend, SCHED_FIFO, &sp);
        if (code != 0)
        {
            throw std::runtime_error
            (
                    "Couldn't set scheduling parameters:\n    \"" +
                    string(strerror(code)) + "\""
            );
        }
#endif /* _POSIX_PRIORITY_SCHEDULING */
    }
    else
    {
        throw std::runtime_error
        (
                "Cannot set affinity to CPU core number " +
                to_string(affinity) + ", which is outside of the range from 0 "
                "to " + to_string(core_count) + "."
        );
    }
#endif /* _GNU_SOURCE */
}
#endif /* VERIFY */

#ifdef __divine__
    __skipcfl
#endif
void Thread::SPINLOCK(const chrono::nanoseconds interval) noexcept
{
    chrono::nanoseconds waited(0);
    chrono::high_resolution_clock::time_point beginning =
            chrono::high_resolution_clock::now();
    while (waited < interval)
    {
        waited = chrono::duration_cast<std::chrono::nanoseconds>(
                chrono::high_resolution_clock::now() - beginning
        );
    }
}
