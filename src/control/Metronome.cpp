#include <cstdint>
#include <thread>
#include <chrono>
#include <mutex>
#include "../../headers/control/Thread.hpp"
#include "../../headers/control/Metronome.hpp"
#include "../../headers/library/Clock.hpp"
#include "../../headers/data/State.hpp"

#ifndef VERIFY
namespace ADAPRO
{
namespace Control
{
    class Logger;
}
}
#endif

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::Data;

/**
 * A serial number used for generating unique names for Metronome instances.
 */
uint32_t S_NO = 0;

Metronome::Metronome
(
#ifndef VERIFY
        Logger& logger,
#endif
        const uint32_t tick_length,
        const int preferred_core
)
noexcept:
        Thread
        (
#ifndef VERIFY
                logger,
#endif
                "Metronome<" + to_string(tick_length) + ">-" + to_string(S_NO++),
                [](const State s){},
                preferred_core
        ),
        semaphore(),
        barrier(),
        tick_duration(chrono::milliseconds(tick_length))
{
    if (tick_length > 0)
    {
        start_sync();
    }
}

void Metronome::prepare() {}

void Metronome::execute()
{
    this_thread::sleep_for(tick_duration);
    barrier.notify_all();
}

void Metronome::finish()
{
    barrier.notify_all();
}

void Metronome::synchronize(const uint32_t ticks)
{
    if (get_state() == RUNNING && ticks > 0)
    {
        const uint64_t beginning{ADAPRO::Library::now()};
        const uint64_t duration{((uint64_t) ticks) * tick_duration.count()};
        unique_lock<mutex> lock{semaphore};
        barrier.wait_for
        (
                lock,
                chrono::milliseconds(duration),
                [this, beginning, duration]()
                {
                    return get_state() < STOPPING &&
                            ADAPRO::Library::now() - beginning >= duration;
                }
        );
    }
}

Metronome::~Metronome() noexcept
{
    barrier.notify_all();
    join();
}